export const pageTheme = {
  brown: { background: "#CD8B7D", textColor: "#FFFFFF" },
  gray: { background: "#9C8D88", textColor: "#FFFFFF" },
  orange: { background: "#F0B8A9", textColor: "#494E54" },
  pink: { background: "#EAD6D5", textColor: "#494E54" },
  biege: { background: "#D9D3C5", textColor: "#494E54" },
  Бежевый: { background: "#D9D3C5", textColor: "#494E54" },
};
