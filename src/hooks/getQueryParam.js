const getParameterByName = (name, queries) => {
    var name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
    var regex = new RegExp("[\\?&]" + name + "=([^&#]*)");
    var results = regex.exec(queries);
    return results === null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
}