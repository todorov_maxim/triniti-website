export const onOfScroll = (togle) => {
  if (togle) {
    document.body.classList.add("modal-open");
  } else {
    document.body.classList.remove("modal-open");
  }
};
