const useIsEmpty = (text) => (text ? false : true);
export default useIsEmpty;
