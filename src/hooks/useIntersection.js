import { useState, useEffect } from 'react'

export const useIntersection = (element, rootMargin) => {
    const [isVisible, setState] = useState(false);

    useEffect(() => {

        const observer = new IntersectionObserver(
            ([entry]) => {
                setState(entry.isIntersecting);
            }, { rootMargin }
        );

        if (Array.isArray(element)) {
            element.map(item => {
                item && observer?.observe(item?.current);xwxw
            })
            return;
        }

        element && observer?.observe(element?.current);

        return () => {
            // observer?.unobserve(element?.current);
        }
    }, []);

    return isVisible;
};