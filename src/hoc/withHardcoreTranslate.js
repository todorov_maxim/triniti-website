import { useRouter } from "next/router";
import React, { useEffect, useState } from "react";
import { onOfScroll } from "../hooks/useOfScroll";

const withHardcoreTranslate = (Component) => {
  return ({ ...props }) => {
    const router = useRouter();
    // const currLocal = router?.locale;
    const currLocal = "ua";

    const ua = {
      flats: "квартири",
      services: "послуги",
      projectSevenHills: "проєкти SEVEN HILLS",
      site: "сайт ЖК",
      contactNumber: "Контактний телефон",
      mainOffice: "Адреса",
      workSchedule: "Графік роботи",
      email: "E-mail",
    };
    const en = {
      flats: "flats",
      services: "services",
      projectSevenHills: "projects SEVEN HILLS",
      site: "site of RC",
      contactNumber: "Contact number",
      mainOffice: "Head office",
      workSchedule: "Work schedule",
      email: "E-mail",
    };
    const ru = {
      flats: "квартири",
      services: "услуги",
      projectSevenHills: "проекты SEVEN HILLS",
      site: "сайт ЖК",
      contactNumber: "Контактный телефон",
      mainOffice: "Главный офис",
      workSchedule: "График работы",
      email: "E-mail",
    };

    const language = { ua, en, ru };
    const currLanguage = language[currLocal];
    useEffect(() => {
      onOfScroll(false);
    }, []);

    return (
      <>
        <Component {...props} currLanguage={currLanguage} />
      </>
    );
  };
};

export default withHardcoreTranslate;
