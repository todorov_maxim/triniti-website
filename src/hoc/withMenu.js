import React, { useEffect, useState } from "react";
import { Menu } from "../components/Menu/Menu";
import { MenuIcon } from "../components/Menu/MenuIcon";
import { onOfScroll } from "../hooks/useOfScroll";

const withMenu = (Component) => {
  return ({ ...props }) => {
    const {
      pageData: { globalContactsData, menu, pageTheme: theme },
    } = props;

    useEffect(() => {
      onOfScroll(false);
    }, []);

    const [isHiddenMenu, setIsHiddenMenu] = useState(false);
    const [isOpenMenu, setIsOpenMenu] = useState(false);
    const [isOpenMenuWithEffect, setIsOpenMenuWithEffect] = useState(false);
    const onOpenMenu = () => {
      setIsOpenMenu((pre) => !pre);
      setTimeout(() => {
        setIsOpenMenuWithEffect((pre) => !pre);
        onOfScroll(!isOpenMenu);
      }, 200);
    };
    const onCloseMenu = () => {
      setIsOpenMenuWithEffect((pre) => !pre);
      onOfScroll(!isOpenMenu);
      setTimeout(() => {
        setIsOpenMenu((pre) => !pre);
      }, 1600);
    };
    const hiddenMenu = () => {
      setIsHiddenMenu(true);
    };
    const showMenu = () => {
      setIsHiddenMenu(false);
    };

    const propsMenu = {
      showMenu,
      hiddenMenu,
      isOpenMenu,
    };
    return (
      <>
        {!isHiddenMenu && (
          <>
            {isOpenMenu && (
              <Menu
                globalContactsData={globalContactsData}
                isOpenMenu={isOpenMenuWithEffect}
                menu={menu}
              ></Menu>
            )}
            <MenuIcon
              isOpenMenu={isOpenMenu}
              onClick={!isOpenMenu ? onOpenMenu : onCloseMenu}
            ></MenuIcon>
          </>
        )}
        <Component {...props} {...propsMenu} />
      </>
    );
  };
};

export default withMenu;
