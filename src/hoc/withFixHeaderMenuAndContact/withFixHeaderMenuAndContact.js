import React, { useEffect, useState } from "react";
import { Contact } from "../../components/Contact/Contact";
import { Menu } from "../../components/Menu/Menu";
import { MenuIcon } from "../../components/Menu/MenuIcon";
import { onOfScroll } from "../../hooks/useOfScroll";
import { pageTheme } from "../../constants/themes";
import { WrapperMenu } from "./StyledwithFixHeaderMenuAndContact";

const withFixHeaderMenuAndContact = (Component) => {
  return ({ ...props }) => {
    const {
      pageData: { globalContactsData, menu, pageTheme: theme },
    } = props;
    const { textColor, background } = pageTheme[theme];
    // const textColor = "gray";

    useEffect(() => {
      onOfScroll(false);
    }, []);

    const [isHiddenMenu, setIsHiddenMenu] = useState(false);
    const [isOpenMenu, setIsOpenMenu] = useState(false);
    const [isOpenMenuWithEffect, setIsOpenMenuWithEffect] = useState(false);

    // console.log(isOpenMenu, 'isOpenMenu2');
    const onOpenMenu = () => {
      setIsOpenMenu((pre) => !pre);
      setTimeout(() => {
        setIsOpenMenuWithEffect((pre) => !pre);
        onOfScroll(!isOpenMenu);
      }, 200);
    };
    const onCloseMenu = () => {
      setIsOpenMenuWithEffect((pre) => !pre);
      onOfScroll(!isOpenMenu);
      setTimeout(() => {
        setIsOpenMenu((pre) => !pre);
      }, 1600);
    };
    const hiddenMenu = () => {
      setIsHiddenMenu(true);
    };
    const showMenu = () => {
      setIsHiddenMenu(false);
    };

    const propsMenu = {
      showMenu,
      hiddenMenu,
      isOpenMenu,
    };
    return (
      <>
        {!isHiddenMenu && (
          <>
            <WrapperMenu>
              <Contact
                // colorLanguage={"#494E54"}
                color={textColor}
                // isRenderTelephone={true}
              />
              {isOpenMenu && (
                <Menu
                  globalContactsData={globalContactsData}
                  isOpenMenu={isOpenMenuWithEffect}
                  menu={menu}
                ></Menu>
              )}
              <MenuIcon
                isOpenMenu={isOpenMenu}
                onClick={!isOpenMenu ? onOpenMenu : onCloseMenu}
              ></MenuIcon>
            </WrapperMenu>
          </>
        )}
        <Component {...props} {...propsMenu} />
      </>
    );
  };
};

export default withFixHeaderMenuAndContact;
