
import styled from "styled-components";
import { screenSize } from "../../constants/media";



export const WrapperMenu = styled.div`
@media (min-width:1920px ) {
  max-width:1920px;
left: calc( 50% - ( 1920px / 2 ) );
  } ;


position: fixed;
                top: 0px;
                right: 0px;
                width: 100%;
                display: flex;
                justify-content: space-between;
                z-index: 40;
                padding-left: 21px;
                padding-top: 18px;
                /* background-color: #FFFFFF; */
                z-index: 20;
                height: 80px;
                /* @media (max-width: ${screenSize.laptopL}) and (orientation: portrait) {

  } ; */

@media (max-width: ${screenSize.mobileL}) and (orientation: portrait) {
  height: 50px;
  padding-left: 20px;
                padding-top: 14px;

  } ;
  @media (max-width: ${screenSize.mobileLLandscape}) and (orientation: landscape) {
    height: 50px;
    padding-left: 20px;
                padding-top: 14px;
  };  

`
