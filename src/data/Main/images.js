import img1 from "../../public/images/Main/main_1.png";
import img3 from "../../public/images/Main/main_3.png";
import img4 from "../../public/images/Main/main_4.png";
import img5 from "../../public/images/Main/main_5.png";
import img6 from "../../public/images/Main/main_6.png";
import img8 from "../../public/images/Main/main_8.png";
import img9 from "../../public/images/Main/main_9.png";
import img11 from "../../public/images/Main/main_11.png";
import img12 from "../../public/images/Main/main_12.png";

const opacity = 1;
export const imgArr = [
  { type: "img", img: img1, opacity, className: "main_1 fadedImg" },
  { type: "UniqueCell_1", className: "main_2" },
  { type: "img", img: img3, opacity, className: "main_3 fadedImg" },
  { type: "img", img: img4, className: "main_4" },
  { type: "img", img: img5, className: "main_5" },
  { type: "img", img: img6, className: "main_6 fadedImg" },
  { type: "UniqueCell_2", className: "main_7" },
  { type: "img", img: img8, opacity, className: "main_8 fadedImg" },
  { type: "img", img: img9, opacity, className: "main_9 fadedImg" },
  { type: "UniqueCell_3", className: "main_10" },
  { type: "img", img: img11, opacity, className: "main_11 fadedImg" },
  { type: "img", img: img12, className: "main_12" },
];
