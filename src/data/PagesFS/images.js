import pepole from "../../public/images/PagesFS/1_pepole.png";
import heritage from "../../public/images/PagesFS/2_1_heritage.png";
import realEstate from "../../public/images/PagesFS/2_RealEstate.png";
import propertyManagement from "../../public/images/PagesFS/5_PropertyManagement.png";
import utilityManagement from "../../public/images/PagesFS/6_UtilityManagement.png";
import rental from "../../public/images/PagesFS/7_Rental.png";
import home from "../../public/images/PagesFS/8_Home.png";
import contacts from "../../public/images/PagesFS/9_Сontacts.png";
import app from "../../public/images/PagesFS/app.png";

const fullScreenImg = [
  {
    img: pepole,
    background: "#CD8B7D",
    numberPage: "01",
    title: "LIFELONG PARTNERSHIP",
    description:
      "Житловий комплекс - це стиль життя, в якому ваш дім, ваші почуття і ви на першому місці",
    textLink: "Про нас",
    textColor: "#FFFFFF",
  },
  {
    img: heritage,
    background: "#9C8D88",
    numberPage: "02",
    title: "HERITAGE",
    description:
      "Житловий комплекс - це стиль життя, в якому ваш дім, ваші почуття і ви на першому місці",
    textLink: "Про ЖК Triiiniry",
    textColor: "#FFFFFF",
  },
  {
    img: realEstate,
    background: "#F0B8A9",
    numberPage: "03",
    title: "ЖИТЛОВИЙ КОМПЛЕКС TRIIINITY",
    description:
      "Житловий комплекс - це стиль життя, в якому ваш дім, ваші почуття і ви на першому місці",
    textLink: "Про проєкти",
    textColor: "#494E54",
  },
  {
    img: app,
    background: "#EAD6D5",
    numberPage: "04",
    title: "Application",
    description:
      "Первоклассный сервис для наших партнеров и клиентов: от инвестиций до проживания, ежедневных услуг и сервиса, а также сопровождение аренды или перепродажи.",
    textLink: "Дізнатись детально",
    textColor: "#494E54",
  },
  {
    img: propertyManagement,
    background: "#D9D3C5",
    numberPage: "05",
    title: "property management",
    description:
      "Первоклассный сервис для наших партнеров и клиентов во время всего клиентского опыта: от инвестиций до проживания, ежедневных услуг и сервиса, а также сопровождение аренды или перепродажи.",
    textLink: "Дізнатись детально",
    textColor: "#494E54",
  },
  {
    img: utilityManagement,
    background: "#CD8B7D",
    numberPage: "06",
    title: "utility management",
    description:
      "Первоклассный сервис для наших партнеров и клиентов во время всего клиентского опыта: от инвестиций до проживания, ежедневных услуг и сервиса, а также сопровождение аренды или перепродажи.",
    textLink: "Дізнатись детально",
    textColor: "#FFFFFF",
  },

  {
    img: rental,
    background: "#EAD6D5",
    numberPage: "07",
    title: "Rental",
    description:
      "Первоклассный сервис для наших партнеров и клиентов во время всего клиентского опыта: от инвестиций до проживания, ежедневных услуг и сервиса, а также сопровождение аренды или перепродажи.",
    textLink: "Обрати приміщення",
    textColor: "#494E54",
  },
  {
    img: home,
    background: "#F0B8A9",
    numberPage: "08",
    title: `Home, feelings & you`,
    description:
      "Первоклассный сервис для наших партнеров и клиентов во время всего клиентского опыта: от инвестиций до проживания, ежедневных услуг и сервиса, а также сопровождение аренды или перепродажи.",
    textLink: "Дізнатись детально",
    textColor: "#494E54",
  },
  {
    img: contacts,
    background: "#9C8D88",
    numberPage: "09",
    textColor: "#FFFFFF",
    isContact: true,
  },
];
export default fullScreenImg;
