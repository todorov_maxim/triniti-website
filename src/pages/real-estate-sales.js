import { compose } from "redux";
import { getRealEstateSalesPageData } from "../../lib/api";
import { MetaHead } from "../components/MetaHead/MetaHead";
import { RealEstateSales } from "../components/Pages/RealEstateSales/RealEstateSales";
import withFixHeaderMenuAndContact from "../hoc/withFixHeaderMenuAndContact/withFixHeaderMenuAndContact";
import withMenu from "../hoc/withMenu";

const RealEstateSalesPage = ({ pageData, ...res }) => {
  const pageTitle = "СУСІДСЬКА СПІЛЬНОТА";
  return (
    <>
      <MetaHead title={pageTitle}/>
      <RealEstateSales pageData={pageData} {...res} />
    </>
  );
};

export default compose(withMenu)(RealEstateSalesPage);

export async function getServerSideProps({ locale }) {
  //const lang = locale.toUpperCase();
  const lang = "UA";
  const pageData = await getRealEstateSalesPageData(lang, `Меню ${lang}`);

  return {
    props: {
      pageData,
    },
  };
}
