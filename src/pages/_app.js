import { useState, useEffect } from "react";
import { getGlobalContactsData } from "../../lib/api";
import { Container } from "../components/Container/Container";
import GlobalContactContext from "../GlobalContactContext";
import "../styles/index.css";
import { useRouter } from "next/router";

function MyApp({ Component, pageProps }) {
  const [contactData, setContactData] = useState(
    pageProps?.pageData?.globalContactsData
  );

  const router = useRouter();
  const {utm_campaign, utm_medium, utm_content, utm_term, utm_source = "direct" } = router.query;
  
  const urlWithQueries = (utm_campaign, utm_medium, utm_content, utm_term, utm_source) ? router.asPath : null;
  const utmMarks = {utm_campaign, utm_medium, utm_content, utm_term, utm_source, urlWithQueries};

  // console.log(utmMarks, 'rrrr');
  
  useEffect(() => {
    
    Object.keys(utmMarks).forEach(key => {
      if (localStorage.getItem(key) || !utmMarks[key]) {
        return;
      }
      localStorage.setItem(key, utmMarks[key]);
    })

  }, [])

  return (
    <Container>
      <GlobalContactContext.Provider value={{ contactData, setContactData }}>
        <Component {...pageProps} />
      </GlobalContactContext.Provider>
    </Container>
  );
}

export default MyApp;
