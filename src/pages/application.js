import { compose } from "redux";
import { getAppPageData, getUtilityManagementPageData } from "../../lib/api";
import ManagementPage from "../components/ManagementPage/ManagementPage";
import { MetaHead } from "../components/MetaHead/MetaHead";
import { Application } from "../components/Pages/Application/Application";
import withFixHeaderMenuAndContact from "../hoc/withFixHeaderMenuAndContact/withFixHeaderMenuAndContact";
import withMenu from "../hoc/withMenu";

const ApplicationPage = ({ pageData, ...res }) => {
  const pageTitle = "App";
  
  return (
    <>
      <MetaHead title={pageTitle}/>
      <Application pageData={pageData} {...res} />
    </>
  );
};

export default compose(withMenu)(ApplicationPage);

export async function getServerSideProps({ locale }) {
  //const lang = locale.toUpperCase();
  const lang = "UA";
  const pageData = await getAppPageData(lang, `Меню ${lang}`);

  return {
    props: {
      pageData,
    },
  };
}
