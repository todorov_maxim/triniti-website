import {compose} from "redux"
import {getContactsPageData, getGalleryPageData} from "../../lib/api"
import Footer from "../components/Footer"
import {MetaHead} from "../components/MetaHead/MetaHead"
import {useEffect, useState, useMemo} from "react"
import withFixHeaderMenuAndContact from "../hoc/withFixHeaderMenuAndContact/withFixHeaderMenuAndContact"
import withMenu from "../hoc/withMenu"
import {StyledFormContent, StyledInput, StyledFormButton, StyledModalOverlay} from "./../components/Contact/Form/StyledForm"
import Script from "next/script"
import {useRouter} from "next/router"
import {HeaderPage} from "../components/Pages/About/SupportComponent/AboutFullscreen/StyledAboutFullscreen"
import {WrapeperLogo} from "../components/FullScreenPage/StyledFullScreenPage"
import {LogoWithText} from "../components/LogoWithText/LogoWithText"
import {pageTheme} from "../constants/themes"
import {Contact} from "../components/Contact/Contact"
import {ContainerContent} from "../components/ManagementPage/SupportComponent/ContactWithCompany/StyledContactWithCompany"
import MiniSlider from "../components/MiniSlider"
import {Gallery} from "../components/Gallery/Gallery"
import {StyledGalleryWrapper} from "../components/MiniSlider/StyledMiniSlider"
import {FullScreenHeader} from "../components/FullScreenPage/SupportComponents/FullScreenHeader"
import { Modal } from "../components/Modal/Modal"
import { onOfScroll } from "../hooks/useOfScroll"

const GalleryPage = ({pageData, showMenu, hiddenMenu}) => {
  const {textColor, background} = pageTheme.gray

  const pageTitle = "Gallery";

  const [isShowModal, setIsShowModal] = useState(false);
  const [selectedGalery, setSelectedGalery] = useState([]);
  const showModal = (galery) => {
    onOfScroll(true);
    hiddenMenu();
    setSelectedGalery(galery);
    setIsShowModal(true);
  };
  const modalHidden = () => {
    onOfScroll(false);
    showMenu();
    setIsShowModal(false);
    setSelectedGalery([]);
  };

  return (
    <>
      <MetaHead title={pageTitle} />

      {isShowModal && (
        <Modal modalHidden={modalHidden}>
          <MiniSlider namePage={"modal"} modal gallery={selectedGalery} />
        </Modal>
      )}
      {/* <HeaderPage background={background}>
        <WrapeperLogo>
          <LogoWithText namePage="GeneralPages" fill={textColor} />
        </WrapeperLogo>
      </HeaderPage> */}

      <FullScreenHeader isContact={false} hiddenHeader={false} />

      <ContainerContent variant={"lg"}>
        <StyledGalleryWrapper>
          <MiniSlider
            heritage
            namePage={"heritage"} gallery={pageData?.slider} showModal={() => showModal(pageData?.slider)}/>
        </StyledGalleryWrapper>
      </ContainerContent>
      <Footer pageData={pageData} />
    </>
  )
}
export default compose(withMenu)(GalleryPage)

export async function getServerSideProps({locale}) {
  //const lang = locale.toUpperCase();
  const lang = "UA"
  const pageData = await getGalleryPageData(lang, `Меню ${lang}`)

  return {
    props: {
      pageData,
    },
  }
}
