import { compose } from "redux";
import { getContactsPageData } from "../../lib/api";
import Footer from "../components/Footer";
import { MetaHead } from "../components/MetaHead/MetaHead";
import { Contact } from "../components/Pages/Contact/Contact";
import { useEffect, useState, useMemo } from "react";
import withFixHeaderMenuAndContact from "../hoc/withFixHeaderMenuAndContact/withFixHeaderMenuAndContact";
import withMenu from "../hoc/withMenu";
import { StyledFormContent, StyledInput, StyledFormButton, StyledModalOverlay } from './../components/Contact/Form/StyledForm';
import Script from "next/script";
import { useRouter } from "next/router";

const ContactPages = ({ pageData }) => {


  // const router = useRouter();
  // const {utm_campaign, utm_medium, utm_content, utm_term, utm_source = "direct" } = router.query;
  // // console.log(router, 'rrrr');
  // const urlWithQueries = (utm_campaign, utm_medium, utm_content, utm_term, utm_source) ? router.asPath : null;
  // const utmMarks = {utm_campaign, utm_medium, utm_content, utm_term, utm_source, urlWithQueries};
  //start code for BX24 
  const [onceLoaded, isOnceLoaded] = useState(false);
  const [isShowedForm, setIsShowedForm] = useState(false);
  
  useEffect(() => {
    // setShown(true);
    isOnceLoaded(true);

    // Object.keys(utmMarks).forEach(key => {
    //   if (localStorage.getItem(key) || !utmMarks[key]) {
    //     return;
    //   }
    //   localStorage.setItem(key, utmMarks[key]);
    // })

  }, [])
  
  // useEffect(() => {
  //   let froms = document.querySelectorAll('.b24-form');
  //   froms.forEach((form, i, arr) => {
        
  //       console.log(i, 'i');
  //       if (i !== arr.length) {
  //           form.classList.remove('shown')
  //       }
  //       isShowedForm ? form.classList.add('shown') : form.classList.remove('shown');
  //   })
  //   console.log(froms);
  //   froms
  // }, [isShowedForm])
  
  const showForm = () => {
    setIsShowedForm(prev => !prev)
  }
  
  // useEffect(() => {
  //   if (typeof document !== 'undefined') {
  //       let forms = document.querySelectorAll('.b24-form');
  //       console.log(forms, 'forms');
  //       forms.forEach((form, i, arr) => {
  //           console.log(i, 'in arr');
  //           if (i !== arr.length) {
  //               console.log('remove');
  //               form.remove();
  //           }
  
  //       })
  //   }
  // }, [onceLoaded])

  const pageTitle = "Contacts";
  return (
    <>
      <MetaHead title={pageTitle}/>
      <StyledModalOverlay shown={isShowedForm} onClick={showForm}/>
      <Contact pageData={pageData} showForm={showForm} onceLoaded={onceLoaded}/>
      <Footer pageData={pageData} />
    </>
  );
};
export default compose(withMenu)(ContactPages);

export async function getServerSideProps({ locale }) {
  //const lang = locale.toUpperCase();
  const lang = "UA";
  const pageData = await getContactsPageData(lang, `Меню ${lang}`);

  return {
    props: {
      pageData,
    },
  };
}
