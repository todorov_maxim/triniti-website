import { getPropertyManagementPageData } from "../../lib/api";
import ManagementPage from "../components/ManagementPage/ManagementPage";
import { MetaHead } from "../components/MetaHead/MetaHead";

export default ({ pageData }) => {
  const pageTitle = "PGD";
  return (
    <>
      <MetaHead title={pageTitle}/>
      <ManagementPage pageData={pageData} namePage="P_Management" />
    </>
  );
};

export async function getServerSideProps({ locale }) {
  //const lang = locale.toUpperCase();
const lang = "UA";
  const pageData = await getPropertyManagementPageData(lang, `Меню ${lang}`);

  return {
    props: {
      pageData,
    },
  };
}
