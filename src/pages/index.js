import { Main } from "../components/Pages/Main/Main";
import Brand from "../components/Pages/Brand/Brand";
import ReactFullpage from "@fullpage/react-fullpage";
import withMenu from "../hoc/withMenu";
import { compose } from "redux";
import { useEffect, useState, useMemo } from "react";
import { DesctopPages } from "../components/DesctopPages/DesctopPages";
import { MobilePages } from "../components/MobilePages/MobilePages";
import { getMainPageData } from "../../lib/api";
import fullScreenImg from "../data/PagesFS/images";
import { MetaHead } from "../components/MetaHead/MetaHead";
import { InfoPopup, InfoPopupContent,InfoPopupClose, InfoPopupLink, InfoPopupText, InfoPopupTitle, InfoPopupTextBox, InfoPopupList, InfoPopupListItem, SuccessPopup, SuccessPopupClose, SuccessPopupText, ModalBody, ModalClose, ModalContent, ModalText, ModalTitle, ModalFooter, ModalFieldsError } from "../components/Pages/Main/StyledMain";
import { StyledFormContent, StyledInput, StyledFormButton, StyledModalOverlay } from './../components/Contact/Form/StyledForm';
import Script from "next/script";
import { useRouter } from "next/router";



const getRequestOptions = () => {
  // var myHeaders = {
  //   "Accept": "application/json",
  //   "Content-Type": "application/json; charset=utf-8",
  //   "ForceUseSession": "true"
  // }
  var headers = new Headers();
  headers.append("Accept", "application/json");
  headers.append("Content-Type", "application/json; charset=utf-8");
  headers.append("ForceUseSession", "true");

  var data = {  
    "UserName":"Supervisor",
    "UserPassword":"Supervisor1"
  };

  return {
    method: 'POST',
    headers: headers,
    body: JSON.stringify(data),
    redirect: 'follow',
  };
}

const baseURI = "https://triiinity.terrasoft.ua";

const getBPMCSRF = async () => {
  const options = getRequestOptions();
  // console.log(options, 'headers');
  fetch(`${baseURI}/ServiceModel/AuthService.svc/Login`, options)
  .then(response => response.text())
  .then(result => {
    console.log(result, 'result')
  })
  .catch(error => console.log('error', error));
}




const Pages = ({ isOpenMenu, pageData }) => {
  // desctop
  let mainBrandPages = [Main, Brand];

  const [pageCount, setPageCount] = useState(1);
  const [fetch, setfetch] = useState(false);
  const [openedModal, setOpenedModal] = useState(true);
  const [openedSuccessModal, setOpenedSuccessModal] = useState(false);
  const [fetch2, setfetch2] = useState(true);
  const [vh, setvh] = useState(true);
  const [down, setdown] = useState("");
  const [up, setup] = useState("");
  const router = useRouter();
  
  const [openedCTAModal, setOpenedCTAModal] = useState(false);
  const [inputsVal, setInputsVal] = useState({
    name: '',
    tel: '',
    email: '',
  })

  // "calcPageCount" смотрит на направлении и от этого увечивает или уменшает счетчик страниц
  const calcPageCount = (pre, isDown, isUp, allPages) => {
    if (isUp) {
      if (pre == 0) {
        return 0;
      } else {
        return (pre -= 1);
      }
    } else if (isDown) {
      if (pre == allPages.length + 1) {
        return allPages.length + 1;
      }
      return (pre += 1);
    }
  };

  useEffect(() => {
    // скорее всего здесь писать код для анимации
  }, [pageCount]);

  useEffect(() => {
    if (fetch) {
      setfetch2(false);
      setPageCount((pre) =>
        calcPageCount(pre, down, up, pageData.commonSlides)
      );
      setfetch(false);
      setTimeout(() => {
        setfetch2(true);
      }, 1000);
    }
  }, [fetch]);

  const setScreenOrientation = () => {
    let vhSize = window.innerHeight * 0.01;
    document.documentElement.style.setProperty("--vh", `${vhSize}px`);
    setvh(vhSize);
  };

  useEffect(() => {
    setScreenOrientation();
    // Then we set the value in the --vh custom property to the root of the document
    window.addEventListener("resize", setScreenOrientation);
  }, [vh]);
  //desctop

  const [isMobile, setIsMobile] = useState(false);

  //onDesctopLeave для отслживания на какой странице мы сейчас в desctop
  const onDesctopLeave = (origin, destination, direction) => {
    setdown(direction == "down");
    setup(direction == "up");

    if (destination.index === 3) {
      fetch2 && setfetch(true);
      return false;
    }

    if (origin.index == 2) {
      fetch2 && setfetch(true);
      if (pageCount >= 1) {
        return false;
      }
      if (pageCount == 0) {
        return true;
      }
    }
  };

  const pageTitle = "PGD";


  //start code for BX24 
  const [onceLoaded, isOnceLoaded] = useState(false);
  const [isShowedForm, setIsShowedForm] = useState(false);
  const [isSuccessSubmitedForm, setSuccessSubmitedForm] = useState(false);
  const [errorMessage, setErrorMessage] = useState('');

  const onModalSubmit = () => {
    // setErrorMessage('');
    // const reqMessage = 'Заповніть наступні поля: ';
    // var validEmailRegex = /^[^\s@]+@[^\s@]+\.[^\s@]+$/;

    // let fields = [];
    // if (!inputsVal.name.length) {
    //   fields.push('імʼя')
    // }

    // if (!inputsVal.tel.length) {
    //   fields.push('телефон')
    // }

    // if (!inputsVal.email.length) {
    //   fields.push('email')
    // }

    // if (fields.length) {
    //   fields.length > 1 ? (reqMessage+= fields.join(', ') + '.') : (reqMessage += fields.join() + '.');
    //   setErrorMessage(reqMessage);
    //   console.log('req');
    //   return false;
    // }

    // if (!inputsVal.email.match(validEmailRegex)) {
    //   console.log('Невірно заповнене поле email');
    //   setErrorMessage('Невірно заповнене поле email');
    //   return false
    // }

    // if ((inputsVal.tel[0] === '+' && inputsVal.tel.length < 13) 
    // || (inputsVal.tel[0] === '3' && inputsVal.tel.length < 12) 
    // || (inputsVal.tel[0] === '0' && inputsVal.tel.length < 10) 
    // || (inputsVal.tel.slice(0, 4) !== '+380' && inputsVal.tel[0] !== '3' && inputsVal.tel[0] !== '0')) {
    //   console.log('Невірно вказаний номер');
    //   setErrorMessage('Невірно вказаний номер');
    //   return false;
    // }

    getBPMCSRF()
    // createObject(); 
    return false

  }


  
  // useEffect(() => {
  //   // setShown(true);
  //   // console.log('event');
  //   isOnceLoaded(true);
  //   // console.log(router.asPath === '/#success', window?.location?.hash === '#success');
  //   if (router.asPath === '/#success' || window?.location?.hash === '#success') {
  //     setOpenedSuccessModal(true);
  //   }

  //   if (router.asPath === '/#creatio-modal' || window?.location?.hash === '#creatio-modal') {
  //     setOpenedCTAModal(true);
  //   }

  //   const onHashChangeStart = (url) => {
  //     if (window?.location?.hash !== '#success') {

  //       setOpenedSuccessModal(false);
  //       setIsShowedForm(false);
  //     } else if (window?.location?.hash !== '#creatio-modal') {
  //       setOpenedCTAModal(false);
  //       setIsShowedForm(false);
  //     } else {
  //       setOpenedSuccessModal(true);
  //       setIsShowedForm(false);
  //     }
  //   };

  //   window.addEventListener("hashchange", onHashChangeStart);

  //   return () => {
  //     window.removeEventListener("hashchange", onHashChangeStart);
  //   };
    
  // }, [router.events, openedSuccessModal])
  
  // useEffect(() => {
    // let froms = document.querySelectorAll('.b24-form');
    // froms.forEach((form, i, arr) => {
        
    //     if (i !== arr.length) {
    //         form.classList.remove('shown')
    //     }
    //     isShowedForm ? form.classList.add('shown') : form.classList.remove('shown');
    // })
    // froms
  // }, [isShowedForm])
  
  const showForm = () => {
    setIsShowedForm(prev => !prev)
  }

  return (
    //ReactFullpage  дял скрола на 100vh
    <>
      <MetaHead title={pageTitle} />
      {/* {openedModal && 
      <InfoPopup className="no-fp-scroll">
         <InfoPopupClose className="icon-close" onClick={() => {
                    setOpenedModal(false)
                  }}/>
                <InfoPopupContent>
                 
                  <InfoPopupTitle>МИ ВІДНОВЛЮЄМО БУДІВЕЛЬНІ РОБОТИ У TRIIINITY!</InfoPopupTitle>
                  <InfoPopupTextBox>
                    <InfoPopupText>На цей момент ми дуже чекали. <br/>
                      Кожна дія веде до перемоги та відродження економіки України.<br/>
                      Ми підтримуємо заклик Президента та Кабінету міністрів про повернення бізнесів до активності. Ми щиро віримо у мирне та стабільне майбутнє нашої Країни.<br/>
                      Адже головне та найважливіше для нас – бути надійним партнером для вас у будь-які, навіть найтяжчі часи..</InfoPopupText>
                      <InfoPopupText>Ми на всі 100% впевнені у нашому проєкті та команді, у наших партнерах та підрядниках.
                      Як ми й обіцяли – ми відновлюємо будівництво при першій безпечній можливості, враховуючі ризики та з думкою про те, що безпека людей на будівельному майданчику для нас – найперший пріоритет.
                      Всі ми разом, від спеціалістів з будівництва чи логістики до менеджменту проєкту, докладаємо всі  зусилля для продовження будівництва TRIIINITY, щоб скоріше створити місце, в якому ви нарешті віднайдете відчуття дому.</InfoPopupText>
                    <InfoPopupText>Які саме роботи ми відновлюємо спочатку:</InfoPopupText>
                    <InfoPopupList>
                      <InfoPopupListItem>монолітні роботи, щоб житловий комплекс TRIIINITY міг зростати далі</InfoPopupListItem>
                      <InfoPopupListItem>роботи з цегляної кладки</InfoPopupListItem>
                      <InfoPopupListItem>влаштування інженерії та інших робіт.</InfoPopupListItem>
                    </InfoPopupList>
                    <InfoPopupText>Хочемо наголосити: роботи  поновлені з 26 квітня 2022 р. згідно вимог безпеки людей, військового стану та обмежень комендантської години.</InfoPopupText>
                    <InfoPopupText>Вже скоро плануємо відкриття офісу, де зможемо знову зустрітись особисто.
  Деталі повідомимо згодом.</InfoPopupText>
                    <InfoPopupText>Ми щиро віримо в Нас, в Майбутнє, в наших Захисніків та Перемогу! <br/>
  Все вийде, коли ми разом!</InfoPopupText>
                    <InfoPopupText>Команда TRIIINITY</InfoPopupText>
                    <InfoPopupLink href={'http://wp.triiinity.com.ua/wordpress/wp-content/uploads/2022/03/info.pdf'} target="_blank">Ознайомитись</InfoPopupLink> 
                  </InfoPopupTextBox>
                </InfoPopupContent>
              </InfoPopup>
        } */}

        {openedSuccessModal && 
        (<>
        <StyledModalOverlay shown={true} onClick={() => {
          setOpenedSuccessModal(false);
          router.push('/', undefined, { shallow: true })
        }}/>
        <SuccessPopup>
          <SuccessPopupClose className="icon-close" onClick={() => {
            setOpenedSuccessModal(false);
            router.push('/', undefined, { shallow: true })
          }}/>
          <SuccessPopupText>Ваш запит відправлено, дякуємо!</SuccessPopupText>
        </SuccessPopup>
        </>)
        } 

        {isShowedForm && 
        <>
        <StyledModalOverlay shown={true} onClick={() => {
          showForm();
          // router.push('/', undefined, { shallow: true })
        }}/>
        <ModalBody>
          <ModalClose className="icon-close" onClick={() => {
            showForm();
            // router.push('/', undefined, { shallow: true })
          }}/>
          <ModalContent>
            <iframe src="/form.html" width="100%" height="240px" frameBorder="0"></iframe>
            {/* <ModalTitle>Лід-форма для тестування</ModalTitle>
            <StyledInput name="name" placeholder="Ім'я" id="name" onChange={(e) => {
                    setErrorMessage('');
                    setInputsVal((prev) => {
                        return {
                            ...prev,
                            name: e.target.value
                        }
                    })
                }} autoComplete="given-name" type="text" value={inputsVal.name}/>

                <StyledInput name="tel" id="tel" placeholder="Телефон" autoComplete="given-tel" type="tel" pattern="[\+]\d{2}\s[\(]\d{3}[\)]\s\d{3}[\-]\d{2}[\-]\d{2}" minLength="13" maxLength="13" value={inputsVal.tel} onChange={(e) => {
                  setErrorMessage('');
                  if (!/^[0-9+]+$/.test(e.target.value) && e.target.value) {
                    e.preventDefault();
                    return
                  }
                    setInputsVal((prev) => {
                        return {
                            ...prev,
                            tel: e.target.value
                        }
                    })
                }}/>

                <StyledInput name="email" id="email" placeholder="E-mail" autoComplete="given-email" type="email" value={inputsVal.email} onChange={(e) => {
                    setErrorMessage('');
                    setInputsVal((prev) => {
                        return {
                            ...prev,
                            email: e.target.value
                        }
                    })
                }}/>

                <StyledInput name="project" autoComplete="given-email" type="hidden" value={'Triiinity'}/>
                <StyledInput name="type" autoComplete="given-email" type="hidden" value={'лид-форма'}/>

                <ModalFieldsError isError={errorMessage.length}>{errorMessage}</ModalFieldsError> */}
          </ModalContent>
          {/* <ModalFooter>
            <StyledFormButton onClick={onModalSubmit}>{"Зв`язатись"}</StyledFormButton>
          </ModalFooter> */}
        </ModalBody>
        </>
        } 

      
      {/* <StyledModalOverlay shown={isShowedForm} onClick={showForm}/> */}
      <ReactFullpage
        licenseKey={"YOUR_KEY_HERE"}
        scrollingSpeed={1000}
        normalScrollElements={".no-fp-scroll"}
        normalScrollElementTouchThreshold={'.no-fp-scroll'}
        onLeave={!isMobile ? onDesctopLeave : () => {}}
        afterLoad={(origin, destination, direction) => {
          if (origin.index == 2) {
            if (pageCount == 0) {
              setPageCount(1);
            }
          }
        }}
        render={({ state, fullpageApi }) => {
          return (
            <>
            
            <ReactFullpage.Wrapper>
              <Wrapper
                fullpageApi={fullpageApi}
                isOpenMenu={isOpenMenu}
                setIsMobile={setIsMobile}
              >
                {/* первые 2 страницы */}
                {mainBrandPages.map((Page) => (
                  <div className="section">
                    <Page fullpageApi={fullpageApi} pageData={pageData} />
                  </div>
                ))}
                {/* первые 2 страницы */}

                {/* остальрные страницы*/}
                <>
                  {!isMobile && (
                    <DesctopPages
                      pageCount={pageCount}
                      fullpageApi={fullpageApi}
                      pageData={pageData}
                      showForm={showForm}
                      onceLoaded={onceLoaded}
                    />
                  )}
                  {isMobile && (
                    <MobilePages
                      isOpenMenu={isOpenMenu}
                      fullpageApi={fullpageApi}
                      pageData={pageData}
                      showForm={showForm}
                      onceLoaded={onceLoaded}
                    />
                  )}
                  ;
                </>
                {/* остальрные страницы*/}
              </Wrapper>
            </ReactFullpage.Wrapper>
            </>
          );
        }}
      />
    </>
  );
};
export default compose(withMenu)(Pages);

// со временм вынесу этот Wrapper из этого файла!
export const Wrapper = ({ children, isOpenMenu, fullpageApi, setIsMobile }) => {
  const [isResize, setIsresize] = useState(false);

  useEffect(() => {
    // для  подстаивания высоты страницы при "resize"
    // по скольку данные в функции "window.addEventListener(`resize`) данные не обновляються
    // а fullpageApi сначала undefined а потом меняеться значения то вынес логику сюда
    fullpageApi && fullpageApi.reBuild();
  }, [isResize]);

  useEffect(() => {
    window.innerWidth <= 768 ? setIsMobile(true) : setIsMobile(false);
    //if the user will resize the screen <=768
    window.addEventListener(
      `resize`,
      (event) => {
        //ниже строчка чтобы отоавливать изменения экрана, она работает в паре с useEffect у которого в засимости [isResize]
        setIsresize((pre) => !pre);
        if (window.screen.width <= 768) {
          setIsMobile(true);
        } else {
          setIsMobile(false);
        }
      },
      false
    );
  }, []);

  // если меню открыто то отключаю scroll
  const onEnableDisableScroll = () => {
    !isOpenMenu && fullpageApi.setAllowScrolling(true);
    isOpenMenu && fullpageApi.setAllowScrolling(false);
  };
  useEffect(() => {
    if (fullpageApi) {
      onEnableDisableScroll();
    }
  }, [isOpenMenu]);

  return <>{children}</>;
};

export async function getServerSideProps({ locale }) {
  //const lang = locale.toUpperCase();
  const lang = "UA";
  const pageData = await getMainPageData(lang, `Меню ${lang}`);

  return {
    props: {
      pageData,
    },
  };
}
