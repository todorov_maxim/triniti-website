import { compose } from "redux";
import { getHeritagePageData } from "../../lib/api";
import { MetaHead } from "../components/MetaHead/MetaHead";
import { Heritage } from "../components/Pages/Heritage/Heritage";
import withFixHeaderMenuAndContact from "../hoc/withFixHeaderMenuAndContact/withFixHeaderMenuAndContact";
import withMenu from "../hoc/withMenu";

const HeritagePage = ({ pageData, ...res }) => {
  const pageTitle = "Heritage";
  return (
    <>
      <MetaHead title={pageTitle}/>
      <Heritage pageData={pageData} {...res} />
    </>
  );
};

export default compose(withMenu)(HeritagePage);

export async function getServerSideProps({ locale }) {
  //const lang = locale.toUpperCase();
  const lang = "UA";
  const pageData = await getHeritagePageData(lang, `Меню ${lang}`);

  return {
    props: {
      pageData,
    },
  };
}
