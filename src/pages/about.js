import { getAboutPageData } from "../../lib/api";
import { AboutContent } from "../components/Pages/About/SupportComponent/AboutContent/AboutContent";
import AboutFullscreen from "../components/Pages/About/SupportComponent/AboutFullscreen/AboutFullscreen";
import { AboutStatistics } from "../components/Pages/About/SupportComponent/AboutStatistics/AboutStatistics";
import { AllPhotoSlider } from "../components/Pages/About/SupportComponent/AllPhotoSlider/AllPhotoSlider";
import Slider from "../components/Slider";
import Footer from "../components/Footer";
import withFixHeaderMenuAndContact from "../hoc/withFixHeaderMenuAndContact/withFixHeaderMenuAndContact";
import { compose } from "redux";
import withMenu from "../hoc/withMenu";
import { MetaHead } from "../components/MetaHead/MetaHead";
import { FullScreenBlockGeneralPages } from "../components/FullScreenBlockGeneralPages/FullScreenBlockGeneralPages";

const About = ({ pageData }) => {
  const pageTitle = "About us";

  return (
    <>
      <MetaHead title={pageTitle} />
      {/* <AboutFullscreen pageData={pageData} /> */}
      <FullScreenBlockGeneralPages  pageData={pageData}/>
      <AboutContent pageData={pageData} />
      <AboutStatistics pageData={pageData} />
      <Slider sliderData={pageData?.profilesSlider} pageData={pageData} />
      <AllPhotoSlider sliderData={pageData?.profilesSlider} />
      <Footer pageData={pageData} />
    </>
  );
};

export default compose(withMenu)(About);

export async function getServerSideProps({ locale }) {
  //const lang = locale.toUpperCase();
  const lang = "UA";
  const pageData = await getAboutPageData(lang, `Меню ${lang}`);

  return {
    props: {
      pageData,
    },
  };
}
