import { compose } from "redux";
import { getRealEstatePageData } from "../../lib/api";
import ManagementPage from "../components/ManagementPage/ManagementPage";
import { MetaHead } from "../components/MetaHead/MetaHead";
import { RealEstate } from "../components/Pages/RealEstate/RealEstate";
import withFixHeaderMenuAndContact from "../hoc/withFixHeaderMenuAndContact/withFixHeaderMenuAndContact";
import withMenu from "../hoc/withMenu";

const RealEstatePage = ({ pageData, ...res }) => {
  const pageTitle = "НАШ ДОСВІД";
  return (
    <>
      <MetaHead title={pageTitle}/>
      <RealEstate pageData={pageData} {...res} />
    </>
  );
};

export default compose(withMenu)(RealEstatePage);

export async function getServerSideProps({ locale }) {
  //const lang = locale.toUpperCase();
  const lang = "UA";
  const pageData = await getRealEstatePageData(lang, `Меню ${lang}`);

  return {
    props: {
      pageData,
    },
  };
}
