import { getUtilityManagementPageData } from "../../lib/api";
import ManagementPage from "../components/ManagementPage/ManagementPage";
import { MetaHead } from "../components/MetaHead/MetaHead";

export default ({ pageData }) => {
  const pageTitle = "СЕРВІС 360";
  return (
    <>
      <MetaHead title={pageTitle}/>
      <ManagementPage pageData={pageData} />
    </>
  );
};

export async function getServerSideProps({ locale }) {
  //const lang = locale.toUpperCase();
const lang = "UA";
  const pageData = await getUtilityManagementPageData(lang, `Меню ${lang}`);

  return {
    props: {
      pageData,
    },
  };
}
