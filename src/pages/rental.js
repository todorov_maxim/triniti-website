import { compose } from "redux";
import { getRentalPageData } from "../../lib/api";
import ManagementPage from "../components/ManagementPage/ManagementPage";
import { MetaHead } from "../components/MetaHead/MetaHead";
import { Rental } from "../components/Pages/Rental/Rental";
import withFixHeaderMenuAndContact from "../hoc/withFixHeaderMenuAndContact/withFixHeaderMenuAndContact";
import withMenu from "../hoc/withMenu";

const RentalPage = ({ pageData, ...res }) => {
  const pageTitle = "PGD";
  return (
    <>
      <MetaHead title={pageTitle}/>
      <Rental pageData={pageData} {...res} />
    </>
  );
};

export default compose(withMenu)(RentalPage);

export async function getServerSideProps({ locale }) {
  //const lang = locale.toUpperCase();
  const lang = "UA";
  const pageData = await getRentalPageData(lang, `Меню ${lang}`);

  return {
    props: {
      pageData,
    },
  };
}
