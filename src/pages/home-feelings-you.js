import { getHomeFeelingsYouPageData } from "../../lib/api";
import { MetaHead } from "../components/MetaHead/MetaHead";
import HomeFeelingsYou from "../components/Pages/HomeFeelingsYou/HomeFeelingsYou";

export default ({ pageData }) => {
  const pageTitle = "Home, Feelings & You";
  return (
    <>
      <MetaHead title={pageTitle}/>
      <HomeFeelingsYou pageData={pageData} />
    </>
  );
};

export async function getServerSideProps({ locale }) {
  //const lang = locale.toUpperCase();
const lang = "UA";
  const pageData = await getHomeFeelingsYouPageData(lang, `Меню ${lang}`);

  return {
    props: {
      pageData,
    },
  };
}
