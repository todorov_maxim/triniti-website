import Image from "next/image";
import { compose } from "redux";
import withHardcoreTranslate from "../../hoc/withHardcoreTranslate";
import {
  FlatsButton,
  FlatsItem,
  FlatsItemContent,
  FlatsItemContentBox,
  FlatsItemImage,
  FlatsItemTitle,
  FlatsList,
  FlatsTitle,
  FlatsWrapper,
} from "./StyledFlats";

const Flats = ({ info: { button, flatsTypes }, currLanguage, textColor }) => {
  return (
    <FlatsWrapper>
      <FlatsTitle color={textColor}>{currLanguage?.flats}</FlatsTitle>
      <FlatsList>
        {flatsTypes.map((flat, key) => {
          return (
            <FlatsItem key={key}>
              <FlatsItemContent>
                <FlatsItemContentBox>
                  <FlatsItemImage>
                    <Image
                      layout="fill"
                      src={flat?.image?.sourceUrl}
                      objectFit="contain"
                    />
                  </FlatsItemImage>
                  <FlatsItemTitle>{flat.name}</FlatsItemTitle>
                </FlatsItemContentBox>
              </FlatsItemContent>
            </FlatsItem>
          );
        })}
      </FlatsList>
      <FlatsButton href={button?.link} target={"_blank"}>{button?.text}</FlatsButton>
    </FlatsWrapper>
  );
};

export default compose(withHardcoreTranslate)(Flats);
