import Image from "next/image";
import styled from "styled-components";
import { screenSize } from "../../constants/media";



export const FlatsWrapper = styled.div(() => ({
    maxWidth: 1742,
    width: "100%",
    padding: "50px 77px 100px",
    margin: "0 auto",
    overflow: "hidden",


    [`@media (max-width: ${screenSize.laptop})`]: {
        padding: "40px 34px 90px",
    },

    [`@media (max-width: ${screenSize.mobileL})`]: {
        padding: "50px 30px 60px",
    },

    [`@media (max-width: ${screenSize.mobileXS})`]: {
        padding: "50px 16px 60px",
    },

}));

export const FlatsTitle = styled.div(({color}) => ({
    fontSize: 60,
    lineHeight: "1.4em",
    color: color ? color : "#F0B8A9",
    opacity: .5,
    textTransform: "uppercase",
    marginBottom: 70,

    [`@media (max-width: ${screenSize.laptopL})`]: {
        fontSize: 50,
        lineHeight: "55px",

    },
    [`@media (max-height: 770px)`]: {
        fontSize: 46,
        lineHeight: "52px",
        marginBottom: "45px",
    },

    [`@media (max-width: ${screenSize.laptop})`]: {
        fontSize: 50,
    },

    [`@media (max-width: ${screenSize.mobileL})`]: {
        fontSize: 32,
        marginBottom: 25,
    },
}));

export const FlatsList = styled.div(() => ({
    display: "flex",
    flexWrap: "wrap",
    margin: "0 -27px 30px",
    padding: "0 50px",

    [`@media (max-width: ${screenSize.laptopL})`]: {
        margin: "0 -8px 20px",
        padding: "0 0px",
    },

    [`@media (max-width: ${screenSize.laptop})`]: {
        padding: "0 90px",
        margin: "0 -14px 20px",
    },

    [`@media (max-width: 992px)`]: {
        padding: "0 50px",
    },

    [`@media (max-width: ${screenSize.tablet})`]: {
        padding: "0",
    },

    [`@media (max-width: ${screenSize.mobileXS})`]: {
        display: "block",
    },

  
}));

export const FlatsItem = styled.div(() => ({
    padding: "0 27px",
    flex: "0 0 25%",
    maxWidth: "25%",
    marginBottom: 22,

    [`@media (max-width: ${screenSize.laptopL})`]: {
        padding: "0 8px",
    },
    [`@media (max-width: ${screenSize.laptop})`]: {
        flex: "0 0 50%",
        maxWidth: "50%",
        padding: "0 14px",
    },

    [`@media (max-width: ${screenSize.mobileL})`]: {
    },

    [`@media (max-width: ${screenSize.mobileXS})`]: {
        flex: "0 0 100%",
        maxWidth: "400px",
        margin: "0 auto 25px"
    },
}));

export const FlatsItemContent = styled.div(() => ({
    padding: "54px 15px 45px",
    border: "1px solid #F0B8A9",
    height: "100%",

}));

export const FlatsItemContentBox = styled.div(() => ({
    maxWidth: 240,
    margin: "0 auto",
    height: "100%",
    display: "flex",
    flexDirection: "column",
}));

export const FlatsItemImage = styled.div(() => ({
    position: "relative",
    width: "100%",
    maxWidth: 240,
    height: 190,
    marginBottom: 65,
    flex: "1 1 auto",
}));

export const FlatsItemTitle = styled.div(() => ({
    fontSize: 24,
    lineHeight: "1.4em",
    letterSpacing: "0.15em",
    textTransform: "uppercase",
    color: "#494E54",
    flex: "0 0 auto",
}));

export const FlatsButton = styled.a(() => ({
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
    cursor: "pointer",
    padding: "20px 15px",
    textAlign: "center",
    minHeight: 60,
    fontSize: 12,
    lineHeight: "1.4em",
    letterSpacing: "0.5em",
    textTransform: "uppercase",
    color: "#fff",
    background: "#494E54",
    borderRadius: 10,
    maxWidth: 300,
    margin: "0 auto",

    [`@media (min-width: 992px)`]: {
        transition: ".3s background-color",

        '&:hover': {
            background: "#9C8D88",
        }
    }
}));
