import styled from "styled-components";
import { screenSize } from "../../constants/media";


const heritagePageStyle={
  span:{
    width:'100% !important',
    height:'100% !important',
  },
  maxWidth: 1169,
  width:'100%',
  height: 641,

  [`@media (max-width: ${screenSize.laptopL})`]: {
    maxWidth: 798,
    height: 480,
  },
  // [`@media (max-width: ${screenSize.laptop})`]: {
  //   maxWidth: 516,
  // },

  [`@media (max-width: ${screenSize.laptopPortrait})`]: {
    maxWidth: 650,
  },

  [`@media (max-width: ${screenSize.mobileLLandscape})  and (orientation: landscape)`]: {
    maxWidth: 360,
    height: 271,
    marginTop:20,
  },

  [`@media (max-width: ${screenSize.mobileL})  and (orientation: portrait)`]: {
    maxWidth: 360,
    height: 271,
  },

}

const modal={
  span:{
    width:'100% !important',
    height:'100% !important',
  },
  maxWidth: 1760,
  width:'100%',
  maxHeight:923,
  height: '100%',

  [`@media (max-width: ${screenSize.laptopPortrait}) `]:{
    maxWidth:768,
  maxHeight:486,
    marginBottom:110,
  },
  [`@media (max-width: ${screenSize.mobileLLandscape}) and (orientation: landscape)`]:
    {
      maxWidth:583,
      maxHeight:320,
        marginBottom:'unset',
    },
   

    [`@media (max-width: ${screenSize.mobileL}) and (orientation: portrait)`]:
    {
      maxWidth:360,
      maxHeight:237,
      marginBottom:110,

    },

    [`@media (min-width: 450px ) and (max-width: 755px) and (orientation: portrait)`]:
    {
      maxWidth:'100%',
  maxHeight:486,
      marginBottom:110,
    },

}

const realEstate = { 
  span: {
    width: "100%!important",
    height: "100%!important"
  },
  maxWidth: "1760px",
  margin: "0 auto",
  height: "960px",



  [`@media (max-width: ${screenSize.laptopL})`]: {
    height: "650px",
  },
  [`@media (max-width: ${screenSize.laptop})`]: {
    height: "620px",
  },

  [`@media (max-width: 992px)`]: {
    height: "500px",
  },

  [`@media (max-width: ${screenSize.laptopPortrait})`]: {
    height: "450px",
  },
  [`@media (max-width: ${screenSize.tabletSmall})`]: {
    height: "370px",
  },

  [`@media (max-width: ${screenSize.mobileXS})`]: {
    height: "300px",
    marginBottom: "40px",
  },

  [`@media (max-width:360px)`]: {
    height: "270px",
  },
  [`@media (max-width:340px)`]: {
    height: "230px",
  },
}

const getStylePage=(namePage)=>{
  switch (namePage) {
    case "heritage":
      return heritagePageStyle
    case "modal":
      return modal
    case "realEstate":
      return realEstate
    default:
      return {}
  }
}


export const StyledGalleryWrapper = styled.div(() => ({
  display: "flex",
  justifyContent: "center",
  padding: "90px 0"
}))

export const StyledMiniSlider = styled.div(({namePage})=>({
...getStylePage(namePage),
position:'relative',
userSelect:'none',
}))

export const WrapperArrows = styled.div(({isRight,modal,realEstate})=>({
  display: 'flex',
  position: 'absolute',
  right:modal?0:isRight?'-77px':'unset',
  left:!isRight&&'-77px',
  bottom:!modal?0:-77,

[`@media (max-width: ${screenSize.mobileLLandscape}) and (orientation:landscape )`]:{
  left:modal?'unset':isRight?0:realEstate?"auto":'unset',
  right:modal?0:realEstate?"-55px":!isRight&&0,
  bottom:realEstate?0:!modal?-26:-38.5,
},
[`@media (max-width: ${screenSize.mobileLLandscape})`]:{
  right:realEstate ? "-55px"  : (!modal && !realEstate && isRight) ? 0 : (!modal && !realEstate) && "auto",
  left: (!modal && !realEstate && isRight) ? "auto" : (!modal && !realEstate) ? 0 : "auto",

},
[`@media (max-width: ${screenSize.mobileL}) and (orientation: portrait)`]:{
      right:!realEstate?0:"-52px",
      bottom:(!modal&&!realEstate)&&-26,
    },
    [`@media (max-width: ${screenSize.tabletSmall})`]:{
      transform: realEstate&&"translateY(50%)"
    },
    [`@media (max-width: ${screenSize.mobileXS})`]: {
      right:realEstate&&"0",
    },

    
}))


export const Arrow = styled.div(({backgroundColor,modal})=>({
backgroundColor:!backgroundColor?'#494E54':backgroundColor,
width: 77,
height: 77,
color: 'white',
display: 'flex',
justifyContent:'center',
alignItems:'center',
transform:!modal&&'rotate(90deg)',
cursor: 'pointer',
[`@media (max-width: ${screenSize.mobileLLandscape}) and (orientation:landscape )`]:{
  width:modal?77: 55,
  height:modal?77: 55,
  fontSize:modal?14:10,
},
[`@media (max-width: ${screenSize.mobileL}) and (orientation: portrait)`]:
    {
      width:modal?77: 52,
      height:modal?77: 52,
      fontSize:modal?14:10,
    },
 
}))


export const FullScreenWrapperDef = styled.div(({isRight})=>({
  right:isRight&& 25,
  left:!isRight&& 25,
  }))

export const FullScreenWrapper = styled(FullScreenWrapperDef)`
position: absolute;
right: 25px;
top:25px;
width: 55px;
height:55px;
display: flex;
justify-content: center;
align-items: center;
background: rgba(255, 255, 255, 0.75);
z-index:4;
cursor: pointer;

`

export const FullScreenIcon = styled.div`
font-size:27px;
color:#494E54;
`
