import Image from "next/image";
import {
  StyledMiniSlider,
  WrapperArrows,
  Arrow,
  FullScreenWrapper,
  FullScreenIcon,
} from "./StyledMiniSlider";

export const MiniSlider = ({
  namePage,
  currentPhoto,
  nextPhoto,
  prevPhoto,
  backgroundColorArrow2,
  isRight = true,
  modal,
  realEstate,
  heritage,
  showModal,
}) => {
  return (
    <StyledMiniSlider namePage={namePage}>
      {heritage && (
        <FullScreenWrapper isRight={isRight} onClick={showModal}>
          <FullScreenIcon className="icon-fullScreen"></FullScreenIcon>
        </FullScreenWrapper>
      )}
      <Image
        src={currentPhoto.sourceUrl}
        layout="responsive"
        objectFit="cover"
        width="100%"
        height="100%"
        priority={true}
      />

      <WrapperArrows modal={modal} realEstate={realEstate} isRight={isRight}>
        <Arrow modal={modal} className="icon-left" onClick={nextPhoto} />
        <Arrow
          modal={modal}
          priority={true}
          className="icon-right"
          backgroundColor={backgroundColorArrow2}
          onClick={prevPhoto}
        />
      </WrapperArrows>
    </StyledMiniSlider>
  );
};
