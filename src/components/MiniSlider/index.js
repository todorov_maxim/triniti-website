import { useState } from "react";
import { MiniSlider } from "./MiniSlider";

export default ({ gallery, ...props }) => {
  
  const arrWithId = gallery.map((it, i) => ({ ...it, id: i + 1 }));

  const startRenderPhoto = arrWithId[0];

  const [currentPhoto, setCurrentphoto] = useState(startRenderPhoto);

  const [allPhotosArr, setAllPhotosArr] = useState(arrWithId);
  const nextPhoto = () => {
    const currId = currentPhoto?.id;
    const isLastPhoto = currId == allPhotosArr[allPhotosArr.length - 1].id;
    const newCurrPhoto = allPhotosArr.reduce(
      (acc, val) =>
        isLastPhoto
          ? val.id == 1
            ? (acc = val)
            : acc
          : val.id == currId + 1
          ? (acc = val)
          : acc,
      {}
    );
    setCurrentphoto(newCurrPhoto);
  };

  const prevPhoto = () => {
    const currId = currentPhoto?.id;
    const isFirstPhoto = currId == allPhotosArr[0].id;
    const idLastPhoto = allPhotosArr[allPhotosArr.length - 1].id;
    const newCurrPhoto = allPhotosArr.reduce(
      (acc, val) =>
        isFirstPhoto
          ? val.id == idLastPhoto
            ? (acc = val)
            : acc
          : val.id == currId - 1
          ? (acc = val)
          : acc,
      {}
    );
    setCurrentphoto(newCurrPhoto);
  };

  const miniSliderControls = { nextPhoto, prevPhoto };
  const { heritage, modal, realEstate } = props;
  const backgroundColorArrow2 = heritage
    ? "#9C8D88"
    : modal
    ? "#CD8B7D"
    : realEstate
    ? "#F0B8A9"
    : "black";
  const miniSliderProps = {
    currentPhoto,
    modal,
    heritage,
    realEstate,
    backgroundColorArrow2,
    ...miniSliderControls,
    ...props,
  };
  return <MiniSlider {...miniSliderProps}></MiniSlider>;
};
