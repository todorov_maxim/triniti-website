import styled from "styled-components";
import { screenSize } from "../../constants/media";
// import { screenSize } from "../../../constants/media";

export const StyledMenuIcon = styled.div(() => ({
  zIndex: 11,
  position: "fixed",
  top: 0,
  right: 0,
  width: 80,
  height: 80,
  backgroundColor: "#6d747c",
  // backgroundColor: "#494E54",
  display: "flex",
  justifyContent: "center",
  alignItems: "center",
  cursor: "pointer",
  [`@media (max-width: ${screenSize.mobileL}) and (orientation: portrait)`]: {
    width: 50,
    height: 50,
  },
  [`@media (max-width: ${screenSize.mobileLLandscape}) and (orientation: landscape)`]:
    {
      width: 50,
      height: 50,
    },
}));

export const MIcon = styled.i(() => ({
  zIndex: 11,
  fontSize: 33,
  color: "#FFFFFF",
  [`@media (max-width: ${screenSize.mobileL}) and (orientation: portrait)`]: {
    fontSize: 22,
  },
  [`@media (max-width: ${screenSize.mobileLLandscape}) and (orientation: landscape)`]:
    {
      fontSize: 22,
    },
}));

export const CloseIcon = styled.i(() => ({
  zIndex: 11,
  fontSize: 26,
  color: "#FFFFFF",
  [`@media (max-width: ${screenSize.mobileL}) and (orientation: portrait)`]: {
    fontSize: 15,
  },
  [`@media (max-width: ${screenSize.mobileLLandscape}) and (orientation: landscape)`]:
    {
      fontSize: 15,
    },
}));

export const ClosedMenu = styled.div`
  z-index: 5;
  width: 100%;
  height: 0px;
  right: 0px;
  top: 0px;
  position: fixed;
  opacity: 0;
  transition: all 0.7s ease 0.7s;
`;

export const StyledMenu = styled(ClosedMenu)(({ isOpenMenu }) => ({
  zIndex: isOpenMenu ? 10 : 0,
  opacity: isOpenMenu && 1,
  backgroundColor: isOpenMenu && "rgba(73, 78, 84, 0.75)",
  [`@media (min-width: 1920px)`]: {
    left: 'calc( 50% - ( 1920px / 2 ) )',
    maxWidth:1920,
  },
  width: isOpenMenu && "100%",
  height: isOpenMenu && "100%",
  position: "fixed",
  transition: isOpenMenu && "all 1s ease 0s",
  display: "flex",
  justifyContent: "flex-end",
}));

export const HiddeMenuContent = styled.div(() => ({
  width: "100%",
  maxWidth: 444,
  backgroundColor: "#494E54",
  display: "flex",
  flexDirection: "column",
  alignItems: "center",
  justifyContent: "space-around",
  opacity: 0,
  transition: "opacity 0.7s ease 0s",
  [`@media (max-width: ${screenSize.laptop})`]: {
    maxWidth: 360,
  },
  [`@media (max-width: ${screenSize.laptop}) and (max-height: 500px)`]: {
    position: "relative", 
    zIndex: 2
  },
  
  [`@media (max-width: ${screenSize.mobileL}) and (orientation: portrait)`]: {
    maxWidth: '100%',

  },

  

  [`@media (max-width: ${screenSize.mobileL}) and (max-height: 500px) and (orientation: portrait)`]: {
    zIndex: 2
  }

}));

export const MenuContent = styled(HiddeMenuContent)(({ isOpenMenu }) => ({
  opacity: isOpenMenu && 1,
  transition: isOpenMenu && "opacity 0.7s ease 1s",
  [`@media (max-width: ${screenSize.mobileLLandscape}) and (orientation: landscape)`]:
   {
display: 'none',
   },
   
}));

export const MenuList = styled.div(({ alignItemsTop}) => ({
  maxHeight: 354,
  marginRight: 5,
  [`@media (max-width: ${screenSize.laptopL})`]: {
    marginRight: 0,
  },
  [`@media (max-width: ${screenSize.laptopL})  and (max-height: 650px)`]: {
    maxHeight: '100%',
    height: "auto",
    marginRight: 5,

  },

  [`@media (max-width: ${screenSize.laptopL})  and (max-height: 550px)`]: {
    columns: 2,
    padding: "0 10px"

  },
  [`@media (max-width: ${screenSize.laptop})`]: {
    marginLeft: 3,
    display:alignItemsTop&& 'flex',
    justifyContent:alignItemsTop&&'flex-start',
    flexDirection:alignItemsTop&&'column',
    alignItems:alignItemsTop&&'start',height:'100%',
  },
}));

export const MenuPage = styled.div(({marginBottom}) => ({
  fontSize: 12,
  letterSpacing: "0.25em",
  lineHeight: "13.8px",
  marginBottom:marginBottom?marginBottom: "20px",
  color: "#FFFFFF",
  fontFamily: "AGLettericaC",
  cursor: "pointer",
  '&:hover':{
    color:'#CD8B7D',
  },

  [`@media (max-width: ${screenSize.laptop}) and (max-height: 500px)`]: {
    marginBottom:15,
    ":last-child": {
      marginBottom:5,
    }
  },

  [`@media (max-width: ${screenSize.laptop}) and (max-height: 400px)`]: {
    marginBottom:7,
    fontSize: 10,
  },

  [`@media (max-width: ${screenSize.mobileL}) and (max-height: 600px) and (orientation: portrait)`]: {
    marginBottom:15,
    ":last-child": {
      marginBottom:5,
    }
  },
  [`@media (max-width: ${screenSize.mobileLLandscape}) and (max-height: 300px)`] : {
    marginBottom:14,
    fontSize: 11,

    ":last-child": {
      marginBottom:4,
    }
  }
}));

export const WrapperLogo2 = styled.div(() => ({
  maxHeight: "29%",
  display: "flex",
  alignItems: "flex-end",
  height: "100%",
  paddingBottom: "20%",
  [`@media (max-width: ${screenSize.laptopL})`]: {
    maxHeight: "30%",
    display: "flex",
    alignItems: "flex-end",
    height: "100%",
    paddingBottom: "11%",
  },

  [`@media (max-width: ${screenSize.laptopL}) and (max-height: 650px)`]: {
    paddingBottom: 15,
    maxHeight: "100%",
    height: "auto",
  },
  [`@media (max-width: ${screenSize.laptop})`]: {
    paddingBottom: "12.9%",
  },
  [`@media (max-width: ${screenSize.laptop}) and (max-height: 650px)`]: {
    paddingBottom: 14,
    maxHeight: 100,
  },
  [`@media (max-width: ${screenSize.laptop}) and (max-height: 500px)`]: {
    position: "absolute",
    left: "50%",
    top: "50%",
    transform: "translate(-50%, -50%)",
    zIndex: -1,
    opacity: .3

  },
  [`@media (max-width: ${screenSize.laptopPortrait}) and (orientation: portrait)`]: {
    paddingBottom: 0,
    justifyContent:'center',
alignItems:'center',
maxHeight: "28%",
    },
    [`@media (max-width: ${screenSize.mobileL}) and (orientation: portrait)`]: {
        paddingBottom: 0,
        paddingTop:'4.59%',
        flex: "0 0 150px",
        alignItens: "center",
        height:"auto",
      },
      [`@media (max-width: ${screenSize.mobileLLandscape}) and (orientation: landscape)`]:{
        maxHeight: "100%",
        paddingBottom: "0%",
        width:'100%',
maxWidth:'25%',
justifyContent:'center',
paddingBottom:'10.6%',
paddingLeft:1
},
[`@media (max-width: ${screenSize.mobileL}) and (max-height: 650px) and (orientation: portrait)`]: {
  paddingTop: 5,
  maxHeight: "100%",
  height: "auto",
  flex: "0 0 auto",

},
[`@media (max-width: ${screenSize.mobileL}) and (max-height: 500px) and (orientation: portrait)`]:{
  maxHeight: 0,
},

}));

export const WrapperMenuList = styled.div(() => ({
  display: "flex",
  maxHeight: "56%",
  height: "100%",
  paddingTop: "2%",
  [`@media (max-width: ${screenSize.laptopL}) and (max-height: 650px)`]: {

    paddingTop: "10px",
  },

  [`@media (max-width: ${screenSize.laptopL})`]: {

  },
  [`@media (max-width: ${screenSize.laptop})`]: {
    paddingTop: "3.9%",
  },
  [`@media (max-width: ${screenSize.laptop}) and (max-height: 650px)`]: {
    maxHeight: "100%",
    height: "auto",
    paddingTop: 5,
  },

  [`@media (max-width: ${screenSize.laptopPortrait}) and (orientation: portrait)`]: {
    paddingTop: 0,
    maxHeight: "100%",
    },
    [`@media (max-width: ${screenSize.mobileL}) and (orientation: portrait)`]: {
        paddingTop: "4.7%",
        flex: "1 1 auto",
      },
      [`@media (max-width: ${screenSize.mobileL}) and (max-height: 650px) and (orientation: portrait)`]: {
        maxHeight: "100%",
        height: "auto",
        paddingTop: 5,
      },
      [`@media (max-width: ${screenSize.mobileLLandscape}) and (orientation: landscape)`]:{
        maxHeight: "100%",
        paddingTop: "12.6%",
        width:'100%',
justifyContent:'space-between',
alignItems:'flex-end',
maxWidth:'75%',
paddingRight:'9.5%'

},
[`@media (max-width: ${screenSize.mobileLLandscape}) and (max-height: 300px)`] : {
  paddingTop: "12px",
}
}));

export const WrapperSocialLinks = styled.div(() => ({
  maxHeight: "15%",
  height: "100%",
  display: "flex",
  justifyContent: "center",
  width: "100%",
  paddingTop: 20,

  [`@media (max-width: ${screenSize.laptopL})`]: {
    maxHeight: "15%",
    height: "100%",
    display: "flex",
    justifyContent: "center",
    width: "100%",
    paddingTop: 35,
  },

  [`@media (max-width: ${screenSize.laptopL}) and (max-height: 550px)`]: {
    paddingTop: 10,
  }
  ,
  [`@media (max-width: ${screenSize.laptop}) and (max-height: 650px)`]: {
    maxHeight: "100%",
    paddingBottom: 5,
      paddingTop: 10,
      height: "auto",
  },
  [`@media (max-width: ${screenSize.laptop}) and (max-height: 400px)`]: {
    paddingBottom: 2,
      paddingTop: 2,
  },
 
  [`@media (max-width: ${screenSize.laptopPortrait}) and (orientation: portrait)`]: {
    paddingTop: 0,
alignItems:'center'
    },
    [`@media (max-width: ${screenSize.mobileL}) and (max-height: 650px) and (orientation: portrait)`]: {
      paddingBottom: 5,
      paddingTop: 10,
      height: "auto",
    },
    
    [`@media (max-width: ${screenSize.mobileL}) and (orientation: portrait)`]: {
paddingBottom:'5%',
maxHeight: "100%",
flex: "0 0 100px"
      },
      [`@media (max-width: ${screenSize.mobileLLandscape}) and (orientation: landscape)`]:{
        paddingTop: "0%",
        alignItems: 'start',
        justifyContent: 'flex-end',
        paddingRight:'20.3%',
        maxHeight: "100%",
},
[`@media (max-width: ${screenSize.mobileL}) and (max-height: 350px) and (orientation: landscape)`]: {
height: "auto",
paddingTop: 15,
paddingBottom: 10,
},


}));


export const MenuMobileLandscapeContent = styled.div(() => ({
   display: 'none',
   [`@media (max-width: ${screenSize.mobileLLandscape}) and (orientation: landscape)`]:
   {
       display: 'flex',
       flexDirection:'column',
width: '100%',
   },

  }));


  export const WrapperLogoAndMenuList = styled.div(() => ({
    //   display: 'none'
    [`@media (max-width: ${screenSize.mobileLLandscape}) and (orientation: landscape)`]:{
        display: "flex",
        justifyContent: "space-betweenx",
        maxHeight: "73%",
        height: "100%"
    },
    [`@media (max-width: ${screenSize.mobileL}) and (max-height: 350px) and (orientation: landscape)`]: {
      maxHeight: "100%",
      height: 'auto',
      flex: "1 1 auto"
    },
    [`@media (max-width: ${screenSize.mobileLLandscape}) and (max-height: 300px)`]:{
      maxHeight: "100%",
      height: 'auto'
    }
   }));


  

  