import { StyledMenuIcon, MIcon, CloseIcon } from "./StyledMenu";

export const MenuIcon = ({ onClick, isOpenMenu }) => {
  return (
    <StyledMenuIcon onClick={onClick}>
      {!isOpenMenu && <MIcon className="icon-menu" />}
      {isOpenMenu && <CloseIcon className="icon-close" />}
    </StyledMenuIcon>
  );
};
