import { Logo2 } from "../Logo2/Logo2";
import { SocialLinks } from "../SocialLinks/SocialLinks";
import { menuData } from "../../data/menuData";
import {
  StyledMenu,
  MenuContent,
  MenuList,
  MenuPage,
  WrapperLogo2,
  WrapperMenuList,
  WrapperSocialLinks,
  MenuMobileLandscapeContent,
  WrapperLogoAndMenuList,
} from "./StyledMenu";
import Link from "next/link";
import { useEffect, useState } from "react";

export const Menu = ({
  isOpenMenu,
  isOpenMenuWithEffect,
  globalContactsData,
  menu,
}) => {
  return (
    <>
      <StyledMenu isOpenMenu={isOpenMenu}>
        <MenuContent isOpenMenu={isOpenMenu}>
          <WrapperLogo2>
            <Logo2 mobSize="78px" color="#CD8B7D" />
          </WrapperLogo2>
          <WrapperMenuList>
            <MenuList>
              {menu.map((page) => {
                return (
                  <Link href={page?.path}>
                    <MenuPage>{page?.label?.toUpperCase()}</MenuPage>
                  </Link>
                );
              })}
            </MenuList>
          </WrapperMenuList>
          <WrapperSocialLinks>
            <SocialLinks globalContactsData={globalContactsData} />
          </WrapperSocialLinks>
        </MenuContent>
        {/* при моб версии албомной ориентации */}
        <MenuMobileLandscapeContent>
          <WrapperLogoAndMenuList>
            <WrapperLogo2>
              <Logo2 mobSize="78px" color="#CD8B7D" />
            </WrapperLogo2>
            <WrapperMenuList>
              <MenuList>
                {menu.map((page, i) => {
                  return (
                    <>
                      {" "}
                      {i <= 5 && (
                        <Link href={page?.path}>
                          <MenuPage marginBottom={i == 5 && "0px"}>
                            {page?.label?.toUpperCase()}
                          </MenuPage>
                        </Link>
                      )}
                    </>
                  );
                })}
              </MenuList>
              <MenuList alignItemsTop>
                {menu.map((page, i, arr) => {
                  return (
                    <>
                      {" "}
                      {i > 5 && i <= 10 && (
                        <Link href={page?.path}>
                          <MenuPage marginBottom={i == arr.length - 1 && "0px"}>
                            {page?.label?.toUpperCase()}
                          </MenuPage>
                        </Link>
                      )}
                    </>
                  );
                })}
              </MenuList>
            </WrapperMenuList>
          </WrapperLogoAndMenuList>

          <WrapperSocialLinks>
            <SocialLinks globalContactsData={globalContactsData} />
          </WrapperSocialLinks>
        </MenuMobileLandscapeContent>
        {/* при моб версии албомной ориентации */}
      </StyledMenu>
    </>
  );
};
