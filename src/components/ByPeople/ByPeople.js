import * as BS from "../Pages/Brand/StyledBrand"
import bgImage from "../../../public/images/Main/bg_2.jpg"
import {SwitchPageArrow} from "../SwitchPageArrow/SwitchPageArrow"
import {Contact} from "../Contact/Contact"
import {LogoWithText} from "../LogoWithText/LogoWithText"
import { Logo } from "./Logo"

export const ByPeople = ({fullpageApi}) => {

  return (
    <BS.StyledBrandWithBG bg={bgImage.src}>
      <Contact renderTelephonNumberBlock />
      <BS.WrapperContent>
        <BS.Content>
          <BS.TopBlock>
            <Logo />
            <BS.ByPeopleForPeople>
              <BS.ByPeopleForPeopleText>by people for people</BS.ByPeopleForPeopleText>
            </BS.ByPeopleForPeople>
          </BS.TopBlock>
          <BS.QuotesBlock>
            <BS.QuotesText>
              <BS.IconCommaStart textColor={"#ACA99F"} className="icon-comma" />
              Віра в те, що ціле більше ніж сума частин.
              <BS.IconCommaEnd textColor={"#ACA99F"} className="icon-comma" />
            </BS.QuotesText>
          </BS.QuotesBlock>
          <BS.TextBlock>
            <BS.TextBox1>
              <BS.TextNumber color={"#494E54"}>1</BS.TextNumber>
              <BS.PhylosophyText>Це і є та філософія, яку ми закладаємо в створення, супровід та розвиток житлових і бізнес просторів</BS.PhylosophyText>
              <BS.PhylosophySecondaryText>Це філософія цілісності.</BS.PhylosophySecondaryText>
            </BS.TextBox1>
            <BS.TextBox2>
              <BS.TextNumber color={"#494E54"} position={"right"}>
                2
              </BS.TextNumber>
              <BS.PhylosophyText>В нашій філософії спільноти, девелопмент та проекти неподільні. Ми називаємо це by people for people</BS.PhylosophyText>
            </BS.TextBox2>
            <BS.TextBox3>
              <BS.TextNumber color={"#CD8B7D"}>3</BS.TextNumber>
              <BS.PhylosophyText>Ми віримо, що кожен наш проект – це формування стилю життя майбутнього покоління, ми маємо забезпечити його актуальність протягом багатьох років.</BS.PhylosophyText>
            </BS.TextBox3>
          </BS.TextBlock>
          <BS.QuotesSecondaryTextBlock>
            <BS.QuotesSecondaryText>
              Ми створюємо магніти, що зрушують старе, наповнюючи новим життям.
              <BS.IconCommaBig textColor={"#ACA99F"} className="icon-comma" />
            </BS.QuotesSecondaryText>
          </BS.QuotesSecondaryTextBlock>
          {/* <BS.WrapperByPeopleText>
              <BS.ByPeopleText>{mainText}</BS.ByPeopleText>
            </BS.WrapperByPeopleText>
            <BS.WrapperStairs>
              <Logo />
              <BS.Stairs>
                {/* {dataStairs.map(step=>{...step,8})}
                {dataStairs
                  .map((step, i) => ({ ...step, text: steps[i] }))
                  .map(({ text, ...resStep }, i, arr) => {
                    return (
                      <>
                        <Step {...resStep} index={i} stepsLength={arr.length}>{text}</Step>
                      </>
                    );
                  })}
              </BS.Stairs>
            </BS.WrapperStairs>
            <BS.WrapperText />
            <BS.WrapperTextAbsolute>{secondaryText}</BS.WrapperTextAbsolute> */}
        </BS.Content>
      </BS.WrapperContent>
      <SwitchPageArrow fullpageApi={fullpageApi} brandPage />
    </BS.StyledBrandWithBG>
  )
}
