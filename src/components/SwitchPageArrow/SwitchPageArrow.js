import {
  StyledSwitchPageArrow,
  UpArrow,
  DownArrow,
  IconArrow,
  WrapperIconArrow,
} from "../Pages/Brand/StyledBrand";

export const SwitchPageArrow = ({
  pages,
  fullpageApi,
  brandPage,
  lastpage,
}) => {
  return (
    <StyledSwitchPageArrow pages={pages} brandPage={brandPage}>
      <WrapperIconArrow>
        <IconArrow className="icon-swipeUpDown" />
      </WrapperIconArrow>
      <UpArrow onClick={() => fullpage_api.moveSectionUp()} />
      <DownArrow
        onClick={!lastpage ? () => fullpageApi.moveSectionDown() : () => {}}
      />
    </StyledSwitchPageArrow>
  );
};
