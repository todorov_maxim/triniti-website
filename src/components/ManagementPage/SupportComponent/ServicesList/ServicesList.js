import { ListItems } from "../ListItems/ListItems";
import { StyledServicesList, Title } from "./StyledListItems";

export const ServicesList = ({
  textColor,
  listArr,
  title,
  variant,
  cols,
  page,
}) => {

  return (
    <StyledServicesList variant={variant} >
      <Title variant={variant}>{title}</Title>
      <ListItems
        textColor={textColor}
        listArr={listArr}
        variant={variant}
        cols={cols}
        page={page}
      />
    </StyledServicesList>
  );
};
