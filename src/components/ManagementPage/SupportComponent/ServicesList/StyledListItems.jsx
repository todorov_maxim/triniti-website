import styled from "styled-components";
import { screenSize } from "../../../../constants/media";


export const StyledServicesList = styled.div(({variant = "sm"}) => ({

    display: "flex",
    maxWidth: variant === "sm" ? "1230px" : "1680px",
width: "100%",
margin: "0 auto",
flexDirection: "column",
marginTop: "72px",
minHeight: "188px",
justifyContent: "space-between",
marginBottom: "144px",
[`@media (max-width: ${screenSize.laptopL})`]: {
maxWidth: "unset",
marginBottom: "85px",
},
[`@media (max-width: ${screenSize.laptop})`]: {
paddingLeft: variant !== "lg" && "44px",
marginTop: "80px",

},

[`@media (max-width: 1065px) `]: {
    paddingLeft: variant == "sm" && "0px",
      },

[`@media (max-width: ${screenSize.laptopPortrait})`]: {
paddingLeft:"unset",
marginTop: "38px",
marginBottom: "55px",

},
[`@media (max-width: ${screenSize.mobileLLandscape}) and (orientation: landscape) `]:{
    minHeight: "157px",
marginTop: "80px",
paddingLeft: variant !== "lg" && "14px",


},
[`@media (max-width: ${screenSize.mobileL}) and (orientation: portrait)`]: {
marginTop: "64px",
    minHeight: "287px",
    alignItems: "center",
paddingLeft:"unset",
marginBottom: "81px",
}
}))



const TitleModifyed = styled.div(({variant = "sm"}) => ({

    paddingLeft: variant === "lg" && "45px",
    marginBottom: variant === "lg" && "40px",


    [`@media (max-width: ${screenSize.laptop})`]: {
        paddingLeft: variant === "lg" && "30px",
    },

    [`@media (max-width: 992px)`]: {
        paddingLeft: variant === "lg" && "20px",
    },
    
}))
export const Title = styled(TitleModifyed)`
font-size: 32px;
line-height: 1.4em;
letter-spacing: 0.15em;
text-transform: uppercase;
color: #494E54;

@media (max-width: ${screenSize.mobileLLandscape}){
    font-size: 24px;
line-height: 32px;
}

@media (max-width: ${screenSize.mobileL}) and (orientation: portrait) {
  width:100%;
  display: flex;
  justify-content: flex-start;
  padding: 0px 27px;
}
`

