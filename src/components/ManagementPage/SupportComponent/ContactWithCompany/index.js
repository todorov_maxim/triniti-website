import { compose } from "redux";
import withHardcoreTranslate from "../../../../hoc/withHardcoreTranslate";
import { ContactWithCompany } from "./ContactWithCompany";

const ContainerContactWithCompany = (props) => {
  const { namePage } = props;
  const isOtherHover = namePage == "P_Management";
  const hoverColor = isOtherHover ? "#CD8B7D" : "#494E54";
  const propsMobileLandspace = { hoverColor, ...props };
  const propsComponent = { hoverColor, propsMobileLandspace, ...props };
  return <ContactWithCompany {...propsComponent} />;
};

export default compose(withHardcoreTranslate)(ContainerContactWithCompany);
