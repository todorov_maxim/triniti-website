import styled from "styled-components";
import { screenSize } from "../../../../constants/media";
import { Logo as LogoOtherPage } from "../ServicesDescription/ImgAndDescription/StyledImgAndDescription";

import {
    SyledWrapperSlider,
    SyledSlider,
    WrapperSelectedPeopleAndSliderPhoto,
    SelectedPeople,
    SliderTitle,
    SliderName,
    SliderDescription,
    SliderIconComma,
    WrapperImg2,
    WrapperCurrentPhotoAndNextPeopleDescription,
  } from "../../../Slider/SyledSlider";


  export const StyledProfileWrapper=styled.div`
    padding: 85px 34px;
    max-width: 1500px;
margin: 0 auto;
margin-bottom: 25px;

@media (max-width: ${screenSize.mobileL}) {
    padding: 55px 34px;
}

  `

export const StyledContactWithCompany=styled.div`
max-width: 1444px;
margin: 0 auto;
margin-bottom: 163px;
padding: 40px 0 10px;
@media (max-width: ${screenSize.laptopL}) {
    /* margin: 0 77px; */
margin-bottom: 115px;

}
@media (max-width: ${screenSize.laptop}) {
padding: 40px 34px 10px;
margin: 0 auto;
margin-bottom: 70px;

}
@media (max-width: ${screenSize.laptopPortrait}) {
padding-right: unset;
padding-left:45px;
margin-bottom: 58px;
};
@media (max-width: ${screenSize.mobileLLandscape}) and (orientation: landscape) {
    margin-bottom: 67px;
};
@media (max-width: ${screenSize.mobileL})  and (orientation: portrait) {
padding: 20px 28px 20px;
};
`

export const ContentPeopleDef=styled(SyledSlider)`
min-height: 475px;
@media (max-width: ${screenSize.laptopL}) {
    min-height: 400px;
}
@media (max-width: ${screenSize.laptop}) {
max-width: 100%;
justify-content: space-between;
}
@media (max-width: ${screenSize.mobileLLandscape}) and (orientation: landscape) {
align-items: flex-start;
}
@media (max-width: ${screenSize.mobileL})  and (orientation: portrait) {
flex-direction: column;
align-items: center;
}

`

export const ContentPeople=styled(ContentPeopleDef)(({mob=false})=>({
    display: mob?'none':'flex',

[`@media (max-width: ${screenSize.mobileLLandscape}) and (orientation: landscape)`]:{
    display: !mob?'none':'flex',
},
}))


export const Title=  styled(SliderTitle)`
min-height: unset;
margin-bottom: 120px;
padding:unset;

@media (max-width: ${screenSize.laptopL}) {
margin-bottom: 90px;
}
@media (max-width: ${screenSize.laptopPortrait}) {
line-height: 50px;
width: 100%;
max-width: 100%;
padding-right: 29px;
text-align: unset;
}
@media (max-width: ${screenSize.mobileLLandscape}) and (orientation: landscape) {
    margin-bottom: 57px;
}
@media (max-width: ${screenSize.mobileL})  and (orientation: portrait) {
    margin-bottom: 51px;
    padding:unset;
}
`


export const WrapperDescription=styled(WrapperSelectedPeopleAndSliderPhoto)`
justify-content: flex-start;
padding-bottom: 45px;
padding:unset;
padding-right:15px;
@media (max-width: ${screenSize.laptopL}) {
    padding-right:85px;
}
@media (max-width: ${screenSize.laptop}) {
    padding-right:118px;
    max-width: 425px;
}
@media (max-width: ${screenSize.laptopPortrait}) {
    padding-right:unset;
}
@media (max-width: ${screenSize.mobileLLandscape}) and (orientation: landscape) {
    padding-right:11px;
}
@media (max-width: ${screenSize.mobileL})  and (orientation: portrait) {
    max-width: 100%;
}

`

export const IconComma=styled(SliderIconComma)`
top:-72px;
left:-20px;
@media (max-width: ${screenSize.laptopL}) {
    top:-28px;
}
@media (max-width: ${screenSize.mobileLLandscape}) and (orientation: landscape) {
    top:-28px;
left:-34px;
}
@media (max-width: ${screenSize.mobileL})  and (orientation: portrait) {
    top:-28px;
left:-34px;
}
`

export const Name=styled(SliderName)`
margin-bottom: 12px;
@media (max-width: ${screenSize.mobileLLandscape}) and (orientation: landscape) {
    padding-right:unset;
margin-bottom: 5px;
}
@media (max-width: ${screenSize.mobileL})  and (orientation: portrait) {
max-width:100%;
}
`

export const Description=styled(SliderDescription)`
line-height: 32px;
@media (max-width: ${screenSize.laptop}) {
   font-size: 18px;
   line-height: 26px;
}
@media (max-width: ${screenSize.mobileL})  and (orientation: portrait) {
    font-size: 16px;
   line-height: 20px;
   max-width: 100%;
}

`

export const PeopleData=styled(SelectedPeople)`
padding:unset;
@media (max-width: ${screenSize.laptopPortrait}) {
padding-right: 48px;
}
@media (max-width: ${screenSize.mobileLLandscape}) and (orientation: landscape) {
    padding-right:unset;
margin-top: unset;
}
@media (max-width: ${screenSize.mobileL})  and (orientation: portrait) {
padding:unset;
max-width: 100%;
}

`

export const WrapperCurrentPhoto=styled(WrapperCurrentPhotoAndNextPeopleDescription)`
`

export const WrapperImg=styled(WrapperImg2)`
height: unset;
max-width: 660px;
min-height: 475px;

@media (max-width: ${screenSize.laptopL}) {
    max-width: 400px;
    min-height: 400px;
}
@media (max-width: ${screenSize.laptop}) {
margin-top: 89px;
}

@media (max-width: ${screenSize.mobileLLandscape}) and (orientation: landscape) {
min-height: 411px;
margin-top: 66px;
}
@media (max-width: ${screenSize.mobileL})  and (orientation: portrait) {
    margin-top: 29px;
min-height: 308px;
}
`

export const ContentContactDef=styled.div`
max-width: 1236px;
display: flex;
margin-top: 69px;
justify-content: space-between;
@media (max-width: ${screenSize.laptopL}) {
    margin-top: 30px;
}
@media (max-width: ${screenSize.laptop}) {
    margin-top: 50px;
}
@media (max-width: 1000px) {
flex-direction: column;
}
@media (max-width: ${screenSize.laptopPortrait}) {
flex-direction: column;
}
@media (max-width: ${screenSize.mobileLLandscape}) and (orientation: landscape) {
margin-top:31px;
}
@media (max-width: ${screenSize.mobileL})  and (orientation: portrait) {
    margin-top: 37px;
align-items: center;
}

`

export const ContentContact=styled(ContentContactDef)(({mob=false})=>({
    display: mob?'none':'flex',

    [`@media (max-width: ${screenSize.mobileLLandscape}) and (orientation: landscape)`]:{
        display: !mob?'none':'flex',
    },
    }))


export const Part1=styled.div`
display: flex;
flex-direction: column;

max-width:645px;
width:100%;
@media (max-width: ${screenSize.laptopL}) {
    max-width:645px;
padding-right: 85px;
}
@media (max-width: ${screenSize.laptop}) {
    max-width:425px;
padding-right: 118px;
/* flex-direction: column; */
}
@media (max-width: 1000px) {
    max-width:100%;
padding-right: unset;
}
@media (max-width: ${screenSize.laptopPortrait}) {
    max-width:100%;
padding-right: unset;
}
@media (max-width: ${screenSize.mobileLLandscape}) and (orientation: landscape) {
margin-top: 41px;
}

@media (max-width: ${screenSize.mobileL})  and (orientation: portrait) {
    max-width:592px;
}


`
export const Part2=styled.div`
width: 100%;
max-width:592px;
display: flex;
// height:233px;

@media (max-width: 1000px) {
    flex-direction: row-reverse;
max-width:100%;
}
@media (max-width: ${screenSize.laptopPortrait}) {
flex-direction: row-reverse;
max-width:100%;
}
@media (max-width: ${screenSize.mobileLLandscape}) and (orientation: landscape) {
    flex-direction: row;
}
@media (max-width: ${screenSize.mobileL})  and (orientation: portrait) {
margin-top: 32px;
height:max-content;
flex-direction: column;
align-items: center;
}
`

export const ContainerContent = styled.div(({variant = "lg"}) => ({
    padding: variant === "lg" && "0px 165px",
    padding: variant === "xl" && "0px 77px",

    [`@media (max-width: ${screenSize.laptopL})`] :{
 padding: variant === "lg" && "0px 78px",
 },
 [`@media (max-width: ${screenSize.laptop})`]: {
 padding: variant === "lg" && "0px 34px",
 padding: variant === "xl" && "0px 77px 0 0",
 },
 [`@media (max-width: ${screenSize.laptopPortrait})`] : {
 padding: variant === "lg" && "0px 38px",
 
 },
 [`@media (max-width: ${screenSize.mobileLLandscape})`]  :{
    padding: variant === "xl" && "0px 55px 0 0",
 },
 [`@media (max-width: ${screenSize.mobileLLandscape}) and (orientation: landscape)`]  :{
     padding: variant === "lg" && "0px",
     
 } ,
 [`@media (max-width: ${screenSize.mobileL}) and (orientation: portrait)`] :{
     padding: variant === "lg" && "0px",
     padding: variant === "xl" && "0px 52px 0 0",
 },
 [`@media (max-width: ${screenSize.mobileXS})`]: {
    padding: variant === "xl" && "0px",
 },

 
}))

export const ContactText=styled.div`
font-size: 18px;
line-height: 24px;
`
export const ContactBlock=styled.div`
display: flex;
flex-direction: column;
`

export const Block=styled.div`
display: flex;
flex-direction: column;
&:first-child{
margin-right:72px;
@media (max-width: ${screenSize.laptop}) {
    padding-right:20px;
    margin-right:unset;
    max-width: 425px;
    width:100%;
}
@media (max-width: ${screenSize.mobileLLandscape}) and (orientation: landscape) {
    padding-right:unset;
    max-width: 100%;
}
@media (max-width: ${screenSize.mobileL})  and (orientation: portrait) {
    padding-right:unset;
}
};
&:nth-last-child(1){
    @media (max-width: ${screenSize.laptop}) {
        max-width: 592px;
    width:100%; 
}

   
}
`

export const TitleText=styled(ContactText)`
    color: #494E54;
`
export const Text=styled(ContactText)(({color='#9C8D88'})=>({
    color
}))

export const Number=styled(ContactBlock)`
margin-bottom: 30px;
@media (max-width: ${screenSize.laptop}) {
    margin-bottom: 16px;
}
@media (max-width: ${screenSize.mobileL})  and (orientation: portrait) {
    margin-bottom: 20px;
}

`

export const Address=styled(ContactBlock)`
@media (max-width: ${screenSize.laptop}) {
    margin-bottom: 16px;
}
@media (max-width: ${screenSize.mobileL})  and (orientation: portrait) {
    margin-bottom: 20px;
}

`

export const Schedule=styled(ContactBlock)`
margin-bottom: 30px;
@media (max-width: ${screenSize.laptop}) {
    margin-bottom: 16px;
}
@media (max-width: ${screenSize.mobileL})  and (orientation: portrait) {
    margin-bottom: 20px;
}
`

export const Email=styled(ContactBlock)`
@media (max-width: ${screenSize.mobileLLandscape}) and (orientation: landscape) {
display:flex;
flex-direction: row;
justify-content: space-between;
}
@media (max-width: ${screenSize.mobileL})  and (orientation: portrait) {
    margin-bottom: 26px;
}
@media (max-width: 575px) and (orientation: landscape) {
flex-direction: column;
justify-content: unset;
}

`

export const Form=styled.div`
display: none;
/* width:304px; */
height: 233px;
background: gray;
flex:0 1 304px;
@media (max-width: ${screenSize.laptopPortrait}) {
max-width: 592px;
width: 100%;
flex:unset;
}
@media (max-width: ${screenSize.mobileLLandscape}) and (orientation: landscape) {
max-width: 304px;
}
@media (max-width: ${screenSize.mobileL})  and (orientation: portrait) {
    height: 233px;
}
`


export const WrapperLogo=styled.div`
flex:1 1 auto;
display:flex;
justify-content: center;
/* align-items: center; */
height: 100%;
margin-top: 23px;

@media (max-width: ${screenSize.laptop}) {
    /* align-items: flex-start; */

}
@media (max-width: ${screenSize.laptopPortrait}) {
max-width: 425px;
width: 100%;
flex:unset;
margin-top: 52px;
}
@media (max-width: ${screenSize.mobileLLandscape}) and (orientation: landscape) {
    margin-top: 14px;
}
@media (max-width: ${screenSize.mobileL})  and (orientation: portrait) {
height: 180px;
margin-top: unset;
justify-content: center;
align-items: center;
}


`

export const Logo=styled(LogoOtherPage)`
margin:unset;
@media (max-width: ${screenSize.mobileLLandscape}) and (orientation: landscape) {
font-size: 136px;
}
@media (max-width: ${screenSize.mobileL})  and (orientation: portrait) {
    font-size: 120px
    }

`


export const SocialLinks=styled.div`
width: 85px;
display: flex;
margin-top: 38px;
display:flex;
justify-content:space-between;
@media (max-width: ${screenSize.laptopPortrait}) {
    margin-top: 12px;
}
@media (max-width: ${screenSize.mobileL})  and (orientation: portrait) {
    margin-top: unset;
}
`


export const WrapperBlocks=styled.div`
display: flex;
@media (max-width: ${screenSize.laptop}) {
flex-direction: column;
}
@media (max-width: 1000px) {
    flex-direction: row;
}

@media (max-width: ${screenSize.laptopPortrait}) {
flex-direction: row;
}
@media (max-width: ${screenSize.mobileLLandscape}) and (orientation: landscape) {
flex-direction: column;
}
@media (max-width: ${screenSize.mobileL})  and (orientation: portrait) {
    flex-direction: column;
}
`

export const EmailText=styled.a(({color='#9C8D88'})=>({
    fontSize: 18,
lineHeight: '24px',
    color,
}))

export const TextContact=styled.a(({color='#9C8D88'})=>({
    fontSize: 18,
lineHeight: '24px',
    color,
}))

