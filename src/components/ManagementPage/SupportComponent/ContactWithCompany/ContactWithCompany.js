import Image from "next/image"
import {
  StyledContactWithCompany,
  ContentPeople,
  Title,
  WrapperDescription,
  IconComma,
  Name,
  Description,
  PeopleData,
  WrapperImg,
  Block,
  Number,
  TitleText,
  Text,
  Address,
  Schedule,
  Email,
  ContentContact,
  WrapperLogo,
  Part1,
  Part2,
  SocialLinks,
  WrapperBlocks,
  EmailText,
  TextContact,
} from "./StyledContactWithCompany"

import {SocialCircle, TelephoneIcon, FacebookIcon, InstagramIcon} from "../../../SocialLinks/StyledSocialLinks"
import useIsEmpty from "../../../../hooks/useIsEmpty"
import Form from "../../../Contact/Form/Form"
import { Logo } from "../../../logo"

export const ContactWithCompany = ({
  contacts: {address, department, email, image, schedule, tel, text, title, facebook, instagram},
  textColor,
  hoverColor,
  propsMobileLandspace,
  currLanguage,
  isRenderContentPeople = true,
}) => {
  // console.log({
  //   textColor,
  // hoverColor,
  // });

  const onFormSubmit = () => {
    BX24.callMethod(
      "crm.lead.add",
      {
        fields: {
          NAME: "Глеб",
          PHONE: "Егорович",
          EMAIL: "Титов",
          STATUS_ID: "NEW",
          OPENED: "Y",
          ASSIGNED_BY_ID: 1,
          CURRENCY_ID: "USD",
          OPPORTUNITY: 12500,
          PHONE: [{VALUE: "555888", VALUE_TYPE: "WORK"}],
        },
        params: {REGISTER_SONET_EVENT: "Y"},
      },
      function (result) {
        if (result.error()) console.error(result.error())
        else console.info("Создан лид с ID " + result.data())
      }
    )
  }

  return (
    <StyledContactWithCompany className="StyledContactWithCompany">
      {isRenderContentPeople && (
        <ContentPeople className="ContentPeople">
          <WrapperDescription>
            <>
              <Title textColor={textColor}>{department}</Title>
              <PeopleData>
                <IconComma textColor={textColor} className="icon-comma" />
                <Name>{title}</Name>
                <Description dangerouslySetInnerHTML={{__html: text}} />
              </PeopleData>
            </>
          </WrapperDescription>
          <WrapperImg>
            <Image src={image?.sourceUrl} objectFit="cover" quality={100} width={600} height={600}></Image>
          </WrapperImg>
        </ContentPeople>
      )}

      <ContentContact>
        <Part1>
          <WrapperBlocks>
            <Block>
              {!useIsEmpty(tel) && (
                <Number>
                  <TitleText>{currLanguage?.contactNumber}</TitleText>
                  {/* <TextContact color={textColor} href={`tel:${tel}`}> */}
                  <TextContact color={"#9C8D88"} href={`tel:${tel}`}>
                    {tel}
                  </TextContact>
                </Number>
              )}
              {!useIsEmpty(address) && (
                <Address>
                  <TitleText>{currLanguage?.mainOffice}</TitleText>
                  <Text color={"#9C8D88"}>{address}</Text>
                  {/* <Text color={textColor}>{address}</Text> */}
                </Address>
              )}
            </Block>

            <Block>
              {!useIsEmpty(schedule) && (
                <Schedule>
                  <TitleText>{currLanguage?.workSchedule}</TitleText>
                  <Text color={"#9C8D88"}>{schedule}</Text>
                </Schedule>
              )}
              {!useIsEmpty(email) && (
                <Email>
                  <TitleText>{currLanguage?.email} </TitleText>

                  <EmailText color={"#9C8D88"} href={`mailto:${email}`}>
                    {email}
                  </EmailText>
                </Email>
              )}
            </Block>
          </WrapperBlocks>
          <SocialLinks>
            {!useIsEmpty(facebook) && (
              <SocialCircle hoverColor={hoverColor} color={textColor} href={facebook?.url} target="_blank" backgroundColor="#FFFFFF">
                <FacebookIcon color={textColor} className="icon-facebook" />
              </SocialCircle>
            )}
            {!useIsEmpty(instagram) && (
              <SocialCircle hoverColor={hoverColor} color={textColor} backgroundColor="#FFFFFF" href={instagram?.url} target="_blank">
                <InstagramIcon color={textColor} className="icon-instagram" />
              </SocialCircle>
            )}
          </SocialLinks>
        </Part1>
        <Part2>
          <Form />
          <WrapperLogo>
          <Logo namePage="AboutPage" fill={"#494E54"} mb={0} />
          </WrapperLogo>
        </Part2>
      </ContentContact>
      <MobileLandspace {...propsMobileLandspace} currLanguage={currLanguage} isRenderContentPeople={isRenderContentPeople} />
    </StyledContactWithCompany>
  )
}

export const MobileLandspace = ({contacts: {address, department, email, image, schedule, tel, text, title}, textColor, hoverColor, currLanguage, isRenderContentPeople}) => {
  return (
    <>
      {isRenderContentPeople && (
        <ContentPeople mob>
          <WrapperDescription>
            <>
              <Title textColor={textColor}>{department}</Title>
              <PeopleData>
                <IconComma textColor={textColor} className="icon-comma" />
                <Name>{title}</Name>
                <Description dangerouslySetInnerHTML={{__html: text}} />
                <Part1>
                  <WrapperBlocks>
                    <Block>
                      <Number>
                        <TitleText>{currLanguage?.contactNumber}</TitleText>
                        <Text>{tel}</Text>
                      </Number>
                      <Address>
                        <TitleText>{currLanguage?.mainOffice}</TitleText>
                        <Text>{address}</Text>
                      </Address>
                    </Block>

                    <Block>
                      <Schedule>
                        <TitleText>{currLanguage?.workSchedule}</TitleText>
                        <Text>{schedule}</Text>
                      </Schedule>
                      <Email>
                        <div>
                          <TitleText>{currLanguage?.email}</TitleText>
                          <Text>{email}</Text>
                        </div>
                        <SocialLinks>
                          <SocialCircle hoverColor={hoverColor} color={textColor} backgroundColor="#FFFFFF">
                            <FacebookIcon color={textColor} href={"#"} className="icon-facebook" />
                          </SocialCircle>
                          <SocialCircle hoverColor={hoverColor} color={textColor} backgroundColor="#FFFFFF">
                            <InstagramIcon color={textColor} href={"#"} className="icon-instagram" />
                          </SocialCircle>
                        </SocialLinks>
                      </Email>
                    </Block>
                  </WrapperBlocks>
                </Part1>
              </PeopleData>
            </>
          </WrapperDescription>
          <WrapperImg>
            <Image src={image?.sourceUrl} objectFit="cover" quality={100} width={300} height={300}></Image>
          </WrapperImg>
        </ContentPeople>
      )}
      <ContentContact mob>
        <Part2>
          <Form />
          <WrapperLogo>
          <Logo
                namePage="AboutPage"
                fill={"#494E54"}
                mb={0}
              />
              </WrapperLogo>
        </Part2>
      </ContentContact>
    </>
  )
}
