import {ItemOfList} from "./ItemOfList/ItemOfList"
import {Number, Text} from "./ItemOfList/StyledItemOfList"
import {StyledListItems} from "./StyledListItems"

export const ListItems = ({textColor, listArr, variant, cols, page}) => {
  return (
    <>
      <StyledListItems page={page} variant={variant}>
        {listArr.map(({text}, currNumber) => {
          const isCurrentNumberMoreThan10 = currNumber + 1 >= 10
          const currNum = isCurrentNumberMoreThan10 ? `${currNumber + 1}` : `0${currNumber + 1}`
          return (
            <ItemOfList colorBorderRight={textColor} variant={variant} cols={cols} page={page}>
              <Number color={"#D9D3C5"}>{currNum}</Number>
              <Text variant={variant}>{text}</Text>
            </ItemOfList>
          )
        })}
      </StyledListItems>
    </>
  )
}
