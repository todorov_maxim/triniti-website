import styled from "styled-components";
import {
  screenSize
} from "../../../../constants/media";



export const StyledListItems = styled.div(({
  variant = "sm",page=false
}) => ({
  display: "flex",
  justifyContent: "flexStart",
  flexWrap: "wrap",
  width: "100%",
  marginTop:variant !== "lg" && "38px",



  [`@media (max-width: ${screenSize.laptopPortrait})`]: {
    paddingLeft: variant !== "lg" && "26px",
  },

  [`@media (max-width: ${screenSize.mobileLLandscape})and (orientation: landscape)`]: {
    // justifyContent:page=='managment'&&'center',
    marginTop:variant !== "lg" && "30px",
    paddingLeft: "unset",
  },
  [`@media (max-width: ${screenSize.mobileL}) and (orientation: portrait)`]: {
    flexDirection: variant !== "lg" && "column",
    alignItems: variant !== "lg" && "center",
    maxWidth: variant !== "lg" && "190px",
    paddingLeft: variant !== "lg" && "unset",

  }

}));