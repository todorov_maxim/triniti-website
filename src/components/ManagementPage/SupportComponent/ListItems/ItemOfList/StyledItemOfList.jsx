import styled, { css, keyframes } from "styled-components";
import { screenSize } from "../../../../../constants/media";




export const StyledItemOfListDef = styled.div`
min-height: 95px;
display: flex;
justify-content: center;
align-items: center;
padding: 0px 10px 0 10px;

   flex-direction: column;
   width:100%;
   @media (max-width: ${screenSize.laptop}) {

  }
  @media (max-width: ${screenSize.laptopPortrait}) {
max-width: 240px;
    // &:nth-child(3n+3) {
    //   align-items: flex-end;
    // }
  }
  // @media (max-width: ${screenSize.mobileLLandscape}) and (orientation: landscape) {

  //   &:nth-child(2) {
  //     flex:0 0 33%;

  //   }
  //   &:nth-child(1) {
  //     flex:0 0 33%;

  //   }
  //   &:nth-child(3) {
  //     flex:0 0 33%;

  //   }

  // }

  // @media (max-width: ${screenSize.mobileL}) and (orientation: portrait) {
  //   &:nth-child(2) {
  //     align-items: flex-start;
  //   };
  //   &:nth-child(3) {
  //     align-items: flex-start;
  //   };
    

}
`
export const StyledItemOfList = styled(StyledItemOfListDef)(({ colorBorderRight = '#D9D3C5', variant = "sm", cols = 3,page=false, isVisible }) => ({
  // borderRight: `solid 1px ${colorBorderRight}`,
  position: "relative",
  minHeight: "95px",
  flex: variant === "lg" ? `0 0 25%` : `0 0 50%`,
  maxWidth: variant === "lg" ? '25%' : '0 0 50%',
  marginBottom: (variant === "lg" || variant === "sm") && "55px",
  justifyContent: variant === "lg" && "center",
  overflow: "hidden",

  [`&:before`]:{
    content: "''",
    display: "block",
    position: "absolute",
    right: 0,
    bottom: 0,
    width: 1,
    height: isVisible ? "100%" : 0,
    transition: isVisible ? "height 1s .3s" : 0,
    background: colorBorderRight,
  },

  [`&:nth-child(${cols}n+${cols})`]: {
    // border: "unset",
    [`&:before`]:{
      display: 'none'
    },
   
  },

  [`@media (max-width: ${screenSize.laptop}) `]: {
    marginBottom: "45px",
    flex: variant !== "lg" && "0 1 50%",
    maxWidth: variant !== "lg" && "50%",
    // [`&:nth-child(3n+1)`]: {
    //   alignItems: variant !== "lg" && "flex-start",
    // },
  },

  // [`@media (max-width: 1065px) `]: {
  //   flex: variant !== "lg" && "0 1 32%",
  //   maxWidth:variant !== "lg" && "32%",


  //   [`&:nth-child(3n+2)`]: {
  //     flex: variant !== "lg" && "0 0 35%",
  //   maxWidth:variant !== "lg" && "35%",
  //   }
  // },

  // [`@media (max-width: 1000px) `]: {
  //   // flex: variant !== "lg" && "0 1 33%",
  //   // maxWidth:variant !== "lg" && "33%",

  //   // [`&:nth-child(3n+2)`]: {
  //   // maxWidth:variant !== "lg" && "33%",
  //   // flex: variant !== "lg" && "0 0 33%",
  //   // }
  // },


  // [`@media (max-width: ${screenSize.mobileLLandscape}) and (orientation: landscape) `]: {
  //   [`&:nth-child(2)`]: {
  //     flex:page=='managment'&& "0 0 33%",
  //     maxWidth:page=='managment' && '33%',
  //   },
  //   [`&:nth-child(1)`]: {
  //     flex:page=='managment'&& "0 0 33%",
  //     maxWidth:page=='managment' &&'33%',
  //   },
  //   [`&:nth-child(3)`]: {
  //     flex:page=='managment'&& "0 0 33%",
  //     maxWidth:page=='managment' &&'33%',

  //   },
  
  // },

  [`@media (max-width: 992px) `]: {
    paddingRight: variant === "lg" && "5px",
    marginBottom: "35px",
  },

  [`@media (max-width: ${screenSize.mobileL})`]: {
    flex: variant === "lg" && `0 0 33.333%`,
    maxWidth: variant === "lg" && `33.333%`,
    marginBottom: "27px",

    [`&:nth-child(1n)`]: {
      [`&:before`]:{
        display: variant === "lg" && `block`,
      }
    },

    [`&:nth-child(3n+3)`]: {

      [`&:before`]:{
        display: variant === "lg" && `none`,
      }
      
    },

    [`&:nth-child(4n+4)`]: {
      [`&:before`]:{
        backgroundColor: variant === "lg" && `${colorBorderRight}`,
      }
      
    },

  },


  [`@media (max-width: ${screenSize.mobileL}) and (orientation: portrait)`]: {
    [`&:nth-child(3n+2)`]: {
      maxWidth:variant !== "lg" && "100%",
      },
      
    maxWidth: variant !== "lg" && '100%',
    width: variant !== "lg" && "100%",
    minHeight: variant !== "lg" && "65px",

    
    [`&:before`]: {
      display: variant !== "lg" &&  `none`,
    }
  },


  [`@media (max-width: 590px) and (orientation: landscape) `]: {
    [`&:nth-child(2)`]: {
      flex:page=='managment'&& "0 0 33%",
      maxWidth: page=='managment' &&'33%',
    },
    [`&:nth-child(1)`]: {
      flex:page=='managment'&& "0 0 33%",
      maxWidth:page=='managment' && '33%',
    },
    [`&:nth-child(3)`]: {
      flex:page=='managment'&& "0 0 33%",
      maxWidth:page=='managment' && '33%',
      paddingRight:page=='managment' &&10,
    },
  
  },


  [`@media (max-width: 574px)`]: {
    flex: variant === "lg" && `0 0 50%`,
    maxWidth: variant === "lg" && `50%`,
    paddingRight: variant === "lg" && "13px", 
    minHeight: variant === "lg" && "67px",

    [`&:before`]: {
      transition: "none",
      height: "100%",
    },
    
    [`&:nth-child(3n+3)`]: {
      [`&:before`]: {
        // borderRight: variant === "lg" && `solid 1px ${colorBorderRight}`,
        display: variant === "lg" && `block`,
      }
      // }
      
    },
    [`&:nth-child(2n+2)`]: {
      [`&:before`]: {
        display: variant === "lg" && `none`,
      }
      
    },
  },

  [`@media (max-width: 359px)`]: {
    paddingRight: variant === "lg" && "7px", 
    paddingLeft: variant === "lg" && "16px", 
  },

  [`@media (max-width: 320px)`]: {
    paddingRight: variant === "lg" && "3px", 
    paddingLeft: variant === "lg" && "3px", 
  }
  
}))

export const StyledItemOfListContent = styled.div(() => ({
  
}));

export const Content = styled.div(({ variant = "sm" }) => ({
  maxWidth: variant === "lg" ? "calc(76% + 40px)" : 280,
  alignSelf: variant === "lg" && "flex-end",
  width: "100%",

  [`@media (max-width: 992px) `]: {
    maxWidth: variant === "lg" && "100%",
  }
}));

export const Number = styled.div(({ color = 'black' }) => ({
  color,
  marginBottom: 5,
  fontSize: 24,
  lineHeight: '24px',
  letterSpacing: '0.15em',
}))


const  isVariantSm=(variant)=>{
if(variant!='sm')return{}
return{
  [`@media (max-width: 1065px) `]: {
fontSize:17,
letterSpacing:'0.1em',
  },

  [`@media (max-width: 1000px) `]: {
    fontSize:16,
    letterSpacing:'0.07em',

  },
}
}


export const Text = styled.div(({ variant = "sm" }) => ({
  lineHeight: "24px",
  color: "#494E54",
  letterSpacing: "0.15em",
  fontSize: "18px",
  textTransform: variant === "lg" ? "initial" : "uppercase",

  [`@media (max-width: ${screenSize.laptop}) `]: {
    fontSize: variant === "lg" && "16px",
  },
...isVariantSm(variant),

  [`@media (max-width: 992px) `]: {
    fontSize: "14px",
    lineHeight: variant === "lg" && "20px",
    letterSpacing: variant === "lg" && "0.1em",
  },
  [`@media (max-width: 600px) and (orientation: landscape)`]:  {
    fontSize: variant !== "lg" && "12px",
  },
  [`@media (max-width: 574px)`]: {
    fontSize: variant === "lg" && "12px",
    lineHeight: variant === "lg" && "16px",
    letterSpacing: variant === "lg" && "0.15em",
  },
  [`@media (max-width: 530px) and (orientation: landscape)`]: {
    fontSize: variant !== "lg" && "10px",
  },

  [`@media (max-width: 359px)`]: {
    fontSize: variant === "lg" && "12px",
    lineHeight: variant === "lg" && "16px",
    letterSpacing: variant === "lg" && "0.1em",
  },

}))





export const ContactDescriptionFooterText = styled.div`
  display: block;
  font-size: 10px;
  line-height: 12px;
  width: 100%;
  max-width: 402px;
  color: #ffffff;
  @media (max-width: ${screenSize.laptop}) {
    max-width: 78%;
    text-align: left;
  }
  @media (min-width: 968px) and (max-height: 600px) {
    max-width: 100%;
  }
  @media (max-width: 967px) and (max-height: 600px) {
    max-width: 100%;
  }
  @media (max-width: ${screenSize.mobileL}) and (orientation: portrait) {
    max-width: 78%;
    text-align: center;
  }
  @media (max-width: ${screenSize.mobileLLandscape}) and (orientation: landscape)
    {
    max-width: 100%;
      text-align: center;
    };
    @media (max-width: ${screenSize.mobileLLandscape}) and (max-height: 300px) {
      font-size: 9px;
      line-height: 9px;
      margin-top: 4px;
    }
`;

//EditeContactDescription
