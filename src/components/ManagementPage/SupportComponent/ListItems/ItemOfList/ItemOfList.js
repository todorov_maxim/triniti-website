import { StyledItemOfList, Content, Number, Text } from "./StyledItemOfList";
import {useRef} from 'react';
import {useIntersection} from './../../../../../hooks/useIntersection';

export const ItemOfList = ({
  children,
  variant,
  cols,
  page,
  colorBorderRight,
}) => {


  const itemRef = useRef();
  const inViewport = useIntersection(itemRef, "0px");


  return (
    <StyledItemOfList
      variant={variant}
      cols={cols}
      page={page}
      ref={itemRef}
      colorBorderRight={"#D9D3C5"}
      isVisible={inViewport}
    >
      <Content variant={variant}>{children}</Content>
    </StyledItemOfList>
  );
};
