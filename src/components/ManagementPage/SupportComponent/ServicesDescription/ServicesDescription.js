import { ImgAndDescription } from "./ImgAndDescription/ImgAndDescription";
import { StyledServicesDescription } from "./StyledServicesDescription";

export const ServicesDescription = ({ services, textColor }) => {
  return (
    <StyledServicesDescription className="StyledServicesDescription">
      {services.map((currServices, i) => {
        return <ImgAndDescription key={i} {...currServices} textColor={textColor} />;
      })}
    </StyledServicesDescription>
  );
};
