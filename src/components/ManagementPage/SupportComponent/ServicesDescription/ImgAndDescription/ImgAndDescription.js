import Image from "next/image";
import { useEffect, useState } from "react";
import {
  StyledImgAndDescription,
  WrapperImg,
  WrapperDescription,
  WrapperLogo,
  WrapperText,
  Title,
  Text,
} from "./StyledImgAndDescription";
import { Logo } from "../../../../logo";

export const ImgAndDescription = ({ description, image, name, textColor }) => {
  const mobileProps = { description, image, name, textColor };
  const [isMobile, setIsMobile] = useState(false);
  const screenMobile = 755;
  useEffect(() => {
    window.innerWidth <= screenMobile ? setIsMobile(true) : setIsMobile(false);
    //if the user will resize the screen <=755
    window.addEventListener(
      `resize`,
      (event) => {
        //ниже строчка чтобы отоавливать изменения экрана, она работает в паре с useEffect у которого в засимости [isResize]
        // setIsresize((pre) => !pre);
        if (window.screen.width <= screenMobile) {
          setIsMobile(true);
        } else {
          setIsMobile(false);
        }
      },
      false
    );
  }, []);

  return (
    <>
      <StyledImgAndDescription>
        <WrapperImg>
          <Image
            width="937"
            height="536"
            layout="responsive"
            src={image?.sourceUrl}
            objectFit="cover"
          />
        </WrapperImg>
        <WrapperDescription className="WrapperDescription">
          <WrapperLogo>
          <Logo
                namePage="AboutPage"
                fill={"#494E54"}
                mb={0}
              />
          </WrapperLogo>
          <WrapperText className="WrapperText">
            <Title>{name}</Title>

            <Text>{description}</Text>
          </WrapperText>
        </WrapperDescription>
        {/* mobile for 360x680 start*/}
        {isMobile && <MobileImgAndDescription {...mobileProps} />}
        {/* mobile for 360x680 end */}
      </StyledImgAndDescription>
    </>
  );
};

export const MobileImgAndDescription = ({
  description,
  image,
  name,
  textColor,
}) => {
  // mobile for 360x680
  return (
    <>
      <WrapperLogo className="WrapperLogo" isMobile>
      <Logo
                namePage="AboutPage"
                fill={"#494E54"}
                mb={0}
              />
      </WrapperLogo>
      <Title className="Title" isMobile>
        {name}
      </Title>
      <WrapperImg className="WrapperImg" isMobile>
        <Image
          width="937"
          height="536"
          layout="responsive"
          src={image?.sourceUrl}
          objectFit="cover"
        />
      </WrapperImg>
      <Text className="Text" isMobile>
        {description}
      </Text>
    </>
  );
};
