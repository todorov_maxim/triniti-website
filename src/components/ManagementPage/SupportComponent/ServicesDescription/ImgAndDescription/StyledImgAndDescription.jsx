import styled from "styled-components"
import {screenSize} from "../../../../../constants/media"

export const MobileDef = styled.div(({isMobile = false}) => ({
  display: isMobile && "none",

  [`@media (max-width: ${screenSize.mobileL}) and (orientation: portrait)`]: {
    display: !isMobile ? "none " : "flex",
  },
}))

export const StyledImgAndDescription = styled.div`
  display: flex;
  justify-content: space-between;
  margin-bottom: 89px;

  &:nth-child(even) {
    flex-direction: row-reverse;
    .WrapperDescription {
      margin-right: 129px;
      @media (max-width: ${screenSize.laptopL}) {
        margin-right: 68.5px;
      }

      @media (max-width: ${screenSize.laptop}) {
        margin-right: 45px;
      }
      @media (max-width: ${screenSize.laptopPortrait}) {
        margin: unset;
        flex-direction: row-reverse;
      }
      @media (max-width: ${screenSize.mobileLLandscape}) and (orientation: landscape) {
        padding-left: 53px;
      }
    }

    @media (max-width: ${screenSize.laptopPortrait}) {
      flex-direction: column-reverse;
    }
    @media (max-width: ${screenSize.mobileL}) and (orientation: portrait) {
      flex-direction: column;
    }
  }
  &:nth-child(odd) {
    .WrapperDescription {
      margin-left: 129px;
      @media (max-width: ${screenSize.laptopL}) {
        margin-left: 68.5px;
      }

      @media (max-width: ${screenSize.laptop}) {
        margin-left: 45px;
      }
      @media (max-width: ${screenSize.laptopPortrait}) {
        margin: unset;
      }
      @media (max-width: ${screenSize.mobileLLandscape}) and (orientation: landscape) {
        padding-right: 53px;
      }
    }
    @media (max-width: ${screenSize.mobileL}) and (orientation: portrait) {
      flex-direction: column;
    }
  }

  @media (max-width: ${screenSize.laptopL}) {
    margin-bottom: 99px;
  }
  @media (max-width: ${screenSize.laptop}) {
    margin-bottom: 109px;
  }

  @media (max-width: ${screenSize.laptopPortrait}) {
    flex-direction: column-reverse;
    margin-bottom: 63px;
  }
  @media (max-width: ${screenSize.mobileLLandscape}) and (orientation: landscape) {
    margin-bottom: 53px;
  }
  @media (max-width: ${screenSize.mobileL}) and (orientation: portrait) {
    flex-direction: column;
  }
  &:nth-last-child(1) {
    margin-bottom: 132px;

    @media (max-width: ${screenSize.laptopL}) {
      margin-bottom: 112px;
    }
    @media (max-width: ${screenSize.laptop}) {
      margin-bottom: 81px;
    }
    @media (max-width: ${screenSize.laptopPortrait}) {
      margin-bottom: 73px;
    }
    @media (max-width: ${screenSize.mobileLLandscape}) and (orientation: landscape) {
      margin-bottom: 84px;
    }
  }
`
export const WrapperDescription = styled(MobileDef)`
  padding-bottom: 20px;
  max-width: 516px;
  @media (max-width: ${screenSize.laptopL}) {
    max-width: 384px;
  }
  @media (max-width: ${screenSize.laptopPortrait}) {
    display: flex;
    flex-direction: row;
    width: 100%;
    max-width: 100%;
    min-height: 224px;
    padding-bottom: 29px;
  }
  @media (max-width: ${screenSize.mobileLLandscape}) and (orientation: landscape) {
    padding-bottom: 40px;
    justify-content: space-between;
  }
  @media (max-width: ${screenSize.mobileL}) and (orientation: portrait) {
    display: none;
  }
`

export const WrapperImg = styled(MobileDef)`
  span {
    width: 100% !important;
    height: 100% !important;
  }
  width: 100%;
  max-width: 937px;
  height: 536px;
  @media (max-width: ${screenSize.laptopL}) {
    max-width: 798px;
  }
  @media (max-width: ${screenSize.laptop}) {
    max-width: 534px;
    height: 480px;
  }
  @media (max-width: ${screenSize.laptopPortrait}) {
    max-width: 100%;
    height: 480px;
  }
  @media (max-width: ${screenSize.mobileLLandscape}) and (orientation: landscape) {
    height: 450px;
  }
  @media (max-width: ${screenSize.mobileL}) and (orientation: portrait) {
    /* height: 314px; */
    margin-bottom: 34.5px;
  }
`

export const WrapperLogoDef = styled(MobileDef)`
  display: flex;
  justify-content: center;
  @media (max-width: ${screenSize.laptopPortrait}) {
    width: 100%;
    max-width: 180px;
    align-items: center;
  }
  @media (max-width: ${screenSize.mobileLLandscape}) and (orientation: landscape) {
    width: unset;
    max-width: unset;
    flex: 1 1 217px;
  }
  @media (max-width: ${screenSize.mobileL}) and (orientation: portrait) {
    width: 100%;
    max-width: 100%;
    margin-bottom: 76px;
  }
`
export const WrapperLogo = styled(WrapperLogoDef)(({isMobile}) => ({
  display: isMobile && "none",
  [`@media (max-width: ${screenSize.mobileL}) and (orientation: portrait)`]: {
    display: !isMobile ? "none " : "flex",
  },
}))

export const Title = styled(MobileDef)`
  color: #494e54;
  font-size: 32px;
  line-height: 42px;
  letter-spacing: 0.15em;
  text-transform: uppercase;
  margin-bottom: 23px;
  margin-top: 55px;
  @media (max-width: ${screenSize.mobileLLandscape}) and (orientation: landscape) {
    font-size: 24px;
    line-height: 32px;
    margin-bottom: 12px;
  }
  @media (max-width: ${screenSize.mobileL}) and (orientation: portrait) {
    font-size: 23px;
    letter-spacing: 0.08em;
    padding: 0px 27px;
    margin-bottom: 20px;
  }
`

export const Text = styled(MobileDef)`
  color: #494e54;
  font-size: 18px;
  line-height: 30px;
  @media (max-width: ${screenSize.laptop}) {
    line-height: 26px;
  }
  @media (max-width: ${screenSize.mobileLLandscape}) and (orientation: landscape) {
    font-size: 16px;
    line-height: 22px;
  }
  @media (max-width: ${screenSize.mobileL}) and (orientation: portrait) {
    padding: 0px 28px;
  }
`

export const WrapperText = styled(MobileDef)`
  @media (max-width: ${screenSize.laptopPortrait}) {
    max-width: 515px;
  }
  @media (max-width: ${screenSize.mobileLLandscape}) and (orientation: landscape) {
    max-width: unset;
    flex: 1 1 409px;
  }
`

export const LogoDef = styled.i`
  font-size: 128px;
  margin-bottom: 93px;
  @media (max-width: ${screenSize.laptopPortrait}) {
    font-size: 116px;
    margin-bottom: unset;
  }
  @media (max-width: ${screenSize.mobileLLandscape}) and (orientation: landscape) {
    font-size: 120px;
  }
  @media (max-width: ${screenSize.mobileL}) and (orientation: portrait) {
    font-size: 90px;
  }
`
export const Logo = styled(LogoDef)(({color}) => ({
  "&:before": {
    color,
  },
}))

export const StyledMobileImgAndDescription = styled.div`
  &:nth-last-child(1) {
    @media (max-width: ${screenSize.mobileL}) and (orientation: portrait) {
      margin-bottom: 80px;
    }
  }
  display: none;
  @media (max-width: ${screenSize.mobileL}) and (orientation: portrait) {
    display: flex;
    flex-direction: column;
    margin-bottom: 81px;
  }
`
