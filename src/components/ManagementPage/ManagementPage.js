import { compose } from "redux";
import { pageTheme } from "../../constants/themes";
import withMenu from "../../hoc/withMenu";
import withHardcoreTranslate from "../../hoc/withHardcoreTranslate";
import Footer from "../Footer";
import { FullScreenBlockGeneralPages } from "../FullScreenBlockGeneralPages/FullScreenBlockGeneralPages";
import Slider from "../Slider";
import { ContainerContent } from "./StyledManagementPage";
import ContactWithCompany from "./SupportComponent/ContactWithCompany";
import { ServicesDescription } from "./SupportComponent/ServicesDescription/ServicesDescription";
import { ServicesList } from "./SupportComponent/ServicesList/ServicesList";

const ManagementPage = ({ pageData, namePage, currLanguage }) => {
  const { background, textColor } = pageTheme[pageData?.pageTheme] || pageTheme.brown;
  const listArr = pageData?.services?.map((item, i) => ({ text: item?.name }));

  return (
    <>
      <FullScreenBlockGeneralPages pageData={pageData} />
      <Slider
        reverse
        sliderData={pageData?.profilesSlider}
        textColor={textColor}
      />
      <ContainerContent>
        <ServicesList
          textColor={textColor}
          listArr={listArr}
          cols={2}
          title={currLanguage?.services}
          page="managment"
        />
        <ServicesDescription
          textColor={textColor}
          services={pageData?.services}
        />
      </ContainerContent>

      <ContactWithCompany
        namePage={namePage}
        contacts={pageData?.contacts}
        textColor={textColor}
      />
      <Footer pageData={pageData} />
    </>
  );
};
export default compose(withMenu, withHardcoreTranslate)(ManagementPage);
