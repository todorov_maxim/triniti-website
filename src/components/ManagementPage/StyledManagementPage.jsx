import styled from "styled-components";
import { screenSize } from "../../constants/media";

export const ManagementPage=styled.div`
/* @media (max-width: ${screenSize.laptopPortrait}) {
padding: 0px;
    }
    @media (max-width: ${screenSize.mobileLLandscape}) and (orientation: landscape) {
        padding: 0px 50px 40px 46px;
} */
`

export const ContainerContent=styled.div`
padding: 0px 165px;

   @media (max-width: ${screenSize.laptopL}) {
padding: 0px 78px;
}
@media (max-width: ${screenSize.laptop}) {
padding: 0px 34px;
}
@media (max-width: ${screenSize.laptopPortrait}) {
padding: 0px 38px;
}
@media (max-width: ${screenSize.mobileLLandscape}) and (orientation: landscape){
    padding: 0px;
} 
@media (max-width: ${screenSize.mobileL}) and (orientation: portrait){
    padding: 0px;
} 
`


