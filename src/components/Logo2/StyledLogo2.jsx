import styled from "styled-components";
import { screenSize } from "../../constants/media";

export const StyledLogo2 = styled.svg(({mobSize,...props}) => ({
// ...props,
[`@media (max-width: ${screenSize.laptopL})`]: {
    width: mobSize,
    height: mobSize,
    },
[`@media (max-width: ${screenSize.mobileL}) and (orientation: portrait)`]: {
width: mobSize,
height: mobSize,
},

[`@media (max-width: ${screenSize.mobileL}) and (max-height: 500px) and (orientation: portrait)`]: {
    width: mobSize,
    height: mobSize,
    position: "absolute",
    left: "50%",
    top: "45%",
    width: "60%",
    height: "60%",
    zIndex: "-1",
    transform: "translate(-50%, -50%)",
    opacity: ".3",
    },
}));  

