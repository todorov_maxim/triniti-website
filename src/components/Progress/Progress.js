import { useState } from "react";
import { ProgressOrdersList, ProgressOrdersTabs,ProgressOrdersTabItem,  ProgressOrder,  ProgressTitle, ProgressWrapp } from "./StyledProgress";
import ProgressList from "./ProgressList"


const Progress = ({info: {ordersList, title}}) => {

    const [activeOrder, setActiveOrder] = useState(0);

    return (
        <ProgressWrapp>
            <ProgressTitle>
                {title}
            </ProgressTitle>
            <ProgressOrdersTabs>
                {ordersList.map(({order}, i) => {
                    return (
                        <ProgressOrdersTabItem key={i} active={activeOrder === i} onClick={() => setActiveOrder(i)}>
                            {order.title}
                        </ProgressOrdersTabItem>
                    )
                })}

            </ProgressOrdersTabs>
            <ProgressOrdersList>
                {
                    ordersList.map(({order}, i) => {
                        return (
                            <ProgressOrder  activeOrder={activeOrder === i}>
                                <ProgressList worksList={order.worksList}/>
                            </ProgressOrder>
                        )
                    })
                }
            </ProgressOrdersList>
        </ProgressWrapp>
    );
};

export default Progress;
