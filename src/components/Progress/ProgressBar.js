import { ProgressBarStyled, ProgressBarPercents } from "./StyledProgress";
import {useRef} from 'react';
import {useIntersection} from './../../hooks/useIntersection';

const ProgressBar = ({progress}) => {

    const barRef = useRef();
    const inViewport = useIntersection(barRef, "0px");

    return (
        <ProgressBarStyled ref={barRef} progress={progress} isVisible={inViewport}>
            <ProgressBarPercents>{progress || 0}%</ProgressBarPercents>
        </ProgressBarStyled>
    );
};

export default ProgressBar;