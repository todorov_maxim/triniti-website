import styled from "styled-components";
import { screenSize } from "../../constants/media";


export const ProgressWrapp = styled.div(() => ({
    maxWidth: 1652,
    width: "100%",
    padding: "80px 77px 50px",
    margin: "0 auto",
    overflow: "hidden",


    [`@media (max-width: ${screenSize.laptop})`]: {
        padding: "80px 34px 70px",
    },

    [`@media (max-width: ${screenSize.mobileL})`]: {
        padding: "50px 30px 60px",
    },

    [`@media (max-width: ${screenSize.mobileL})`]: {
        padding: "50px 16px 60px",
    },
}))

export const ProgressTitle = styled.div(() => ({
    fontSize: 32,
    lineHeight: "44px",
    letterSpacing: "0.15em",
    textTransform: "uppercase",
    color: "#494E54",
    marginBottom: 25,

    [`@media (max-width: 992px)`]: {
        fontSize: 24,
        lineHeight: "32px",
        marginBottom: 20,
    },

    [`@media (max-width: ${screenSize.mobileXS})`]: {
        marginBottom: 10,
    },
}))

export const ProgressListStyled = styled.div(() => ({
    display: "flex",
    flexWrap: "wrap",
    margin: "0 -40px",


    [`@media (max-width: ${screenSize.laptopL})`]: {
        margin: "0 -22px",
    },

    [`@media (max-width: ${screenSize.mobileL})`]: {
        margin: "0 -18px",
    },


    [`@media (max-width: ${screenSize.mobileXS})`]: {
        margin: "0 -14px",
    },

    [`@media (max-width: 340px)`]: {
        margin: "0 -10px",
    },
}))

export const ProgressOrdersTabs = styled.div(() => ({
    display: "flex",
    flexWrap: "wrap",
    padding: "15px 0",
}))

export const ProgressOrdersTabItem = styled.div(({active}) => ({
    borderLeft: '1px solid #CD8B7D',
    textTransform: "uppercase",
    marginBottom: 6,
    fontSize: 14,
    color: active && "#FFFFFF",
    padding: "0 10px",
    cursor:"pointer",
    position: "relative",
    zIndex: 2,
    transition: "color 1s",

    [`&:before`]: {
        content: "''",
        position: "absolute",
        zIndex: -1,
        left: 0,
        bottom: 0,
        width: "100%",
        height: active ? "100%" : 0,
        transition: "height 1s",
        background: "#CD8B7D",
    }
}))

export const ProgressOrdersList = styled.div(() => ({
    display: "block",
}))

export const ProgressOrder = styled.div(({activeOrder}) => ({
    display: !activeOrder && "none",
}))

export const ProgressItem = styled.div(({fadeOut}) => ({
    display: "flex",
    flexDirection: "column",
    flex: "0 0 20%",
    maxWidth: "20%",
    padding: "20px 40px",
    opacity: !fadeOut && " 0.6",

    [`@media (max-width: ${screenSize.laptopL})`]: {
        padding: "20px 22px",
    },


    [`@media (max-width: ${screenSize.laptop})`]: {
        flex: "0 0 25%",
        maxWidth: "25%",
    },

    [`@media (max-width: 992px)`]: {
        flex: "0 0 33.333%",
        maxWidth: "33.333%",
    },

    [`@media (max-width: ${screenSize.mobileL})`]: {
        padding: "20px 18px",
    },

    [`@media (max-width: ${screenSize.mobileXS})`]: {
        padding: "20px 14px",
        flex: "0 0 50%",
        maxWidth: "50%",
    },

    [`@media (max-width: 340px)`]: {
        padding: "20px 10px",
    },
}))

export const ProgressBarStyled = styled.div(({progress, isVisible}) => ({
    width: "100%",
    height: 10,
    borderRadius: "10px",
    background: "#EAD6D5",
    position: "relative",
    marginBottom: 10,
    display: "flex",
    alignItems: "center",
    justifyContent: 'center',

    '&:before': {
        content: '""',
        display: "block",
        position: "absolute",
        left: 0,
        top: 0,
        height: "100%",
        width: isVisible ? progress + "%" : 0,
        borderRadius: "10px",
        background: "#CD8B7D",
        transition: isVisible ? `width ${3 / 100 * progress}s .5s linear` : "none",
    }
}))

export const ProgressBarPercents = styled.div(() => ({
    color: "#fff",
    fontSize: 10,
    lineHeight: 14,
    fontWeight: 700,
    textAlign: 'center',
    zIndex: 2,
})) 
export const ProgressInfo = styled.div(() => ({
    color: "#CD8B7D",
    fontSize: "9px",
    lineHeight: "1.4em",
    letterSpacing: "0.15em",
    display: "flex",
    justifyContent: "space-between",



    [`@media (max-width: ${screenSize.mobileL})`]: {
        fontSize: "8px",
    },
}))

export const ProgressInfoName = styled.div(() => ({
    textTransform: "uppercase",
    
}))
export const ProgressInfoValue = styled.div(() => ({
    paddingLeft: 10,
}))