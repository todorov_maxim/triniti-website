import {ProgressItem, ProgressInfo, ProgressInfoName, ProgressInfoValue, ProgressListStyled } from "./StyledProgress";
import ProgressBar from './ProgressBar'

const ProgressList = ({worksList}) => {

    const getDate = (d) => {
        let date = d.split('.');
        let month = date[1];
        let year = date[2];
        const monthNames = ["Січень", "Лютий", "Березень", "Квітень", "Травень", "Червень",
                 "Липень", "Серпень", "Вересень", "Жовтень", "Листопад", "Грудень"];
        return {
            month: monthNames[Number(month)-1],
            year
        }
    }


return (
        <ProgressListStyled>
            {worksList?.map((item, i) => {
                let d = getDate(item.date);
                return (
                <ProgressItem key={i} fadeOut={item.value > 0}>
                    <ProgressBar progress={item.value} />
                    <ProgressInfo>
                        <ProgressInfoName>
                            {item.name}
                        </ProgressInfoName>
                        {item?.date &&
                        <ProgressInfoValue>
                            {d.month + ' ' + d.year}
                        </ProgressInfoValue>
                        }
                    </ProgressInfo>
                </ProgressItem>
                )
            })
            }
        </ProgressListStyled>
    );
};

export default ProgressList;