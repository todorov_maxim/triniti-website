import styled from "styled-components";
import { screenSize } from "../../constants/media";
import { Circle,CircleLink } from "../Pages/Brand/StyledBrand";

export const StyledSocialLinks = styled.div(({}) => ({
  width: "100%",
  maxWidth: "157px",
  display: "flex",
  justifyContent: "space-between",
  [`@media (max-width: ${screenSize.laptopL})`]: {
    maxHeight:'unset'
            },
            [`@media (max-width: ${screenSize.mobileLLandscape}) and (orientation: landscape)`]:{
              maxWidth: "137px",
      },
      

}));

export const SocialCircle = styled(CircleLink)(
  ({ isBorder = true, color, backgroundColor='unset',hoverColor= '#CD8B7D'}) => ({
    width: 33,
    height: 33,
    border: isBorder && `solid 1px ${color}`,
    color,
    backgroundColor,
    "&:hover": {
      border: isBorder && `solid 1px ${hoverColor}`,
      i: {
        color: hoverColor,
      },
    },
    
  })
);


export const MenuContactCircle = styled(Circle)(({}) => ({
  borderRadius:'50%',
    backgroundColor:'#CD8B7D',
    width: 33,
    height: 33,
    ":hover": {
      backgroundColor:'#EAD6D5',
      transform: 'scale(1.29)',
        border:'unset',
        margin:0,
        i: {
          color:'#494E54',
            fontSize: 17,
            
        },
      },

      [`@media (min-width: 768px) and (max-height: 340px)`]: {
        width: 26,
        height: 26,
    },
  }));
  

export const SocialIcons = styled.i(({}) => ({
  fontSize: 17,
  
}));

export const FacebookIcon = styled(SocialIcons)(({color='#494E54'}) => ({
  color,
}));

export const InstagramIcon = styled(SocialIcons)(({color='#494E54'}) => ({
  color,
  [`@media (max-width: ${screenSize.laptopL})`]: {
    fontSize: 15,
            },
}));
export const TelephoneIcon = styled.a(({}) => ({
  color: "#FFFFFF",
  fontSize: 17,
}));
