import {
  SocialCircle,
  StyledSocialLinks,
  TelephoneIcon,
  FacebookIcon,
  InstagramIcon,
  MenuContactCircle,
} from "./StyledSocialLinks";
import useIsEmpty from "../../hooks/useIsEmpty";

export const SocialLinks = ({ globalContactsData }) => {
  const { facebook, instagram, phone } = globalContactsData;

  return (
    <StyledSocialLinks>
      {!useIsEmpty(phone) && (
        <MenuContactCircle color="#CD8B7D">
          <TelephoneIcon
            href={`tel:${phone}`}
            target="_blank"
            className="icon-telephone"
          />
        </MenuContactCircle>
      )}
      {!useIsEmpty(facebook?.url) && (
        <SocialCircle
          href={facebook?.url}
          isHasBorder={false}
          backgroundColor="#FFFFFF"
          target="_blank"
        >
          <FacebookIcon className="icon-facebook" />
        </SocialCircle>
      )}
      {!useIsEmpty(instagram?.url) && (
        <SocialCircle
          href={instagram?.url}
          isHasBorder={false}
          backgroundColor="#FFFFFF"
          target="_blank"
        >
          <InstagramIcon className="icon-instagram" />
        </SocialCircle>
      )}
    </StyledSocialLinks>
  );
};
