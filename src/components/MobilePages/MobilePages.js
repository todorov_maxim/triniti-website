import { fullScreenImg } from "../../data/PagesFS/images";
import FullScreenPage from "../FullScreenPage/FullScreenPage";

export const MobilePages = ({
  fullpageApi,
  pageData: { commonSlides, contactsInfo },
  showForm,
  onceLoaded,
}) => {
  const objContact = {
    ...contactsInfo,
    img: contactsInfo?.img,
    background: "#9C8D88",
    numberPage: "09",
    textColor: "#FFFFFF",
    isContact: true,
  };

  const slideData = [...commonSlides, objContact].map((page, i) => ({
    ...page,
    pageCount: i + 1,
  }));
  return (
    <>
      {slideData.map((page, i, arr) => {
        return (
          <>
            <div className={"section"}>
              <FullScreenPage
                fullpageApi={fullpageApi}
                page={page}
                arr={arr}
                isContact={page?.isContact}
                // mobVersionDesctopPhoto для оотображеняи фото при альбомной ориентации
                mobVersionDesctopPhoto={true}
                pageCount={page.pageCount}
                pageData={page}
                showForm={showForm}
                onceLoaded={onceLoaded}
              />
            </div>
          </>
        );
      })}
    </>
  );
};
