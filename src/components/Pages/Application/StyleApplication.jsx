import styled from "styled-components";
import { screenSize } from "../../../constants/media";

export const WrapperSlider=styled.div`
margin-bottom:126px;

@media (max-width: ${screenSize.laptopL}) {
    margin-bottom:110px;
}
@media (max-width: ${screenSize.laptop}) {
    margin-bottom:81px;
}
@media (max-width: ${screenSize.laptopPortrait}) {
    margin-bottom:79px;
}

@media (max-width: ${screenSize.mobileLLandscape}) and (orientation: landscape) {
    margin-bottome:91px;
    margin-top:41px;
}
@media (max-width: ${screenSize.mobileL}) and (orientation: portrait) {
    margin-bottom:97px;
    margin-top:51.5px;

}
`
