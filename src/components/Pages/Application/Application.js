import Footer from "../../Footer";
import { useState } from "react";
import { FullScreenBlockGeneralPages } from "../../FullScreenBlockGeneralPages/FullScreenBlockGeneralPages";
import Slider from "../../Slider";
import { VideoBox } from "../../VideoBox/VideoBox";
import { Modal } from "../../Modal/Modal";
import { pageTheme } from "../../../constants/themes";
import { WrapperSlider } from "./StyleApplication";

export const Application = ({ pageData, showMenu, hiddenMenu }) => {
  const { background, textColor } = pageTheme[pageData?.pageTheme] || pageTheme.brown;
  const [isShowModal, setIsShowModal] = useState(false);

  const showModal = () => {
    hiddenMenu();

    setIsShowModal(true);
  };
  const modalHidden = () => {
    showMenu();
    setIsShowModal(false);

  };

  return (
    <>
      {isShowModal && (
        <Modal modalHidden={modalHidden}>
            <VideoBox video={pageData?.intro?.videoLink}/>
        </Modal>
      )}
      <FullScreenBlockGeneralPages pageData={pageData} hasVideo showModal={showModal}/>

      <WrapperSlider>
        <Slider
          reverse
          sliderData={pageData?.profilesSlider}
          textColor={textColor}
        />
      </WrapperSlider>

      <Footer pageData={pageData} />
    </>
  );
};
