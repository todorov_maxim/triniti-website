import { Logo } from "../../logo";
import { Contact } from "../../Contact/Contact";
import * as BS from "./StyledBrand";
import { dataStairs } from "../../../data/Brand/dataStairs";
import { Step } from "./SupportComponents/Step";
import { SwitchPageArrow } from "../../SwitchPageArrow/SwitchPageArrow";
import { compose } from "redux";
import { ByPeople } from "../../ByPeople/ByPeople";

const Brand = ({
  fullpageApi,
  pageData: {
    brand: { steps, secondaryText, mainText },
  },
}) => {
  return (
    <>
      <ByPeople fullpageApi={fullpageApi}/>
      
    </>
  );
};

export default compose()(Brand);
