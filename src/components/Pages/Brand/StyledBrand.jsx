import styled from "styled-components"
import {screenSize} from "../../../constants/media"

export const StyledBrand = styled.div`
  padding: 18px 21px;
  height: 100vh;
  height: 100%;
  width: 100vw;
  display: flex;
  flex-direction: column;
  justify-content: flex-start;
  position: relative;

  @media (max-width: 992px) and (max-height: 370px) {
    padding: 16px 21px 0px;
  }
  @media (max-width: ${screenSize.mobileL}) and (orientation: portrait) {
    padding: 14px 21px;
  }
  @media (max-width: ${screenSize.mobileLLandscape}) and (orientation: landscape) {
    padding-bottom: 0px;
  }
`

export const StyledBrandWithBG = styled(StyledBrand)`
  background-image: url(${props => props.bg});
  background-size: cover;
  background-position: center;
  background-repeat: no-repeat;
`

export const StyledContact = styled.div(() => ({
  width: "100%",
  maxWidth: 441,
  height: 26,
  maxHeight: 26,
  display: "flex",
  [`@media (max-width: ${screenSize.laptopL})`]: {
    maxWidth: 443,
  },

  [`@media (max-width: ${screenSize.laptop})`]: {
    maxWidth: 352,
  },
  [`@media (max-width: ${screenSize.mobileLLandscape}) and (orientation: landscape)`]: {
    maxWidth: 260,
  },
  [`@media (max-width: ${screenSize.mobileL}) and (orientation: portrait)`]: {
    maxWidth: 260,
  },
}))

export const Circle = styled.div(({isHasBorder = true, color = "black", mobWidthHeight, backgroundColor}) => ({
  width: 26,
  borderRadius: "50%",
  height: 26,
  border: isHasBorder && `solid 1px ${color}`,
  display: "flex",
  justifyContent: "center",
  alignItems: "center",
  fontSize: 8,
  lineHeight: 0.9,
  color,
  backgroundColor,

  [`@media (max-width: ${screenSize.mobileL}) and (orientation: portrait)`]: {
    width: mobWidthHeight,
    height: mobWidthHeight,
  },
}))

export const CircleLink = styled.a(({isHasBorder = true, color = "black", mobWidthHeight, backgroundColor}) => ({
  width: 26,
  borderRadius: "50%",
  height: 26,
  border: isHasBorder && `solid 1px ${color}`,
  display: "flex",
  justifyContent: "center",
  alignItems: "center",
  fontSize: 8,
  lineHeight: 0.9,
  color,
  backgroundColor,

  [`@media (max-width: ${screenSize.mobileL}) and (orientation: portrait)`]: {
    width: mobWidthHeight,
    height: mobWidthHeight,
  },
}))

export const LanguageCircle = styled(Circle)(({isOpen, color}) => ({
  display: "flex",
  flexDirection: "column",
  justifyContent: "space-between",
  padding: "8px 7px",
  fontSize: 8,
  lineHeight: "9px",
  maxHeight: 70,
  position: "absolute",
  borderRadius: 20,
  transition: !isOpen ? "height 0.5s ease 0.5s" : "height 0.5s ease",
  height: !isOpen ? 26 : 70,
  "&:hover": {
    backgroundColor: color,
    ".LanguageItem": {
      color: color == "#494E54" ? "#FFFFFF" : "#494E54",
      [`&:nth-child(1)`]: {
        color: color == "#494E54" ? "#FFFFFF" : "#494E54",
      },
    },
  },
}))

const calcColor = (firstElement, color) => {
  if (firstElement) {
    return color == "#494E54" ? "#FFFFFF" : "#494E54"
  }
  return "#CD8B7D"
}

export const LanguageItem = styled.div(({isDefaltValue, isOpen, color, defColor, firstElement}) => ({
  pointerEvents: !isOpen && "none",
  color: isOpen ? color : defColor,
  cursor: isOpen && "pointer",
  zIndex: !isOpen && 0,
  transition: isDefaltValue ? "opacity 0.5s ease" : "opacity 0.4s ease 0.8s",
  opacity: isDefaltValue ? 0 : 1,
  textTransform: "uppercase",
  "&:hover": {
    color: calcColor(firstElement, color),
  },
}))

export const StyledLanguage = styled.div(() => ({
  flex: "0 0 auto",
  minWidth: 26,
  cursor: "pointer",
}))

export const ContactCircle = styled(Circle)(() => ({
  [`@media (max-width: ${screenSize.mobileLLandscape}) and (orientation: landscape)`]: {
    width: 32,
    height: 32,
  },
}))

export const WrapperLink = styled.div(() => ({
  display: "flex",
  alignItems: "center",
  fontSize: 8,
  lineHeight: "9px",
  flex: "1 1 auto",
  [`@media (max-width: ${screenSize.laptopL})`]: {
    marginTop: -1,
  },
  [`@media (max-width: ${screenSize.mobileL}) and (orientation: portrait)`]: {
    flex: "0 0 auto",
  },

  [`@media (max-width: ${screenSize.mobileLLandscape}) and (orientation: landscape)`]: {
    paddingTop: 1,
  },
}))

export const Telephone = styled.a(({mob, maxWidth, pageMediaBool}) => ({
  justifyContent: "space-between",
  alignItems: "center",
  display: "flex",
  flex: "0 0 auto",
  width: "100%",
  maxWidth: mob ? maxWidth : 134,
  ":hover": {
    div: {
      color: "#494E54",
      borderColor: "#494E54",
    },
    i: {
      color: "#494E54",
    },
  },
  [`@media (max-width: 985px)`]: {
    maxWidth: pageMediaBool && 47,
  },

  [`@media (max-width: ${screenSize.mobileL}) and (orientation: portrait)`]: {
    maxWidth: "32px",
    marginLeft: "33px",
    width: "unset",
    padding: "unset",
  },
  "@media (max-width: 340px)": {
    marginLeft: 9,
  },

  [`@media (max-width: ${screenSize.mobileLLandscape}) and (orientation: landscape)`]: {
    maxWidth: 71,
    // paddingLeft: 10,
    padding: "unset",

    width: "unset",
  },
}))

export const Language = styled.div(() => ({
  flex: "0 0 auto",
}))

export const TelephoneIcon = styled.i(() => ({
  color: "#CD8B7D",
  fontSize: "10px",
  [`@media (max-width: ${screenSize.laptopL})`]: {
    fontSize: "12px",
    marginLeft: 2,
  },
  [`@media (max-width: ${screenSize.mobileLLandscape}) and (orientation: landscape)`]: {
    fontSize: "14px",
    marginLeft: 4,
  },
}))

export const Number = styled.div(({pageMediaBool}) => ({
  color: "#CD8B7D",
  letterSpacing: "0.25em",
  fontSize: "10px",

  [`@media (max-width: 985px)`]: {
    display: pageMediaBool && "none",
  },
  [`@media (max-width: ${screenSize.laptopL})`]: {
    marginBottom: 3,
    marginLeft: 1,
  },
  [`@media (max-width: ${screenSize.mobileL}) and (orientation: portrait)`]: {
    display: "none",
  },
  [`@media (max-width: ${screenSize.mobileLLandscape}) and (orientation: landscape)`]: {
    display: "none",
  },
}))

export const WrapperContent = styled.div(() => ({
  display: "flex",
  justifyContent: "center",
  alignItems: "start",
  paddingTop: "94px",
  width: "100%",
  height: "100%",

  [`@media (max-width: ${screenSize.laptopL}), (max-height: 700px)`]: {
    paddingTop: "10px",
  },
  [`@media (max-width:992px) and (max-height: 350px)`]: {
    paddingTop: 5,
  },

  [`@media (max-width: ${screenSize.laptopPortrait}) and (orientation: portrait)`]: {
    paddingTop: "calc(123px - 69px)",
  },

  [`@media (max-width: ${screenSize.mobileL}) and (orientation: portrait)`]: {
    paddingTop: "calc(123px - 77px)",
  },

  [`@media (max-width: ${screenSize.mobileL}) and (max-height: 600px) and (orientation: portrait)`]: {
    paddingTop: "15px",
  },

  [`@media (max-width: ${screenSize.mobileLLandscape}) and (orientation: landscape)`]: {
    padding: "unset",
    alignItems: "flex-end",
  },

  [`@media (max-height: 600px)`]: {
    paddingTop: "0px",
  },
}))

export const Content = styled.div(() => ({
  position: "relative",
  width: "100%",
  maxWidth: 1535,
  maxHeight: "100%",
  display: "flex",
  flexDirection: "column",
  alignItems: "stretch",
  justifyContent: "center",
  height: "100%",
  [`@media (max-width: ${screenSize.laptopL})`]: {
    // maxHeight: 537,
    maxWidth: 1243,
  },

  [`@media (max-width: ${screenSize.laptop})`]: {
    padding: "0 30px",
  },

  [`@media (max-width: ${screenSize.laptopPortrait}) and (orientation: portrait)`]: {
    flexDirection: "column",
    justifyContent: "flex-start",
    // maxHeight: 771,
  },

  [`@media (max-width: ${screenSize.mobileL}) and (orientation: portrait)`]: {
    flexDirection: "column",
    justifyContent: "flex-start",
  },
  [`@media (max-width: ${screenSize.mobileLLandscape}) and (orientation: landscape)`]: {
    // maxHeight: 285,
  },

  [`@media (max-width: 500px) and (orientation: landscape)`]: {
    flexDirection: "column",
    justifyContent: "flex-start",
    height: "100%",
    maxHeight: "100%",
    paddingTop: 15,
  },

  [`@media (max-width: ${screenSize.tabletSmall})`]: {
    padding: 0
  }
}))

export const WrapperStairs = styled.div(() => ({
  display: "flex",
  flexDirection: "column",
  justifyContent: "space-between",
  alignItems: "center",
  width: "100%",
  height: "100%",

  [`@media (max-width: ${screenSize.laptopPortrait}) and (orientation: portrait)`]: {
    maxHeight: 536,
  },

  [`@media (max-width: ${screenSize.mobileL}) and (orientation: portrait)`]: {
    maxHeight: 315,
    flex: "0 0 auto",
  },
  [`@media (max-width: ${screenSize.mobileLLandscape}) and (orientation: landscape)`]: {
    maxWidth: 329,
  },
}))

export const Stairs = styled.div(() => ({
  maxHeight: 462,
  width: "100%",
  height: "100%",
  display: "flex",
  alignItems: "center",
  flexDirection: "column",

  [`@media (max-width: ${screenSize.laptopL})`]: {
    maxHeight: 372,
  },

  [`@media (max-width: ${screenSize.laptop})`]: {
    maxWidth: 660,
    width: "100%",
  },

  [`@media (max-width: ${screenSize.laptopPortrait}) and (orientation: portrait)`]: {
    maxWidth: 537,
  },

  [`@media (max-width: ${screenSize.mobileLLandscape}) and (orientation: landscape)`]: {
    maxHeight: 215,
  },
}))

export const ByPeopleText = styled.div(() => ({
  color: "#494E54",
  textTransform: "uppercase",

  [`@media (max-width: ${screenSize.laptopL})`]: {
    fontSize: "32px",
    lineHeight: "42px",
  },
  [`@media (max-width: ${screenSize.laptop}) and (max-height: 450px)`]: {
    fontSize: "24px",
    lineHeight: "32px",
  },
  [`@media (max-width: ${screenSize.laptopPortrait}) and (orientation: portrait)`]: {
    maxWidth: 210,
  },

  [`@media (max-width:992px) and (max-height: 350px)`]: {
    fontSize: "20px",
    lineHeight: "26px",
  },

  [`@media (max-width: ${screenSize.mobileL}) and (orientation: portrait)`]: {
    marginBottom: "unset",
    maxWidth: 160,
    fontSize: "24px",
    lineHeight: "28px",
  },

  [`@media (max-width: ${screenSize.mobileLLandscape}) and (orientation: landscape)`]: {
    fontSize: "24px",
    lineHeight: "28px",
  },

  [`@media (max-width: 500px) and (max-height: 580px)`]: {
    fontSize: "20px",
    lineHeight: "22px",
  },
}))

export const WrapperByPeopleText = styled.div(() => ({
  maxWidth: "261px",
  width: "100%",
  fontSize: "40px",
  lineHeight: "50px",
  marginBottom: 172,
  width: "100%",

  [`@media (max-width: ${screenSize.laptopL})`]: {
    marginBottom: 140,
    maxWidth: "207px",
  },
  [`@media (max-width: ${screenSize.laptop})`]: {
    position: "absolute",
    left: 10,
    top: 0,
  },

  [`@media (max-width: ${screenSize.laptopPortrait}) and (orientation: portrait)`]: {
    position: "unset",
    marginBottom: 27,
    width: "100%",
    maxWidth: "unset",
    paddingLeft: 56,
  },
  [`@media (max-width:992px) and (max-height: 350px)`]: {
    marginBottom: 120,
    maxWidth: "160px",
  },
  [`@media (max-width: ${screenSize.mobileL}) and (orientation: portrait)`]: {
    marginBottom: 44,
    maxWidth: "unset",
    fontSize: "24px",
    lineHeight: "28px",
    paddingLeft: 0,
  },
  [`@media (max-width: ${screenSize.mobileLLandscape}) and (orientation: landscape)`]: {
    top: 23,
  },
  [`@media (max-width: 500px) and (orientation: landscape)`]: {
    position: "unset",
    marginBottom: 15,
    width: "100%",
    maxWidth: "unset",
    paddingLeft: 0,
    maxWidth: 190,
    alignSelf: "flex-start",
  },
  [`@media (max-width: 500px) and (orientation: portrait)`]: {
    position: "static",
    marginBottom: 15,
    flex: "1 1 auto",
  },
  [`@media (max-width: 500px) and (max-height: 580px) and (orientation: portrait)`]: {
    marginBottom: 15,
  },
}))

export const StyledStep = styled.div(({backgroundColor, maxWidth, color, index, stepsLength}) => ({
  letterSpacing: "0.15em",
  fontSize: 12,
  lineHeight: "12px",
  width: "100%",
  maxHeight: 78,
  textTransform: "uppercase",
  height: "100%",
  display: "flex",
  justifyContent: "center",
  alignItems: "center",
  backgroundColor,
  maxWidth,
  textAlign: "center",
  padding: 8,
  color,
  marginBottom: 18,

  "&:last-child": {
    marginBottom: 0,
  },

  ["@media (min-width: 992px)"]: {
    opacity: 0,
    transition: "opacity 1s .5s",

    [`.fp-table.active &:nth-child(${index + 1})`]: {
      transition: `opacity 2s ${(stepsLength - index) / 2 + 0.5}s`,
      opacity: 1,
    },
  },

  [`@media (max-width: ${screenSize.laptopL})`]: {
    maxHeight: 63,
    marginBottom: 15,
  },
  [`@media (max-width: ${screenSize.laptopL}) and (max-height: 470px)`]: {
    marginBottom: 8,
  },
  [`@media (max-width: ${screenSize.laptopL}) and (max-height: 400px)`]: {
    marginBottom: 4,
  },
  [`@media (max-width: ${screenSize.laptop})`]: {
    padding: "0px 4px",
  },

  [`@media (max-width: ${screenSize.mobileL}) and (orientation: portrait)`]: {
    maxHeight: 38.9,
    padding: 4,
    fontSize: 8,
  },
  [`@media (max-width: ${screenSize.mobileLLandscape}) and (orientation: landscape)`]: {
    fontSize: 8,
    lineHeight: "12px",
    maxHeight: 38.9,
  },

  [`@media (max-width: 768px) and (max-height: 630px)`]: {
    marginBottom: 6,
  },

  [`@media (max-width: 768px) and (max-height: 500px)`]: {
    marginBottom: 5,
  },

  [`@media (max-width: 768px) and (max-height: 300px)`]: {
    fontSize: 6,
    lineHeight: "8px",
    maxHeight: 22,
    marginBottom: 3,
  },
}))

export const WrapperText = styled.div(() => ({
  height: 88,
  marginBottom: 172,
  maxWidth: "261px",
  width: "100%",
  fontSize: "16px",
  lineHeight: "22px",
  textAlign: "right",
  marginBottom: "172px",

  [`@media (max-width: ${screenSize.laptop})`]: {
    display: "none",
  },
  [`@media (max-width: ${screenSize.laptopL})`]: {
    maxWidth: "207px",
  },

  [`@media (max-width: ${screenSize.laptopPortrait}) and (orientation: portrait)`]: {
    marginBottom: 21,
    height: "unset",
  },

  [`@media (max-width: ${screenSize.mobileL}) and (orientation: portrait)`]: {
    marginBottom: 21,
    height: "unset",
  },
}))

export const WrapperTextAbsolute = styled.div(() => ({
  color: "#494E54",
  height: 88,
  marginBottom: 172,
  width: "100%",
  fontSize: "16px",
  lineHeight: "22px",
  textAlign: "right",
  marginBottom: "172px",
  maxWidth: "361px",
  position: "absolute",
  top: 198,
  right: 0,
  [`@media (max-width: ${screenSize.laptopL})`]: {
    fontSize: "12px",
    lineHeight: "18px",
    maxWidth: "278px",
    right: 9,
    top: 162,
  },
  [`@media (max-width: ${screenSize.laptop})`]: {
    right: -16,
  },
  [`@media (min-width: 968px) and (max-height: 600px)`]: {
    top: 100,
  },
  [`@media (max-width: 968px) and (max-height: 500px)`]: {
    top: 50,
  },
  [`@media (max-width: ${screenSize.laptopPortrait}) and (orientation: portrait)`]: {
    marginBottom: "unset",
    position: "unset",
    textAlign: "unset",
    fontSize: "18px",
    lineHeight: "24px",
    width: "100%",
    maxWidth: 537,
    marginTop: 37,
  },
  [`@media (max-width: ${screenSize.mobileL}) and (orientation: portrait)`]: {
    marginBottom: "unset",
    position: "unset",
    textAlign: "unset",
    fontSize: "13px",
    lineHeight: "18px",
    marginTop: 17,
    flex: "0 0 auto",
    maxWidth: 530,
  },
  [`@media (max-width: ${screenSize.mobileL}) and (orientation: portrait) and (max-height: 650px)`]: {
    paddingLeft: 15,
  },
  [`@media (max-width: ${screenSize.mobileLLandscape}) and (orientation: landscape)`]: {
    fontSize: "13px",
    lineHeight: "18px",
    position: "absolute",
    right: 25,
    top: 23,
    maxWidth: 218,
  },

  [`@media (max-width: 630px) and (orientation: landscape)`]: {
    fontSize: "11px",
    lineHeight: "13px",
    position: "absolute",
    right: 0,
    top: 23,
    maxWidth: 190,
  },
  [`@media (max-width: 630px) and (orientation: landscape)`]: {
    fontSize: "10px",
    lineHeight: "12px",
    position: "absolute",
    right: 0,
    top: 23,
    maxWidth: 190,
  },
  [`@media (max-width: 500px) and (orientation: portrait)`]: {
    maxWidth: 320,
  },
  [`@media (max-width: 500px) and (orientation: landscape)`]: {
    fontSize: "10px",
    lineHeight: "12px",
    position: "static",
    right: 0,
    top: 23,
    maxWidth: "100%",
    textAlign: "center",
    paddingLeft: 50,
    paddingRight: 50,
  },

  [`@media (max-width: 500px) and (max-height: 630px)`]: {
    maxWidth: "calc(100% - 80px)",
    marginTop: 5,
    marginBottom: "5px",
  },

  [`@media (max-width: 359px) `]: {
    maxWidth: "calc(100% - 30px)",
    fontSize: "10px",
    lineHeight: "12px",
    marginTop: 12,
  },
}))

export const StyledSwitchPageArrow = styled.div(({pages, brandPage}) => ({
  position: "absolute",
  bottom: 0,
  left: !pages && 0,
  right: pages && "calc(0px - 14.58%)",
  width: 83,
  height: 108,
  backgroundColor: "#494E54",
  display: "flex",
  flexDirection: "column",
  zIndex: 7,
  [`@media (max-width: ${screenSize.laptopL})`]: {
    right: pages && "calc(0px - 17.2%)",
  },
  [`@media (max-width: ${screenSize.laptop})`]: {
    right: pages && "calc(0px - 21.34%)",
  },
  [`@media (max-width: ${screenSize.laptopPortrait}) and (orientation: portrait)`]: {
    left: 0,
    bottom: 0,
  },
  [`@media (max-width: ${screenSize.mobileL}) and (orientation: portrait)`]: {
    left: pages && 0,
    width: 46,
    height: 60,
  },
  [`@media (max-width: ${screenSize.mobileLLandscape}) and (orientation: landscape)`]: {
    width: 46,
    height: 60,
    right: pages && "calc(0px - 15%)",
  },
  [`@media (max-width: ${screenSize.mobileLLandscape}) and (max-height: 300px)`]: {
    width: 25,
    height: 60,
  },
  [`@media (max-width: 536px) and (max-height: 600px)`]: {
    width: 25,
    height: 80,
  },
  [`@media (max-width: 536px) and (max-height: 300px)`]: {
    width: 25,
    height: 60,
  },
  [`@media (max-width: 350px) and (min-height: 601px)`]: {
    width: 25,
    height: 80,
    right: pages && "calc(0px - 15%)",
  },
}))

export const UpArrow = styled.div(() => ({
  width: "100%",
  height: "50%",
  position: "absolute",
  top: 0,
  left: 0,
}))

export const DownArrow = styled.div(() => ({
  width: "100%",
  height: "50%",
  position: "absolute",
  bottom: 0,
  left: 0,
}))

export const WrapperIconArrow = styled.div(() => ({
  width: "100%",
  height: "100%",
  display: "flex",
  justifyContent: "center",
  alignItems: "center",

  [`@media (min-width:992px)`]: {
    cursor: "pointer",
  },
}))

export const IconArrow = styled.i(() => ({
  fontSize: 53,
  color: "#EAD6D5",

  [`@media (max-width: ${screenSize.mobileL}) and (orientation: portrait)`]: {
    fontSize: 30,
  },
  [`@media (max-width: ${screenSize.mobileLLandscape}) and (orientation: landscape)`]: {
    fontSize: 30,
  },
}))

const brandPageStyleLogo = width => ({
  marginBottom: 10,
  width,

  ["@media (min-width: 992px)"]: {
    opacity: 0,
    transition: "opacity 1s .5s",

    [`.fp-table.active &`]: {
      transition: `opacity 2s 3.5s`,
      opacity: 1,
    },
  },
  [`@media (max-width: ${screenSize.laptopL})`]: {
    width: 60,
    height: 120,
  },
  [`@media (max-width: ${screenSize.mobileL}) and (orientation: portrait)`]: {
    width: 30,
    height: 81,
    marginBottom: 20.7,
  },
  [`@media (max-width: ${screenSize.mobileLLandscape}) and (orientation: landscape)`]: {
    width: 30,
    height: 60,
    marginBottom: 11,
  },
})

const aboutPageStyleLogo = {
  // [`@media (max-width: ${screenSize.laptopL})`]: {
  //     width: 40,
  //     height: 80,
  //     },
  //     [`@media (max-width: ${screenSize.laptop})`]: {
  //         width: 60,
  //         height: 118,
  //         },
  //         [`@media (max-width: ${screenSize.laptopPortrait}) and (orientation: portrait) `]: {
  //             width: 43,
  //             height: 86,
  //             },
  //             [`@media (max-width: ${screenSize.mobileL}) and (orientation: portrait) `]: {
  //                 width: 36,
  //                 height: 72,
  //                 },
  //                 [`@media (min-width: 769px) and (max-width: 930px) `]: {
  //                     width: 36,
  //                     height: 72,
  //                     },
  //                 [`@media (max-width: ${screenSize.mobileLLandscape}) and (orientation: landscape) `]: {
  //                     width: 25,
  //                     height: 51,
  //                     // стиль е работает чего то проверить!!!
  //                     },
}

const renderStyleLogoPages = (namePage, width) => {
  switch (namePage) {
    case "BrandPage":
      return brandPageStyleLogo(width)
    case "AboutPage":
      return aboutPageStyleLogo
    default:
      return {}
  }
}

export const StyledLogo = styled.svg(({marginBottom, namePage, width}) => ({
  marginBottom,
  ...renderStyleLogoPages(namePage, width),
}))

export const Partition = styled.div(() => ({
  height: 18.4,

  [`@media (max-width: ${screenSize.laptopL})`]: {
    height: 14.9,
  },

  [`@media (max-width: ${screenSize.mobileL}) and (orientation: portrait)`]: {
    height: 5.6,
  },
}))

export const LogoBox = styled.svg(() => ({
  height: 126,

  [`@media (max-width: ${screenSize.mobileL}), (max-height: 700px)`]: {
    height: 100,
  },

  [`@media (max-height: 600px)`]: {
    height: 70,
  },
  [`@media (max-height: 490px)`]: {
    height: 45,
  },
}))
export const TopBlock = styled.div(() => ({
  display: "flex",
  justifyContent: "center",
  alignItems: "center",
  position: "relative",
  padding: "19px 350px",
  marginBottom: 40,

  [`@media (max-width: ${screenSize.laptop})`]: {
    padding: "19px 190px",
    marginBottom: 20,
  },

  [`@media (max-width: ${screenSize.mobileL}), (max-height: 700px)`]: {
    padding: "10px 140px",
    marginBottom: 0,
  },

  [`@media (max-width: ${screenSize.tabletSmall}), (max-height: 700px)`]: {
    padding: "10px 120px",
  }
}))

export const ByPeopleForPeople = styled.div(() => ({
  position: "absolute",
  left: "calc(100% - 300px)",
  bottom: 0,
  width: "390px",
  background: "#D5CDBF33",

  [`@media (max-width: ${screenSize.laptop})`]: {
    left: "calc(100% - 120px)",
    width: "190px",
    bottom: "auto",
    top: 0,
  },

  [`@media (max-width: ${screenSize.mobileL}), (max-height: 700px)`]: {
    top: 30,
    bottom: "auto",
    // left: "calc(100% - 100px)",
    // width: "150px",
  },

  [`@media (max-height: 500px)`]: {
    top: 40,
  },
}))

export const ByPeopleForPeopleText = styled.div(() => ({
  maxWidth: 290,
  fontSize: 32,
  lineHeight: 1.2,
  padding: "27px 32px",
  textTransform: "uppercase",
  fontWeight: 400,

  [`@media (max-width: ${screenSize.laptop})`]: {
    maxWidth: "190px",
    fontSize: 20,
    padding: "18px 20px",
  },

  [`@media (max-width: ${screenSize.mobileL}), (max-height: 700px)`]: {
    fontSize: 18,
    padding: "8px 10px",
    maxWidth: "140px",
  },
}))

export const QuotesBlock = styled.div(() => ({
  maxWidth: "380px",
  margin: "0 auto",
  padding: 10,
  position: "relative",
  marginBottom: 30,

  [`@media (max-width: ${screenSize.mobileL}), (max-height: 700px)`]: {
    marginBottom: 10,
  },

  [`@media (max-height: 600px)`]: {
    marginBottom: 0,
  },
}))

export const QuotesText = styled.div(() => ({
  fontSize: 32,
  lineHeight: 1.1,
  color: "#494E54",
  textAlign: "center",
  fontWeight: 400,

  [`@media (max-width: ${screenSize.mobileL}), (max-height: 700px)`]: {
    fontSize: 18,
  },
  [`@media (max-height: 600px)`]: {
    fontSize: 16,
  },
}))
export const TextBlock = styled.div(() => ({
  display: "flex",
  flexDirection: "column",
  maxWidth: "940px",
  width: "100%",
  margin: "0 auto",
  fontWeight: 400,
}))

export const TextBox1 = styled.div(() => ({
  padding: "18px 6px 18px 30px",
  position: "relative",
  background: "#D5CDBF50",
  maxWidth: "445px",
  fontWeight: 400,

  [`@media (max-width: ${screenSize.mobileL}), (max-height: 700px)`]: {
    padding: "12px 6px 14px 20px",
  },

  [`@media (max-width: ${screenSize.mobileL}) and (min-height: 700px)`]: {
    marginBottom: 25,
  },

  [`@media (max-width: ${screenSize.tabletSmall})`]: {
    padding: "12px 6px 10px 15px",
    maxWidth: "100%",
    marginBottom: 25,
  },

  [`@media (max-width: ${screenSize.tabletSmall}) and (min-height: 660px)`]: {
    marginBottom: 25,
  }
}))
export const TextBox2 = styled.div(() => ({
  alignSelf: "flex-end",
  padding: "12px 40px 13px 35px",
  position: "relative",
  background: "#93827D50",
  maxWidth: "368px",
  alignSelf: "flex-end",
  textAlign: "right",
  marginBottom: 20,
  fontWeight: 400,

  [`@media (max-width: ${screenSize.mobileL}) and (min-height: 700px)`]: {
    marginBottom: 25,
  },
  [`@media (max-width: ${screenSize.tabletSmall})`]: {
    padding: "12px 6px 10px 15px",
    maxWidth: "100%",
    alignSelf: "flex-start",
    textAlign: "left",
  },
}))
export const TextBox3 = styled.div(() => ({
  padding: "28px 15px 20px 39px",
  position: "relative",
  marginLeft: "50px",
  background: "#CC7F7350",
  maxWidth: "530px",
  fontWeight: 400,

  [`@media (max-width: ${screenSize.mobileL}) and (min-height: 700px)`]: {
    marginBottom: 25,
  },
  [`@media (max-width: ${screenSize.tabletSmall})`]: {
    padding: "12px 6px 10px 15px",
    maxWidth: "100%",
    marginLeft: "0",
  },
}))

export const TextNumber = styled.div`
  font-family: "Silenty";
  font-size: 108px;
  font-style: italic;
  font-weight: 700;
  position: absolute;
  left: ${props => (props.position !== "right" ? 0 : "auto")};
  right: ${props => (props.position !== "right" ? "auto" : 0)};
  top: 0;
  opacity: 0.5;
  transform: ${props => (props.position !== "right" ? "translate(-50%, -50%)" : "translate(60%, -60%)")};
  color: ${props => props.color || "#494E54"};

  @media (max-width: ${screenSize.mobileL}), (max-height: 700px) {
    font-size: 60px;
  }

  @media (max-width: ${screenSize.tabletSmall}) {
    left: 0;
    right: auto;
    transform: translate(-50%, -20%);
  }
`

export const PhylosophyText = styled.div`
  color: #494e54;
  font-weight: 400;

  @media (max-width: ${screenSize.mobileL}), (max-height: 700px) {
    font-size: 14;
    line-height: 1.1;
  }
`

export const PhylosophySecondaryText = styled.div(() => ({
  position: "absolute",
  top: "calc(100% - 12px)",
  left: 30,
  color: "#494E54",
  textDecoration: "underline",
  padding: "2px 4px",
  background: "#E9D0CF50",
  fontWeight: 400,

  [`@media (max-width: ${screenSize.mobileL}), (max-height: 700px)`]: {
    fontSize: 14,
  },
}))

export const QuotesSecondaryTextBlock = styled.div(() => ({
  position: "absolute",
  right: 32,
  bottom: 35,
  zIndex: 2,

  [`@media (max-width: ${screenSize.mobileL}), (max-height: 700px)`]: {
    right: 5,
    bottom: 5,
  },

  [`@media (max-width: ${screenSize.mobileXS}) and (max-height: 600px)`]: {
    right: 0,
  }
}))

export const QuotesSecondaryText = styled.div(() => ({
  padding: 10,
  color: "#494E54",
  fontSize: 22,
  lineHeight: 1.1,
  maxWidth: "366px",
  textAlign: "right",
  fontWeight: 400,

  [`@media (max-width: ${screenSize.mobileL}), (max-height: 700px)`]: {
    fontSize: 16,
    maxWidth: "260px",
  },

  [`@media (max-width: ${screenSize.mobileXS}) and (max-height: 600px)`]: {
    maxWidth: "100%",
    textAlign: "center",
    padding: "0 15px",
    fontSize: 14,
  }
}))

export const IconCommaStart = styled.i(() => ({
  position: "absolute",
  left: 0,
  bottom: "100%",
  color: "#ACA99F50",
  fontSize: 30,

  [`@media (max-width: ${screenSize.tabletSmall}), (max-height: 700px)`]: {
    fontSize: 20,
    bottom: "calc(100% - 10px)",
  }
}))

export const IconCommaEnd = styled.i(() => ({
  position: "absolute",
  right: 0,
  top: "100%",
  transform: "rotate(180deg)",
  color: "#ACA99F50",
  fontSize: 30,
  [`@media (max-width: ${screenSize.tabletSmall}), (max-height: 700px)`]: {
    fontSize: 20,
    top: "calc(100% - 10px)",
  }
}))
export const IconCommaBig = styled.i(() => ({
  position: "absolute",
  color: "#ACA99F50",
  right: 10,
  top: 0,
  fontSize: 60,
  transform: "translateY(-25%)",
  zIndex: -1,
}))
