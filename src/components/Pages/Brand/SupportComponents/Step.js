import { StyledStep } from "../StyledBrand";

export const Step = ({ children, ...style }) => {
  return <StyledStep {...style}>{children}</StyledStep>;
};
