import styled from "styled-components";
import { screenSize } from "../../../constants/media";


export const StyledHeritages = styled.div`

    @media (max-width: ${screenSize.mobileLLandscape}) and (orientation: landscape) {
      width: 15.34%;
    }
`;



export const WrapperSlider = styled.div`
margin-top: 77px;

@media (max-width: ${screenSize.laptopL}) {
  margin-top: 70px;
    }
   

`;
