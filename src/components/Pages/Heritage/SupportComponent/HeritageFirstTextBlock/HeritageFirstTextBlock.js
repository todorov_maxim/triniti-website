import Image from "next/image";
import {
  StyledHeritageFirstTextBlock,
  HImage,
  WrapperHeritageImage,
  WrapperContent,
  HeritageTitle,
  Content,
  HeritageDescription,
  WrapperImageAndLogo,
  WrapperLogo,
  LogoBlock1,
} from "./StyledHeritageFirstTextBlock";
import sevenHils from "../../../../../public/images/Heritage/sevenHils.png";

export const HeritageFirstTextBlock = ({ description, title = "" }) => {
  return (
    <StyledHeritageFirstTextBlock>
      <WrapperImageAndLogo>
        <HImage>
          <WrapperHeritageImage>
            <Image
              src={sevenHils}
              width="298px"
              height="98px"
              layout="responsive"
            />
          </WrapperHeritageImage>
        </HImage>
      <WrapperLogo>
        <Logo namePage="AboutPage"
                fill={"#494E54"}
                mb={0}
              />
              </WrapperLogo>
      </WrapperImageAndLogo>
      <WrapperContent>
        <Content>
          <HeritageTitle color="#9C8D88">{title}</HeritageTitle>
          <HeritageDescription
            dangerouslySetInnerHTML={{ __html: description }}
          ></HeritageDescription>
        </Content>
      </WrapperContent>
    </StyledHeritageFirstTextBlock>
  );
};
