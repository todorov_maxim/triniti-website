import styled from "styled-components";
import { screenSize } from "../../../../../constants/media";
import { ProfileTitle } from "../../../About/SupportComponent/AboutContent/StyledAboutContent";

export const StyledHeritageFirstTextBlock = styled.div`
display: flex;

@media (max-width: ${screenSize.mobileLLandscape}) and (orientation: landscape) {
flex-direction: row-reverse;
    }

    @media (max-width: ${screenSize.mobileL}) and (orientation:portrait ){
padding: 25px 27px 0px 27px;
flex-direction: column;
    }
`;
export const HImage = styled.div`
background: #494E54;
min-height: 454px;
max-width: 444px;
width: 100%;
display: flex;
justify-content: center;
align-items: center;
   @media (max-width: ${screenSize.laptopL}) {
    max-width: 449px;
    }
    @media (max-width: ${screenSize.laptop}) {
    max-width: 362px;
    }
    @media (max-width: ${screenSize.laptop})  and (orientation: portrait) {
  max-width: 444px;
    }
    @media (max-width: ${screenSize.laptopPortrait}) {
      max-width: 304px;
min-height: unset;
height: 100%;
  max-height: 338px;
    }
    @media (max-width: ${screenSize.mobileLLandscape}) and (orientation: landscape){
      max-width: 298px;
min-height: 337px;
padding:12px;
    }
    @media (max-width: ${screenSize.mobileL}) and (orientation:portrait ){
      max-width: 100%;
min-height: 337px;

    }
`;

export const WrapperHeritageImage = styled.div`
width: 298px;
height: 98px;
span {
  width: 100% !important;
  height: 100% !important;
}
@media (max-width: ${screenSize.laptopPortrait}) {
  width: 250px;
height: 82px;
    }
`;



export const WrapperContent = styled.div`
padding-top: 114px;
padding:114px 33px 0px 33px;
width:100%;
display: flex;
align-items: center;
justify-content: center;
@media (max-width: ${screenSize.laptopL}) {
  padding:86px 33px 0px 33px
    }
    @media (max-width: ${screenSize.laptop}) {
      padding-top: 79px;
    }
    @media (max-width: ${screenSize.laptopPortrait}) {
      padding-top: 109px;
    }
    @media (max-width: ${screenSize.mobileLLandscape}) and (orientation: landscape){
      padding-top: 36px;
      padding:36px 40px 0px 30px;
align-items: flex-start;

    }
    @media (max-width: ${screenSize.mobileL}) and (orientation:portrait ){
padding: 46px 0px 0px 0px;
    }
`;


export const Content = styled.div`
max-width: 1196px;
width:100%;
/* min-height: 288px; */
display: flex;
flex-direction: column;
justify-content: space-between;
@media (max-width: ${screenSize.laptopL}) {
  max-width: 798px;

    }
    @media (max-width: ${screenSize.laptop}) {
      max-width: 556px;

    }
    @media (max-width: ${screenSize.laptopPortrait}) {
      max-width: 377px;

    }
    @media (max-width: ${screenSize.mobileL}) and (orientation:portrait ){
      max-width: 100%;
    }
`;


export const HeritageTitle = styled(ProfileTitle)`
margin-left:unset;
max-width: 100%;
@media (max-width: ${screenSize.laptopPortrait}) {
position: unset;
padding:0px;
    }
    @media (max-width: ${screenSize.mobileLLandscape}) and (orientation: landscape) {
      font-size: 40px;
line-height: 50px;
max-width: 340px;
    }
    @media (max-width: ${screenSize.mobileL}) and (orientation:portrait ){
      font-size: 40px;
line-height: 50px;
max-width: 340px;
    }
    
`;

export const HeritageDescription =styled.div`
font-size: 18px;
line-height: 30px;
color: #494E54;
margin-top:45px;

@media (max-width: ${screenSize.laptopL}) {
    }
    @media (max-width: ${screenSize.laptop}) {
      margin-top:32px;
    }
    @media (max-width: ${screenSize.laptopPortrait}) {
      margin-top:21px;
    }
    @media (max-width: ${screenSize.mobileLLandscape}) and (orientation: landscape) {
      font-size: 14px;
line-height: 20px;
margin-top:12px;
    }
    @media (max-width: ${screenSize.mobileL}) and (orientation:portrait ){
      font-size: 18px;
line-height: 26px;
margin-top:12px;
    }
`
 

export const WrapperImageAndLogo = styled.div`
  max-width: 444px;
width:100%;
  display: flex;
          flex-direction: column;
          justify-content: space-between;
    @media (max-width: ${screenSize.laptop}) {
    max-width: 360px;
    }
    @media (max-width: ${screenSize.laptop})  and (orientation: portrait) {
  max-width: 444px;
    }

@media (max-width: ${screenSize.laptopPortrait}) {
  max-width: 304px;
  align-items: center;
  min-height: 543px;
    }
    @media (max-width: ${screenSize.mobileLLandscape}) and (orientation: landscape) {
      max-width: calc( 100% - 382px);
  min-height: unset;
      ;
    }
    @media (max-width: ${screenSize.mobileL}) and (orientation:portrait ){
  min-height: unset;
      max-width: 100%;
min-height: 337px;

    }
`;



export const LogoBlock1 = styled.i`
  display: none;
@media (max-width: ${screenSize.laptopPortrait}) {
  font-size: 116px;
display:block;
&:before{
  color:#9C8D88;
}
    }
    @media (max-width: ${screenSize.mobileLLandscape}) and (orientation: landscape) {
display:none;
    }
    @media (max-width: ${screenSize.mobileL}) and (orientation:portrait ){
display:none;
    }
`;




