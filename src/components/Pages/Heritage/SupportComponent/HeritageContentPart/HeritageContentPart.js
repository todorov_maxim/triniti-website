import { HeritageTitle } from "../HeritageFirstTextBlock/StyledHeritageFirstTextBlock";
import {
  StyledHeritageContentPart,
  HeritageContentTitle,
  HeaderContentPart,
  LogoBlock2,
  WrapperLogo,
  WrapperTextAndImage,
  WrapperText,
  WrapperGalery,
  Adress,
  Description,
  ButtonLink,
  Title,
  StyledPortraitContent,
  WrapperButton,
} from "./StyledHeritageContentPart";
import MiniSlider from "../../../../MiniSlider";
import { compose } from "redux";
import withHardcoreTranslate from "../../../../../hoc/withHardcoreTranslate";
import { Logo } from "../../../../logo";

const HeritageContentPart = ({
  isFirstChild,
  name,
  address,
  description,
  link,
  gallery,
  content,
  showModal,
  currLanguage,
}) => {
  return (
    <>
      <StyledHeritageContentPart isFirstChild={isFirstChild}>
        {isFirstChild && (
          <HeaderContentPart>
            <HeritageContentTitle color={"#9C8D88"}>
              {currLanguage?.projectSevenHills}
            </HeritageContentTitle>
            <WrapperLogo>
            <Logo
                namePage="AboutPage"
                fill={"#494E54"}
                mb={0}
              />
            </WrapperLogo>
          </HeaderContentPart>
        )}
        <WrapperTextAndImage isRow={isFirstChild}>
          <WrapperText isFirstChild={isFirstChild}>
            <Title>{name}</Title>
            <Adress color="#9C8D88">{address}</Adress>
            <Description>{description}</Description>
            <WrapperButton
              isFirstChild={isFirstChild}
              href={link}
              target="_blank"
            >
              <ButtonLink backgroundColor="#9C8D88">
                {currLanguage?.site}
              </ButtonLink>
            </WrapperButton>
          </WrapperText>
          <MiniSlider
            isRight={isFirstChild}
            heritage
            namePage={"heritage"}
            gallery={gallery}
            showModal={() => showModal(gallery)}
          ></MiniSlider>
        </WrapperTextAndImage>
        {/* Только при портретной ориентации показываеться PortraitContent */}
        <PortraitContent
          {...content}
          showModal={() => showModal(gallery)}
          currLanguage={currLanguage}
        />
      </StyledHeritageContentPart>
    </>
  );
};
export default compose(withHardcoreTranslate)(HeritageContentPart);

export const PortraitContent = ({
  isFirstChild,
  name,
  address,
  description,
  btnLink,
  gallery,
  showModal,
  currLanguage,
}) => {
  // при портретной ориентации показываеться
  return (
    <StyledPortraitContent>
      <WrapperTextAndImage portrait isRow={isFirstChild}>
        <WrapperText isFirstChild={isFirstChild}>
          <Title>{name}</Title>
          <Adress color="#9C8D88">{address}</Adress>
          <MiniSlider
            namePage={"heritage"}
            heritage
            gallery={gallery}
            showModal={showModal}
          ></MiniSlider>
          <Description>{description}</Description>
          <WrapperButton href={btnLink} target="_blank">
            <ButtonLink backgroundColor="#9C8D88">
              {currLanguage?.site}
            </ButtonLink>
          </WrapperButton>
        </WrapperText>
      </WrapperTextAndImage>
    </StyledPortraitContent>
  );
};
