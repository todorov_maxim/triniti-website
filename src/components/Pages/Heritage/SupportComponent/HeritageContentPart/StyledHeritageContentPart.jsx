import styled from "styled-components";
import { screenSize } from "../../../../../constants/media";
import { HeritageDescription, HeritageTitle} from "../HeritageFirstTextBlock/StyledHeritageFirstTextBlock";


export const StyledHeritageContentPart= styled.div(({isFirstChild=true})=>({
  padding:isFirstChild? '0px 77px 0px 145px':'0px 160px 0px 77px',
marginBottom: 115,
[`@media (max-width: ${screenSize.laptopL})`]:{
  padding:isFirstChild? '0px 77px 0px 76px':'0px 82px 0px 77px',
},
[`@media (max-width: ${screenSize.laptop})`]:{
  padding:isFirstChild? '0px 77px 0px 33px':'0px 44px 0px 77px',
},
[`@media (max-width: ${screenSize.laptopPortrait})`]:{
  padding:'0px 77px 0px 41px',
marginBottom: 70,
},
[`@media (max-width: ${screenSize.mobileLLandscape}) and (orientation: landscape)`]:{
  padding:isFirstChild? '0px 23px 0px 0px':'0px 0px 0px 20px',
marginBottom: 60,
position: 'relative',
},
[`@media (max-width: ${screenSize.mobileL}) and (orientation: portrait)`]:{
  padding: '0px 0px 0px 0px',
marginBottom: 60,
position: 'relative',
},
}))


export const HeritageContentTitleDef = styled(HeritageTitle)`
max-width: 448px;
margin-top: 70px;
@media (max-width: ${screenSize.laptopL}){
  margin-top: 41px;
};
@media (max-width: ${screenSize.laptop}){
  margin-top: 17px;
};
@media (max-width: ${screenSize.laptopPortrait}){
max-width: 100%;
width:100%;
margin-top: unset;
};
`;
export const HeritageContentTitle = styled(HeritageContentTitleDef)(({color='#9C8D88'})=>({
  color
}))




export const HeaderContentPart = styled.div`
display: flex;
margin-top: 48px;
justify-content: space-between;
margin-bottom:58px;
@media (max-width: ${screenSize.laptopL}){
  margin-top: 43px;
  margin-bottom:40px;
};
@media (max-width: ${screenSize.laptop}){
  margin-top: 65px;
};
@media (max-width: ${screenSize.laptopPortrait}){
  margin-top: 83px;
margin-bottom:33px;
};
@media (max-width: ${screenSize.mobileLLandscape}) and (orientation: landscape){
  padding-left: 30px;
  margin-top: 80px;
margin-bottom:13px;
};
@media (max-width: ${screenSize.mobileL}) and (orientation: portrait){
padding: 0px 27px;
flex-direction: column-reverse;
margin-top: 61px;
};
`;



export const WrapperLogo = styled.div`
max-width: 1169px;
width:100%;
display:flex;
justify-content: center;
align-items: flex-start;
@media (max-width: ${screenSize.laptopPortrait}){
display: none;
};
@media (max-width: ${screenSize.mobileLLandscape})and (orientation: landscape){
  display: flex;
  padding-left: 90px;
};
@media (max-width: ${screenSize.mobileL})and (orientation: portrait){
  display: flex;
};


`;

export const LogoBlock2Def=styled.i(({color})=>({
  "&:before":{
    color: color?color:'black',
  }
}))


export const LogoBlock2 = styled(LogoBlock2Def)`
font-size: 128px;
margin-right:240px;
@media (max-width: ${screenSize.laptopL}){
  margin-right:0px;
     };
     @media (max-width: ${screenSize.mobileLLandscape}) and (orientation: landscape){
      font-size: 96px;
     };
     @media (max-width: ${screenSize.mobileL}) and (orientation: portrait){
      font-size: 90px;
      margin-bottom: 80px;
};
`;


export const WrapperTextAndImage = styled.div(({isRow=true,portrait=false})=>({
display: 'flex',
flexDirection:isRow?'row':'row-reverse',
width: '100%',
justifyContent:'space-between',
alignItems:'center',
[`@media (max-width: ${screenSize.laptopPortrait})`]:{
display: !portrait&&"none",
},
[`@media (max-width: ${screenSize.mobileLLandscape}) and (orientation: landscape)`]:{
  display: portrait?'none':'flex',
  flexDirection:isRow?'row-reverse':'row',
  alignItems:'flex-start',
  paddingBottom:60,
  marginBottom:127,
  },
}))
  

export const WrapperTextDef = styled.div(({isFirstChild=true})=>({
  margin:isFirstChild?'0px 65px 0px 0px':'0px 0px 0px 70px',
  [`@media (max-width: ${screenSize.laptop})`]:{
    margin:isFirstChild?'0px 33px 0px 0px':'0px 0px 0px 42px',
  },
  [`@media (max-width: ${screenSize.laptopPortrait})`]:{
    margin:0,
  },
  }))

export const WrapperText = styled(WrapperTextDef)`
  display: flex;
  flex-direction: column;
  max-width: 384px;
  width: 100%;
  @media (max-width: ${screenSize.laptop}){
    max-width: 364px;

};
  @media (max-width: ${screenSize.laptopPortrait}){
      max-width: 100%;
};
@media (max-width: ${screenSize.mobileLLandscape}) and (orientation: landscape){
  max-width: 273px;
margin-left: 20px;
};
@media (max-width: ${screenSize.mobileL}) and (orientation: portrait){
align-items: center;
};
@media (max-width: 362px){
align-items: unset;
};
  `;

  
  export const WrapperGalery = styled.div`
  span{
    width:100% !important;
    height:100% !important;
  };
max-width: 1169px;
width:100%;
@media (max-width: ${screenSize.laptopL}){
  max-width: 798px;
height: 536px;
     };
     @media (max-width: ${screenSize.laptop}){
      max-width: 516px;
};
@media (max-width: ${screenSize.laptopPortrait}){
      max-width: 650px;
};
@media (max-width: ${screenSize.mobileLLandscape}) and (orientation: landscape) {
      max-width: 360px;
height: 271px;
margin-top:20px;
};
@media (max-width: ${screenSize.mobileL}) and (orientation: portrait) {
      max-width: 360px;
height: 271px;
};
  `;
  export const Title = styled.div`
  text-transform: uppercase;
color: #494E54;
font-size:32px;
line-height: 65px;
letter-spacing: 0.15em;
@media (max-width: ${screenSize.mobileLLandscape}) and (orientation: landscape){
  font-size:24px;
line-height: 55px;

};
@media (max-width: ${screenSize.mobileL}) and (orientation: portrait){
  font-size:24px;
  padding:0px 27px;
};
`;

  
export const AdressDef = styled.div(({color='black'})=>({
  color
}))
  

  export const Adress = styled(AdressDef)`
  font-size: 18px;
  line-height: 30px;
letter-spacing:0.15em;
margin-bottom: 60px;

@media (max-width: ${screenSize.laptopL}){
  margin-bottom: 35px;
}
@media (max-width: ${screenSize.laptopPortrait}) and (orientation: portrait) {
  margin-bottom: 30px;
     };
     @media (max-width: ${screenSize.mobileLLandscape}) and (orientation: landscape){
      margin-bottom: 3px;
      font-size:14px;
};
@media (max-width: ${screenSize.mobileL}) and (orientation: portrait) {
  font-size: 14px;
  padding:0px 27px;
  margin-bottom: 22px;
  
     };
  `;
  

  

  export const Description = styled(HeritageDescription)`
margin-top: unset;
margin-bottom: 60px;
@media (max-width: ${screenSize.laptopL}){
  margin-bottom: 35px;
}
@media (max-width: ${screenSize.laptop}){
  margin-bottom: 38px;
};
@media (max-width: ${screenSize.laptopPortrait}){
margin-top: 30px;
  margin-bottom: 20px;
};
@media (max-width: ${screenSize.mobileLLandscape}) and (orientation: landscape){
  margin-top: unset;
  margin-bottom: unset;
};
@media (max-width: ${screenSize.mobileL}) and (orientation: portrait){
padding: 0px 27px;
margin-top: 60px;
  margin-bottom: 34px;
};
  `;

  export const ButtonLinkDef = styled.button(({isClick=false,backgroundColor='#CD8B7D'})=>({
  backgroundColor,
  '&:hover':{
    backgroundColor:'#494E54',
  }
}))
  
  
  export const ButtonLink = styled(ButtonLinkDef)`
  letter-spacing: 0.5em;
padding:24px 15px 20px 15px;
max-width:300px;
width:100%;
border-radius: 10px;
color:white;
text-transform: uppercase;
@media (max-width: ${screenSize.laptopPortrait}){
  padding:20px 15px 16px 15px;
};
@media (max-width: ${screenSize.mobileLLandscape}) and (orientation: landscape)  {
width:300px;

};
`;


export const WrapperButtonDef = styled.a(({isFirstChild})=>({
  // [`@media (max-width: ${screenSize.mobileLLandscape}) and (orientation: landscape)`]:{
  //   left: isFirstChild?'50%':'unset',
  //   right:!isFirstChild?'0%':'unset',
  //   transform:!isFirstChild? 'translateX(50%)':'translateX(-50%)',
  // },

}))

export const WrapperButton = styled(WrapperButtonDef)`
display: flex;
justify-content: center;
width:300px;
margin:0 auto;
@media (max-width: ${screenSize.mobileLLandscape}) and (orientation: landscape)  {
position: absolute;
bottom: -48px;
left: 50%;
transform:translateX(-50%);
};
@media (max-width: ${screenSize.mobileL}) and (orientation: portrait){
  max-width:300px;
width:100%;
};
@media (max-width: 362px){
  padding: 0px 27px;
};
`;




export const StyledPortraitContent = styled.div`
display: none;
@media (max-width: ${screenSize.laptopPortrait}){
  display: block;
};
width: 100%;
height: 100%;
`;


