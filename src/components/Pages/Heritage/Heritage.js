import { FullScreenBlockGeneralPages } from "../../FullScreenBlockGeneralPages/FullScreenBlockGeneralPages";
import HeritageContentPart from "./SupportComponent/HeritageContentPart/HeritageContentPart";
import { HeritageFirstTextBlock } from "./SupportComponent/HeritageFirstTextBlock/HeritageFirstTextBlock";
import { Modal } from "../../Modal/Modal";
import { useState } from "react";
import { pageTheme } from "../../../constants/themes";
import MiniSlider from "../../MiniSlider";
import Footer from "../../Footer";
import { onOfScroll } from "../../../hooks/useOfScroll";
import Slider from "../../Slider";
import { WrapperSlider } from "./StyledHeritage";

export const Heritage = ({ pageData, showMenu, hiddenMenu }) => {
  const { background, textColor } = pageTheme[pageData?.pageTheme] || pageTheme.brown;
  const { projects } = pageData;
  const [isShowModal, setIsShowModal] = useState(false);
  const [selectedGalery, setSelectedGalery] = useState([]);
  const showModal = (galery) => {
    onOfScroll(true);
    hiddenMenu();
    setSelectedGalery(galery);
    setIsShowModal(true);
  };
  const modalHidden = () => {
    onOfScroll(false);
    showMenu();
    setIsShowModal(false);
    setSelectedGalery([]);
  };

  return (
    <>
      {isShowModal && (
        <Modal modalHidden={modalHidden}>
          <MiniSlider namePage={"modal"} modal gallery={selectedGalery} />
        </Modal>
      )}

      <FullScreenBlockGeneralPages pageData={pageData} />
      <HeritageFirstTextBlock {...pageData?.notes} />
      <WrapperSlider>
        <Slider reverse textColor={textColor} sliderData={pageData?.profilesSlider} pageData={pageData} />
      </WrapperSlider>

      <>
        {projects.map((content, ind) => {
          return (
            <HeritageContentPart
              textColor={textColor}
              isFirstChild={ind % 2 == 0}
              {...content}
              content={content}
              showModal={showModal}
            />
          );
        })}
      </>
      <Footer pageData={pageData} />
    </>
  );
};
