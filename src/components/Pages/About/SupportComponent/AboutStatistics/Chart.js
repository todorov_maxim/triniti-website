import {
  chartItemText,
  chartItemValue,
  PieWrapper,
} from "./StyledAboutStatistics";
import { useState, useEffect } from "react";
import Highcharts from "highcharts";
// import HighchartsExporting from "highcharts/modules/exporting";
import variablePie from "highcharts/modules/variable-pie.js";
import HighchartsReact from "highcharts-react-official";
import {useRef} from 'react';
import {useIntersection} from './../../../../../hooks/useIntersection';

export const AboutChart = ({ pageData }) => {

  const chartRef = useRef();
  const inViewport = useIntersection(chartRef, "0px");

  const {
    statistics: { diagram },
  } = pageData;

  const [chartParams, setChartParams] = useState({
    pieSize: "540px",
    innerSize: "10%",
    labelsDistance: 10,
    alignTo: "connectors"
  });

  if (typeof Highcharts === "object") {
    variablePie(Highcharts);
  }

  const getWindowWidth = () => {
    if (window.innerWidth < 380) {
      setChartParams({
        pieSize: "210px",
        innerSize: "10%",
        labelsDistance: 80,
        alignTo: "connectors"
      });
    } else if (window.innerWidth <= 500) {
      setChartParams({
        pieSize: "250px",
        innerSize: "10%",
        labelsDistance: 100,
        alignTo: "connectors"
      });
    } else if (window.innerWidth <= 755) {
      setChartParams({
        pieSize: "340px",
        innerSize: "10%",
        labelsDistance: 60,
        alignTo: "connectors"
      });
    } else if (window.innerWidth <= 768) {
      setChartParams({
        pieSize: "400px",
        innerSize: "10%",
        labelsDistance: 50,
        alignTo: "connectors"
      });
    } else if (window.innerWidth <= 992) {
      setChartParams({
        pieSize: "340px",
        innerSize: "10%",
        labelsDistance: 100,
        alignTo: "connectors"
      });
    } else if (window.innerWidth <= 1200) {
      setChartParams({
        pieSize: "420px",
        innerSize: "10%",
        labelsDistance: 50,
        alignTo: "toPlotEdges"
      });
    } else {
      setChartParams({
        pieSize: "480px",
        innerSize: "10%",
        labelsDistance: 50,
        alignTo: "toPlotEdges"
      });
    }
  };

  useEffect(() => {
    getWindowWidth();
    window.addEventListener(
      `resize`,
      (event) => {
        getWindowWidth();
      },
      false
    );
  }, []);


  const pieLabelsDataSizes = [
    {
      y: 1,
      z: 0.7,
    },
    {
      y: 2,
      z: 1,
    },
    {
      y: 2,
      z: 1.2,
    },
    {
      y: 6.4,
      z: 0.8,
    },
    {
      y: 2,
      z: 1.1,
    },
    {
      y: 3,
      z: 0.75,
    },
    {
      y: 1.1,
      z: 1.2,
    },
    {
      y: 4,
      z: 1,
    },
    {
      y: 2,
      z: 0.9,
    },
    {
      y: 5,
      z: 0.7,
    },
    {
      y: 3,
      z: 1,
    },
    {
      y: 0.5,
      z: 0.85,
    },
    {
      y: 4,
      z: 0.7,
    },
    {
      y: 2,
      z: 0.7,
    },
  ];

  const pieLabelsData = diagram.map((item, index) => {
    return {
      name: `${item.value} ${item.caption}`,
      dataValue: item.value,
      dataTitle: item.caption,
      // ...pieLabelsDataSizes[index],
      y: item.value,
      z: pieLabelsDataSizes[index].z,
    };
  });


  const options = {
    chart: {
      type: "variablepie",
      size: "100%",
    },
    title: {
      text: null,
    },
    subtitle: {
      text: null,
    },
    credits: {
      enabled: false,
    },
    hover: { mode: null },
    states: {
      hover: {
        filter: {
          type: "none",
        },
      },
    },
    plotOptions: {
      pie: {
        allowPointSelect: false,
        center: ["50%", "50%"],
        dataLabels: {
          enabled: false,
        },
        hover: {
          enabled: false,
        },
      },
      series: {
        states: {
          hover: {
            enabled: false,
          },
        },
      },
    },
    tooltip: { enabled: false },
    colors: [
      "#CD8B7D",
      "#9C8D88",
      "#EAD6D5",
      "#CD8B7D",
      "#9C8D88",
      "#494E54",
      "#D9D3C5",
      "#EAD6D5",
      "#CD8B7D",
      "#9C8D88",
      "#494E54",
      "#9C8D88",
      "#EAD6D5",
    ],
    series: [
      {
        startAngle: 15,
        minPointSize: 0,
        borderWidth: 0,
        showInLegend: false,
        innerSize: chartParams.innerSize,
        size: chartParams.pieSize,
        zMin: 0,
        zMax: 2,
        dataLabels: {
          alignTo: chartParams.alignTo,
          connectorShape: "straight",
          distance: chartParams.labelsDistance,
          enabled: true,
          formatter() {
            const text = this.point.dataTitle.split(" ").join("<br/>");
            return `<span class="highcharts-label-box">${text}</span>`;
          },
        },
        data: pieLabelsData,
      },
    ],
  };

  return (
    <PieWrapper ref={chartRef}>
      {/* <Doughnut data={data} width={"100%"} options={pieOptions} /> */}
      {/* <Pie data={data} width={"100%"} options={pieOptions} /> */}
      {inViewport && <HighchartsReact
        highcharts={Highcharts}
        options={options}
        containerProps={{ style: { height: "100%", flex: "1 1 auto" } }}
      />}
    </PieWrapper>
  );
};
