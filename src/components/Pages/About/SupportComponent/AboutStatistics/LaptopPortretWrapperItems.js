import {
  ListItems,
  StyledLaptopPortretWrapperItems,
  Part,
} from "./StyledAboutStatistics";

export const LaptopPortretWrapperItems = ({ list }) => {
  return (
    <StyledLaptopPortretWrapperItems>
      <Part>
        {list.points.map(({ item }, i, arr) => {
          return (
            <>{i < arr.length / 2 && <ListItems key={i}>{item}</ListItems>}</>
          );
        })}
      </Part>
      <Part>
        {list.points.map(({ item }, i, arr) => {
          return (
            <>{i >= arr.length / 2 && <ListItems key={i}>{item}</ListItems>}</>
          );
        })}
      </Part>
    </StyledLaptopPortretWrapperItems>
  );
};
