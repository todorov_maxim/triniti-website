import { AboutChart } from "./Chart";
import {useRef} from 'react';
import {useIntersection} from './../../../../../hooks/useIntersection';
import { LaptopPortretWrapperItems } from "./LaptopPortretWrapperItems";
import {
  StyledAboutStatistics,
  ListTitle,
  ListItems,
  Diagramma,
  NumberOfProjects,
  Year,
  ProjectMap,
  WrapperListAndDiagramma,
  List,
  ContainProjectMap,
  WrapperItems,
  IconRomb,
  Wrapper,
} from "./StyledAboutStatistics";


export const AboutStatistics = ({ pageData }) => {
  const {
    statistics: { list, roadmap },
  } = pageData;

  const roadmapRef = useRef();
  const inViewport = useIntersection(roadmapRef, "0px");

  return (
    <StyledAboutStatistics>
      <WrapperListAndDiagramma>
        <List>
          <ListTitle>{list?.title}</ListTitle>
          {/* LaptopPortretWrapperItems когда портретный режим показываю его делящтй список на 2 части  */}
          <LaptopPortretWrapperItems list={list} />
          {/* WrapperItems до портретного покказываеться этот блок потом скрываю его */}
          <WrapperItems>
            {list.points.map(({ item }, i) => (
              <ListItems key={i}>{item}</ListItems>
            ))}
          </WrapperItems>
        </List>
        <AboutChart pageData={pageData} />
      </WrapperListAndDiagramma>
      <ProjectMap ref={roadmapRef} isVisible={inViewport} itemsCount={roadmap.length}>
        {roadmap.map(({ caption, value, active }, i, arr) => {
          const lastChild = arr.length - 1;
          return (
            <ContainProjectMap
              active={active}
              key={i}
              lastChild={i == lastChild}
              isVisible={inViewport}
              orderNumber={i}
            >
              <Year active={active} lastChild={i == lastChild}>
                {value}
              </Year>
              <NumberOfProjects active={active} lastChild={i == lastChild}>
                {caption}
              </NumberOfProjects>
            </ContainProjectMap>
          );
        })}
      </ProjectMap>
    </StyledAboutStatistics>
  );
};
