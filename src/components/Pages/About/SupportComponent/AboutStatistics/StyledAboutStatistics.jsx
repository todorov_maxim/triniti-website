import styled from "styled-components";
import { screenSize } from "../../../../../constants/media";

export const StyledAboutStatistics = styled.div`
  height: max-content;
  display: flex;
  flex-direction: column;
  max-width: 1469px;
  min-height: 882px;
  justify-content: space-between;
  margin: 0 auto;
  margin-top: 132px;
  @media (max-width: ${screenSize.laptopL}) {
    margin-top: 101px;
  }
  @media (max-width: ${screenSize.laptop}) {
    margin-top: 54px;
  }
  @media (max-width: ${screenSize.laptopPortrait}) and (orientation: portrait) {
    padding: 0px 36px;
    margin-top: 65px;
  }
  @media (max-width: ${screenSize.mobileL}) and (orientation: portrait) {
    padding: 0px 0px;
    margin-top: 55px;
  }
  @media (max-width: ${screenSize.mobileLLandscape}) and (orientation: landscape) {
    padding: 0px 0px;
    margin-top: 47px;
  }
`;

export const ListTitle = styled.div`
  font-size: 32px;
  line-height: 42px;
  letter-spacing: 0.15em;
  text-transform: uppercase;
  margin-bottom: 35px;
  @media (max-width: ${screenSize.mobileL}) and (orientation: portrait) {
    font-size: 24px;
    line-height: 32px;
    margin-bottom: 13px;
  }
  @media (max-width: ${screenSize.mobileLLandscape}) and (orientation: landscape) {
    font-size: 24px;
    line-height: 32px;
    margin-bottom: 15px;
  }
`;

export const WrapperItems = styled.div`
  @media (max-width: ${screenSize.laptopPortrait}) and (orientation: portrait) {
    display: none;
  }

  @media (max-width: ${screenSize.mobileL}) and (orientation: portrait) {
    display: flex;
    flex-direction: column;
  }

  @media (max-width: ${screenSize.mobileLLandscape}) and (orientation: landscape) {
    display: none;
  }
  @media (max-width: 550px) and (orientation: landscape) {
    display: flex;
    flex-direction: column;
  }
`;

export const StyledLaptopPortretWrapperItems = styled.div`
  display: flex;
  justify-content: space-between;
  display: none;

  @media (max-width: ${screenSize.laptopPortrait}) and (orientation: portrait) {
    display: flex;
  }

  @media (max-width: ${screenSize.mobileL}) and (orientation: portrait) {
    display: none;
  }

  @media (max-width: ${screenSize.mobileLLandscape}) and (orientation: landscape) {
    display: flex;
  }
  @media (max-width: 550px) and (orientation: landscape) {
    display: none;
  }
`;

export const Part = styled.div`
  display: flex;
  flex-direction: column;
  /* justify-content: space-between; */
  @media (max-width: ${screenSize.laptopPortrait}) and (orientation: portrait) {
    min-height: 160px;
  }

  @media (max-width: ${screenSize.mobileLLandscape}) and (orientation: landscape) {
    min-height: 128px;
  }
`;


export const Wrapper = styled.div`
display: flex;

`;

export const ListItems = styled.div`
  font-size: 24px;
  line-height: 1.4em;
  margin-bottom: 24px;
  position: relative;
  padding-left: 20px;
  &::before{
    content:'';
    position: absolute;
    left: 0px;
    top: 50%;
transform: translateY(-50%) rotate(45deg);
    width: 10px;
    height: 10px;
    border: solid 1px #9C8D88;
  }

  @media (max-width: ${screenSize.laptop}) {
    font-size: 18px;
    line-height: 1.4em;
  margin-bottom: 28px;
  }
  @media (max-width: ${screenSize.mobileL}) and (orientation: portrait) {
    line-height: 1.4em;
  margin-bottom: 22px;
  }
  @media (max-width: ${screenSize.mobileLLandscape}) and (orientation: landscape) {
    line-height: 1.4em;
  margin-bottom: 22px;
  }
`;

export const IconRomb = styled.i`
  font-size: 10px;
`;



export const Diagramma = styled.div`
  max-width: 740px;
  width: 100%;
  background: gainsboro;
  min-height: 720px;
  @media (max-width: ${screenSize.laptop}) {
    max-width: 547px;
    max-height: 598px;
  }

  @media (max-width: ${screenSize.laptopPortrait}) and (orientation: portrait) {
    max-width: 658px;
    height: 598px;
    margin-top: 37px;
  }
  @media (max-width: ${screenSize.mobileL}) and (orientation: portrait) {
    margin-top: 42px;
    padding: 0px 6px;
  }
  @media (max-width: ${screenSize.mobileLLandscape}) and (orientation: landscape) {
    max-width: 404px;
    height: 400px;
    margin-top: 37px;
  }
`;

export const NumberOfProjectsDef = styled.div`
  font-size: 14px;
  line-height: 16px;
  letter-spacing: 0.15em;
  text-align: center;
  position: absolute;
  top: calc(100% + 20px);
  transform: translateX(-50%);
  left: 50%;
  width: max-content;
  max-width: 116px;
  @media (max-width: ${screenSize.mobileL}) {
    max-width: 90px;
    font-size: 10px;
    line-height: 12px;
}

  @media (max-width: ${screenSize.mobileL}) and (orientation: portrait) {
  
    font-size: 10px;
    line-height: 12px;
  max-width: 101px;
  

  }
  @media (max-width: 574px)  {
    max-width: 80px;
    font-size: 10px;
    line-height: 12px;
  }
`;

export const NumberOfProjects = styled(NumberOfProjectsDef)(
  ({ active, lastChild }) => ({
    color: active ? "#5A3E3D" : lastChild ? "#D9D3C5" : "#494E54",
  })
);

export const YearDefault = styled.div`
  position: absolute;
  bottom: calc(100% + 20px);
  font-size: 18px;
  line-height: 20px;
  text-align: center;
  max-width: 116px;
  left: 50%;
  transform: translateX(-50%);
  @media (max-width: ${screenSize.mobileL}) and (orientation: portrait) {
    font-size: 14px;
    line-height: 16px;
  }
  @media (max-width: ${screenSize.mobileLLandscape}) and (orientation: landscape) {
    font-size: 14px;
    line-height: 16px;
  }
  

`;

export const Year = styled(YearDefault)(({ active, lastChild }) => ({
  color: active ? "#CD8B7D" : lastChild ? "#D9D3C5" : "#494E54",
}));

// ProjectMap

const ProjectMapLineAnimation = styled.div(({isVisible, itemsCount}) => ({
  '&:before': {
    content: "''",
    width: isVisible ? "calc(100% - 45px * 2)" : 0,
    margin: "0 auto",
    height: "2px",
    background: "#D9D3C5",
    position: "absolute",
    left: 45,
    top: "50%",
    zIndex: "-1",
    transform: "translateY(-50%)",
    transition: isVisible ? `width ${itemsCount - 1}s 1s linear` : 'none'
  },

  [`@media (max-width: ${screenSize.mobileL}) `] : {
    '&:before': {
      width: 'calc(100% - 40px * 2)',
      left: 40,
      transition: "none"
    },
  }
}))

export const ProjectMap = styled(ProjectMapLineAnimation)`
  width: 100%;
  display: flex;
  justify-content: space-between;
  position: relative;
  z-index: 2;
  padding: 147px 45px;
  

  @media (max-width: ${screenSize.laptopL}) {
    margin: 0 auto;
    max-width: 1100px;
    
  }
  @media (max-width: ${screenSize.laptop}) {
    max-width: 911px;
    overflow: hidden;
  }

  @media (max-width: ${screenSize.laptopPortrait}) {
    max-width: 759px;
    padding-top: 80px;
    padding-bottom: 80px;
    margin-bottom: 40px;

  }

  @media (max-width: ${screenSize.mobileL}) {
    padding: 70px 40px;
    margin-bottom: 20px;
  }
`;
// ProjectMap

export const WrapperListAndDiagramma = styled.div`
  display: flex;
  justify-content: space-between;
  align-items: center;
  padding: 10px 10px 40px 10px;
  color: #494E54;
  min-height: 607px;
  max-width: 1321px;
  margin: 0 auto;
  width: 100%;

  @media (max-width: ${screenSize.laptopPortrait}) and (orientation: portrait) {
    flex-direction: column;
    align-items: center;
    padding: unset;
  }
  @media (max-width: ${screenSize.mobileLLandscape}) and (orientation: landscape) {
    flex-direction: column;
    align-items: center;
    padding: unset;
  }
`;

export const List = styled.div`
  display: flex;
  flex-direction: column;
  min-height: 440px;
  max-width: 417px;
  @media (max-width: ${screenSize.laptopL}) {
    max-width: 402px;
  }
  @media (max-width: ${screenSize.laptop}) {
padding-left: 54px;
  }
  @media (max-width: ${screenSize.laptopPortrait}) and (orientation: portrait) {
    width: 100%;
    max-width: 100%;
    min-height: 218px;
padding-left: unset;

  }

  @media (max-width: 992px) {
    max-width: 320px;
  }

  @media (max-width: 768px) {
    max-width: 100%;
  }
  @media (max-width: 768px) and (orientation: landscape) {
    max-width: 320px;
  }

  @media (max-width: ${screenSize.mobileL}) and (orientation: portrait) {
    padding: 0px 27px;
  }

  @media (max-width: ${screenSize.mobileLLandscape}) and (orientation: landscape) {
    min-height: 205px;
    width: 100%;
    max-width: 100%;
    padding: 0px 27px 0px 43px;
  }
`;

export const ContainProjectMapDef = styled.div`
  display: block;
  position: relative;
  &:before {
    content: "";
    display: block;
    width: 20px;
    height: 20px;
    background: #494e54;
    border-radius: 100%;
  }
  &:after {
    content: "";
    width: 38px;
    height: 38px;
    border: 2px solid #cd8b7d;
    top: 50%;
    left: 50%;
    transform: translate(-50%, -50%);
    border-radius: 100%;
    position: absolute;
  }
  @media (max-width: ${screenSize.mobileL}) and (orientation: portrait) {
    &:before {
      content: "";
      width: 12px;
      height: 12px;
    }
    &:after {
      width: 24.5px;
      height: 24.5px;
    }
  }

  @media (max-width: ${screenSize.mobileLLandscape}) and (orientation: landscape) {
    &:before {
      content: "";
      width: 12px;
      height: 12px;
    }
    &:after {
      width: 24.5px;
      height: 24.5px;
    }
  }
`;

export const ContainProjectMap = styled(ContainProjectMapDef)(
  ({ active, lastChild, isVisible, orderNumber }) => ({
    opacity: isVisible ? 1 : 0,
    transition: isVisible ? `opacity .3s ${orderNumber + 1}s` : "none",
    
    "&:before": {
      background: active ? "#CD8B7D" : lastChild ? "#D9D3C5" : "#494E54",
    },
    "&:after": {
      display: active ? "block" : "none",
    },

    [`@media (max-width: ${screenSize.mobileL}) `] : {
      opacity: 1,
      transition: "none",
    }
  })
);

export const PieWrapper = styled.div`
  flex: 0 0 auto;
  max-width: calc(100% - 417px);
  width: 100%;
  padding: 0 20px;
  align-self: stretch;
  pointer-events: none;
  position: relative;

  &:before {
    content: "";
    position: absolute;
    left: 50%;
    top: 50%;
    transform: translate(-50%, -50%);
    width: 220px;
    height: 220px;
    border-radius: 100%;
    background: #cd8b7d;
    mix-blend-mode: multiply;
    opacity: 0.25;
    z-index: 2;
  }

  @media (max-width: ${screenSize.laptopL}) {
    max-width: calc(100% - 402px);
    padding: 0;
  }

  @media (max-width: 1200px) {
    &:before {
      width: 190px;
      height: 190px;
    }
  }
  @media (max-width: 992px) {
    max-width: calc(100% - 320px);
    &:before {
      width: 150px;
      height: 150px;
    }
  }
  @media (max-width: 768px) {
    max-width: 100%;
    padding: 0 5px;
    &:before {
      width: 170px;
      height: 170px;
    }
  }
  @media (max-width: 768px) and (orientation: landscape)  {
    max-width: calc(100% - 320px);

  }
  @media (max-width: ${screenSize.mobileL}) {
  max-width: 100%;

    height: 500px;
    display: flex;
    &:before {
      width: 120px;
      height: 120px;
    }
  }

  @media (max-width: 380px) {
    height: 460px;
    &:before {
      width: 100px;
      height: 100px;
    }
  }
`;
