import styled, {css} from "styled-components"
import {screenSize} from "../../../../../constants/media"

export const StyledAboutContentDef = styled.div(({realEstateSales}) => ({
  marginTop: realEstateSales ? 102 : 143,
  // [`@media (max-width: ${screenSize.mobileL}) and (orientation: portrait)`]: {
  //   padding: 'unset',
  // paddingTop:realEstateSales?100:140,
  //   },
}))

export const StyledAboutContent = styled(StyledAboutContentDef)`
  height: max-content;
  display: flex;
  width: 100%;
  justify-content: center;

  @media (max-width: ${screenSize.laptopL}) {
    margin-top: 71px;
    /* justify-content: center; */
  }

  @media (max-width: ${screenSize.laptopL}) {
    margin-top: 71px;
    justify-content: flex-end;
  }
  @media (max-width: ${screenSize.laptopPortrait}) and (orientation: portrait) {
    margin-top: unset;
    padding: 0px 34px;
    padding-top: 201px;
    position: relative;
    justify-content: center;
  }
  @media (max-width: ${screenSize.mobileL}) and (orientation: portrait) {
    padding: unset;
    padding-top: 175px;
  }
`

export const AboutProfile = styled.div`
  display: flex;
  width: 100%;
  max-width: 1582px;

  @media (max-width: ${screenSize.laptopL}) {
    /* justify-content: flex-end; */
    max-width: 1316px;
    justify-content: flex-end;
  }
  @media (max-width: ${screenSize.laptop}) {
    max-width: 990px;
    justify-content: flex-end;
  }

  @media (max-width: ${screenSize.laptopPortrait}) and (orientation: portrait) {
    flex-direction: column;
    align-items: center;
    max-width: 697px;
  }
`

export const WrapperImg = styled.div`
  max-width: 648px;
  width: 100%;
  @media (max-width: ${screenSize.laptopL}) {
    height: 680px;
    max-width: 610px;
    span {
      height: 100% !important;
    }
  }
  @media (max-width: ${screenSize.laptop}) {
    height: 768px;
    max-width: 534px;
    span {
      height: 100% !important;
    }
  }

  @media (min-width: 769px) and (max-width: 930px) {
    height: 476px;
    max-width: 376px;
  }
  @media (max-width: ${screenSize.laptopPortrait}) and (orientation: portrait) {
    height: 504px;
    /* height: max-content; */
    max-width: 697px;
  }
  @media (max-width: ${screenSize.laptopPortrait}) and (orientation: landscape) {
    /* height: max-content; */
    max-width: 450px;
  }

  @media (max-width: ${screenSize.mobileL}) and (orientation: portrait) {
    height: 330px;
    max-width: 300px;
  }
  @media (min-width: 361px) and (max-width: 756px) and (orientation: portrait) {
    height: max-content;
    max-width: 100%;
  }
  @media (max-width: ${screenSize.mobileLLandscape}) and (orientation: landscape) {
    height: 452px;
    max-width: 314px;
    z-index: 2;
  }
  @media (min-width: 321px) and (max-width: 630px) and (orientation: landscape) {
    max-width: 50%;
    img {
      object-fit: cover;
    }
  }
`

export const ProfileContent = styled.div`
  max-width: calc(100% - 712px);
  width: 100%;
  display: flex;
  flex-direction: column;
  padding-top: 5px;
  align-items: center;
  position: relative;
  padding-bottom: 235px;
  justify-content: space-between;
  @media (max-width: ${screenSize.laptopL}) {
    padding-bottom: 205px;
  }
  @media (max-width: ${screenSize.laptopL}) {
    padding-left: 8px;
    padding-top: unset;
  }
  @media (max-width: ${screenSize.laptop}) {
    max-width: calc(100% - 534px);
    padding-top: 55px;
  }

  @media (max-width: ${screenSize.laptopPortrait}) and (orientation: portrait) {
    max-width: unset;
    position: unset;
    margin-top: 84.5px;
    padding: unset;
  }
  @media (max-width: ${screenSize.laptopPortrait}) and (orientation: landscape) {
    max-width: calc(100% - 450px);
  }

  @media (min-width: 769px) and (max-width: 930px) {
    max-width: calc(100% - 376px);
    padding-top: unset;
    padding-bottom: 140px;
  }

  @media (max-width: ${screenSize.mobileL}) and (orientation: portrait) {
    margin-top: 64px;
  }
  @media (max-width: ${screenSize.mobileLLandscape}) and (orientation: landscape) {
    max-width: calc(100% - 314px);
    padding-top: unset;
    padding: 0px 5px;
    padding-bottom: 126px;
  }
  @media (min-width: 321px) and (max-width: 630px) and (orientation: landscape) {
    max-width: 50%;
  }
`

export const ProfileTitleDef = styled.div(({color}) => ({
  color: color ? color : "#D7D1C5",
}))

export const ProfileTitle = styled(ProfileTitleDef)`
  text-transform: uppercase;

  max-width: 662px;
  width: 100%;
  font-size: 60px;
  line-height: 65px;
  opacity: 0.5;
  margin-left: 46px;
  @media (max-width: ${screenSize.laptopL}) {
    max-width: 455px;
    margin-left: 10px;
    font-size: 50px;
    line-height: 55px;
  }

  @media (max-height: 770px) {
    font-size: 46px;
    line-height: 52px;
  }

  @media (max-width: ${screenSize.laptop}) {
    font-size: 50px;
    line-height: 65px;
    max-width: 100%;
  }

  @media (max-width: ${screenSize.laptopPortrait}) and (orientation: portrait) {
    position: absolute;
    top: 72px;
    left: 0px;
    font-size: 40px;
    margin-left: unset;
    padding: 0px 34px;
    line-height: 50px;
  }

  @media (max-width: ${screenSize.laptopPortrait}) and (orientation: landscape) {
    font-size: 40px;
  }

  @media (max-width: ${screenSize.mobileL}) and (orientation: portrait) {
    top: 44px;

    font-size: 32px;
    line-height: 40px;
  }
  @media (min-width: 769px) and (max-width: 930px) {
    font-size: 44px;
    line-height: 57px;
  }

  @media (max-width: ${screenSize.mobileLLandscape}) and (orientation: landscape) {
    font-size: 32px;
    line-height: 40px;
    max-width: 300px;
  }

  @media (min-width: 321px) and (max-width: 630px) and (orientation: landscape) {
    font-size: 25px;
    line-height: 33px;
    text-align: center;
  }
`

export const WrapperNameAndDescription = styled.div`
  width: 100%;
  position: relative;
  flex-direction: column;
  display: flex;
  align-items: center;
  margin-bottom: 30px;
  @media (max-width: ${screenSize.laptopL}) {
    max-width: 455px;
  }

  @media (max-width: ${screenSize.laptop}) {
    max-width: 351px;
    margin-bottom: 65px;
  }

  @media (max-width: ${screenSize.laptopPortrait}) and (orientation: portrait) {
    margin-bottom: 43px;
    max-width: unset;
  }
  @media (min-width: 769px) and (max-width: 930px) {
    margin-bottom: 30px;
  }
  @media (max-width: ${screenSize.mobileL}) and (orientation: portrait) {
    padding: 0px 27px;
  }

  @media (max-width: ${screenSize.mobileLLandscape}) and (orientation: landscape) {
    max-width: 290px;
    margin-bottom: 46px;
    margin-left: 10px;
  }
  @media (min-width: 321px) and (max-width: 630px) and (orientation: landscape) {
    margin-bottom: 10px;
    min-height: 220px;
  }
`

export const ProfileIconComma = styled.i(({color = "#D7D1C5"}) => ({
  top: -51,
  left: 103,
  position: "absolute",
  fontSize: 112,
  zIndex: 1,
  opacity: "0.3",
  color: color ? color : "#D7D1C5",
  [`@media (max-width: ${screenSize.laptopL})`]: {
    left: -26,
    top: -28,
    fontSize: 80,
  },
  [`@media (max-width: ${screenSize.laptop})`]: {
    left: -26,
  },
  [`@media (max-width: ${screenSize.laptopPortrait}) and (orientation: portrait)`]: {
    left: 54,
    top: -71,
  },
  [`@media (max-width: ${screenSize.mobileL}) and (orientation: portrait)`]: {
    left: -8,
    top: -46,
    fontSize: 88,
  },
  [`@media (max-width: ${screenSize.mobileLLandscape}) and (orientation: landscape)`]: {
    left: -32,
    top: -46,
    fontSize: 88,
  },
  [`@media (min-width: 321px) and (max-width: 630px) and (orientation: landscape)`]: {
    left: -20,
    top: -20,
    fontSize: 60,
  },
}))

export const ProfileName = styled.div`
  z-index: 3;
  width: 100%;
  font-size: 32px;
  line-height: 42px;
  color: #494e54;
  letter-spacing: 0.15em;
  text-transform: uppercase;
  max-width: 600px;
  margin-bottom: 38px;
  @media (max-width: ${screenSize.laptopL}) {
    font-size: 28px;
    line-height: 34px;
    margin-bottom: 26px;
  }
  @media (max-width: ${screenSize.laptopPortrait}) and (orientation: portrait) {
    margin-bottom: 16px;
    max-width: 632px;
  }

  @media (max-width: ${screenSize.mobileL}) and (orientation: portrait) {
    font-size: 24px;
    line-height: 42px;
  }
  @media (min-width: 769px) and (max-width: 930px) {
    font-size: 29px;
    line-height: 39px;
    margin-bottom: 16px;
  }
  @media (max-width: ${screenSize.mobileLLandscape}) and (orientation: landscape) {
    font-size: 24px;
    line-height: 42px;
    margin-bottom: 16px;
  }

  @media (min-width: 321px) and (max-width: 630px) and (orientation: landscape) {
    font-size: 20px;
    line-height: 38px;
    margin-bottom: 20px;
  }
`

export const ProfileDescription = styled.div`
  width: 100%;
  font-size: 24px;
  line-height: 30px;
  max-width: 600px;
  color: #494e54;
  @media (max-width: ${screenSize.laptopL}) {
    font-size: 18px;
    line-height: 24px;
  }
  @media (max-width: ${screenSize.laptopPortrait}) and (orientation: portrait) {
    max-width: 632px;
  }
  @media (max-width: ${screenSize.mobileL}) and (orientation: portrait) {
    font-size: 16px;
    line-height: 20px;
  }
  @media (min-width: 769px) and (max-width: 930px) {
    font-size: 16px;
    line-height: 24px;
  }
  @media (max-width: ${screenSize.mobileLLandscape}) and (orientation: landscape) {
    font-size: 16px;
    line-height: 20px;
  }
  @media (min-width: 321px) and (max-width: 630px) and (orientation: landscape) {
    font-size: 14px;
    line-height: 16px;
  }
`

export const WrapperLogo = styled.div`
max-width: 206px;
width: 100%;
display: flex;
margin: 0 25px;
justify-content: center;
@media (max-width: ${screenSize.laptopL}) {
  max-width: 140px;
};
@media (max-width: ${screenSize.laptopL}) {
  max-width: 187px;
justify-content: flex-start;
}

@media (max-width: ${screenSize.laptopPortrait}) and (orientation: portrait) {
  max-width: 143px;
justify-content: flex-start;
}
@media (max-width: 550px) and (orientation: portrait) {
  max-width: 143px;
justify-content: center;
margin: 0 15px;
}
@media (max-width:${screenSize.mobileL} ) and (orientation: portrait) {
  max-width: 113px;
  justify-content: center;
}

@media (max-width: ${screenSize.mobileLLandscape}) and (orientation: landscape) {
  max-width: 92px;
  justify-content: center,
}

@media (max-width: 370px) and (orientation: portrait) {
  max-width: 90px;
}

`

// Note
export const Note = styled.div`
  z-index: 3;
  display: flex;
  justify-content: space-between;
  background: #494e54;
  align-items: center;
  width: 1077px;
  position: absolute;
  right: 0px;
  bottom: 0px;
  max-height: 235px;
  height: 235px;
  @media (max-width: ${screenSize.laptopL}) {
    margin-bottom: unset;
    width: 811px;
    height: 200px;
  }
  @media (max-width: ${screenSize.laptop}) {
    width: 644px;
  }
  @media (max-width: ${screenSize.laptopPortrait}) and (orientation: portrait) {
    max-width: unset;
    width: 100%;
    position: unset;
  }
  @media (min-width: 769px) and (max-width: 930px) {
    height: 140px;
    max-width: 562px;
  }
  @media (max-width: ${screenSize.mobileLLandscape}) and (orientation: landscape) {
    max-width: 462px;
    height: 126px;
  }
  @media (min-width: 321px) and (max-width: 630px) and (orientation: landscape) {
    height: 100px;
    max-width: 362px;
  }
  @media (max-width: 400px) and (orientation: landscape) {
    max-width: 330px;
  }
`

export const WrapperNoteContent = styled.div`
  display: flex;
  flex-direction: column;
  max-width: calc(100% - 206px);
  width: 100%;
  align-items: flex-start;
  padding-right: 206px;
  @media (max-width: ${screenSize.laptopL}) {
    max-width: calc(100% - 140px);
    padding-right: 140px;
  }
  @media (max-width: ${screenSize.laptop}) {
    max-width: calc(100% - 187px);
    padding-left: 30px;
    align-items: flex-start;
  }
  @media (max-width: ${screenSize.laptopPortrait}) and (orientation: portrait) {
    max-width: calc(100% - 143px);
    padding-left: 30px;
    padding-right: 30px;
    padding-right: unset;
    align-items: flex-start;
  }
  @media (max-width: 550px) and (orientation: portrait) {
    padding-left: 10;
  }
  @media (max-width: ${screenSize.laptopPortrait}) and (orientation: portrait) {
    max-width: calc(100% - 113px);
    padding-left: 10;
  }
  @media (max-width: ${screenSize.mobileLLandscape}) and (orientation: landscape) {
    padding: 0px;
    padding-left: 10;
    align-items: flex-start;
    max-width: calc(100% - 92px);
  }
  @media (max-width: 370px) and (orientation: portrait) {
    max-width: calc(100% - 90px);
  }
`

export const NoteDescription = styled.div`
  font-size: 24px;
  line-height: 32px;
  color: #d9d3c5;
  letter-spacing: 0.15em;
  ${({realEstateSales}) =>
    realEstateSales &&
    css`
      color: #d9d3c5;
    `}
  @media (max-width: ${screenSize.laptop}) {
    max-width: 305px;
  }
  @media (max-width: ${screenSize.laptopPortrait}) and (orientation: portrait) {
    max-width: 100%;
    width: 100%;
  }

  @media (max-width: 550px) and (orientation: portrait) {
    font-size: 14px;
    line-height: 1.3;
  }
  @media (min-width: 769px) and (max-width: 930px) {
    font-size: 18px;
    line-height: 24px;
    max-width: unset;
  }
  @media (max-width: ${screenSize.mobileLLandscape}) and (orientation: landscape) {
    font-size: 18px;
    line-height: 24px;
    max-width: unset;
  }
  @media (min-width: 321px) and (max-width: 630px) and (orientation: landscape) {
    font-size: 14px;
    line-height: 20px;
  }
`

export const NoteText = styled.div`
  font-size: 100px;
  line-height: 115px;
  color: #d9d3c5;
  @media (max-width: ${screenSize.laptopL}) {
    font-size: 80px;
    line-height: 85px;
  }

  ${({realEstateSales}) =>
    realEstateSales &&
    css`
      width: max-content;
      font-size: 80px;
      color: #d9d3c5;
      @media (max-width: ${screenSize.laptopL}) {
        font-size: 60px;
        line-height: 68px;
      }
      @media (max-width: 930px) {
        font-size: 46px;
        line-height: 54px;
      }
      @media (max-width: 490px) {
        font-size: 32px;
        line-height: 1.2;
        width: auto;
      }
    `};

  ${({realEstateSales}) =>
    !realEstateSales &&
    css`
      @media (max-width: 550px) and (orientation: portrait) {
        font-size: 60px;
        line-height: 68px;
        margin-bottom: 10px;
      }
      @media (min-width: 769px) and (max-width: 930px) {
        font-size: 70px;
        line-height: 68px;
      }
      @media (max-width: ${screenSize.mobileLLandscape}) and (orientation: landscape) {
        font-size: 60px;
        line-height: 68px;
      }
      @media (min-width: 321px) and (max-width: 630px) and (orientation: landscape) {
        font-size: 40px;
        line-height: 48px;
      }
    `};
`

// Note
