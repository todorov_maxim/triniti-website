import {
  StyledAboutContent,
  AboutProfile,
  WrapperImg,
  ProfileContent,
  ProfileTitle,
  ProfileName,
  ProfileDescription,
  WrapperNameAndDescription,
  Note,
  WrapperNoteContent,
  NoteDescription,
  NoteText,
  WrapperLogo,
  ProfileIconComma,
} from "./StyledAboutContent";
import img8 from "../../../../../public/images/Main/main_8.png";
import Image from "next/image";
import { Logo } from "../../../../logo";
export const AboutContent = ({ pageData, className, realEstateSales, textColor }) => {
  // const {
  //   profile: { img, description, name, note },
  // } = pageData;
  const profile = pageData?.profile;
  const title = profile?.title ? profile?.title : "";
  const img = profile?.img ? profile?.img : { sourceUrl: img8 };
  const description = profile?.description ? profile?.description : "";
  const name = profile?.name ? profile?.name : "";
  const note = profile?.note ? profile?.note : "";

  return (
    <StyledAboutContent className={className} realEstateSales={realEstateSales}>
      <AboutProfile>
        <WrapperImg>
          <Image
            layout="responsive"
            src={img?.sourceUrl}
            width={900}
            height={900}
            objectFit="cover"
            priority={true}
          />
        </WrapperImg>

        <ProfileContent>
          <ProfileTitle color={textColor}>{title}</ProfileTitle>
          <WrapperNameAndDescription>
            <ProfileIconComma className="icon-comma" color={textColor}/>
            <ProfileName>{name}</ProfileName>
            <ProfileDescription
              dangerouslySetInnerHTML={{ __html: description }}
            ></ProfileDescription>
          </WrapperNameAndDescription>

          <Note>
            <WrapperLogo>
              <Logo
                namePage="AboutPage"
                fill={realEstateSales ? "#d9d3c5" : "#D7D1C5"}
                mb={0}
              />
            </WrapperLogo>
            <WrapperNoteContent>
              <NoteText realEstateSales={realEstateSales}>
                {note?.mainText}
              </NoteText>
              <NoteDescription realEstateSales={realEstateSales}>{note?.description}</NoteDescription>
            </WrapperNoteContent>
          </Note>
        </ProfileContent>
      </AboutProfile>
    </StyledAboutContent>
  );
};
