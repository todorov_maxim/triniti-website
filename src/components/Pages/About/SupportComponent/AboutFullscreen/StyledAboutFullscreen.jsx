import { unstable_batchedUpdates } from "react-dom";
import styled,{css} from "styled-components";
import { screenSize } from "../../../../../constants/media";
import { TextLink } from "../../../../FullScreenPage/StyledFullScreenPage";

export const StyledAboutFullscreen = styled.div`
  height: 100%;
  display: flex;
  @media (max-width: ${screenSize.laptopPortrait}) and (orientation: portrait) {
    /* flex-direction: column; */
    padding-top: 250px;
    flex-wrap: wrap;
  height: max-content;

  }
  @media (max-width: 1000px) and  (max-height: 750px) and (orientation: landscape) {
    height: auto;
  }
  @media (min-height: 250px) and  (max-height: 650px) {
    height: max-content;
  }
  @media (max-width: ${screenSize.mobileL}) and (orientation: portrait) {
    padding-top: 185px;
align-content: flex-start;
  }
  @media (max-width: ${screenSize.mobileLLandscape}) and (orientation: landscape) {
flex-direction: column;
height: max-content;
position: relative;
  }
`;

// FullpageAbout

export const WrapperHeaderAndDescripton = styled.div(
  ({ background, color }) => ({
    maxWidth: 444,
    width: "100%",
    display: "flex",
    flexDirection:'column',
    padding: "18px 21px",
    background,
    color,
    [`@media (max-width: ${screenSize.laptop}) and (orientation: landscape)`]: {
      maxWidth: 360,
      minHeight: 'max-content',
    },
    [`@media (max-width: ${screenSize.laptopPortrait}) and (orientation: portrait)`]:
      {
        maxWidth: "unset",
        maxHeight: "unset",
        height: "max-content",
        padding: "33px 0px 18px 0px",
        order: 3,
        minHeight:260,
      },
      [`@media (max-width: ${screenSize.mobileL}) and (orientation: portrait)`]:
      {
        order: 2,
        maxHeight: "190px",
        height: '100%',
      },
      [`@media (max-width: ${screenSize.mobileL}) and (orientation: portrait)`]:
      {
        maxHeight: 'unset',
paddingBottom:0,
        height: 'max-content',
        paddingTop:30,
        paddingBottom:20,

      },
      [`@media (max-width: ${screenSize.mobileLLandscape}) and (orientation: landscape)`]:
      {
        maxWidth: '382px',
        height: 'max-content',
    padding: "13px 21px",
    paddingBottom:32,
minHeight:360,
paddingBottom:20,
     
      },
      
  })
);

export const IntroLink = styled(TextLink)`
margin-top: 20px;
margin-bottom: 0px;
`

export const HeaderPage = styled.div(({ background }) => ({
  flex: '0 0 50%',
  width: "100%",
  background,
  [`@media (max-width: ${screenSize.laptopPortrait}) and (orientation: portrait)`]:
    {
      position: "absolute",
      top: 0,
      left: 0,
      height: "100%",
      maxHeight: 250,
      padding: "16.8px 21px 0px 21px",
    },
    [`@media (max-width: ${screenSize.mobileL}) and (orientation: portrait)`]:
    {
        maxHeight: 185,
    },
    [`@media (max-width: ${screenSize.mobileLLandscape}) and (orientation: landscape)`]:
    {
minHeight:193,
height: 'unset',
    },
}));

export const WrapperDescripton = styled.div`
  max-width: 305px;
  margin: 0 auto;
  width: 100%;
  flex: 0 0 50%;
  color: inherit;
  display: flex;
  flex-direction: column;
  justify-content: center;
  @media (max-width: ${screenSize.laptopL}) and (orientation: landscape) {
    justify-content: start;
  }
  @media (max-width: ${screenSize.laptop}) and (orientation: landscape) {
    padding-bottom: 29px;
  }
  @media (max-height: 620px) {
    justify-content: end;
padding-bottom: 20px;
  } ;
  @media (max-width: ${screenSize.laptopPortrait}) and (orientation: portrait) {
    width: 100%;
    max-width: unset;
    /* max-height: 260px; */
    padding: 0px 63px;
    height: max-content;
  } ;
  @media (max-width: ${screenSize.mobileL}) and (orientation: portrait) {
    width: 100%;
    max-width: unset;
    max-height: unset;
    padding: 0px 29px;
    height: max-content;
  } ;
  @media (max-width: ${screenSize.mobileLLandscape}) and (orientation: landscape) {
    max-width: unset;
    padding: unset;
    max-height: max-content;

  } ;
`;

export const BackgroundImage = styled.div(({ url }) => ({
  maxWidth: "100%",
  width: "100%",
  backgroundImage: `url(${url})`,
  backgroundSize: "cover",
  backgroundPosition: "center",
  [`@media (max-width: ${screenSize.laptopL}) and (orientation: landscape)`]: {
    maxWidth: '100%',
  },
  [`@media (max-width: ${screenSize.laptop}) and (orientation: landscape)`]: {
    maxWidth: "calc(100% -  360px + 337px)",
  },
  [`@media (max-width: ${screenSize.laptopPortrait}) and (orientation: portrait)`]:
    {
      width: "60%",
      order: 1,
      height: 512,
    },
    [`@media (max-width: ${screenSize.mobileL}) and (orientation: portrait)`]:
    {
      width: "100%",
      order: 1,
      height: 392,
      maxHeight: 392,
    },
    [`@media (max-width: ${screenSize.mobileLLandscape}) and (orientation: landscape)`]:
    {
      width: 'calc( 100% - 382px )',
      order: 2,
      height: '100%',
      position: 'absolute',
      right: 0,
      top: 0,
    },

}));

export const WrapperSideImgAndStats = styled.div`
  max-width: 547px;
  width: 100%;
  display: flex;
  flex-direction: column;
  // flex-wrap: wrap;
  @media (max-width: ${screenSize.laptopL}) and (orientation: landscape){
    max-width: 452px;
  }
  @media (max-width: ${screenSize.laptop}) and (orientation: landscape){
    max-width: 337px;
  }

  @media (max-width: ${screenSize.laptopPortrait}) and (orientation: portrait) {
    order: 2;
    width: 40%;
    height: 512px;
  }
  @media (max-width: ${screenSize.mobileL}) and (orientation: portrait) {
    order: 3;
    height: 152px;
    width: 100%;
    max-width: unset;
  }
  @media (max-width: ${screenSize.mobileLLandscape}) and (orientation: landscape) {
    order: 3;
    max-width: 56.5%;
    // min-height: 147px;
  }
`;

export const StatsBox = styled.div`
    display: flex; 
    padding: 25px 0;
    `;

export const SideImg = styled.div`
  max-width: 547px;
  width: 100%;
  flex: 1 1 auto;
  display: flex;
  span {
    width: 100% !important;
    height: 100% !important;
  }
  @media (max-width: ${screenSize.laptop}) and (orientation: landscape) {
    max-width: 337px;
  }
  @media (max-height:1000px )  {
    height: 75%;
  }
  @media (max-height:770px )  {
    height: 65%;
  }

  @media (max-width: ${screenSize.laptopPortrait}) and (orientation: portrait) {
    height: 72.5%;

  }
  @media (max-width: ${screenSize.mobileL}) and (orientation: portrait) {
display: none;
  }
  @media (max-width: ${screenSize.mobileL}) and (orientation: landscape) {
display: none;
  }
`;


const isAllWidth=(allWidth)=>{
  if(!allWidth) return {}
  if(allWidth){
    return {
      width:'100%',
      display:'flex',
      alignItems:'center',
      flexDirection:'column'
    }
  }
}

export const WrapperValueCaption = styled.div(({ curr,allWidth }) => ({
  ...isAllWidth(allWidth),

  // padding: curr == 1 && "18px 57px 16px 57px",

  // [`@media (max-height: 1080px ) and (orientation: portrait)`]:
  // {
  //   padding: curr == 1 && "18px 33% 16px 33%",

  // },
  // [`@media (max-width: ${screenSize.laptopL}) and (orientation: landscape)`]: {
  //   padding: curr == 1 && "18px 47px 16px 47px",
  // },
  // [`@media (max-width: ${screenSize.laptop} ) and (orientation: landscape)`]:
  //   {
  //     padding: curr == 1 && "18px 27px 16px 27px",
     
  //   },
  //   [`@media (max-width: 1110px)`]:
  //   {
  //     padding: curr == 1 && "14px 28% 16px 28%",
  //   },

  //   [`@media (max-width: 976px)`]:
  //   {
  //     padding: curr == 1 && "14px 28% 16px 28%",
  //   },
  // [`@media (max-width: ${screenSize.laptopPortrait}) and (orientation: portrait)`]:
  //   {
  //     padding: curr == 1 && "18px 31px 16px 31px",
  //   },
  //   [`@media (max-width: 710px) and (orientation: portrait)`]:
  //   {
  //     padding: curr == 1 && "18px 31px 16px 31px",
  //   },
    // [`@media (max-height: 592px)`]:
    // {
    //   paddingTop: curr == 1 && 5,
    //   paddingBottom: curr == 1 && 5,

    // },
    [`@media (max-width: ${screenSize.mobileL}) and (orientation: portrait)`]:
    {
      width: curr == 1 && '100%',
      ...(()=>(curr == 1&&{width:'100%',display:'flex',justifyContent:'center',alignItems:'center',flexDirection:'column'}))()
    },
}));

export const ValueDef = styled.div(({ isFourthBlock }) => ({
width:isFourthBlock&& "100%",
textAlign:isFourthBlock&&'center !important',
}));


export const Value = styled(ValueDef)`
  font-size: 32px;
  line-height: 28px;
  text-align: center;
  margin-bottom: 5px;
  text-align: left;
  white-space: nowrap;

  @media (max-width: 1200px) {
    font-size: 28px;
    line-height: 1;
  }
  @media (max-width: 710px) and (orientation: portrait) {
    font-size: 26px;
  line-height: 24px;
  }
  @media (max-width: 976px)  {
    font-size: 24px;
  line-height: 24px;
  }
  @media (max-width: ${screenSize.mobileL}) and (orientation: portrait) {
    font-size: 32px;
  line-height: 28px;
  }
`;


export const CaptionDef = styled.div(({ isFourthBlock }) => ({
  width:isFourthBlock&& "100%",
  maxWidth:isFourthBlock&&'100% !important',
  textAlign:isFourthBlock&&'center !important',
  }));

export const Caption = styled(CaptionDef)(({ isThirdBlock }) => ({
  fontSize: "14px",
  lineHeight: isThirdBlock ? "18px" : "28px",
  textAlign: "center",
  maxWidth: "120px",
  textTransform: "lowercase",
  textAlign: 'left',
  [`@media (max-width: 976px ) `]:{
    fontSize: "12px",
  lineHeight: '16px',
  padding:isThirdBlock&&'0px 4px',

    },
}));

export const WrapperStats = styled.div(({ background, color }) => ({
  flex: "0 0 auto",
  width: "100%",
  display: "flex",
  // height: 'calc( 100% - 82.5% )',
  background,
  color,
  flexDirection:'column',
  justifyContent:'center',
 
  // [`@media (max-height: 1000px) `]:
  // {
  //   height: '25%'
  // },
  // [`@media (max-height: 770px)` ]:
  // {
  //   height: '35%'
  // },
 
  [`@media (max-width: ${screenSize.laptopPortrait}) and (orientation: portrait)`]:
  {
    height: 'calc( 100% - 72.5% )',

  },
  [`@media (max-width: ${screenSize.mobileL}) and (orientation: portrait)`]:
  {
    height: 'unset',
  },
  [`@media (max-width: ${screenSize.mobileLLandscape}) and (orientation: landscape)`]:
  {
width:382,
    height: 'unset',
  },
}));

export const Stats = styled.div(({isThirdBlock,isFourthBlock}) => ({
  flex: "1 1 auto",
  padding: "8px 5px 5px 8px",
  display: "flex",
  alignItems: "center",
  flexDirection: "column",
  color: "inherit",
  borderRight: "1px solid #fff",

  [`&:last-child`]: {
    borderRight: "none"
  },

  [`@media (max-width: 1200px) `]:{
    flex: "1 1 auto"
  },
  // [`@media (max-width: 1000px) `]:
  // {
  //   paddingLeft:isThirdBlock&&12,
  // },
}));

export const NamePage = styled.div`
opacity: 0.5;
  font-size: 12px;
  line-height: 18px;
  letter-spacing: 0.15em;
  min-height: 40px;
`;
export const Title = styled.div`
  font-size: 32px;
  line-height: 42px;
  letter-spacing: 0.15em;
  min-height: 147px;
  padding-bottom: 10px;
  text-transform: uppercase;
  ${({modify}) => modify === "small" && css`
  max-width: 290px;
  `}
  @media (max-width: ${screenSize.laptopPortrait}) and (orientation: portrait) {
    min-height: 42px;
    padding-bottom: unset;
    margin-bottom: 8px;
    max-width: 308px;
  } ;
  @media (max-height: 720px) {
    font-size: 22px;
  line-height: 24px;
   min-height: 100px;
  } ;
  @media (max-height: 620px) {
   min-height: 72px;
  } ;

  @media (max-width: ${screenSize.mobileL}) and (orientation: portrait) {
    font-size: 24px;
  line-height: 28px;
  max-width: 240px;

  } ;
  @media (max-width: ${screenSize.mobileLLandscape}) and (orientation: landscape) {
  max-width: 240px;
    font-size: 24px;
  line-height: 28px;
  min-height: 72px;
  } ;
`;

export const Description = styled.div`
  font-size: 12px;
  line-height: 18px;
  margin-bottom: 47px;
  @media (max-width: ${screenSize.laptopL}) and (orientation: landscape) {
    margin-bottom: unset;
  } ;
  @media (max-height: 620px) {
    /* margin-bottom: unset; */
  } ;
  @media (max-width: ${screenSize.mobileL}) and (orientation: portrait) {
    margin-bottom: unset;
    font-size: 13px;
  line-height: 16px;
  } ;
  @media (max-width: ${screenSize.mobileLLandscape}) and (orientation: landscape) {
    margin-bottom: unset;
    font-size: 12px;
  line-height: 14px;
  } ;
 
`;

export const FourBlock = styled.div`
margin-top: 20px;
padding-bottom: 5px;
  @media (max-width: ${screenSize.laptopL})  {
    margin-bottom: unset;
  } ;

  @media (max-width: ${screenSize.mobileL}) and (orientation: portrait) {
margin-top: 10px;
    padding-bottom: 10px;
  } ;
  @media (max-width: ${screenSize.mobileLLandscape}) and (orientation: landscape) {
   
  } ;
 
`;





// FullpageAbout
