import {
  StyledAboutFullscreen,
  NamePage,
  Title,
  Description,
  WrapperDescripton,
  HeaderPage,
  BackgroundImage,
  SideImg,
  WrapperStats,
  Stats,
  Value,
  Caption,
  WrapperValueCaption,
  WrapperHeaderAndDescripton,
  WrapperSideImgAndStats,
  StatsBox,
  FourBlock,
} from "./StyledAboutFullscreen";
import { Contact } from "../../../../Contact/Contact";
import { WrapeperLogo } from "../../../../FullScreenPage/StyledFullScreenPage";
import { LogoWithText } from "../../../../LogoWithText/LogoWithText";
import Image from "next/image";
import { pageTheme } from "../../../../../constants/themes";
// import withMenu from "../../../../../hoc/withMenu/withMenu";
import { compose } from "redux";
import { BackgroundImageBlock } from "../../../../FullScreenBlockGeneralPages/StyledFullScreenBlockGeneralPages";

const AboutFullscreen = ({ pageData }) => {
  const { textColor, background } = pageTheme[pageData?.pageTheme] || pageTheme.brown;
  const {
    intro: {
      stats,
      sideImg,
      pageName,
      description,
      background: backgroundImg,
      title,
    },
  } = pageData;

  return (
    <StyledAboutFullscreen>
      <WrapperHeaderAndDescripton background={background} color={textColor}>
        <HeaderPage background={background}>
          <Contact isRenderTelephone={false} color={textColor} />
          <WrapeperLogo>
            <LogoWithText namePage="GeneralPages" fill={textColor} />
          </WrapeperLogo>
        </HeaderPage>

        <WrapperDescripton>
          <NamePage>{pageName}</NamePage>

          <Title>{title}</Title>

          <Description>{description}</Description>
        </WrapperDescripton>
      </WrapperHeaderAndDescripton>

      <BackgroundImageBlock url={backgroundImg?.sourceUrl}></BackgroundImageBlock>
      <StatsBox>
            {stats.map(({ point }, i) => {
              return (
                <>
                  {i <= 22 && (
                    <Stats isThirdBlock={i == 2}>
                      <WrapperValueCaption
                        curr={i}
                        className={i == 1 && "verticalLine"}
                      >
                        <Value>{point?.value}</Value>
                        <Caption isThirdBlock={i == 2}>
                          {point?.caption}
                        </Caption>
                      </WrapperValueCaption>
                    </Stats>
                  )}
                </>
              );
            })}
          </StatsBox>
      {/* <WrapperSideImgAndStats>
         <SideImg>
          <Image
            objectFit="cover"
            layout="responsive"
            src={sideImg?.sourceUrl}
            quality={100}
            width={300}
            height={300}
            priority={true}
          ></Image>
        </SideImg> 
        <WrapperStats background={background} color={textColor}>
          <StatsBox>
            {stats.map(({ point }, i) => {
              return (
                <>
                  {i <= 22 && (
                    <Stats isThirdBlock={i == 2}>
                      <WrapperValueCaption
                        curr={i}
                        className={i == 1 && "verticalLine"}
                      >
                        <Value>{point?.value}</Value>
                        <Caption isThirdBlock={i == 2}>
                          {point?.caption}
                        </Caption>
                      </WrapperValueCaption>
                    </Stats>
                  )}
                </>
              );
            })}
          </StatsBox>
        </WrapperStats>
      </WrapperSideImgAndStats> */}
    </StyledAboutFullscreen>
  );
};

export default compose()(AboutFullscreen);
