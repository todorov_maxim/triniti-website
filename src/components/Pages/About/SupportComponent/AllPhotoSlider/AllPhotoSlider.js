import Image from "next/image";
import { useEffect, useState } from "react";
import {
  StyledAllPhotoSlider,
  WrapperArrowBottom,
  ArrowBottom,
  WrapperImgAllPhotoFirst,
  WrapperImgAllPhotoSecond,
  FirstSection,
  SecondSection,
} from "./StyledAllPhotoSlider";

export const AllPhotoSlider = ({ sliderData }) => {
  const [isMobile, setIsMobile] = useState(false);
  useEffect(() => {
    (window.innerWidth <= 755 && window.innerWidth < window.innerHeight) ||
    window.innerWidth <= 610
      ? setIsMobile(true)
      : setIsMobile(false);
    //if the user will resize the screen <=768
    window.addEventListener(
      `resize`,
      (event) => {
        //ниже строчка чтобы отоавливать изменения экрана, она работает в паре с useEffect у которого в засимости [isResize]
        if (
          (window.innerWidth <= 755 &&
            window.innerWidth < window.innerHeight) ||
          window.innerWidth <= 610
        ) {
          setIsMobile(true);
        } else {
          setIsMobile(false);
        }
      },
      false
    );
  }, []);

  const allPhoto = sliderData
    .map((obj, i) => ({ ...obj, number: i + 1 }))
    .reverse();

  const [isOpen, setIsOpen] = useState(false);
  return (
    <StyledAllPhotoSlider className="StyledAllPhotoSlider" isOpen={isOpen}>
      {/* <FirstSection>
        <div
          style={{
            display: "flex",
            justifyContent: "flex-start",
            width: "100%",
            flexWrap: "wrap",
          }}
        >
          {allPhoto.map((item, i) => {
            if (i < 5) {
              return (
                <WrapperImgAllPhotoFirst curr={i}>
                  <Image
                    key={item.id}
                    src={item.profilesSliderData?.img?.sourceUrl}
                    objectFit="cover"
                    layout="responsive"
                    width="100%"
                    height="100%"
                  ></Image>
                </WrapperImgAllPhotoFirst>
              );
            }
          })}
          {isMobile && (
            <WrapperArrowBottom
              onClick={() => setIsOpen((pre) => !pre)}
              isOpen={isOpen}
            >
              <ArrowBottom className="icon-right" />
            </WrapperArrowBottom>
          )}
        </div>

        {!isMobile && (
          <WrapperArrowBottom
            onClick={() => setIsOpen((pre) => !pre)}
            isOpen={isOpen}
          >
            <ArrowBottom className="icon-right" />
          </WrapperArrowBottom>
        )}
      </FirstSection>
      <SecondSection>
        {[...allPhoto, ...allPhoto].map((item, i) => {
          if (isOpen && i >= 5) {
            return (
              <WrapperImgAllPhotoSecond>
                <Image
                  key={item.id}
                  src={item.profilesSliderData?.img?.sourceUrl}
                  objectFit="cover"
                  layout="responsive"
                  width="100%"
                  height="100%"
                ></Image>
              </WrapperImgAllPhotoSecond>
            );
          }
        })}
      </SecondSection>
     */}
    </StyledAllPhotoSlider>
  );
};
