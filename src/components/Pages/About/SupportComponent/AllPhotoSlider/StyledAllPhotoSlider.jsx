import styled from "styled-components";
import { screenSize } from "../../../../../constants/media";
import { WrapperImg } from "../../../../Slider/SyledSlider";
 

export const StyledAllPhotoSlider = styled.div(({isOpen})=>({
  display: 'flex',
  justifyContent:'flex-start',
flexWrap:'wrap',
maxWidth:1582,
margin: '0px auto',
marginBottom:104,
[`@media (max-width: ${screenSize.laptopL})`]: {
  maxWidth:1322,
  justifyContent:'flex-start',
margin: 'unset',
marginBottom:90,
},
[`@media (max-width: ${screenSize.laptop})`]: {
  maxWidth:989,
},
[`@media (max-width: ${screenSize.laptopPortrait})`]: {
  marginBottom:54,
},
[`@media (max-width: ${screenSize.slyderMobileL}) and (orientation: portrait)`]: {
justifyContent:'center',
flexDirection:'column',
alignItems:'center',
},
[`@media (max-width: ${screenSize.mobileLLandscape})`]: {
  marginBottom:72,
  },
  [`@media (max-width: ${screenSize.mobileL})`]: {
    marginBottom:53,
justifyContent:'center',
flexDirection:'column',
alignItems:'center'
    },
    [`@media (max-width: 610px)`]: {
      justifyContent:'center',
      flexDirection:'column',
      alignItems:'center',
      },
}))

export const WrapperImgAllSlider = styled.div`
`;

export const WrapperArrowBottomDef= styled.div(({isOpen})=>({
  background:isOpen? '#494E54':'#CD8B7D',
  'i':{
    transform:isOpen? 'rotate(270deg)':'rotate(90deg)',
  }
}))

export const WrapperArrowBottom = styled(WrapperArrowBottomDef)`
max-width: 132px;
width:100%;
height: 250px;
display: flex;
justify-content: center;
align-items: center;
cursor:pointer;
&:hover {
  background: #494E54;
  }
  @media (max-width: ${screenSize.laptopL})  {
    max-width: 104px;
height: 210px;
};
@media (max-width: ${screenSize.laptop})  {
    max-width: 78px;
};
@media (max-width: ${screenSize.laptopPortrait})  {
    max-width: 48.7px;
height: 166px;
};
@media (max-width: ${screenSize.mobileLLandscape}) and (orientation: landscape)  {
    max-width: 139px;
height: 150px;
};
@media (max-width: ${screenSize.slyderMobileL}) and (orientation: portrait)  {
max-width: 50%;
/* height: 216px; */
height: 150px;
width:180px;
};
@media (max-width: 610px)  {
  max-width: 50%;
height: 150px;
width:180px;
  };
@media (max-width: 470px) and (orientation: portrait)  {
/* height: 180px; */
};
@media (max-width: 360px) and (orientation: portrait)  {
height: 150px;
};

`;

export const ArrowBottom = styled.i`
color:#FFFFFF;
font-size:21px;
/* transform: rotate(90deg); */
&:hover {
  background: #494E54;
  }
  cursor: pointer;
  @media (max-width: ${screenSize.laptop})  {
    font-size:18px;
};
@media (max-width: ${screenSize.laptopPortrait})  {
  font-size:14px;
};
`;



export const WrapperImgAllPhotoFirst = styled(WrapperImg)(({curr})=>({
  // width: '20%',
  width:'calc( 100% / 5)',

  [`@media (max-width: ${screenSize.laptop})`]: {
display: curr<4?'block':'none',
// width: '25%',
width:'calc( 100% / 4)',
  },
  [`@media (max-width: ${screenSize.mobileLLandscape}) and (orientation: landscape)`]: {
    display: curr<3?'block':'none',
    width: 'calc( 100% / 3)',
      },
      [`@media (max-width: ${screenSize.mobileL}) and (orientation: portrait)`]: {
        width: '50%',
          },
      [`@media (max-width: ${screenSize.slyderMobileL}) and (orientation: portrait)`]: {
        display: curr<3?'block':'none',
        width: '50%',
height: 150,
maxWidth:180,
          },
          [`@media (max-width: 610px)`]: {
            display: curr<3?'block':'none',
            width: '50%',
    height: 150,
    maxWidth:180,
            },
          [`@media (max-width: 470px) and (orientation: portrait)`]: {
            // height: 180,
height: 150,

                      },
          [`@media (max-width: 360px ) and (orientation: portrait)`]: {
            height: 150,
                      },
          
}))

export const WrapperImgAllPhotoSecond = styled(WrapperImg)(()=>({
  width: '20%',
  [`@media (max-width: ${screenSize.laptop})`]: {
width: '25%',
  },
  [`@media (max-width: ${screenSize.mobileLLandscape}) and (orientation: landscape)`]: {
    width: '33.33%',
      },
     
      [`@media (max-width: ${screenSize.slyderMobileL}) and (orientation: portrait)`]: {
width: '50%',
// height: 216,
height: 150,
maxWidth:180,
display: 'block !important' ,
          },
          [`@media (max-width: 610px)`]: {
            width: '50%',
            height: 150,
            maxWidth:180,
            display: 'block !important' ,
            },
          
          [`@media (max-width: 470px) and (orientation: portrait)`]: {
            // height: 180,
                      },
          [`@media (max-width: 360px ) and (orientation: portrait)`]: {
            height: 150,
                      },
}))

export const FirstSection = styled.div`
 display: flex;
          justify-content: space-between;
          width: 100%;
          @media (max-width: ${screenSize.slyderMobileL}) and (orientation: portrait)   {
flex-wrap:wrap;
max-width: 360px;
};
@media (max-width: 610px)  {
  flex-wrap:wrap;
max-width: 360px;
  };
`;

export const SecondSection = styled.div`
display: flex;
justify-content: flex-start;
width: 100%;
padding-right: 132px;
flex-wrap: wrap;

@media (max-width: ${screenSize.laptopL})  {
  padding-right: 104px;
};
@media (max-width: ${screenSize.laptop})  {
  padding-right: 78px;
};
@media (max-width: ${screenSize.laptopPortrait})  {
  padding-right: 48.7px;
};
@media (max-width: ${screenSize.mobileLLandscape})  and (orientation: landscape)  {
  padding-right: 139px;
  /* padding-right: 48.7px; */
};
@media (max-width: ${screenSize.slyderMobileL}) and (orientation: portrait)   {
  padding-right: unset;
max-width: 360px;
};
@media (max-width: 610px)  {
  padding-right: unset;
max-width: 360px;
  };
`;


