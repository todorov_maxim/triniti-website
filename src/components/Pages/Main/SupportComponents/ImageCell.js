import { StyledImageSell } from "../StyledMain";

export const ImageCell = ({ img, width = 100, height = 100, className }) => {
  return (
    <StyledImageSell className={className} width={width} height={height}>
      {img}
    </StyledImageSell>
  );
};
