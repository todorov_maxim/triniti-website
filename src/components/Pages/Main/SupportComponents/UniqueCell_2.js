import {useRef} from "react"
import {StyledUniqueCell_2, StyledUniqueCell_2_Link, TextUniqueCell_2} from "../StyledMain"
import gradient from "./../../../../../public/images/Main/gradient.jpg"
import Image from "next/image"
import Link from "next/link"

export const UniqueCell_2 = ({width, height, className, mainText}) => {
  const text = useRef(null)
  text.current && (text.current.innerHTML = mainText)
  //UniqueCell_2 уникальный компонент используеться только на main


  return (
    <StyledUniqueCell_2 background={gradient.src} className={className} width={width} height={height}>
      {"Скоро буде"}
      <StyledUniqueCell_2_Link href={"https://forms.gle/n9gTFrr8uGDYCTsB6"} target={"_blank"}>
        підписатись на новини
      </StyledUniqueCell_2_Link>
      {/* <TextUniqueCell_2 ref={text} top marginBottom="20px"></TextUniqueCell_2> */}
      {/* <TextUniqueCell_2 maxWidth="300px">
        {/* ПАРТНЕРСТВО НА ВСЕ ЖИТТЯ */}
      {/* </TextUniqueCell_2> */}
    </StyledUniqueCell_2>
  )
}
