import {
  Arrow,
  StyledUniqueCell_3,
  TextUniqueCell_3,
  FooterUniqueCell_3,
  VerticalText,
  MobileIcon,
} from "../StyledMain";

export const UniqueCell_3 = ({ width, height, className, secondaryText }) => {
  //UniqueCell_3 уникальный компонент используеться только на main
  return (
    <StyledUniqueCell_3 width={width} className={className}>
      <TextUniqueCell_3 className="main_10-text">
        {secondaryText}
      </TextUniqueCell_3>
      <FooterUniqueCell_3>
        <MobileIcon className="icon-swipe"></MobileIcon>

        <VerticalText fontSize="8px" color="#CD8B7D">
          SCROLL
        </VerticalText>
        <Arrow className="icon-Line-2"></Arrow>
      </FooterUniqueCell_3>
    </StyledUniqueCell_3>
  );
};
