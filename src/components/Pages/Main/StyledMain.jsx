import styled, {keyframes} from "styled-components";
import { screenSize } from "../../../constants/media";
import { CloseIcon } from "../../Modal/StyledModal";
//main_1 - main_12 стили для моб версии для каждого блока на main

const mobileHeight = "20.58%";
const mobileWidth = "50%";

const laptopReverseHeight = "25%";
const laptopReverseWidth = "33%";


const fadeInAnim = keyframes`
0% {
  opacity: 0;
}
100% {
  opacity: 1;
}
`

const fadeOutAnim = keyframes`
0% {
  opacity: 1;
}
100% {
  opacity: 0;
}
`
export const StyledMain = styled.div`
  display: flex;
  flex-wrap: wrap;

  @media (min-width: 992px) {
    .fadedImg {
      opacity: 1;
      animation: ${fadeOutAnim} 1s .5s forwards;

      .active & {
        opacity: 0;
        animation: ${fadeInAnim} 2s .5s forwards;
      }
    }
  }

  .main_1 {
    order: 1;
  }
  .main_10 {
    order: 2; //10
    
  }
  .main_3 {
    order: 3;
  }
  
  .main_2 {
    order: 4; //2
  }
  .main_4 {
    order: 5;
  }
  .main_5 {
    order: 6;
  }
  .main_7 {
    order: 7; //8
  }
  .main_6 {
    order: 8;
  }
  
  .main_8 {
    order: 9;
  }
  .main_9 {
    order: 10;
  }
  

  .main_11 {
    order: 11;
  }
  .main_12 {
    order: 12;
  }

  //

  @media (max-width: ${screenSize.laptopPortrait}) and (orientation: portrait) {
    /* @media (max-width: ${screenSize.laptop}) { */
    .main_allMobilePhoto {
      height: ${laptopReverseHeight} !important;
      width: ${laptopReverseWidth} !important;
    }
    .main_1 {
      order: 1;
    }
    .main_2 {
      order: 2;
    }
    .main_3 {
      order: 3;
    }
    .main_4 {
      order: 6;
    }
    .main_5 {
      order: 4;
    }
    .main_6 {
      order: 7;
    }
    .main_7 {
      order: 8;
    }
    .main_8 {
      order: 5;
    }
    .main_9 {
      order: 10;
    }
    .main_10 {
      order: 11;
      
    }

    .main_11 {
      order: 12;
    }
    .main_12 {
      order: 9;
    }
  }

  @media (max-width: ${screenSize.mobileL}) and (orientation: portrait) {
    .main_allMobilePhoto {
      height: ${mobileHeight} !important;
      width: ${mobileWidth} !important;
    }
    .main_1 {
      order: 2;
    }
    .main_2 {
      order: 1;
    }
    .main_3 {
      order: 6;
    }
    .main_4 {
      order: 1;
      display: none;
    }
    .main_5 {
      order: 3;
    }
    .main_6 {
      order: 5;
    }
    .main_7 {
      padding-bottom: 22px;
      padding-right: 16px;
      line-height: 18px;
      order: 4;
      font-size: 18px;
    }
    .main_8 {
      order: 5;
      display: none;
    }
    .main_9 {
      order: 1;
      display: none;
    }
    .main_10 {
      height: 17.68% !important;
      order: 9;
      width: 100% !important;


    }
    .main_11 {
      order: 7;
    }
    .main_12 {
      order: 8;
    }
  }
  @media (max-width: ${screenSize.laptopPortrait}) and (orientation: portrait) {
    /* @media (max-width: ${screenSize.laptop}) { */
    .main_allMobilePhoto {
      height: ${laptopReverseHeight} !important;
      width: ${laptopReverseWidth} !important;
    }
    .main_1 {
      order: 1;
    }
    .main_2 {
      order: 2;
    }
    .main_3 {
      order: 3;
    }
    .main_4 {
      order: 6;
    }
    .main_5 {
      order: 4;
    }
    .main_6 {
      order: 7;
    }
    .main_7 {
      order: 8;
    }
    .main_8 {
      order: 5;
    }
    .main_9 {
      order: 10;
    }
    .main_10 {
      /* height: 17.68% !important;
        order: 9;
        width: 100% !important;
        padding-right: 105px; */
      order: 11;

    }
    .main_11 {
      order: 12;
    }
    .main_12 {
      order: 9;
    }
  }

  @media (max-width: ${screenSize.mobileL}) and (orientation: portrait) {
    .main_allMobilePhoto {
      height: ${mobileHeight} !important;
      width: ${mobileWidth} !important;
    }
    .main_1 {
      order: 2;
    }
    .main_2 {
      order: 1;
    }
    .main_3 {
      order: 6;
    }
    .main_4 {
      order: 1;
      display: none;
    }
    .main_5 {
      order: 3;
    }
    .main_6 {
      order: 5;
    }
    .main_7 {
      padding-bottom: 22px;
      padding-right: 16px;
      line-height: 18px;
      order: 4;
    }
    .main_8 {
      order: 5;
      display: none;
    }
    .main_9 {
      order: 1;
      display: none;
    }
    .main_10 {
      height: 17.68% !important;
      order: 9;
      width: 100% !important;


    }
    .main_11 {
      order: 7;
    }
    .main_12 {
      order: 8;
    }
  }

  @media (max-width: 570px) and (orientation: portrait) and (max-height: 630px) {
    .main_10 {
      padding-top: 15px;
      padding-right: 7px;
      padding-left: 15px;
    }
  }

  @media (max-width: 570px) and (orientation: portrait) and (max-height: 568px) {
    .main_10 {
      padding-top: 5px;
      padding-right: 0px;
      padding-left: 15px;
    }
  }
`;

export const StyledUniqueCell_1 = styled.div(({ width, height }) => ({
  width,
  height,
  display: "flex",
  flexDirection: "column",
  justifyContent: "center",
  alignItems: "center",
  backgroundColor: "#5A3E3D",
}));

export const StyledUniqueCell_2 = styled.div(({ width, height, background }) => ({
  width,
  height,
  backgroundImage: `url(${background})`,
  backgroundSize: "cover",
  backgroundPosition: "center",
  backgroundRepeat: 'no-repeat',
  fontSize: 30,
  letterSpacing: "0.1em",
  lineHeight: "28px",
  display: "flex",
  flexDirection: "column",
  padding: '5px',
  justifyContent: "start",
  color: "#ffffff",
  display: "flex",
  flexDirection: "column",
  alignItems: "center",
  justifyContent: "center",
  textAlign: "center",
  // [`@media (max-width: ${screenSize.laptopL})`]: {
  //   paddingLeft: "3.28%",
  //   paddingTop: "3%",
  // },

  // [`@media (max-width: ${screenSize.laptop})`]: {
  //   paddingLeft: "3.9%",
  //   paddingTop: "4.68%",
  // },
  // [`@media (max-width: ${screenSize.laptop}) and (max-height: 400px)`] : {
  //   paddingTop: "12px",
  //   paddingLeft: "20px",
  // },
  // [`@media (max-width: 968px)`]: {
  //   paddingLeft: "1.5%",
  //   paddingTop: "2.23%",
  // },

  // [`@media (max-width: ${screenSize.laptopPortrait}) and (orientation: portrait)`]:
  //   {
  //     paddingLeft: "5.20%",
  //     paddingTop: "6.38%",
  //   },
  // [`@media (max-width: 968px) and (max-height: 600px)`]: {
  //   paddingTop: 10,
  // },
  // [`@media (max-width: ${screenSize.mobileL}) and (orientation: portrait)`]: {
  //   alignItems: "start",
  //   paddingLeft: 26,
  //   paddingTop: 26,
  // },

  // [`@media (max-width: ${screenSize.mobileLLandscape}) and (orientation: landscape)`]:
  //   {
  //     paddingLeft: "2.64%",
  //     paddingTop: "3.23%",
  //     paddingRight: "2%",
  //   },
  // [`@media (max-width:990px) and (max-height: 300px)`]: {
  //   paddingLeft: 5,
  //   paddingTop: 5,
  //   paddingRight: 2,
  //   justifyContent: "center",
  // },
  // [`@media (max-width:570px) and (max-height: 400px)`]: {
  //   paddingLeft: 5,
  //   paddingTop: 5,
  //   paddingRight: 2,
  //   justifyContent: "center",
  // },
}));

export const StyledUniqueCell_2_Link = styled.a(() => ({
  marginTop: 10,
  maxWidth: "150px",
  fontSize:17,
  lineHeight: 1.2,
  textDecoration: "underline",

  [`@media (max-width: ${screenSize.mobileL}) and (orientation: portrait)`]: {
    fontSize:14,
  },
}))

export const StyledUniqueCell_3 = styled.div(({ width }) => ({
  width,
  backgroundColor: "#494E54",
  // padding: 35,
  paddingTop: "2.55%",
  paddingRight: "3.24%",
  paddingLeft: "3.24%",
  paddingBottom: 22,
  color: "#FFFFFF",
  fontSize: 12,
  lineHeight: "18px",
  display: "flex",
  justifyContent: "space-between",
  flexDirection: "column",
  position: "relative",

  [`@media (max-width: ${screenSize.laptopL})`]: {
    paddingRight: "3.24%",
    paddingLeft: "3.08%",
  },
  [`@media (max-width: ${screenSize.laptop})`]: {
    paddingTop: "2.05%",
    paddingRight: "6.8%",
    paddingLeft: "2.73%",
  },

  [`@media (max-width: ${screenSize.laptop})  and (max-height: 400px)`]: {
    padding: "10px 45px 10px 10px",
  },

  [`@media (max-width: ${screenSize.laptopPortrait}) and (orientation: portrait)`]:
    {
      paddingLeft: "3.64%",
      paddingTop: "2.86%",
    },
  [`@media (max-width:992px) and (max-height: 370px)`]: {
    paddingTop: "7px",
    paddingRight: "31px",
    paddingLeft: "10px",
    paddingBottom: 7,
  },

  [`@media (max-width: ${screenSize.mobileL}) and (orientation: portrait)`]: {
    paddingTop: "27px",
    paddingRight: "17px",
    paddingLeft: "34px",
    flexDirection: "row",
  },

  [`@media (max-width: ${screenSize.mobileLLandscape}) and (orientation: landscape)`]:
    {
      paddingTop: "1.5%",
      paddingRight: "unset",
      paddingLeft: "1.83%",
    },

  [`@media (max-width: ${screenSize.mobileLLandscape}) and (max-height: 300px)`]:
    {
      paddingTop: "7px",
      paddingRight: "31px",
      paddingLeft: "10px",
      paddingBottom: "2px",
    },

  [`@media (max-width: 765px) and (max-height: 300px)`]: {
    paddingTop: "3px",
    paddingRight: "31px",
    paddingLeft: "3px",
    paddingBottom: "2px",
  },

  [`@media (max-width: 765px) and (max-height: 300px)`]: {
    paddingTop: "2px",
    paddingRight: "31px",
    paddingLeft: "2px",
    paddingBottom: "2px",
  },

  [`@media (max-width: 360px) and (max-height: 600px)`]: {
    maxHeight: "42.3%",
  },
}));

export const StyledImageSell = styled.div(({ width, height }) => ({
  width,
  height,
  display: "flex",
  flexGrow: 1,
}));

export const WrapperImage = styled.div(() => ({
  // width: "64%",

  // [`@media (min-width:1200px) and (max-height: 900px)`]: {
  //   height: 170
  // },

  // [`@media (min-width:1200px) and (max-height: 500px)`]: {
  //   height: 120
  // },

  // [`@media (max-width: 1400px) and (max-height: 400px)`]: {
  //   height: 60
  // },

  // [`@media (max-width: ${screenSize.laptop})`]: {
  //   height: 120
  // },

  // [`@media (max-width:  ${screenSize.laptop}) and (max-height: 400px)`]: {
  //   height: 60
  // },
  // [`@media (max-width: ${screenSize.mobileL}) and (orientation: portrait)`]: {
    
  // },

  // [`@media (max-width: ${screenSize.mobileLLandscape}) and (orientation: landscape)`]:
  //   {
      
  //   },
}));

export const FooterUniqueCell_3 = styled.div(() => ({
  position: "absolute",
  bottom: 32,
  right: 19,
  height: 101,
  display: "flex",
  justifyContent: "space-between",
  alignItems: "center",
  flexDirection: "column",

  [`@media (max-width: ${screenSize.laptopL})`]: {
    bottom: 22,
    right: 8,
  },
  [`@media (max-width: ${screenSize.laptop})`]: {
    bottom: 18,
  },
  [`@media (max-width:992px) and (max-height: 370px)`]: {
    height: 70,
    bottom: 10,
  },

  [`@media (max-width: ${screenSize.mobileL}) and (orientation: portrait)`]: {
    bottom: "-4px !important",
    right: 16,
    position: "unset",
  },

  [`@media (max-width: ${screenSize.mobileLLandscape}) and (orientation: landscape)`]:
    {
      height: 69,
      right: -3,
    },
  [`@media (max-width: 765px) and (max-height: 300px)`]: {
    height: 59,
    bottom: 10,
  },
}));

export const Arrow = styled.i(() => ({
  fontSize: 58,
  color: "#CD8B7D",
  [`@media (max-width: ${screenSize.mobileL}) and (orientation: portrait)`]: {
    display: "none",
  },

  [`@media (max-width:992px) and (max-height: 370px)`]: {
    fontSize: 36,
  },
  [`@media (max-width: ${screenSize.mobileLLandscape}) and (orientation: landscape)`]:
    {
      fontSize: 32,
    },
  [`@media (max-width: 765px) and (max-height: 300px)`]: {
    fontSize: 24,
  },
}));

export const VerticalText = styled.div(({ children, ...props }) => ({
  height: 14,
  letterSpacing: "0.25em",
  transform: "rotate(270deg)",
  ...props,
  [`@media (max-width: ${screenSize.mobileL}) and (orientation: portrait)`]: {
    display: "none",
  },
}));

export const TextUniqueCell_3 = styled.div(({}) => ({
  fontSize: 14,
  lineHeight: "18px",
  maxWidth: 301,

  [`@media (max-width: ${screenSize.laptopL})`]: {
    maxWidth: 262,
    fontSize: 12,
  },
  [`@media (min-width: 968px) and (max-height: 600px)`]: {
    fontSize: 10,
    lineHeight: "14px",
  },
  [`@media (max-width: 967px) and (max-height: 600px)`]: {
    fontSize: 10,
    lineHeight: "14px",
  },
  [`@media (max-width: ${screenSize.mobileLLandscape}) and (orientation: landscape)`]:
    {
      fontSize: 10,
      lineHeight: "14px",
      maxWidth: 120,
    },

  [`@media (max-width: 768px) and (max-height: 630px)`]: {
    fontSize: 9,
    lineHeight: "11px",
    maxWidth: 340,
    paddingRight: 25,
  },

  [`@media (max-width: 768px) and (max-height: 300px)`]: {
    fontSize: 9,
    lineHeight: "11px",
    paddingRight: 5,
  },
  [`@media (max-width: 768px) and (max-height: 250px)`]: {
    fontSize: 7,
    lineHeight: "10px",
  },
  [`@media (max-width: 470px) and (orientation: landscape)`]: {
    fontSize: 9,
    lineРeight: "11px",
  },
}));

export const FacebookText = styled.a(({color}) => ({
  // color,
  "&:hover":{
    color:color=='#494E54'?'#CD8B7D':'#494E54'
  },
  letterSpacing: "0.25em",
  marginLeft: "29px",
  [`@media (max-width: ${screenSize.laptop})`]: {
    marginLeft: 20,
  },
  [`@media (max-width: ${screenSize.mobileL}) and (orientation: portrait)`]: {
    marginLeft: 19,
  },
  [`@media (max-width: ${screenSize.mobileLLandscape}) and (orientation: landscape)`]:
    {
      marginLeft: 19,
    },
}));


const hjh=(color)=>{
  return color=='#494E54'?'#CD8B7D':'#494E54'
}


export const InstagramText = styled.a(({color}) => ({
  position: "relative",
  zIndex: 2,
  // color,

  "&:hover":{
    color:hjh(color),
  },
  letterSpacing: "0.25em",
  marginLeft: "33px",
  [`@media (max-width: ${screenSize.laptop})`]: {
    marginLeft: 23,
  },
  [`@media (max-width: ${screenSize.mobileL}) and (orientation: portrait)`]: {
    marginLeft: 24,
  },
  [`@media (max-width: ${screenSize.mobileLLandscape}) and (orientation: landscape)`]:
    {
      marginLeft: 22,
    },
}));

export const Text = styled.div(
  ({ children, mobMarginLeft, className, ...props }) => ({
    [`@media (max-width: ${screenSize.laptop})`]: {
      marginLeft: mobMarginLeft,
    },
    [`@media (max-width: ${screenSize.mobileL}) and (orientation: portrait)`]: {
      marginLeft: mobMarginLeft,
    },
    ...props,
  })
);

export const MobileIcon = styled.i(() => ({
  display: "none",
  fontSize: 80,
  color: "#CD8B7D",
  [`@media (max-width: ${screenSize.mobileL}) and (orientation: portrait)`]: {
    display: "block",
  },
}));

export const TextUniqueCell_2 = styled.div(({ children, top, ...props }) => ({
  ...props,
  textTransform: "uppercase",

  [`@media (max-width: ${screenSize.laptopL})`]: {
    maxWidth: !top && 206,
    marginBottom: top ? props.marginBottom : 0,
    fontSize: 20,
    lineHeight: "28px",
  },

  [`@media (max-width:1200px)`]: {
    fontSize: 19,
    lineHeight: 1.4,
  },

  [`@media (max-width: ${screenSize.laptopPortrait}) and (orientation: portrait)`]:
    {
      maxWidth: !top && 200,
    },
    [`@media (min-width: 1200px) and (max-height: 400px)`]: {
      fontSize: 14,
    
    },
  [`@media (min-width: 968px) and (max-height: 600px)`]: {
    fontSize: "16px",
    lineHeight: "18px",
    marginBottom: top ? "10px" : 0,
  },
  [`@media (min-width: 968px) and (max-height: 400px)`]: {
    fontSize: 14,
  
  },

 

  [`@media (max-width: 967px) and (max-height: 600px)`]: {
    fontSize: 16,
    lineHeight: "18px",
  },

  [`@media (max-width: ${screenSize.mobileL}) and (orientation: portrait)`]: {
    marginBottom: top ? props.marginBottom : 0,
    lineHeight: "18px",
    fontSize: "14px",
  },

  [`@media (max-width: 570px) and (orientation: portrait) and (max-height: 660px)`]:
    {
      marginBottom: top ? 10 : 0,
      lineHeight: "14px",
      fontSize: "12px",
    },

  [`@media (max-width: ${screenSize.mobileLLandscape}) and (orientation: landscape)`]:
    {
      lineHeight: "18px !important",
      fontSize: "14px !important",
      marginBottom: top ? 13 : 0,
    },
  [`@media (max-width:992px) and (max-height: 400px)`]: {
    lineHeight: "16px !important",
    fontSize: "14px !important",
    marginBottom: top ? 5 : 0,
  },
  [`@media (max-width:992px) and (max-height: 300px)`]: {
    lineHeight: "12px !important",
    fontSize: "14px !important",
    marginBottom: top ? 5 : 0,
  },
  [`@media (max-width: 768px) and (max-height: 400px)`]: {
    lineHeight: "15px !important",
    fontSize: "13px !important",
    marginBottom: top ? 5 : 0,
  },
  [`@media (max-width: 768px) and (max-height: 300px)`]: {
    lineHeight: "14px !important",
    fontSize: "12px !important",
    marginBottom: 7,
  },
  [`@media (max-width: 768px) and (max-height: 250px)`]: {
    lineHeight: "11px !important",
    fontSize: "9px !important",
    marginBottom: 4,
  },
  [`@media (max-width: 570px) and (max-height: 300px)`]: {
    lineHeight: "11px !important",
    fontSize: "9px !important",
    marginBottom: 4,
  },

  [`@media (max-width: 660px) and (orientation: landscape)`]: {
    lineHeight: '14px !important',
    fontSize: '12px !important',
  },
  [`@media (max-width:660px) and (max-height: 300px)`]:{
    lineHeight: '12px !important',
    fontSize: '14px !important',
    marginBottom:top?5:0,
  },

  [`@media (max-width:600px) and (max-height: 500px)`]: {
    lineHeight: '14px !important',
    fontSize: '10px !important',
    marginBottom:top?7:0,
  },

  [`@media (max-width:560px) and (max-height: 400px)`]: {
    lineHeight: '14px !important',
    fontSize: '12px !important',
    marginBottom:top?4:0,
  },
}));



export const InfoPopup = styled.div(() => ({
  position: 'fixed',
  left: '50%',
  top: '50%',
  transform: 'translate(-50%, -50%)',
  padding: '40px 10px 30px',
  maxWidth: '70%',
  width: '100%',
  background: '#323233',
  zIndex: 20,
  maxHeight: '90%',
}))

export const InfoPopupContent = styled.div(() => ({
  color: "#fff",
  display: 'flex',
  flexDirection: 'column',
  alignItems: 'center',
  position: 'relative',

  ['&:before'] : {
    content: "''",
    display: "block",
    position: "absolute",
    right: 0,
    left: 0,
    top: 0,
    width: '100%',
    height: 'calc(100% + 2px)',
    pointerEvents: 'none',
    boxShadow: 'inset 0 -15px 18px -4px #323233',
    zIndex: 2,
  }
}))

export const InfoPopupClose = styled.div(() => ({
  position: 'absolute',
  cursor: 'pointer',
  right: 20,
  top: 20,
  color: 'red',
  
}))

export const InfoPopupTitle = styled.div(() => ({
  textTransform: 'uppercase',
  fontSize: 21,
  marginBottom: 25,
  textAlign: 'center',
  color: "#CD8B7D",

  [`@media (max-width: 400px)`]:{
    fontSize: 15,
    marginBottom: 20,
  },

  [`@media (max-height: 400px)`]:{
    fontSize: 14,
    marginBottom: 15,
  },
}))

export const InfoPopupText = styled.div(() => ({
  marginBottom: 10,
  alignSelf: 'flex-start'
}))

export const InfoPopupList = styled.ul(() => ({
  margin: '0px 0px 10px',
  paddingLeft: 20,
  listStyle: 'circle',
}))


export const InfoPopupListItem = styled.li(() => ({
  marginBottom: 10,
  fontSize: 12,
}))

export const InfoPopupTextBox = styled.div(() => ({
  maxHeight: '45vh',
  overflowY: 'auto',
  fontSize: 12,
  padding: '0 10px 0',
}))

export const InfoPopupLink = styled.a(() => ({
  display: 'inline-block',
  fontSize: 20,
  margin: '0 auto',
  fontStyle: 'italic',
  textDecoration: 'underline',
}))

export const SuccessPopup = styled.div(() => ({
  position: "absolute",
  left: "50%",
  top: "50%",
  transform: "translate(-50%, -50%)",
  padding: "60px 20px",
  maxWidth: 400,
  width: "100%",
  zIndex: 20,
  background: "#ffffff"
}))

export const SuccessPopupText = styled.div(() => ({
  textAlign: "center",
  fontSize: 20,

}))

export const SuccessPopupClose = styled.div(() => ({
position: 'absolute',
  cursor: 'pointer',
  right: 20,
  top: 20,
  color: 'red',
}))


export const ModalBody = styled.div(() => ({
  position: "absolute",
  left: "50%",
  top: "50%",
  transform: "translate(-50%, -50%)",
  padding: "60px 20px 20px",
  maxWidth: 400,
  width: "100%",
  zIndex: 20,
  background: "#ffffff"
}))
export const ModalContent = styled.div(() => ({
  paddingBottom: 20,
  position: "relative",
}))

export const ModalTitle = styled.div(() => ({
  fontSize: 20,
  marginBottom: 20,
  textAlign: 'center',
}))

export const ModalText = styled.div(() => ({
  textAlign: "center",
  fontSize: 20,

}))

export const ModalClose = styled.div(() => ({
position: 'absolute',
  cursor: 'pointer',
  right: 20,
  top: 20,
  color: 'red',
}))

export const ModalFooter = styled.div(() => ({
  paddingTop: 20,
}))

export const ModalFieldsError = styled.div(({isError}) => ({
  display: isError ? 'block' : 'none',
  fontSize: 11,
  color: '#eb3a3a',
}))