import { imgArr } from "../../../data/Main/images";
import { StyledMain } from "./StyledMain";
import Image from "next/image";
import * as MS from "./SupportComponents";
import Link from "next/link";

//ширина всех картинок на страницу main
const widthImage = "25%";
//высота всех картинок на страницу main
const heightImage = "33.333%";

export const Main = ({ pageData: { grid } }) => {
  const { images, logo, mainText, secondaryText } = grid;

  return (
    <>
      <StyledMain className="page-content">
        {imgArr.map((img) => {
          return (
            <>
              {img.type == "UniqueCell_1" ? (
                <MS.UniqueCell_1
                  width={widthImage}
                  className={"main_allMobilePhoto " + img?.className}
                  img={logo.sourceUrl}
                />
              ) : img.type == "UniqueCell_2" ? (
                <MS.UniqueCell_2
                  width={widthImage}
                  className={"main_allMobilePhoto " + img?.className}
                  mainText={mainText}
                />
              ) : (
                img.type == "UniqueCell_3" && (
                  <MS.UniqueCell_3
                    width={widthImage}
                    className={"main_allMobilePhoto " + img?.className}
                    height={heightImage}
                    secondaryText={secondaryText}
                  />
                )
              )}
            </>
          );
        })}
        {/* imgArr нижк на первой стадии филтрую и получаю только те где есть фото  */}
        {/* на второй стадии заменя фото которые у меня локально на те что приходят с бэка */}
        {/* на третей просто отображаю */}
        {imgArr
          .filter((img1) => img1.type == "img")
          // .map((obj, i) => ({ ...obj, img: images[i].sourceUrl }))
          .map((img, i) => {
            return (
              <MS.ImageCell
                className={`main_allMobilePhoto ${img?.className}`}
                width={widthImage}
                height={heightImage}
                img={
                  <Image
                    className={`${img?.opacity && "image-cover"}`}
                    src={images[i]?.sourceUrl || img?.img}
                    quality={100}
                    width={300}
                    height={300}
                    objectFit="cover"
                    priority={true}
                  ></Image>
                }
              />
            );
          })}
      </StyledMain>
    </>
  );
};

// calc( ((100% - 132px) / 5) * 2 + 132px )

// calc( ((100% - 132px) / 5) * 3 )
