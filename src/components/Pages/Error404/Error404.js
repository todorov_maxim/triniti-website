// import withMenu from "../../../hoc/withMenu/withMenu";
import { compose } from "redux";
import {
  StyledError404,
  Error404Content,
  ErrorText,
  TexTop,
  TextBottom,
  WrapperErrorContent,
  ErrorLogo,
} from "./StyledError404";
import { Contact } from "../../Contact/Contact";

const Error404 = () => {
  return (
    <StyledError404>
      <Contact pageMediaBool={false} />
      <WrapperErrorContent>
        <Error404Content className="Error404Content">
          <ErrorText className="ErrorText">
            <TexTop>404</TexTop>
            <TextBottom>
              нажаль сторінка не знайдена. повторіть свій запит.
            </TextBottom>
          </ErrorText>
          <ErrorLogo className="icon-logoIcon"></ErrorLogo>
        </Error404Content>
      </WrapperErrorContent>
    </StyledError404>
  );
};

// export default compose(withMenu)(Error404);
export default Error404;
