import styled from "styled-components";
import { screenSize } from "../../../constants/media";

export const StyledError404 = styled.div`
display: flex;
flex-direction: column;
justify-content: space-between;
padding: 18px 21px;
height: 100%;
font-family: 'AGLettericaC';
position: relative;
overflow: hidden;
@media (max-width: ${screenSize.mobileL})  {
    padding: 14px 21px;
};
`

export const WrapperErrorContent = styled.div`
display: flex;
justify-content: center;
width: 100%;
height: 100%;


`

export const Error404Content = styled.div`
display: flex;
justify-content: space-between;
align-items: center;
max-width: 86%;
width: 100%;
margin-left:50px;

@media (max-width: ${screenSize.laptop})  {
    margin-left:unset;
};

@media (max-width: ${screenSize.laptopPortrait}) and (orientation: portrait){
    justify-content: center;
};
@media (max-width: ${screenSize.mobileL})  {
max-width: 100%;
};

@media (max-width: ${screenSize.mobileLLandscape}) and (orientation: landscape){
    justify-content: center;

};
`

export const ErrorText = styled.div`
display: flex;
flex-direction: column;
justify-content: center;
max-width: 50%;
width: 100%;
@media (max-width: ${screenSize.laptop})  {
    max-width: 57%;
};
@media (max-width: ${screenSize.laptopPortrait}) and (orientation: portrait){
    max-width: 400px;
    margin-bottom: 370px;
    margin-left: 20px; 
    z-index: 2;


};
@media (max-width: ${screenSize.mobileL})  {
    margin-bottom: 275px;
    margin-left: 14px;
max-width: 100%;
z-index: 2;
};

@media (max-width: ${screenSize.mobileLLandscape}) and (orientation: landscape){
    margin-bottom: 45px;
max-width: 360px;
margin-left: 13px;

};

`

export const TexTop = styled.div`
font-size: 250px;
line-height: 287px;
color: #494E54;
text-align: center;
letter-spacing: 0.15em;

@media (max-width: ${screenSize.laptopL}) {
    font-size: 200px;
line-height: 230px;
};
@media (max-width: ${screenSize.laptop})  {
    font-size: 150px;
line-height: 172px;
};

@media (max-width: ${screenSize.mobileL})  {
    font-size: 100px;
line-height: 115px;
};
@media (max-width: ${screenSize.mobileLLandscape}) and (orientation: landscape){
    margin-bottom: 15px;
};

`
export const TextBottom = styled.div`
font-size: 32px;
line-height: 60px;
text-align: center;
letter-spacing: 0.15em;
color: #494E54;
@media (max-width: ${screenSize.laptopL}) {
    font-size: 24px;
line-height: 40px;
};
@media (max-width: ${screenSize.laptop})  {
    font-size: 20px;
line-height: 35px;
};

@media (max-width: ${screenSize.mobileL})  {
    font-size: 16px;
line-height: 28px;
};
`

// ErrorLogo

export const ErrorLogo = styled.i`
font-size: 748px; opacity: 0.5; color: #EAD6D5;
@media (max-width: ${screenSize.laptopL}) {
font-size: 630px;
};

@media (max-width: ${screenSize.laptop})  {
    position: absolute;
right: -105px;

@media (max-width: 1090px) and (orientation: landscape){
    font-size: 460px;
};
@media (max-width: 1110px) and (orientation: portrait){
    font-size: 460px;
};

@media (max-width: ${screenSize.laptopPortrait}) and (orientation: portrait){
    right: unset;
    };
    @media (max-width: ${screenSize.mobileL})  {
    font-size: 300px
};
};
@media (max-width: ${screenSize.mobileLLandscape}) and (orientation: landscape){
    font-size: 260px;
    left: 50%;
    transform: translate(-50%,0px);
    right: unset;
};
`
// ErrorLogo