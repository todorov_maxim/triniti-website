import { Modal } from "../../Modal/Modal";
import { createContext, useState } from "react";
import Footer from "../../Footer";
import { FullScreenBlockGeneralPages } from "../../FullScreenBlockGeneralPages/FullScreenBlockGeneralPages";
import { pageTheme } from "../../../constants/themes";
import { VideoBox } from "../../VideoBox/VideoBox";
import ContactWithCompany from "../../ManagementPage/SupportComponent/ContactWithCompany";
import InfoTabsBlock from "../../InfoTabs/InfoTabsBlock";
import Slider from "../../Slider";
import { ModalImage } from "../../InfoTabs/StyledInfoTabs";

export const ModalImgSrcContext = createContext();

export const Rental = ({ pageData, showMenu, hiddenMenu }) => {
  const { background, textColor } = pageTheme[pageData?.pageTheme] || pageTheme.brown;
  const [isShowModal, setIsShowModal] = useState(false);
  const [modalContent, setModalContent] = useState(null);
  
  const [modalImgSrc, setmModalImgSrc] = useState(null);
  const tabsData = pageData?.tabsBlock;
  const showImgModal = (src) => {
    hiddenMenu();
    
    setmModalImgSrc(src);
    setModalContent('img');
    setIsShowModal(true);
  };
  const showVideoModal = () => {
    hiddenMenu();
    setIsShowModal(true);
    setModalContent('video')
  };
  const modalHidden = () => {
    showMenu();
    setIsShowModal(false);
  };


  return (
    <>
      {isShowModal && (
        <Modal modalHidden={modalHidden}>
          {modalContent === 'video' && <VideoBox video={pageData?.intro?.videoLink} />}
          {modalContent === 'img' && <ModalImage src={modalImgSrc || ''}/>}
        </Modal>
      )}
      <FullScreenBlockGeneralPages
        pageData={pageData}
        hasVideo
        textColor={textColor}
        showModal={showVideoModal}
      />
      <Slider
        reverse
        sliderData={pageData?.profilesSlider}
        textColor={textColor}
      />
      <ModalImgSrcContext.Provider value={{showImgModal}}>
        <InfoTabsBlock title={tabsData.title} subtitle={tabsData.subtitle} description={tabsData.description} tabs={tabsData.tabs}/>
      </ModalImgSrcContext.Provider>
      
      <ContactWithCompany contacts={pageData?.contacts} textColor={textColor} />
      <Footer pageData={pageData} />
    </>
  );
};
