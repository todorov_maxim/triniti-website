import { pageTheme } from "../../../constants/themes";
import { FullScreenBlockGeneralPages } from "../../FullScreenBlockGeneralPages/FullScreenBlockGeneralPages";
import { Content } from "./StyledContact";
import { ContactData } from "./SupportComponent/ContactData/ContactData";
import { MapAndFormAndLogo } from "./SupportComponent/MapAndFormAndLogo/MapAndFormAndLogo";

export const Contact = ({ pageData,showForm, onceLoaded }) => {
  const { background, textColor } = pageTheme[pageData?.pageTheme] || pageTheme.brown;

  return (
    <>
      <FullScreenBlockGeneralPages pageData={pageData} modify={{title: "small"}} />
      <Content>
        <ContactData
          textColor={background}
          departmentsContacts={pageData?.departmentsContacts}
          showForm={showForm} onceLoaded={onceLoaded}
        />
        <MapAndFormAndLogo logoColor={textColor} maplink={pageData?.maplink} />
      </Content>
    </>
  );
};
