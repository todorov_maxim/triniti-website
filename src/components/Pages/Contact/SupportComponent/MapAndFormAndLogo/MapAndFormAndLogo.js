import { useRef } from "react";
import { Logo } from "../../../../logo";
import {
  StyledMapAndFormAndLogo,
  Maps,
  WrapperFormaAndLodo,
  Forma,
  WrapperLogo,
} from "./StyledMapAndFormAndLogo";

export const MapAndFormAndLogo = ({ maplink, logoColor }) => {
  const iframeRef = useRef();

  return (
    <StyledMapAndFormAndLogo>
      <Maps
        src={maplink + "&output=embed"}
        allowfullscreen={""}
        loading={"lazy"}
      ></Maps>

      <WrapperFormaAndLodo>
        
        
        <iframe id="frame" ref={iframeRef} src="/form.html" width="100%" height="240px" frameBorder="0"></iframe>
        <WrapperLogo>
        <Logo
                namePage="AboutPage"
                fill={"#494E54"}
                mb={0}
              />
        </WrapperLogo>
      </WrapperFormaAndLodo>
    </StyledMapAndFormAndLogo>
  );
};
