
import styled from "styled-components";
import { screenSize } from "../../../../../constants/media";


export const StyledMapAndFormAndLogo = styled.div`
 max-width: 976px;
 width: 100%;
    @media (max-width: ${screenSize.laptopL}) {
      max-width: 707px;
    }
    @media (max-width: ${screenSize.laptop}) {
      max-width: 547px;
    }
    @media (max-width:995px) {
      display: flex;
flex-direction: column-reverse;
max-width: unset;
    }
    @media (max-width: ${screenSize.laptopPortrait}) {
      display: flex;
flex-direction: column-reverse;
max-width: unset;
    }
    
`;


export const Maps = styled.iframe`
 max-width: 976px;
 width: 100%;
  height: 508px;
  @media (max-width: ${screenSize.laptopL}) {
      max-width: 707px;
      height: 517px;
    }
    @media (max-width: ${screenSize.laptop}) {
      max-width: 547px;
    }
    @media (max-width:995px) {
      max-width: 100%;
    }
    @media (max-width: ${screenSize.laptopPortrait}) {
max-width: 100%;
    }
    @media (max-width: ${screenSize.mobileL}) and (orientation: portrait)  {
      height: 436px;
     
    }
`;



export const WrapperFormaAndLodo = styled.div`
display:flex;
margin-top: 41px;
@media (max-width: ${screenSize.laptopL}) {
  margin-top: 44px;
    }
    @media (max-width:995px) {
      margin-bottom: 53px;
  justify-content: center;
  margin-top: 70px;
    }
@media (max-width: ${screenSize.laptopPortrait}) {
  margin-bottom: 53px;
  justify-content: flex-end;
  padding-left: 35px;
  margin-top: 70px;
    }
    @media (max-width: ${screenSize.mobileLLandscape})  and (orientation: landscape)  {
      justify-content: center;
  margin-bottom: 73px;
  margin-top: 63px;
    }
    @media (max-width: ${screenSize.mobileL}) and (orientation: portrait)  {
      justify-content: center;
padding: unset;
margin-top: unset;
margin-bottom: 72px;
    }
`


export const FormContainer = styled.div`
display:flex;
flex: 1 1 auto;
`


export const WrapperLogo = styled.div`
flex:1 1 auto;
display:flex;
justify-content: center;
/* align-items: center; */
padding-top: 25px;
height: inherit;
@media (max-width:995px) {
  flex:0 1 375px;
    }
@media (max-width: ${screenSize.laptopPortrait}) {
flex:0 1 375px;
    }
    @media (max-width: ${screenSize.mobileLLandscape})  and (orientation: landscape)  {
display:none;
    }
    @media (max-width: ${screenSize.mobileL}) and (orientation: portrait)  {
      display:none;

    }


`


export const LogoVar = styled.i(({color='gray'})=>({
  '&:before':{
      color,
  }
}))
export const Logo = styled(LogoVar)`
font-size: 136px;
`

