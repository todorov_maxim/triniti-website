import useIsEmpty from "../../../../../hooks/useIsEmpty";
import {
  StyledContactData,
  WrapperBlocks,
  Block,
  Number,
  TitleText,
  Text,
  TextLink,
  Address,
  Schedule,
  Email,
  Title,
} from "./StyledContactData";
import { useEffect, useState, useMemo, useRef } from "react";
import Script from "next/script";
import { StyledFormButton } from "../../../../Contact/Form/StyledForm";

export const ContactData = ({ departmentsContacts, textColor,showForm, onceLoaded }) => {


  
  // useEffect(() => {
  //   // let d = document.querySelector('#frame');
  //   // console.log(iframeRef.current, d);
    

  //   iframeRef.current.postMessage(JSON.stringify({
  //     utm_campaign: localStorage.getItem('utm_campaign'), 
  //     utm_medium: localStorage.getItem('utm_medium'), 
  //     utm_source: localStorage.getItem('utm_source')
  //   }), "*");
  // }, [])

//   window.onmessage = function(e) {

//     var payload = JSON.parse(e.data);
//     localStorage.setItem(payload.key, payload.data);

// };
  

  return (
    <StyledContactData id={'bx24-form-content'}>
      {departmentsContacts.map(
        ({ department, tel, address, schedule, email }) => {
          return (
            <>
              <Title>{department}</Title>
              <WrapperBlocks>
                <Block>
                  {!useIsEmpty(tel) && (
                    <Number>
                      <TitleText>Контактний телефон</TitleText>
                      <TextLink
                        href={`tel:+${tel}`}
                        target="_blank"
                        color={textColor}
                      >
                        {tel}
                      </TextLink>
                    </Number>
                  )}
                  {!useIsEmpty(schedule) && (
                    <Schedule>
                      <TitleText>Графік роботи</TitleText>
                      <Text color={textColor}>{schedule}</Text>
                    </Schedule>
                  )}
                </Block>

                <Block>
                  {!useIsEmpty(address) && (
                    <Address>
                      <TitleText>Адреса</TitleText>
                      <Text color={textColor}>{address}</Text>
                    </Address>
                  )}
                  {!useIsEmpty(email) && (
                    <Email>
                      <TitleText>E-mail </TitleText>
                      <TextLink
                        href={`mailto:${email}`}
                        target="_blank"
                        color={textColor}
                      >
                        {email}
                      </TextLink>
                    </Email>
                  )}
                  
                </Block>
                
              </WrapperBlocks>
             
              {/* {onceLoaded && b24SScript} */}
            </>
          );
        }
      )}
    </StyledContactData>
  );
};
