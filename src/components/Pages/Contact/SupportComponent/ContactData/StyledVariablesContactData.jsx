
import styled from "styled-components";
import { screenSize } from "../../../../../constants/media";


export const ContactText=styled.div`
font-size: 14px;
line-height: 18px;
max-width: 278px;
width: 100%;
@media (max-width: ${screenSize.laptop}) {
    max-width: 160px;
    }
    @media (max-width: ${screenSize.laptopPortrait}) and (orientation: landscape)  {
      max-width: inherit;
    }
    @media (max-width: ${screenSize.mobileL}) and (orientation: portrait)  {
      max-width: inherit;
    }
`
export const ContactTextLink=styled.a`
font-size: 14px;
line-height: 18px;
max-width: 278px;
width: 100%;
@media (max-width: ${screenSize.laptop}) {
    max-width: 160px;
    }
    @media (max-width: ${screenSize.laptopPortrait}) and (orientation: landscape)  {
      max-width: inherit;
    }
    @media (max-width: ${screenSize.mobileL}) and (orientation: portrait)  {
      max-width: inherit;
    }
`

export const TextVarLink = styled(ContactTextLink)(({color='black'})=>({
  color
}))

export const TextVar = styled(ContactText)(({color='black'})=>({
    color
  }))