
import styled from "styled-components";
import { screenSize } from "../../../../../constants/media";
import { ContactText, TextVar, TextVarLink } from "./StyledVariablesContactData";

export const StyledContactData = styled.div`
margin-right:133px;
@media (max-width: ${screenSize.laptopL}) {
margin-right:84px;
    }
    @media (max-width: ${screenSize.laptop}) {
      margin-right:40px;
    }
    @media (max-width:995px) {
      margin-right:unset;
      padding:0px 35px;
    }
    @media (max-width: ${screenSize.laptopPortrait}) {
      margin-right:unset;
      padding:0px 35px;
    }
    
    @media (max-width: ${screenSize.mobileLLandscape}) and (orientation: landscape)  {
      padding:0px 60px;
    }
    @media (max-width: ${screenSize.mobileL}) and (orientation: portrait)  {
      padding:0px 30px;
    }
`;


export const ContactBlock=styled.div`
display: flex;
flex-direction: column;
margin-bottom: 32px;
max-width: 278px;
width: 100%;

@media (max-width: ${screenSize.laptop}) {
  max-width: 166px;
    }
    @media (max-width:995px) {
      max-width: unset;
flex:1 1 166px;
    }
    @media (max-width: ${screenSize.laptopPortrait}) {
  max-width: unset;
flex:1 1 166px;
    }
    @media (max-width: ${screenSize.mobileLLandscape}) and (orientation: landscape)  {
      flex:unset;
      max-width: 260px;
    }
    @media (max-width: ${screenSize.mobileL}) and (orientation: portrait)  {
      flex: unset;
margin-bottom: 20px;
    max-width: inherit;
    }
`

export const WrapperBlocks = styled.div`
  display: flex;
  flex-direction: column;
  max-width: 471px;
width:100%;
/* justify-content: space-between; */
margin-bottom:20px;
@media (max-width: ${screenSize.laptopL}) {
  max-width: 441px;
    }
    @media (max-width: ${screenSize.laptop}) {
  max-width: 361px;
  margin-bottom:15px;
    }
    @media (max-width:995px) {
      flex-direction: row;
    max-width: 100%;
    }
    @media (max-width: ${screenSize.laptopPortrait}) {
    flex-direction: row;
    max-width: 100%;
margin-bottom:52px;

    }
    @media (max-width: ${screenSize.mobileLLandscape}) and (orientation: landscape)  {
      flex-direction: row;
    max-width: 100%;
margin-bottom:60px;

    }
    @media (max-width: ${screenSize.mobileL}) and (orientation: portrait)  {
      flex-direction: column;
margin-bottom:38px;
    
    }
    
`;


export const Title = styled.div`
color:#494E54;
margin-bottom: 54px;
font-size: 32px;
line-height: 42px;
letter-spacing: 0.15em;
text-transform: uppercase;
@media (max-width: ${screenSize.mobileLLandscape})  {
  font-size: 24px;
line-height: 28px;
}
@media (max-width: ${screenSize.mobileL}) and (orientation: portrait)  {
      flex-direction: column;
margin-bottom:24px;
    }
`;


export const Block = styled.div`
  display: flex;
  @media (max-width:995px) {
    flex:1 1 auto;
    }
  @media (max-width: ${screenSize.laptopPortrait}) {
flex:1 1 auto;
    }
    @media (max-width: ${screenSize.mobileLLandscape}) and (orientation: landscape)  {
      flex-direction: column;
      flex:unset;
      min-width: 166px;
      max-width:260px;
      margin-right:55px
    }
    @media (max-width: ${screenSize.mobileL}) and (orientation: portrait)  {
      flex-direction: column;
    }
    
`;


export const Number = styled(ContactBlock)`
margin-right: 80px;
@media (max-width: ${screenSize.laptopL}) {
  margin-right: 50px;
    }
    @media (max-width:995px) {
      margin-right: 18px;
    }
    @media (max-width: ${screenSize.laptopPortrait}) {
      margin-right: 18px;
    }
    @media (max-width: ${screenSize.mobileLLandscape}) and (orientation: landscape)  {
      margin-right: unset;
    }
    
`;


export const TitleText = styled(ContactText)`
color:#494E54;
`;


export const TextLink= styled(TextVarLink)`

`;
export const Text = styled(TextVar)`

`;


export const Address = styled(ContactBlock)`
margin-right: 80px;
@media (max-width: ${screenSize.laptopL}) {
  margin-right: 50px;
    }
    @media (max-width:995px) {
      margin-right: 18px;
    }
    @media (max-width: ${screenSize.laptopPortrait}) {
      margin-right: 18px;
    }
  

`;

export const Schedule = styled(ContactBlock)`
margin-right:unset;
@media (max-width:995px) {
      margin-right: 18px;
    }
@media (max-width: ${screenSize.laptopPortrait}) {
      margin-right: 18px;
    }
    @media (max-width: ${screenSize.mobileLLandscape}) and (orientation: landscape)  {
      margin-right: unset;
    }
  
`;

export const Email = styled(ContactBlock)`
margin-right:unset;
  
`;
  