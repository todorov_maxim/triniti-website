import styled from "styled-components";
import { screenSize } from "../../../constants/media";



export const Content = styled.div`
display: flex;
justify-content: space-between;
max-width: 1660px;
width: 100%;
margin:0 auto;
padding:0px 40px;
margin-top:118px;
margin-bottom: 136px;
@media (max-width: ${screenSize.laptopL}) {
      max-width: 1292px;
padding:0px 35px;
margin-top:75px;
margin-bottom: 86px;
    }

    @media (max-width: ${screenSize.laptop}) {
margin-bottom: 77px;
    }

    @media (max-width:995px) {
      flex-direction: column;
padding:0px;
margin-top:60px;
margin-bottom: unset;
    }

    @media (max-width: ${screenSize.laptopPortrait}) {
flex-direction: column;
padding:0px;
margin-top:60px;
    }

`;