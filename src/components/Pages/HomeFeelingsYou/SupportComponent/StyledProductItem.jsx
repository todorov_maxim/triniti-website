import styled from "styled-components";
import { screenSize } from "../../../../constants/media";



export const StyledProductItem = styled.div`
display:flex;
flex-direction: column;
max-width: 50%;
padding-right: 49px;
margin-bottom: 132px;
@media (max-width: ${screenSize.laptopL}) {
    padding-right: 39px;
margin-bottom: 104px;
  };
  @media (max-width: ${screenSize.laptop}) {
    padding-right: 36px;
margin-bottom: 68px;

  };
  @media (max-width: ${screenSize.laptopPortrait}) {
    padding-right: 27px;
margin-bottom: 62px;
  };
  @media (max-width: ${screenSize.mobileLLandscape})and (orientation: landscape) {
    padding-right: 18px;
margin-bottom: 38.5px;

  };
  @media (max-width: ${screenSize.mobileL})and (orientation: portrait) {
    padding-right: unset;
max-width: 100%;
margin-bottom: 55px;
  };

&:nth-child(2n+2){
padding-left: 49px;
padding-right: unset;
@media (max-width: ${screenSize.laptopL}) {
    padding-left: 39px;
  };
  @media (max-width: ${screenSize.laptop}) {
    padding-left: 36px;
  };
  @media (max-width: ${screenSize.laptopPortrait}) {
    padding-left: 27px;
  };
  @media (max-width: ${screenSize.mobileLLandscape})and (orientation: landscape) {
    padding-left: 18px;
  };
  @media (max-width: ${screenSize.mobileL})and (orientation: portrait) {
    padding-left: unset;
  };
}

`

export const WrapperImg = styled.div`
flex: 0 0 auto;

span{
    width: 100% !important;
    height: 100% !important;
}
height: 540px;
margin-bottom: 33px;

@media (max-width: ${screenSize.laptopL}) {
height: 500px;
margin-bottom: 26px;
  };

  @media (max-width: ${screenSize.laptopPortrait}) {
    margin-bottom: 16px;
  };
  @media (max-width: 420px) {
    height: 400px;
  }
`


export const WrapperTextContent = styled.div`
display: flex;
flex: 1 1 auto;
justify-content: space-between;
@media (max-width: 1000px) {
    flex-direction: column;
  };
@media (max-width: ${screenSize.laptopPortrait}) {
flex-direction: column;
  };
 
`

export const WrapperTitleAndDescription = styled.div`
display: flex;
flex-direction: column;
max-width: 443px;
margin-right:10px;
@media (max-width: ${screenSize.laptop}) {
    margin-right:19px;
  };
  @media (max-width: 1000px) {
    margin-right:unset;
  };

`

export const Title = styled.div`
font-size: 24px;
line-height: 1.2em;
letter-spacing: 0.15em;
text-transform: uppercase;
color: #494E54;
`

export const Description = styled.div`
font-size: 12px;
line-height: 18px;
color: #494E54;

`

export const WrapperPriceAndButton = styled.div`
display: flex;
flex-direction: column;
@media (max-width: 1000px) {
    flex-direction: row;
align-items: center;
justify-content: space-between;
  };
@media (max-width: ${screenSize.laptopPortrait}) {
flex-direction: row;
align-items: center;
justify-content: space-between;
  };
  @media (max-width: 665px) and (orientation: landscape)  {
flex-direction: column;
  };
  @media (max-width: 350px) {
flex-direction: column;
  };
`

export const Price = styled.div`
font-size: 24px;
line-height: 65px;
text-align: right;
letter-spacing: 0.15em;
text-transform: uppercase;
color: #CD8B7D;
margin-right:10px;
min-width: max-content;
`

export const Button = styled.a`
&:hover{
  background: #494E54;
}
font-size: 12px;
line-height: 12px;
padding: 19px 41px;
background: #CD8B7D;
border-radius:10px;
color: #FFFFFF;
text-align: center;
letter-spacing: 0.5em;
text-transform: uppercase;
@media (max-width: ${screenSize.laptopL}) {
    padding: 14px 20px;
  };
`
