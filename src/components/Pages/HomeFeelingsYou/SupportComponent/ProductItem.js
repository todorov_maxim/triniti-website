import Image from "next/image";
import useIsEmpty from "../../../../hooks/useIsEmpty";
import {
  StyledProductItem,
  WrapperImg,
  WrapperTextContent,
  WrapperTitleAndDescription,
  Title,
  Description,
  WrapperPriceAndButton,
  Price,
  Button,
} from "./StyledProductItem";

export const ProductItem = ({
  description,
  link,
  linktext,
  name,
  price,
  img,
}) => {
  return (
    <StyledProductItem>
      <WrapperImg>
        <Image
          src={img?.sourceUrl}
          objectFit="cover"
          quality={100}
          width={740}
          height={433}
        />
      </WrapperImg>
      <WrapperTextContent>
        <WrapperTitleAndDescription>
          <Title>{name}</Title>
          <Description>{description}</Description>
        </WrapperTitleAndDescription>
        <WrapperPriceAndButton>
          <Price>{price}</Price>
          {!useIsEmpty(link) && (
            <Button target="_blank" href={link}>
              {linktext}
            </Button>
          )}
        </WrapperPriceAndButton>
      </WrapperTextContent>
    </StyledProductItem>
  );
};
