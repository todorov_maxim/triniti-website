import styled from "styled-components";
import { screenSize } from "../../../../../constants/media";

export const StyledProductList = styled.div`
margin-top: 150px;
padding: 0px 169px;
display: flex;
flex-wrap: wrap;

@media (max-width: ${screenSize.laptopL}) {
    padding: 0px 120px;
margin-top: 100px;
  };
  @media (max-width: ${screenSize.laptop}) {
    padding: 0px 34px;
margin-top: 104px;
  };
  @media (max-width: ${screenSize.laptopPortrait}) {
    padding: 0px 27px;
margin-top: 51px;

  };
  @media (max-width: ${screenSize.mobileLLandscape})and (orientation: landscape) {
    padding: 0px 20px;
margin-top: 73px;

  };
  @media (max-width: ${screenSize.mobileL})and (orientation: portrait) {
flex-direction: column;
margin-top: 43px;

  };
`
