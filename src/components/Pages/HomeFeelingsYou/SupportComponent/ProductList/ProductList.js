import { ProductItem } from "../ProductItem";
import { StyledProductList } from "./StyledProductList";

export const ProductList = ({ production }) => {
  return (
    <StyledProductList>
      {production.map(({ production }) => (
        <ProductItem {...production} />
      ))}
    </StyledProductList>
  );
};
