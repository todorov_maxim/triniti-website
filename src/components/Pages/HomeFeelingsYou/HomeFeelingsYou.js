import { compose } from "redux";
import { pageTheme } from "../../../constants/themes";
import withFixHeaderMenuAndContact from "../../../hoc/withFixHeaderMenuAndContact/withFixHeaderMenuAndContact";
import withMenu from "../../../hoc/withMenu";
import Footer from "../../Footer";
import { FullScreenBlockGeneralPages } from "../../FullScreenBlockGeneralPages/FullScreenBlockGeneralPages";
import Slider from "../../Slider";
import { ProductList } from "./SupportComponent/ProductList/ProductList";
import ContactWithCompany from "../../ManagementPage/SupportComponent/ContactWithCompany";

const HomeFeelingsYou = ({ pageData }) => {
  const { background, textColor } = pageTheme[pageData?.pageTheme] || pageTheme.brown;
  const { production } = pageData;
  return (
    <>
      <FullScreenBlockGeneralPages pageData={pageData} />
      <Slider
        reverse
        sliderData={pageData?.profilesSlider}
        textColor={textColor}
      />
      <ProductList production={production} />
      <ContactWithCompany contacts={pageData?.contacts} textColor={textColor} />
      <Footer pageData={pageData} />
    </>
  );
};
// кнопку не обязательно можно не выводить если
export default compose(withMenu)(HomeFeelingsYou);
