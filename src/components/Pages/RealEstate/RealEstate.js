import {Modal} from "../../Modal/Modal"
import {useState} from "react"
import MiniSlider from "../../MiniSlider"
import Footer from "../../Footer"
import {FullScreenBlockGeneralPages} from "../../FullScreenBlockGeneralPages/FullScreenBlockGeneralPages"
import Slider from "../../Slider"
import {pageTheme} from "../../../constants/themes"
import {VideoBox} from "../../VideoBox/VideoBox"
import {ServicesList} from "../../ManagementPage/SupportComponent/ServicesList/ServicesList"
import {ContainerContent} from "../../ManagementPage/SupportComponent/ContactWithCompany/StyledContactWithCompany"
import ContactWithCompany from "../../ManagementPage/SupportComponent/ContactWithCompany"
import Progress from "../../Progress/Progress"
import {SelectedPeople, SliderDescription, SliderIconComma, SliderName, SliderTitle, WrapperImg, WrapperSelectedPeopleAndSliderPhoto} from "../../Slider/SyledSlider"
import Image from "next/image"
import { Profile } from "../../Profile/Profile"

export const RealEstate = ({pageData, showMenu, hiddenMenu}) => {
  const {background, textColor} = pageTheme[pageData?.pageTheme] || pageTheme.brown
  const listArr = pageData?.advantages?.advantagesList.map((item, i) => ({
    text: item?.text,
  }))
  const advantagesTitle = pageData?.advantages?.title

  const [isShowModal, setIsShowModal] = useState(false)
  const [selectedGalery, setSelectedGalery] = useState([])
  const showModal = () => {
    hiddenMenu()
    // setSelectedGalery(galery);
    setIsShowModal(true)
  }
  const modalHidden = () => {
    showMenu()
    setIsShowModal(false)
    setSelectedGalery([])
  }
  
  return (
    <>
      {isShowModal && (
        <Modal modalHidden={modalHidden}>
          <VideoBox video={pageData?.intro?.videoLink} />
        </Modal>
      )}
      <FullScreenBlockGeneralPages pageData={pageData} hasVideo showModal={showModal} />

      <Slider reverse sliderData={pageData?.profilesSlider} textColor={textColor} />
      <Profile/>
      {/* <ContainerContent variant={"sm"}>
        <ServicesList
          textColor={textColor}
          listArr={listArr}
          title={advantagesTitle}
          variant={"lg"}
          cols={4}
        />
      </ContainerContent>
      <ContainerContent variant={"xl"}>
        <MiniSlider
          namePage={"realEstate"}
          realEstate
          gallery={pageData?.slider}
        />
      </ContainerContent>

      <Progress info={pageData?.progress} /> */}
      <ContactWithCompany contacts={pageData?.contacts} textColor={textColor} />
      <Footer pageData={pageData} />
    </>
  )
}
