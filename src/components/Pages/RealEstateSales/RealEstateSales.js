import { Modal } from "../../Modal/Modal";
import { useState } from "react";
import Footer from "../../Footer";
import { FullScreenBlockGeneralPages } from "../../FullScreenBlockGeneralPages/FullScreenBlockGeneralPages";
import Slider from "../../Slider";
import { pageTheme } from "../../../constants/themes";
import { VideoBox } from "../../VideoBox/VideoBox";
import ContactWithCompany from "../../ManagementPage/SupportComponent/ContactWithCompany";
import { AboutContent } from "../About/SupportComponent/AboutContent/AboutContent";
import { WrapperSlider } from "./StyledRealEstateSales";
import Flats from "../../Flats/Flats";

export const RealEstateSales = ({ pageData, showMenu, hiddenMenu }) => {
  const { background, textColor } = pageTheme[pageData?.pageTheme] || pageTheme.brown;
  const listArr = pageData?.advantages?.advantagesList.map((item, i) => ({
    text: item?.text,
  }));
  const advantagesTitle = pageData?.advantages?.title;

  const [isShowModal, setIsShowModal] = useState(false);
  const [selectedGalery, setSelectedGalery] = useState([]);
  const showModal = () => {
    hiddenMenu();
    // setSelectedGalery(galery);
    setIsShowModal(true);
  };
  const modalHidden = () => {
    showMenu();
    setIsShowModal(false);
    setSelectedGalery([]);
  };

  return (
    <>
      {isShowModal && (
        <Modal modalHidden={modalHidden}>
          <VideoBox video={pageData?.intro?.videoLink} />
        </Modal>
      )}
      <FullScreenBlockGeneralPages
        pageData={pageData}
        hasVideo
        showModal={showModal}
      />
      <AboutContent textColor={textColor} pageData={pageData} realEstateSales />
      {/* <Flats textColor={textColor} info={pageData?.flats} /> */}

      <WrapperSlider>
        <Slider sliderData={pageData?.profilesSlider} textColor={textColor} />
      </WrapperSlider>
      <ContactWithCompany contacts={pageData?.contacts} textColor={textColor} />
      <Footer pageData={pageData} />
    </>
  );
};
