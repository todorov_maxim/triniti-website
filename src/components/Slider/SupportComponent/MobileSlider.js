import {
  WrapperSelectedPeopleAndSliderPhoto,
  SelectedPeople,
  SliderTitle,
  SliderName,
  SliderDescription,
  SliderIconComma,
  WrapperImg2,
  StyledMobileSlider,
  SliderPhotosListWrapper,
  CentralImg,
} from "../SyledSlider";
import Image from "next/image";
import { DescriptionNextPeople } from "./DescriptionNextPeople/DescriptionNextPeople";
import { SliderPhotos } from "./SliderPhotos";

export const MobileSlider = ({
  currentPeople,
  setCurrentPeople,
  setNextPeople,
  showPeopleArr,
  setShowPeopleArr,
  calcNextPeople,
  setIsLoadMobileImg,
  isLoadMobileImg,
  isHiddenSlidesPhoto,
  startHiddenPhotos,
  setisHiddenSlidesPhoto,
  textColor,
  reverse
}) => {
  return (
    <StyledMobileSlider>
      <WrapperSelectedPeopleAndSliderPhoto>
        {currentPeople.map(({ profilesSliderData: currSlider }, i) => {
          return (
            <>
              <SliderTitle>{currSlider.post}</SliderTitle>
              <SelectedPeople>
                <SliderIconComma className="icon-comma" />
                <SliderName>{currSlider.name}</SliderName>
                <SliderDescription>{currSlider.description}</SliderDescription>
              </SelectedPeople>
            </>
          );
        })}
        <CentralImg>
          {currentPeople.map((item, i) => {
            return (
              <WrapperImg2 className={`appear ${isLoadMobileImg && "hidden"}`} key={i}>
                <Image
                  src={item?.profilesSliderData?.img?.sourceUrl}
                  width={300}
                  height={300}
                  quality={100}
                  objectFit="cover"
                  onLoad={() => setIsLoadMobileImg(false)}
                ></Image>
              </WrapperImg2>
            );
          })}
        </CentralImg>
        <SliderPhotosListWrapper reverse={reverse}>
        {showPeopleArr.length > 1 && 
        <><SliderPhotos
            setCurrentPeople={setCurrentPeople}
            currentPeople={currentPeople}
            showPeopleArr={showPeopleArr}
            setShowPeopleArr={setShowPeopleArr}
            isHiddenSlidesPhoto={isHiddenSlidesPhoto}
            startHiddenPhotos={startHiddenPhotos}
            setisHiddenSlidesPhoto={setisHiddenSlidesPhoto}
          />
          <DescriptionNextPeople
            nextPeople={calcNextPeople()}
            textColor={textColor}
            reverse={reverse}
            currNumber={currentPeople[0].number}
            setNextPeople={setNextPeople}
            allPeopleLength={showPeopleArr.length}
          /></>
        }
        </SliderPhotosListWrapper>
      </WrapperSelectedPeopleAndSliderPhoto>
    </StyledMobileSlider>
  );
};
