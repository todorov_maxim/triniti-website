import styled, {css}  from "styled-components";
import { screenSize } from "../../../../constants/media";


export const StyledDescriptionNextPeopleDef=styled.div`
display: flex;
width: 290px; height: 250px; background:#494E54;
flex-direction: column;
justify-content: space-between;
padding: 50px 38px 20px;
// position: absolute;
// left: 0px;
// bottom: 0px;


@media (max-width: ${screenSize.laptopL}) {
    width: 244px;
   height: 210px;
padding:33px 28px 35px 35px;
  };
  @media (max-width:770px) {
height: 186px;
  };
  
  @media (max-width: ${screenSize.laptop}) {
    width: 200px;
   height: 200px;
padding:33px 22px 15px 22px;
  };
  @media (max-width: 992px) {
    width: 100%

  }
  @media (max-width: ${screenSize.mobileLLandscape})  and (orientation: landscape) {
    width: calc( 100% - 139px);
   height: 150px;
padding:27px 14px 12px 14px;
  };
  @media (max-width: ${screenSize.slyderMobileL} ){
position: unset;
width: 50%;
padding:27px 14px 12px 14px;
  };
  @media (max-width: 420px ) {
    width: 60%
  }
  @media (max-width: 358px ) and (orientation: portrait) {
width: 100%;
  };

`

const setReverse=(reverse)=>{
  if(!reverse) return {}
  // 1920x1080
  return{
    right:0,
    left:'unset'
}
  // 1920x1080

}

export const StyledDescriptionNextPeople=styled(StyledDescriptionNextPeopleDef)(({reverse})=>({
  ...setReverse(reverse)
}))




export const NextPost=styled.div`
font-size: 12px;
line-height: 18px;
letter-spacing: 0.15em;
text-transform: lowercase;
${({textColor =  "#D9D3C5"}) => textColor && css`
  color: ${ textColor ? textColor : "#D9D3C5"}

`};
max-width: 135px;

@media (max-width: ${screenSize.laptopL}) {
    max-width: 95px;
  width: 100%;
  };
  @media (max-width: ${screenSize.slyderMobileL} ) and (orientation: portrait) {
    max-width: 150px;
  };
  @media (max-width: 610px)  {
    max-width: 150px;
  };
`

export const TopNextPeople=styled.div`
display: flex; 
${({reverse}) => reverse && css`
  flex-direction: row-reverse;
`}
justify-content: space-between;
align-items: center;
`

export const BottomNextPeople=styled.div`
display: flex; justify-content: space-between;
align-items: flex-end;
`

export const NameNextPeople=styled.div`
font-size: 18px;
line-height: 22px;
color:#FFFFFF;
text-transform: uppercase;
cursor: pointer;
flex: 0 1 calc(100% - 45px);
max-width: calc(100% - 45px);

@media (max-width: ${screenSize.laptop}) {
  font-size: 16px;
line-height: 20px;
flex: 0 1 80%;
max-width: 80%;
}
  @media (max-width: ${screenSize.slyderMobileL} ) and (orientation: portrait) {
font-size: 18px;
  };
  @media (max-width: 610px)  {
    font-size: 18px;
  };

`
export const ArrowNext=styled.i`
font-size:21px;
color:#FFFFFF;
cursor: pointer;
@media (max-width: ${screenSize.laptopL}) {
  font-size:18px;
  };

`

export const StatusNumber=styled.div`
min-width: max-content;
display: flex;

`

export const CurrNum=styled.div`
font-size: 12px;
line-height: 18px;
min-width: max-content;
${({textColor = "#D9D3C5"}) => textColor && css`
  color: ${ textColor ? textColor : "#D9D3C5"}

`};
margin-right: 4px;
`

export const AllNum=styled.div`
font-size: 12px;
line-height: 18px;
min-width: max-content;
${({textColor = "#D9D3C5"}) => textColor && css`
  color: ${ textColor ? textColor : "#D9D3C5"}

`};
opacity: 0.25;
`
