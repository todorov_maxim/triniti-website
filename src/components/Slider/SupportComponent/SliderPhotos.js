import { WrapperImg, StyledSliderPhotos } from "../SyledSlider";
import Image from "next/image";

export const SliderPhotos = ({
  showPeopleArr,
  setShowPeopleArr,
  setCurrentPeople,
  isHiddenSlidesPhoto,
  setisHiddenSlidesPhoto,
  startHiddenPhotos,
  reverse,
}) => {
  const setPeople = (item, ind, id) => {
    startHiddenPhotos();
    setTimeout(() => {
      // setCurrentPeople выбранный слайд показываю
      setCurrentPeople(showPeopleArr.filter((item) => item.id == id));

      // setShowPeopleArr меняю масив слайдов относительрно выбраного слайда
      setShowPeopleArr((pre) => [
        ...pre.filter((it, i) => i >= ind),
        ...pre.filter((it, i) => i < ind),
      ]);
      // Включаю анимацию появления маленьких фоток
      setisHiddenSlidesPhoto(false);
    }, 800);
  };

  return (
    <StyledSliderPhotos reverse={reverse}>
      {showPeopleArr.map((item, i, arr) => {

        const numberShow = arr.length - 1;
        const isOnlyTwoPhoto = arr?.length == 2;
        if (i > 0) {
          return (
            <WrapperImg
              className={`appear ${isHiddenSlidesPhoto && "hidden"}`}
              curr={i}
              key={i}
              numberShow={numberShow}
              isOnlyTwoPhoto={isOnlyTwoPhoto}
              isHidden={isHiddenSlidesPhoto}
            >
              <Image
                key={item.id}
                onClick={() => {
                  setPeople(item, i, item.id);
                }}
                src={item.profilesSliderData?.img?.sourceUrl}
                objectFit="cover"
                layout="responsive"
                quality={100}
                width={300}
                height={300}
                priority={true}
              ></Image>
            </WrapperImg>
          );
        }
      })}
    </StyledSliderPhotos>
  );
};
