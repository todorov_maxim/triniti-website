import styled from "styled-components";
import { screenSize } from "../../constants/media";
import { ProfileDescription, ProfileIconComma, ProfileName, ProfileTitle, WrapperNameAndDescription } from "../Pages/About/SupportComponent/AboutContent/StyledAboutContent";


export const SyledWrapperSliderDef=styled.div`
min-height: 740px;
max-width: 1444px;
margin: 0 auto;
padding: 20px 0 40px;


@media (max-width: ${screenSize.laptopL}) {
max-width: 1400px;
// margin: unset;

  min-height: max-content;
  };
//   @media (max-width: ${screenSize.laptop}) {
//     max-width: 989px;
// margin: unset;
//   };




`

const SetReverseSyledWrapperSlider=(reverse)=>{
  if(!reverse) return {}
return {
  maxWidth:1582,
  marginTop:127,
  [`@media (max-width: ${screenSize.laptopL} )`]: {
    marginTop:77,
  },
  [`@media (max-width: ${screenSize.laptop} )`]: {
    maxWidth:'unset',
    marginTop:70,
  },
  [`@media (max-width: ${screenSize.laptopPortrait} )`]: {
    marginTop:54,
  },
  [`@media (max-width: ${screenSize.mobileLLandscape} )`]: {
    marginTop:34,
  },
  [`@media (max-width: ${screenSize.mobileLLandscape} )`]: {
    marginTop:23,
  },
}
}

export const SyledWrapperSlider=styled(SyledWrapperSliderDef)(({reverse})=>({
 ...SetReverseSyledWrapperSlider(reverse)
}))


export const SyledSliderDef=styled.div`
display: flex;
min-height: 740px;
justify-content: center;

  @media (max-width: ${screenSize.laptopPortrait}) {
  min-height: 580px;
  };

  @media (max-width: ${screenSize.mobileLLandscape})  and (orientation: landscape) {
  min-height: 406px;
  padding-top:104px;
position: relative;
  };

  @media (max-width: 992px) {
    min-height: 600px;
  }

  @media (max-width: ${screenSize.slyderMobileL} ) and (orientation: portrait)  {
  display: none;
  };
  @media (max-width: 610px)  {
  display: none;
  };
  @media (max-height: 770px) {
   height: 100%;
  min-height: unset;
  };

`


export const SyledSlider=styled(SyledSliderDef)(({reverse})=>({
flexDirection:reverse&&'row-reverse',

[`@media (max-width: ${screenSize.mobileLLandscape}) and (orientation: landscape)`]:
{
  minHeight:reverse&& 463,
  paddingTop:'unset',
  alignItems: 'flex-end'
},
}))


export const WrapperSelectedPeopleAndSliderPhotoDef=styled.div`
display: flex;
flex-direction: column;
justify-content: space-between;
max-width: calc( ((100% - 132px) / 5) * 3 );
width: 100%;

@media (max-width: ${screenSize.laptopL}) {
  /* max-width: 730px; */
max-width: calc( ((100% - 104px) / 5) * 3 );

  };
  @media (max-width: ${screenSize.laptop}) {
max-width: calc( ((100% - 78px) / 4) * 2 );

  /* max-width: 455px; */
  };
  @media (max-width: ${screenSize.laptopPortrait}) {
  /* max-width: 360px; */
max-width: calc( ((100% - 48px) / 4) * 2 );
  };
  @media (max-width: ${screenSize.mobileLLandscape}) and (orientation: landscape) {
    max-width: calc( ((100% - 139px) / 3) * 2 );
  };
  @media (max-width: ${screenSize.slyderMobileL})  and (orientation: portrait) {
    max-width: 100%;
  };

  @media (max-width: ${screenSize.mobileL}) and (orientation:portrait ) {
  max-width: 100%;
  };

  @media (max-width: 610px)  {
    max-width: 100%;
  };
  
`

export const FlexBlock=styled.div`
display: flex;
@media (max-width: 992px) {
  display: block;
  };
`
export const FlexBox=styled.div`
flex: 0 0 50%;
max-width: 50%;

@media (max-width: 992px) {
  flex: 0 0 100%;
max-width: 100%;
  };
`

export const WrapperSelectedPeopleAndSliderPhoto=styled(WrapperSelectedPeopleAndSliderPhotoDef)(({reverse})=>({
  [`@media (max-width: ${screenSize.mobileLLandscape}) and (orientation: landscape)`]:{
    minHeight:reverse&& 463,
  },
}))

export const SelectedPeopleDef=styled.div`
min-height: unset;
width: 100%;
max-width: 672px;
position: relative;


@media (max-width: 1623px) {
  padding-left: 80px;
  max-width: 100%;
  };

@media (max-width: ${screenSize.laptopL}) {
  padding-left: 80px;
  max-width: 100%;
margin-bottom: 30px;
  };

  @media (max-width: ${screenSize.laptop}) {
  padding-left: 37px;
padding-right: 18px;
  };


  @media (max-width: ${screenSize.laptopPortrait}) {
max-width: 360px;
  };

  @media (max-width: ${screenSize.mobileLLandscape})  and (orientation: landscape) {
  margin-top: 33px;
margin-bottom: unset;
padding-bottom: 15px;
  };
  @media (max-width: ${screenSize.slyderMobileL} ) and (orientation: portrait)  {
max-width: unset;
padding-left: 35px;
padding-right: 35px;
margin-bottom: unset;
  };
  @media (max-width: 610px)  {
    max-width: unset;
padding-left: 35px;
padding-right: 35px;
margin-bottom: unset;
  };

`


const SetReverseSelectedPeople=(reverse)=>{
  if(!reverse) return {}
  return{
    marginLeft:127,
    [`@media (max-width: 1623px)`]: {
      paddingLeft:'unset',
      maxWidth:672,
    },
    [`@media (max-width: ${screenSize.laptopL} )`]: {
      maxWidth:560,
    marginLeft:96,

    },
    [`@media (max-width: ${screenSize.laptop} )`]: {
      padding:'unset',
      marginLeft:55,
      maxWidth:'calc( 100% - 55px)',
    },
    [`@media (max-width: ${screenSize.laptopPortrait} )`]: {
      marginLeft:37,
      maxWidth:305,
    },
    [`@media (max-width: ${screenSize.mobileLLandscape}) and (orientation: landscape)`]:
    {
      paddingBottom:38,
marginTop:60,
marginLeft:66,
    maxWidth:'calc( 100% - 66px)',
    },
  }
  }

  export const SelectedPeople=styled(SelectedPeopleDef)(({reverse})=>({
    ...SetReverseSelectedPeople(reverse)
  }))
  
  export const ProfileContent =styled.div`
  padding-top: 20px;
  `

export const SliderTitleDef=styled(ProfileTitle)`
margin-left: unset;
min-height: 156px;
padding-right: 15px;
@media (max-width: 1623px) {
  padding-left: 80px;
  max-width: 100%;
  };
@media (max-width: ${screenSize.laptopL}) {
  padding-left: 80px;
  max-width: 100%;
  font-size: 44px;
    line-height: 52px;
  };

  @media (max-width: ${screenSize.laptop}) {
  padding-left: 37px;
padding-right: 18px;
position: unset;
font-size: 40px;
    line-height: 48px;
  };
  @media (max-width: ${screenSize.laptopPortrait}) {
    min-height: 139px;
    font-size: 38px;
    line-height: 46px;
  };
  @media (max-width: ${screenSize.tabletSmall}) {
    padding-top: 20px
  };
  @media (max-width: ${screenSize.mobileLLandscape}) and (orientation: landscape) {
/* position: absolute; */
top: 0px;
left:0px;
min-height: 40px;
  };
  @media (max-width: ${screenSize.slyderMobileL} )  and (orientation: portrait)  {
position: unset;
  };
  @media (max-width: 610px)  {
    position: unset;
  };
`

const SetReverseSliderTitle=(reverse)=>{
if(!reverse) return {}
return{
  marginLeft:127,
  [`@media (max-width: 1623px)`]: {
    paddingLeft:'unset',
    maxWidth:'unset',
    width:'unset',
  },
  [`@media (max-width: ${screenSize.laptopL} )`]: {
    marginLeft:96,
    padding: 'unset',
    maxWidth:560,
  },
  [`@media (max-width: ${screenSize.laptop} )`]: {
    marginLeft:55,
    maxWidth:370,
  },
  [`@media (max-width: ${screenSize.laptopPortrait} )`]: {
    marginLeft:37,
    maxWidth:305,
  },
  [`@media (max-width: ${screenSize.mobileLLandscape}) and (orientation: landscape)`]:
    {
position:'unset',
maxWidth:280,
marginLeft:66,

    },
}
}

export const SliderTitle=styled(SliderTitleDef)(({reverse,textColor=false})=>({
  ...SetReverseSliderTitle(reverse),
  color:textColor&&textColor
}))


// SliderIconComma start
export const SliderIconCommaDef=styled(ProfileIconComma)`
top:-64px;
left:52px;
@media (max-width: ${screenSize.laptopL}) {
    top: -28px;
    font-size: 80px;
}
@media (max-width: ${screenSize.laptop}) {
  top:-28px;
left:5px;
  };
  @media (max-width: ${screenSize.mobileLLandscape}) and (orientation: landscape){
  top:-28px;
left:6px;
  };
  @media (max-width: ${screenSize.slyderMobileL} ) and (orientation: portrait)  {
    top:-28px;
left:-1px;
};
@media (max-width: 610px)  {
  top:-31px;
left:-1px;
  };
`
const SetReverseSliderIconComma=(reverse)=>{
  if(!reverse) return {}
  return{
    [`@media (max-width: ${screenSize.laptopL} )`]: {
left:0,
    },
  }
  }
  
  export const SliderIconComma=styled(SliderIconCommaDef)(({reverse,textColor=false})=>({
    ...SetReverseSliderIconComma(reverse),
color:textColor&&textColor
  }))


  // SliderIconComma end



export const SliderNameDef=styled(ProfileName)`
margin-bottom: 32px;
position: relative;
@media (max-height: 770px) {
  margin-bottom: 18px;
  };
@media (max-width: ${screenSize.laptop}) {
  margin-bottom: 18px;
  };
  @media (max-width: ${screenSize.mobileLLandscape}) and (orientation: landscape) {
  margin-bottom: 18px;
  };

  

`

export const SliderName=styled(SliderNameDef)(({reverse})=>({
  [`@media (max-width: ${screenSize.mobileLLandscape}) and (orientation: landscape)`]:
    {
      maxWidth:280,
      paddingRight:10,

    },
}))


export const SliderDescriptionDef=styled(ProfileDescription)`
color:#494E54;
@media (max-width: ${screenSize.laptopL}) {
  max-width: 560px;
  };
  @media (max-height: 770px) {
    font-size: 18px;
  line-height: 24px;
  margin-bottom: 32px;
  };
  @media (max-width: ${screenSize.laptop}) {
  max-width: 350px;
  font-size: 16px;
  line-height: 22px;
  };
  @media (max-width: ${screenSize.slyderMobileL} ) and (orientation: portrait)  {
  max-width: unset;
  };
  @media (max-width: 610px)  {
    max-width: unset;
  };
 
`

export const SliderDescription=styled(SliderDescriptionDef)(({reverse})=>({
  [`@media (max-width: ${screenSize.laptop} )`]: {
paddingRight:reverse&&25
        },
        [`@media (max-width: ${screenSize.mobileLLandscape}) and (orientation: landscape)`]:
        {
          marginBottom:'unset',
      lineHeight:'20px',
paddingRight:reverse&&18

        },
}))



export const WrapperSliderNameAndDescription=styled(WrapperNameAndDescription
)`

`


export const getDisplayStyle=(isOnlyTwoPhoto,curr)=>{
  if(isOnlyTwoPhoto){
  return{
    display: curr==1?'block':'none',
  }
  }
  return {
    display: curr==1?'none':'block',
  }

}

export const WrapperImgDef=styled.div(({curr,isHidden,isOnlyTwoPhoto,numberShow})=>({

  // opacity: isHidden? 0:1,
  display: curr==numberShow ? "block" : "none",
  transition:`opacity 0.4s ease ${curr>0?curr*0.11:0}s`,

    [`@media (max-height: 770px)`]: {
height: 186,
    },
[`@media (max-width: ${screenSize.laptop})`]: {
  // ...getDisplayStyle(isOnlyTwoPhoto,curr),
},
[`@media (max-width: ${screenSize.slyderMobileL} )  and (orientation: portrait)`]: {
  // ...getDisplayStyle(isOnlyTwoPhoto,curr) // display:isOnlyTwoPhoto?'none':'block' curr>1?'none':'block',
    display: curr==numberShow?'block':'none',

  },
  [`@media (max-width: 610px )`]: {
    // ...getDisplayStyle(isOnlyTwoPhoto,curr)
    display: curr==numberShow?'block':'none',
    },
  
}))

export const SliderPhotosListWrapper = styled.div(({reverse}) => ({
  position: 'relative',
  display: "flex",
  flexDirection: reverse && 'row-reverse',
  justifyContent: !reverse ? "start" : "end",
  padding: !reverse ? "15px 10px 0px 60px" : "15px 60px 0px 10px",

  [`@media (max-width: ${screenSize.slyderMobileL} )`] : {
    maxWidth: 480,
    width: '100%',
    padding: 0,
    margin: "0 auto",
  },

  [`@media (max-width: 610px )`] : {
    maxWidth: 440,
  }

}));

export const WrapperImg=styled(WrapperImgDef)`
   width: 290px;
                height: 250px;
                cursor: pointer;
span {
    width: 100% !important;
    height: 100% !important;
  }

  @media (max-width: ${screenSize.laptopL}) {
  width: 244px;
   height: 210px;
  };
  @media (max-width: ${screenSize.laptop}) {
    width: 200px;
    height: 200px;
  };

  @media (max-width: 992px) {
    width: 130px
  }

  @media (max-width: ${screenSize.mobileLLandscape}) and (orientation: landscape) {
   height: 150px;
  };
  

  @media (max-width: ${screenSize.slyderMobileL} )  {
    width: 100%;
  };

  @media (max-width: 358px ) and (orientation: portrait) {
   display: none;
  };


`

export const WrapperImg2=styled.div`
   max-width: 712px;
   min-height: 740px;
   height: 100%;
   flex: 1 1 auto;
   width:100%;
span {
    width: 100% !important;
    height: 100% !important;
  }

  @media (max-width: ${screenSize.laptopL}) {
    max-width: 630px;
    min-height: 650px;
  };

  @media (max-width: ${screenSize.laptopPortrait}) {
    max-width: 408px;
   height: 580px;
  };

  @media (max-width: 992px) {
    min-height: 600px;
  }

  @media (max-width: ${screenSize.mobileLLandscape}) and (orientation: landscape) {
    max-width: 100%;
   height: 406px;
  };
  @media (max-width: ${screenSize.slyderMobileL} )  {
    max-width: 480px;
   height: 580px;
   min-height: 0;
  };
  @media (max-width: 610px)  {
    max-width: 440px;
    height: 500px;
  };
`

export const StyledSliderPhotos=styled.div(({reverse})=>({
  display: 'flex',
  justifyContent: 'flex-end',
  flexDirection:reverse&&'row-reverse',
  flex: "0 0 auto",

  [`@media (max-width: 992px)`]: {
    flex: "0 1 130px"
  },

  [`@media (max-width: ${screenSize.slyderMobileL} )`] : {
    flex: "0 0 50%",
  },

  [`@media (max-width: 420px )`] : {
    flex: "0 0 40%",
  },

  [`@media (max-width: 358px) and (orientation: portrait)`] : {
    display: "none",
  }
}))


export const WrapperCurrentPhotoAndNextPeopleDescriptionDef=styled.div`
position: relative;
 display: flex;
 flex-direction: column;
width: calc( ((100% - 132px) / 5) * 2 + 132px );
// @media (max-width: ${screenSize.laptopL}) {
//   /* width: calc( 100% - 730px); */
// width: calc( ((100% - 104px) / 5) * 2 + 104px );
//    max-height: 768px;
//   };


  @media (max-width: ${screenSize.laptop}) {
  /* width: calc( 100% - 455px); */
width: calc( ((100% - 78px) / 4) * 2 + 78px );
  };

  @media (max-width: ${screenSize.laptopPortrait}) {
    /* width: calc( 100% - 360px); */
width: calc( ((100% - 48px) / 4) * 2 + 48px );
    max-height: 700px;
    display: flex;
    flex-direction: column;
    justify-content: flex-end;
  };

  @media (max-width: ${screenSize.mobileLLandscape}) and (orientation: landscape)  {
    max-height: 406px;
width: calc( ((100% - 139px) / 3) * 1 + 139px );

  };

`

const SetReverseWrapperCurrentPhotoAndNextPeopleDescription=(reverse)=>{
  if(!reverse) return {}
  return{
    [`@media (max-width: ${screenSize.laptopL})`]: {
      maxWidth:592,
    },
  }
  }
  
  export const WrapperCurrentPhotoAndNextPeopleDescription=styled(WrapperCurrentPhotoAndNextPeopleDescriptionDef)(({reverse})=>({
    ...SetReverseWrapperCurrentPhotoAndNextPeopleDescription(reverse),
    
  }))


export const StyledMobileSlider=styled.div`
display: none;
  @media (max-width: ${screenSize.slyderMobileL} )  and (orientation: portrait){
display: flex;
justify-content: center;
  };
  @media (max-width: 610px)  {
    display: flex;
justify-content: center;
  };

`

export const CentralImg=styled.div`
padding: 19px 30px 34px 30px;
width: 100%;
display: flex;
justify-content: center;

@media (max-width: ${screenSize.slyderMobileL})  {
  padding: 20px 0 25px
}

`







