import {
  SyledWrapperSlider,
  SyledSlider,
  WrapperSelectedPeopleAndSliderPhoto,
  SelectedPeople,
  SliderTitle,
  SliderName,
  SliderDescription,
  SliderIconComma,
  WrapperImg2,
  SliderPhotosListWrapper,
  WrapperCurrentPhotoAndNextPeopleDescription,
} from "./SyledSlider"
import Image from "next/image"
import {DescriptionNextPeople} from "./SupportComponent/DescriptionNextPeople/DescriptionNextPeople"
import {SliderPhotos} from "./SupportComponent/SliderPhotos"
import {MobileSlider} from "./SupportComponent/MobileSlider"

export const Slider = ({
  currentPeople,
  setCurrentPeople,
  setNextPeople,
  showPeopleArr,
  setShowPeopleArr,
  calcNextPeople,
  sliderProps,
  setIsLoadDesktopImg,
  isLoadDesktopImg,
  isHiddenSlidesPhoto,
  setisHiddenSlidesPhoto,
  startHiddenPhotos,
  reverse,
  textColor,
}) => {
  
  // Если есть reverse слайдера меняться верстка к противообратнной от первоначальной, только по стилям
  return (
    <SyledWrapperSlider reverse={reverse}>
      <SyledSlider reverse={reverse}>
        <WrapperSelectedPeopleAndSliderPhoto reverse={reverse}>
          {currentPeople.map(({profilesSliderData: currSlider}) => {
            return (
              <>
                <SliderTitle textColor={textColor} reverse={reverse}>
                  {currSlider.post}
                </SliderTitle>
                <SelectedPeople reverse={reverse}>
                  <SliderIconComma textColor={textColor} reverse={reverse} className="icon-comma" />
                  <SliderName reverse={reverse}>{currSlider.name}</SliderName>
                  <SliderDescription reverse={reverse}>{currSlider.description}</SliderDescription>
                </SelectedPeople>
              </>
            )
          })}
          <SliderPhotosListWrapper reverse={reverse}>
            {showPeopleArr.length > 1 && (
              <>
                <SliderPhotos
                  setCurrentPeople={setCurrentPeople}
                  currentPeople={currentPeople}
                  showPeopleArr={showPeopleArr}
                  setShowPeopleArr={setShowPeopleArr}
                  isHiddenSlidesPhoto={isHiddenSlidesPhoto}
                  setisHiddenSlidesPhoto={setisHiddenSlidesPhoto}
                  startHiddenPhotos={startHiddenPhotos}
                  reverse={reverse}
                />
                <DescriptionNextPeople
                  reverse={reverse}
                  textColor={textColor}
                  nextPeople={calcNextPeople()}
                  currNumber={currentPeople[0].number}
                  setNextPeople={setNextPeople}
                  allPeopleLength={showPeopleArr.length}
                />
              </>
            )}
          </SliderPhotosListWrapper>
        </WrapperSelectedPeopleAndSliderPhoto>

        <WrapperCurrentPhotoAndNextPeopleDescription reverse={reverse}>
          {currentPeople.map((item, i) => {
            return (
              <WrapperImg2 className={`appear ${isLoadDesktopImg && "hidden"}`} key={i}>
                <Image src={item?.profilesSliderData?.img?.sourceUrl} objectFit="cover" quality={100} width={1000} height={1000} onLoad={() => setIsLoadDesktopImg(false)} priority={true}></Image>
              </WrapperImg2>
            )
          })}
        </WrapperCurrentPhotoAndNextPeopleDescription>
      </SyledSlider>
      <MobileSlider {...sliderProps} />
    </SyledWrapperSlider>
  )
}
