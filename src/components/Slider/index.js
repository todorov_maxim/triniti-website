import { Slider } from "./Slider";
import { useState } from "react";
import { pageTheme } from "../../constants/themes";

export default ({ sliderData,textColor, ...res }) => {

  // Для анимации переключения фото главного desktop
  const [isLoadDesktopImg, setIsLoadDesktopImg] = useState(false);
  // Для анимации переключения фото главного mobile
  const [isLoadMobileImg, setIsLoadMobileImg] = useState(false);

  // для перключния слайдов
  const [isHiddenSlidesPhoto, setisHiddenSlidesPhoto] = useState(false);
  const sliderDataWithId = () =>
    sliderData[0]?.id
      ? sliderData
      : sliderData.map((obj, i) => ({ ...obj, id: i + 1 }));
  // даю нумерацию каждому слайду
  const dataWithNumber = sliderDataWithId()
    .map((obj, i) => ({ ...obj, number: i + 1 }))
    .reverse();

    const isHasData=dataWithNumber.length==0

    const isShowSlider=!isHasData

  const idFirstPeople = dataWithNumber[dataWithNumber.length - 1]?.id;

  // Делаю стартовый масив в котором первый человек становиться в начало массива так как являеться выбранным
  const StartArray = [
    dataWithNumber[dataWithNumber.length - 1],
    ...dataWithNumber.filter((it, i) => {
      return it.id != idFirstPeople;
    }),
  ];

  const [showPeopleArr, setShowPeopleArr] = useState(StartArray);
  // showFirstPeople показывает первого человека на слайде до 1 - го переключения
  const showFirstPeople = showPeopleArr.filter((item, i) => item.number == 1);
  const [currentPeople, setCurrentPeople] = useState(showFirstPeople);

  const onAnimationPhoto = () => {
    const id = showPeopleArr[showPeopleArr.length - 1].id;
    setTimeout(() => {
      // беру последнего человека массива ставлю его на главную, последнего потому что у меня стоит reverse() на массиве
      setCurrentPeople(
        showPeopleArr.filter((item, i, arr) => i == arr.length - 1)
      );
      // ставлю  последни элемент масива первым в масиве
      // меняю пока скрыты фотки, последовательность фоток, для того чтобы не было мигания
      setShowPeopleArr((pre) => [
        showPeopleArr[showPeopleArr.length - 1],
        ...pre.filter((item) => item.id != id),
      ]);
      // Включаю анимацию появления маленьких фоток
      setisHiddenSlidesPhoto(false);
    }, 800);
  };

  const startHiddenPhotos = () => {
    // При переключения на след чел я скрываю главное фото "opacity=0"
    setIsLoadDesktopImg(true);
    setIsLoadMobileImg(true);
    // я скрываю маленькие фото
    setisHiddenSlidesPhoto(true);
  };
  const setNextPeople = () => {
    startHiddenPhotos();
    // запускаю анимацию смены фоток через 0.8s
    // задержка нуджа для того чтобы успела выполниться transition скрытия фото
    onAnimationPhoto();
  };

  // Расчитывает кто будет следуйщий: начадо
  // Возвращает массив
  const calcNextPeople = () => {
    let currentNumber = currentPeople[0].number;
    const isLastElement = checkedLastElement(showPeopleArr, currentNumber);
    return isLastElement
      ? returnFirstElement()
      : returnNextElement(currentNumber);
  };
  const checkedLastElement = (arr, currentNumber) =>
    arr.length == currentNumber;
  const returnFirstElement = () => showPeopleArr.filter((it) => it.number == 1);
  const returnNextElement = (currentNumber) =>
    showPeopleArr.filter((it) => it.number == currentNumber + 1);

  // Расчитывает кто будет следуйщий: конец

  const sliderProps = {
    ...res,
    currentPeople,
    setCurrentPeople,
    setNextPeople,
    showPeopleArr,
    setShowPeopleArr,
    calcNextPeople,
    setIsLoadDesktopImg,
    isLoadDesktopImg,
    setIsLoadMobileImg,
    isLoadMobileImg,
    isHiddenSlidesPhoto,
    setisHiddenSlidesPhoto,
    startHiddenPhotos,
    textColor,
  };

  return (
    <>
   { isShowSlider&& <Slider {...sliderProps} sliderProps={sliderProps} />}
   </>
   )
};
