
import styled from "styled-components";
import { screenSize } from "../../constants/media";
import { StyledAboutFullscreen,BackgroundImage } from "../Pages/About/SupportComponent/AboutFullscreen/StyledAboutFullscreen";


export const StyledFullScreenBlockGeneralPages = styled(StyledAboutFullscreen)`

@media (max-width: ${screenSize.mobileLLandscape}) and (orientation: landscape) {
flex-direction: row;
  }
`;

export const BackgroundImageBlock = styled(BackgroundImage)`
position: relative;
max-width: calc( 100% - 444px );
@media (max-width: ${screenSize.laptop}) {
  max-width: calc( 100% - 360px );
  } ;
@media (max-width: ${screenSize.laptopPortrait}) and (orientation: portrait) {
    max-width: 100%;
    width: 100%;
  } ;
  @media (max-width: ${screenSize.mobileLLandscape}) and (orientation: landscape) {
    width: calc( 100% - 382px );
       order: unset;
       height: unset;
       position: relative;
       right: unset;
       top: unset;
  }; 

`



export const VideoBtnDef = styled.div`
position: absolute;
right: 0;
bottom: 0;
width: 137px;
height: 137px;
display: flex;
flex-direction: column;
align-items: center;
justify-content: center;
color: #fff;
font-size: 8px;
line-height: 12px;
letter-spacing: 0.25em;
text-transform: uppercase;


&:before{
  content: "\\e90f";
  font-family: 'icomoon';
  display: block;
  font-size: 44px;
  line-height: 44px;
  margin-bottom: 16px;
}


`
export const VideoBtn = styled(VideoBtnDef)(({background})=>({
  background,

  [`@media (min-width: 992px)`]: {
    transition: '.3s background-color',
    '&:hover': {
      cursor: "pointer",
      background: "#494E54",
    },
  },
}))
