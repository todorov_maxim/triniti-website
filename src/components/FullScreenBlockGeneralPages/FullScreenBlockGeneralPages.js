import {
  WrapperHeaderAndDescripton,
  HeaderPage,
  WrapperDescripton,
  NamePage,
  Title,
  Description,
  IntroLink,
} from "../Pages/About/SupportComponent/AboutFullscreen/StyledAboutFullscreen";
import { Contact } from "../Contact/Contact";
import { TextLink, WrapeperLogo } from "../FullScreenPage/StyledFullScreenPage";
import { LogoWithText } from "../LogoWithText/LogoWithText";
import { pageTheme } from "../../constants/themes";
import {
  StyledFullScreenBlockGeneralPages,
  BackgroundImageBlock,
  VideoBtn,
} from "./StyledFullScreenBlockGeneralPages";


export const FullScreenBlockGeneralPages = ({
  pageData,
  hasVideo,
  showModal,
  modify
}) => {
  // Это первый блок для каждой страницы кроме About и Contact,  отображаеться когда человек перешел
  // с страницы main сюда
  const { textColor, background } = pageTheme[pageData?.pageTheme] || pageTheme.brown;
  const {
    intro: { pageName, description, background: backgroundImg, title, link },
  } = pageData;

  return (
    <StyledFullScreenBlockGeneralPages>
      <WrapperHeaderAndDescripton background={background} color={textColor}>
        <HeaderPage background={background}>
          <Contact color={textColor} isRenderTelephone={false} />
          <WrapeperLogo>
            <LogoWithText namePage="GeneralPages" fill={textColor} />
          </WrapeperLogo>
        </HeaderPage>

        <WrapperDescripton>
          <NamePage>{pageName}</NamePage>
          <Title modify={modify?.title}>{title}</Title>
          <Description>{description}</Description>
         {link?.linkUrl && 
          <IntroLink href={link?.linkUrl} target={"_blank"}>{link?.linkText}</IntroLink>
         } 
        </WrapperDescripton>
      </WrapperHeaderAndDescripton>

      <BackgroundImageBlock url={backgroundImg?.sourceUrl}>
        {/* {hasVideo && (
          <VideoBtn background={background} onClick={() => showModal()}>
            play video
          </VideoBtn>
        )} */}
      </BackgroundImageBlock>
    </StyledFullScreenBlockGeneralPages>
  );
};
