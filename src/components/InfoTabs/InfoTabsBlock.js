import InfoTabs from "./InfoTabs";
import { InfoTabsButton, InfoTabsButtonsBox, InfoTabsContent, InfoTabsDescription, InfoTabsIntroInfo, InfoTabsIntroInfoBox, InfoTabsSubTitle, InfoTabsTitle, InfoTabsWrapper } from "./StyledInfoTabs";

const InfoTabsBlock = ({title, subtitle, description, tabs}) => {
    
    return (
        <InfoTabsWrapper>
            <InfoTabsContent>
                <InfoTabsIntroInfo>
                    <InfoTabsIntroInfoBox>
                        <InfoTabsSubTitle>{subtitle}</InfoTabsSubTitle>
                        <InfoTabsTitle>{title}</InfoTabsTitle>
                    </InfoTabsIntroInfoBox>
                    <InfoTabsIntroInfoBox>
                        <InfoTabsDescription>{description}</InfoTabsDescription>
                        <InfoTabsButtonsBox>
                            <InfoTabsButton href={'http://re-triiinity.com.ua/'} target="_blank">Більше про ЖК TRIIINITY</InfoTabsButton>
                        </InfoTabsButtonsBox>
                    </InfoTabsIntroInfoBox>
                </InfoTabsIntroInfo>
                <InfoTabs tabs={tabs}/>
            </InfoTabsContent>

        </InfoTabsWrapper>
    );
};

export default InfoTabsBlock;