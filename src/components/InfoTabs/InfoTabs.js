


import React, { useContext, useState } from 'react';
import { TabItem,TabList,TabContent, TabItemIcon, TabItemText, InfoTabsContainer,InfoTabsContainerBox, TabContentImage, TabContentItem, TabContentText, TabContentButton, TabContentImageWrapper  } from './StyledInfoTabs';
import { ModalImgSrcContext } from '../Pages/Rental/Rental';


const InfoTabs = ({ defaultActiveTab = 0, tabs = [] }) => {
  const [activeTab, setActiveTab] = useState(defaultActiveTab);
  const {showImgModal} = useContext(ModalImgSrcContext)

  const handleTabClick = (tabIndex) => {
    setActiveTab(tabIndex);
  };

  const handleImageClick = (tab) => {
    if (!tab?.inModal) {
      return
    }
    showImgModal(tab?.img.sourceUrl)
  }

  return (
    <InfoTabsContainer>
      <InfoTabsContainerBox>
      <TabList>
        {tabs.map((tab, index) => (
          <TabItem
            key={'tabitem' + index + tab.title}
            isActive={index === activeTab}
            onClick={() => handleTabClick(index)} >
            <TabItemIcon src={tab.icon.sourceUrl}/><TabItemText isActive={index === activeTab}>{tab.tabName}</TabItemText>
          </TabItem>
        ))}
      </TabList>
      </InfoTabsContainerBox>
      <InfoTabsContainerBox>
        <TabContent>
        {tabs.map((tab, index) => (
          <TabContentItem key={'tabcontent' + index + tab.title} isActive={index === activeTab}>
              <TabContentImageWrapper onClick={() => handleImageClick(tab)} inModal={tab?.inModal}>
                <TabContentImage inModal={tab?.inModal} src={tab.img.sourceUrl}/>
              </TabContentImageWrapper>
              <TabContentText dangerouslySetInnerHTML={{ __html:tab.description}}>{}</TabContentText>
              <TabContentButton href={'https://re-triiinity.com.ua/choice-plan#/?typePlacementUrl=комерційні%20приміщення&displayType=/api/view_types/1&curSpinnerName=complex&curSpinnerId=62&curEntityName=complex&curEntityId=62&floorData=%7B%22selectedHouse%22:%7B%22name%22:%22house%22,%22name-id%22:%22100%22%7D,%22selectedSection%22:%7B%22markup%22:null,%22markup-id%22:null%7D,%22selectedFloor%22:null,%22selectedFloorNumber%22:null,%22filteredIds%22:%5B%5D,%22filteredSections%22:%5B%5D,%22selectedApartmentId%22:%22%22,%22fromComplex%22:true%7D&collapsedFilter=false&'} target="_blank">Oбрати комерційне приміщення</TabContentButton>
              
          </TabContentItem>
        ))}
            
        </TabContent>
      </InfoTabsContainerBox>
    </InfoTabsContainer>
  );
};

export default InfoTabs;