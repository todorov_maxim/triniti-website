import styled from 'styled-components';

export const StyledFullScreenPage = styled.div(() => ({
    position: "relative",
    display: "flex",
}))

export const InfoTabsWrapper = styled.div(() => ({
  maxWidth: 1440,
  margin: "0 auto",
  padding: "100px 80px 60px",
  overflow: "hidden",

  ['@media screen and (max-width: 1279px)']: {
    padding: "100px 34px 60px",
  },
  ['@media screen and (max-width: 767px)']: {
    padding: "80px 34px 40px",
  },
  ['@media screen and (max-width: 579px)']: {
    padding: "50px 34px 30px",
  },
}))

export const InfoTabsContent = styled.div(() => ({
  fontFamily: 'AGLettericaC',
  color: "#4B4F54",
  
}))

export const InfoTabsIntroInfo = styled.div(() => ({
  display: "flex",
  margin: "0 -30px",
  
  ['@media screen and (max-width: 991px)']: {
    display: "block",
    margin: "0 0",
  }
}))

export const InfoTabsIntroInfoBox = styled.div(() => ({
  flex: '0 0 50%',
  padding: "0 30px",
  width: "50%",

  ['@media screen and (max-width: 991px)']: {
    flex: '0 0 auto',
    padding: "10px 0",
    width: "100%",
  }
}))

export const InfoTabsSubTitle = styled.div(() => ({
  letterSpacing: "0.1em",
  position: "relative",
  color: "#4B4F54",
  textTransform: "uppercase",
  fontFamily: 'AGLettericaC',
  fontStyle: "normal",
  fontWeight: "400",
  fontSize: "24px",
  display: "flex",
  alignItems: 'center',
  marginBottom: 10,

  [`&:after`]: {
    content: "''",
    display: 'block',
    marginLeft: 30,
    top: '50%',
    height: "1px",
    maxWidth: 165,
    background: "#4B4F54",
    flex: '1 1 165px',
  },

  
  ['@media screen and (max-width: 1279px)']: {
    fontSize: "20px",

    [`&:after`]: {
      maxWidth: 100,
      flex: '1 1 auto',
    }
  },
  ['@media screen and (max-width: 579px)']: {
    fontSize: "16px",
    [`&:after`]: {
      content: "none"
    }
  }
}))

export const InfoTabsTitle = styled.div(() => ({
  fontFamily: 'AGLettericaC',
  fontStyle: "normal",
  fontWeight: "700",
  fontSize: "64px",
  lineHeight: "70px",
  letterSpacing: "0.05em",
  textTransform: "uppercase",

  ['@media screen and (max-width: 1279px)']: {
    fontSize: "50px",
    lineHeight: "140%",
  },

  ['@media screen and (max-width: 579px)']: {
    fontSize: "32px",
  },
}))

export const InfoTabsDescription = styled.div(() => ({
  fontWeight: "400",
  fontSize: "16px",
  lineHeight: "190%",
  letterSpacing: "0.05em",


['@media screen and (max-width: 579px)']: {
    fontSize: "13px",
    lineHeight: "150%",
  },
}))

export const InfoTabsButtonsBox = styled.div(() => ({
  padding: "35px 0",
  ['@media screen and (max-width: 579px)']: {
    padding: "20px 0",
  }
}))
export const InfoTabsButton = styled.a(() => ({
  letterSpacing: "0.14em",
  fontSize: "14px",
  textTransform: "uppercase",
  display: "inline-flex",
  alignItems: "center",
  justifyContent: "center",
  textAlign: "center",
  minHeight: 56,
  padding: "4px 36px",
  border: "1px solid #4B4F54",

  ['@media screen and (max-width: 1279px)']: {
    fontSize: "12px",
  },
  ['@media screen and (max-width: 579px)']: {
    padding: "4px 10px",
    fontSize: "11px",
  },
}))


export const InfoTabsContainer = styled.div`
  display: flex;
  margin: 0 -30px;

  @media screen and (max-width: 991px){
    margin: 0 -15px;
  }
  @media screen and (max-width: 579px){
    display: block;
    margin: 0;
  }
`;

export const InfoTabsContainerBox = styled.div`
  flex: 0 0 50%;
  width: 50%;
  padding: 0 30px;
  display: flex;
  @media screen and (max-width: 991px){
    padding: 0 15px;

    &:first-child {
      flex: 0 0 190px;
      width: 190px;
    }
    &:last-child {
      flex: 0 0 calc(100% - 190px);
      width: calc(100% - 190px);
    }
  };


  @media screen and (max-width: 579px){
    flex: 1 1 auto;
    width: 100%;
    padding: 15px 0;

    &:first-child {
      flex: 1 1 auto;
      width: 100%;
    }
    &:last-child {
      flex: 1 1 auto;
      width: 100%;
    }
  }
`;

export const TabList = styled.div`
  border: 1px solid #4B4F54;
  align-self: flex-start;
  width: 100%;
  
  
  @media screen and (max-width: 579px){
    display: flex;
    position: relative;
    margin-bottom: 20px;
  }
  
`;

export const TabItem = styled.div`
  padding: 24px 35px;
  background-color: ${(props) => (props.isActive ? '#E4D5D3' : '#fff')};
  color: #4B4F54;
  cursor: pointer;
  display: flex;
  align-items: center;
  border-bottom: 1px solid #4B4F54;
  
  @media screen and (max-width: 1279px){
    padding: 20px;
  }
  @media screen and (max-width: 991px){
    padding: 12px;
    display: block;
  }

  @media screen and (max-width: 767px){
    padding: 14px;
  }

  @media screen and (max-width: 579px){
    flex: 1 1 auto;
    border-bottom: none;
    border-right: 1px solid #4B4F54;
    padding: 8px;

    &:last-child{
      border-right: none;
    }
  }

  &:last-child{
    border-bottom: none;
  }
`;

export const TabItemIcon = styled.img`
  margin-right: 30px;

  @media screen and (max-width: 1279px){
    margin-right: 20px;
  }

  @media screen and (max-width: 991px){
    margin-bottom: 5px;
    margin-right: 0;
    height: 25px;
  }
  @media screen and (max-width: 579px){
    margin: 0 auto;
  }
`;

export const TabItemText = styled.div`
  text-transform: uppercase;
  font-size: 14px;
  line-height: 140%;
  text-align: left;
  letter-spacing: 0.14em;

  @media screen and (max-width: 1279px){
    font-size: 12px;
  }

  @media screen and (max-width: 991px){
    font-size: 10px;
  }

  @media screen and (max-width: 579px){
    position: absolute;
    top: calc(100% + 10px);
    left: 0;
    display: ${(props) => (props.isActive ? 'block' : 'none')};;
  }
`;

export const TabContent = styled.div`
  display: flex;
`;


export const TabContentItem = styled.div`
  padding: 50px 0 40px 55px;
  position: relative;
  z-index: 2;
  background-color: #E4D5D3;
  display: ${(props) => (props.isActive ? 'flex' : 'none')};
  flex-direction: column;
  
  @media screen and (max-width: 1279px){
    padding: 40px 0 30px 45px;
  }

  @media screen and (max-width: 991px){
    padding: 20px 0 20px 35px;
  }

  @media screen and (max-width: 767px){
    padding: 20px 0 20px 35px;
  }

  @media screen and (max-width: 579px){
    padding: 15px 0 15px 20px;
  }

  &:before {
    content: '';
    position: absolute;
    z-index: -1;
    display: block;
    left: calc(100% - 1px);
    top: 0;
    height: 100%;
    background-color: #E4D5D3;
    width: 85px;
  }
`;


export const ModalImage = styled.img`
  display: block;
  max-width: 100%;
  max-height: 100%;
`;
export const TabContentImageWrapper = styled.div`
  cursor: ${(props) => (props.inModal ? 'zoom-in' : 'default')};
  position: relative;
  overflow: hidden;

  /* &:after{
    content: ${(props) => (props.inModal ? '"+"' : 'none')};
    position: absolute;
    left: 50%;
    top: 50%;
    width: 30px;
    height: 30px;
    font-size: 16px;
    line-height: 1;
    display: flex;
    align-items: center;
    justify-content: center;
    pointer-events: none;
    color: #fff;
    border: 3px solid #fff;
    border-radius: 100%;
    transform: translate(-50%, -50%);
    z-index: 2;
  } */
`;

export const TabContentImage = styled.img`
  flex: 0 0 auto;
  height: 300px;
  width: 100%;
  max-width: 100%;
  display: block;
  object-fit: cover;
  transform: ${(props) => (props.inModal ? 'scale(1.6)' : 'none')};

  @media screen and (max-width: 767px){
    height: 220px;
  }

  @media screen and (max-width: 579px){
    height: 190px;
  }
`;
export const TabContentText = styled.div`
  padding: 32px 0;
  flex: 1 1 auto;
  font-weight: 400;
  font-size: 16px;
  line-height: 180%;

  @media screen and (max-width: 1279px){
    padding: 24px 0;
    font-size: 14px;
    line-height: 160%;
  }
`;
export const TabContentButton = styled(InfoTabsButton)`
  flex: 0 0 auto;
`;