import { FullScreeDescription } from "../FullScreenPage/SupportComponents/FullScreeDescription";
import { FullScreenHeader } from "../FullScreenPage/SupportComponents/FullScreenHeader";
import fullScreenImg from "../../data/PagesFS/images";
import { pageTheme } from "../../constants/themes";
import { ContentFullScreenPage } from "../FullScreenPage/StyledFullScreenPage";
import { FullScreeImg } from "../FullScreenPage/SupportComponents/FullScreeImg";
import FullScreeContactDescription from "../FullScreenPage/SupportComponents/FullScreeContactDescription";
import { useEffect, useState } from "react";

export const DesctopPages = ({
  pageCount,
  fullpageApi,
  pageData: { commonSlides, contactsInfo },
  showForm,
  onceLoaded
}) => {
  // showBlockPage
  //если  pageCount == 0  то тогда происходит переход с третей страницу на вторую "параметром и i == 0" я задаю чтобы показывала значения третей страницы, иначе слетают значения
  //этой параметр "i + 1 == pageCount" показывает свой блок  для каждой страницы
  const showBlockPage = (i) => (pageCount == 0 ? i == 0 : i + 1 == pageCount);

  const [isEditeMode, setIsEditeMode] = useState(false);
  const onChangeEdite = () => setIsEditeMode((pre) => !pre);

  const objContact = {
    ...contactsInfo,
    img: contactsInfo?.img,
    background: "#9C8D88",
    numberPage: "09",
    textColor: "#FFFFFF",
    isContact: true,
  };

  const slideData = [...commonSlides, objContact];
  // const slideData = [objContact];

  return (
    <>
      <div className={"section"} style={{ position: "relative" }}>
        <ContentFullScreenPage>
          <FullScreenHeader
            isContact={pageCount == 10}
            hiddenHeader={false && pageCount == 9}
          />
          {slideData.map((page, i, arr) => {
            return (
              <>
                {!page?.isContact && (
                  <FullScreeDescription
                    mob={false}
                    isActiveDescription={showBlockPage(i)}
                    fullpageApi={fullpageApi}
                    {...page}
                    allPages={arr?.length}
                    pageCount={pageCount}
                  />
                )}

                {/* меняеться вложеность  блока Description если это страница контакта */}

                {page?.isContact && (
                  <FullScreeContactDescription
                    mob={false}
                    fullpageApi={fullpageApi}
                    isActiveDescription={showBlockPage(i)}
                    pageData={page}
                    {...page}
                    showForm={showForm}
                    onceLoaded={onceLoaded}
                    allPages={arr?.length}
                    onChangeEdite={onChangeEdite}
                    isEditeMode={isEditeMode}
                    pageCount={pageCount}
                  />
                )}
              </>
            );
          })}
        </ContentFullScreenPage>
        {slideData.map((page, i) => {
          return (
            <>
              <FullScreeImg
                mob={false}
                isActiveImg={showBlockPage(i)}
                imgSrc={page?.img?.sourceUrl}
                themeColor={pageTheme[page?.sectionTheme]}
                signature={page?.signature}
                isChangeWidth={false && page?.isContact}
              />
            </>
          );
        })}
      </div>

      <div className="section">1</div>
    </>
  );
};

const dataReplacement = (origin, willReplacement, contactObj) => {
  return origin.map((obj, i) => ({ ...obj }));
};

const photoReplacement = (origin, willReplacement, contactObj) => {
  // меняю фото origin на фото из willReplacement
  // и одно фото беру из contactObj
  return origin.map((obj, i) => ({
    ...obj,
    img: !obj.isContact
      ? willReplacement[i]?.img?.sourceUrl
      : contactObj?.img?.sourceUrl,
  }));
};
