import {ContainerContent} from "../ManagementPage/SupportComponent/ContactWithCompany/StyledContactWithCompany"
import { MiniSlider } from "../MiniSlider/MiniSlider"
export const Gallery = data => {
    
  return (
    <ContainerContent variant={"xl"}>
      {data && <MiniSlider namePage={"realEstate"} gallery={data} />}
    </ContainerContent>
  )
}
