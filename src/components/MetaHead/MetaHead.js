import Head from "next/head"
import Script from "next/script";
import favicon from './../../public/favicon/icon.ico';



export const MetaHead = ({title}) => {

    return (
        <>
        <Head>
            
            <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
            <link rel="shortcut icon" href={favicon.src} type="image/x-icon" />
            <title>{title ? title : 'triiinity.com.ua'}</title>
        </Head>
        
        </>
    )
}