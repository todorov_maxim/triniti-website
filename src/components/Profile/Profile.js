import Image from "next/image"
import YV from "../../../public/images/Main/yv.jpg"
import {
  FlexBlock,
  FlexBox,
  ProfileContent,
  SelectedPeople,
  SliderDescription,
  SliderIconComma,
  SliderName,
  SliderPhotosListWrapper,
  SliderTitle,
  SyledSlider,
  SyledWrapperSliderDef,
  WrapperCurrentPhotoAndNextPeopleDescription,
  WrapperImg2,
  WrapperSelectedPeopleAndSliderPhoto,
} from "../Slider/SyledSlider"
import {ContentPeople, Description, IconComma, Name, PeopleData, StyledContactWithCompany, StyledProfileWrapper, Title, WrapperDescription, WrapperImg} from "../ManagementPage/SupportComponent/ContactWithCompany/StyledContactWithCompany"

export const Profile = () => {
  return (
    <>
      {/* <StyledContactWithCompany>
        <SyledSlider>
          <WrapperSelectedPeopleAndSliderPhoto>
            <SliderTitle>Голова відділу будівницва</SliderTitle>
            <SelectedPeople reverse={false}>
              <div>
                <SliderIconComma textColor={"#494E54"} reverse={false} className="icon-comma" />
                <SliderName reverse={false}>Юрій Величко</SliderName>
                <SliderDescription reverse={false}>
                  Ми прагнемо, щоб наш вклад був поштовхом до розвитку нашого міста, створюючи якірні масштабні об'єкти, які сприятимуть, в тому числі, розвитку інфраструктури.
                </SliderDescription>
              </div>
            </SelectedPeople>
          </WrapperSelectedPeopleAndSliderPhoto>
          <WrapperCurrentPhotoAndNextPeopleDescription reverse={false}>
            {/* <WrapperImg>
            <Image src={YV.src} objectFit="cover" layout="responsive" quality={100} width={400} height={430} priority={true}></Image>
          </WrapperImg> 
            <WrapperImg2 className={`appear"}`}>
              <Image src={YV.src} objectFit="cover" quality={100} width={1000} height={1000} priority={true}></Image>
            </WrapperImg2>
          </WrapperCurrentPhotoAndNextPeopleDescription>
        </SyledSlider>
      </StyledContactWithCompany> */}
      <StyledProfileWrapper>
      <ContentPeople className="ContentPeople">
          <WrapperDescription>
            <>
              <Title textColor={"#F0B8A9"}>МЕНЕДЖЕР ПРОЄКТІВ</Title>
              <PeopleData>
                <IconComma textColor={"#F0B8A9"} className="icon-comma" />
                <Name>Юрій Величко</Name>
                <Description dangerouslySetInnerHTML={{__html: "Ми прагнемо, щоб наш вклад був поштовхом до розвитку нашого міста, створюючи якірні масштабні об'єкти, які сприятимуть, в тому числі, розвитку інфраструктури."}} />
              </PeopleData>
            </>
          </WrapperDescription>
          <WrapperImg>
            <Image src={YV.src} objectFit="cover" quality={100} width={600} height={600}></Image>
          </WrapperImg>
        </ContentPeople>
        </StyledProfileWrapper>
    </>
  )
}
