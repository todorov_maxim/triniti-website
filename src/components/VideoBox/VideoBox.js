import { StyledVideoBox } from "./StyledVideoBox";

export const VideoBox = ({
  width,
  height,
  video: {
    src,
    poster: { sourceUrl },
  },
}) => {
  return (
    <>
    {src && 
    <StyledVideoBox controls poster={sourceUrl && sourceUrl}>
      <source src={src} type="video/mp4" />
    </StyledVideoBox>
  }
    
    </>
  );
};
