import styled from "styled-components";

export const StyledVideoBox = styled.video(({width, height}) => ({
    width: width && "800px",
    height: height && "400px",
    maxWidth: "100%",
}))