import {
  WrapperModal,
  StyledModal,
  WrapperClose,
  CloseIcon,
} from "./StyledModal";

export const Modal = ({ children, modalHidden }) => {
  return (
    <WrapperModal>
      <StyledModal>
        <WrapperClose onClick={modalHidden}>
          <CloseIcon className="icon-close" />
        </WrapperClose>
        {children}
      </StyledModal>
    </WrapperModal>
  );
};
