import styled from "styled-components";
import { screenSize } from "../../constants/media";

export const WrapperModal=styled.div`
max-width: 1920px;
position: fixed;
width: 100%;
height:100%;
background: rgba(255, 255, 255, 0.9);
z-index:10;
padding: 80px 80px 77px 80px;

@media (max-width: ${screenSize.laptopPortrait}) {
padding: 0px;
    }
    @media (max-width: ${screenSize.mobileLLandscape}) and (orientation: landscape) {
        padding: 0px 50px 40px 46px;
}

`

export const StyledModal=styled.div`
position: relative;
width: 100%;
height:100%;
display: flex;
justify-content: center;
align-items: center;

    @media (max-width: ${screenSize.mobileLLandscape}) and (orientation: landscape) {
        align-items: flex-start;

}
`

export const WrapperClose=styled.div`
top: -80px;
right: -80px;
width: 80px;
height:80px;
background: #CD8B7D;
display: flex;
justify-content: center;
align-items: center;
z-index:10;
position: absolute;
@media (max-width: ${screenSize.laptopPortrait}) {
top: 0px;
right: 0px;
background: #494E54;
}
@media (max-width: ${screenSize.mobileLLandscape}) and (orientation: landscape) {
    width: 50px;
height:50px;
right: -50px;
}
@media (max-width: ${screenSize.mobileL}) and (orientation: portrait) {
    width: 50px;
height:50px;
}
`

export const CloseIcon=styled.i`
color: white;
font-size:26px;
z-index:11;
@media (max-width: ${screenSize.mobileLLandscape}) and (orientation: landscape) {
    font-size:15px;
}
@media (max-width: ${screenSize.mobileL}) and (orientation: portrait) {
    font-size:15px;
}
`




