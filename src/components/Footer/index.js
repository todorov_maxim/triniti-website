import { Footer } from "./Footer";
import { ItemLink } from "./StyledFooter";

export default ({ pageData }) => {
  // const { menu } = pageData;
  // const FooterMenuColon1 = menu.map(({ label }, i) => {
  //   if (i < 4) {
  //     return (
  //       <ItemLink style={{ width: "max-content" }}>
  //         {label.toUpperCase()}
  //       </ItemLink>
  //     );
  //   }
  // });
  // const FooterMenuColon2 = menu.map(({ label }, i) => {
  //   if (i >= 4 && i < 7) {
  //     return <ItemLink>{label.toUpperCase()}</ItemLink>;
  //   }
  // });
  // const FooterMenuColon3 = menu.map(({ label }, i) => {
  //   if (i >= 7) {
  //     return <ItemLink>{label.toUpperCase()}</ItemLink>;
  //   }
  // });

  // const footerProps = { FooterMenuColon1, FooterMenuColon2, FooterMenuColon3 };
  // {...footerProps}
  return <Footer pageData={pageData} />;
};
