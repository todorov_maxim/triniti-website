import styled from 'styled-components';
import { screenSize } from '../../constants/media';

export const StyledFooter = styled.div`
min-height: 344px;
background: #494E54;
display: flex;
flex-direction: column;
align-items: center;
@media (max-width: 925px) {
  padding: 0px 25px;
    }
@media (max-width: ${screenSize.mobileLLandscape}) {
  padding: 0px 0px;
  min-height: 370px;
    }
`
  

export const WrapeperLogo = styled.div(() => ({
  display: "flex",
  justifyContent: "center",
  alignItems: "center",
  width: "100%",
  height: "100%",
  maxHeight:188,
marginTop:40,
marginBottom:30,

  [`@media (max-width: ${screenSize.laptop}) `]:
    {
maxHeight:144,
marginTop:45,
marginBottom:45,
    },
    [`@media (max-width: ${screenSize.laptopPortrait}) `]:
    {
marginTop:52,
marginBottom:63,
    },
    [`@media (max-width: ${screenSize.mobileLLandscape}) `]:
    {
marginTop:75,
    },

}));


export const WrapperPagesAndSocialLinks = styled.div`
display: flex;
flex-direction: column;
align-items: center;
max-width: 1150px;
width: 100%;
justify-content: space-between;
padding-bottom: 60px;

@media (max-width: ${screenSize.laptop}) {
  max-width: 860px;
    }
    @media (max-width: ${screenSize.laptopPortrait}) {
  max-width: 687px;
    }
    @media (max-width: ${screenSize.mobileLLandscape}) {
      justify-content: center;
    }

`
export const Footerpages = styled.div`
/* justify-content: space-between;
display: flex; */
columns: 4;
font-size: 8px;
line-height: 9px;
letter-spacing: 0.25em;
color: white;
width: 100%;
max-width: 850px;
padding-left: 80px;
margin-bottom: 40px;
@media (max-width: ${screenSize.mobileLLandscape}) {
display:none;
    }
`
export const WrapperSocialLinks = styled.div`
width: 157px;
display: flex;
justify-content: space-between;
align-items: center;
`

export const Block = styled.div`
display: flex;
flex-direction: column;
`

export const ItemLink = styled.div`
margin-bottom: 10px;
min-width: 180px;
padding-right:7px;
cursor: pointer;
&:hover{
  color:#CD8B7D;
}
`

