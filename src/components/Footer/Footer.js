import { LogoWithText } from "../LogoWithText/LogoWithText";
import {
  StyledFooter,
  WrapeperLogo,
  WrapperPagesAndSocialLinks,
  Footerpages,
  WrapperSocialLinks,
  Block,
  ItemLink,
} from "./StyledFooter";
import { SocialLinks } from "../SocialLinks/SocialLinks";
import Link from "next/link";

export const menuData = [
  "девелопер",
  "heritage",
  "Real Estate",
  "мобільний додаток",
  "Property Management",
  "Utility Management",
  "Rental Estate",
  "Home, feelings & you",
  "контакти",
];

export const Footer = ({
  pageData,
  FooterMenuColon1,
  FooterMenuColon2,
  FooterMenuColon3,
}) => {
  const { menu } = pageData;

  return (
    <StyledFooter>
      <WrapeperLogo>
        <LogoWithText namePage="Footer" fill={"#FFFFFF"} />
      </WrapeperLogo>
      <WrapperPagesAndSocialLinks>
        <Footerpages>
          {menu.map((page, i) => {
            return (
              <Link href={page?.path} key={i}>
                <ItemLink className="ItemLink">
                  {page?.label?.toUpperCase()}
                </ItemLink>
              </Link>
            );
          })}
        </Footerpages>

        <WrapperSocialLinks>
          <SocialLinks globalContactsData={pageData?.globalContactsData} />
        </WrapperSocialLinks>
      </WrapperPagesAndSocialLinks>
    </StyledFooter>
  );
};
