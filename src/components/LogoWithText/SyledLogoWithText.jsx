import styled from "styled-components";
import { screenSize } from "../../constants/media";

const mainPage = {
  marginTop:20,
  marginRight:2,
  
  [`@media (max-width: ${screenSize.laptopL})`]: {
  width:250,
  marginRight:0,
  marginTop:9,
  },
  [`@media (max-width: ${screenSize.laptop})`]: {
  width:250,
  marginRight:-2
  },
  
  [`@media (min-width: 968px) and (max-height: 600px)`]: {
          height: 'auto',
  },
  
  [`@media (max-width: 991px) and (max-height: 600px)`]: {
          height: "70%",
  },
  
  [`@media (max-width: ${screenSize.laptopPortrait}) and (orientation: portrait)`]: {
  width:228,
  marginRight:0,
  marginTop:2,
  }, 
  [`@media (max-width: ${screenSize.mobileL}) and (orientation: portrait)`]: {
  width: 134,
  height: 86,
  marginTop:7
  },
  
  [`@media (max-width: ${screenSize.mobileLLandscape}) and (orientation: landscape)`]: {
    width: 131,
    height: 84,
    marginTop:0
            },
  
            [`@media (max-width: 768px) and (max-height: 500px)`]: {
                  height: "90%",
          },
  
          [`@media (max-width: 768px) and (max-height: 450px)`]: {
                  width: 90,
          },
  
  [`@media (max-width: ${screenSize.mobileLLandscape}) and (max-height: 300px)`]: {
          width: 100,
          height: 60,
  }
};

const generalPages={
        width: 250,
        marginTop:74,
        [`@media (max-width: ${screenSize.laptopL})`]: {
          marginTop:'unset',
          marginBottom:'10px',
        },
        [`@media (max-width: ${screenSize.laptopPortrait})`]: {
          width: 228,
        },
        
        [`@media (max-height: 620px)`]: {
          width: 156,
        },

        [`@media (max-width: ${screenSize.mobileL}) and (orientation: portrait)`]: {
          width: 134,
        },
        [`@media (max-width: ${screenSize.mobileLLandscape}) and (orientation: landscape)`]: {
          width: 156,
          marginBottom:'unset',
          height: 100,
          marginTop:30,
        },
      }


      const footer ={
        width: 250,
        [`@media (max-width: ${screenSize.laptop})`]: {
        width: 190,  
        },
        [`@media (max-width: ${screenSize.mobileL}) and (orientation: portrait)`]: {
          width: 218,
        },
        [`@media (max-width: ${screenSize.mobileLLandscape}) and (orientation: landscape)`]: {
          width: 218,
        },
      }
// renderStylePages добавил эту фукцию потому что в нескольких местах применяеться это и адаптив у них разный
// в зависимости от страницы рендерит соответствующие стили
const renderStylePages = (namePage) => {
  switch (namePage) {
    case "MainPage":
      return mainPage;
      case "GeneralPages":
      return generalPages;
      case "Footer":
        return footer;
    default:
return {}
  }
};


export const SyledLogoWithText = styled.svg(({ namePage }) => ({
  ...renderStylePages(namePage),
}));
