import styled from "styled-components"
import {screenSize} from "../../../constants/media"

export const StyledFormContent = styled.div`
  width: 100%;
  /* padding-top: 50px; */
`
export const StyledInput = styled.input`
width: 100%;
max-width: 500px;
height: 40px;
background: #d9d3c533;
margin: 0 auto 7px;
display: block;
border-radius: 10px;
padding: 5px 20px;
font-family: 'AGLettericaC';
font-style: normal;
font-weight: 400;
font-size: 10px;
line-height: 12px;
letter-spacing: 0.2em;
text-transform: uppercase;
color: #514946;

&:placeholder{
    color: #9C8D88;
    letter-spacing: 0.5em;
}

@media (max-width: ${screenSize.laptop}) and (max-height: 500px) {
    height: 35px!important;
    font-size: 14px;
  }
`

export const StyledFormButton = styled.button`
  background: #494e54;
  border-radius: 10px;
  height: 50px;
  max-width: 240px;
  display: flex;
  width: 100%;
  align-items: center;
  justify-content: center;
  font-family: "AGLettericaC";
  font-style: normal;
  font-weight: 400;
  font-size: 12px;
  line-height: 12px;
  text-align: center;
  letter-spacing: 0.5em;
  text-transform: uppercase;
  color: #ffffff;
  margin: 10px auto 0;
  transition: opacity 0.3s;
  outline: none;
  padding: 4px 15px;

  &:hover {
    opacity: 0.8;
  }

  &:focus-visible,
  &:focus {
    outline: none;
  }
`

export const StyledModalOverlay = styled.div(({shown}) => ({
  background: "#000",
  opacity: ".9",
  position: "fixed",
  left: 0,
  top: 0,
  width: "100%",
  height: "100%",
  zIndex: 20,
  display: shown ? "block" : "none",
  cursor: "pointer",

  ["&:before"]: {
    content: '"✖"',
    position: "fixed",
    right: "30px",
    top: "24px",
    fontSize: 20,
    color: "#fff",
    diplay: "block",
    cursor: "pointer",
  },
}))
