// import BX24 from 'bx24-api';
import Script from 'next/script';
import React, { useEffect, useMemo, useState } from 'react';
import { memo } from 'react/cjs/react.production.min';
// import bx24 from '../../../BX24/bx24';

import { FormContainer } from '../../Pages/Contact/SupportComponent/MapAndFormAndLogo/StyledMapAndFormAndLogo';
import { StyledFormContent, StyledInput, StyledFormButton, StyledModalOverlay } from './StyledForm';

const Form = () => {
const [wasShown, setShown] = useState(false);
const [onceLoaded, isOnceLoaded] = useState(false);
const [isShowedForm, setIsShowedForm] = useState(false)
const [inputsVal, setInputsVal] = useState({
    name: '',
    tel: '',
    email: '',
})

    const formWasShown = useMemo(() => wasShown, [wasShown]);
    const onceLoadedState = useMemo(() => onceLoaded, [onceLoaded]);
    

    const b24SScript = useMemo(() => {
        return (<Script data-b24-form="inline/92/favjx5" strategy="lazyOnload" async="true">{`(function(w,d,u){
            var s=d.createElement('script');s.async=true;s.src=u+'?'+(Date.now()/180000|0);
            var h=d.getElementsByTagName('script')[0];
            var formBox = d.getElementById('bx24-form-content');
            formBox.parentNode.insertBefore(s,formBox);
          })(window,document,'https://cdn.bitrix24.ua/b18512913/crm/form/loader_92.js');` }</Script>)
    }, [onceLoaded])

    useEffect(() => {
        // setShown(true);
        isOnceLoaded(true);
    }, [])

    // useEffect(() => {
    //     let froms = document.querySelectorAll('.b24-form');
    //     froms.forEach((form, i, arr) => {
            
    //         console.log(i, 'i');
    //         if (i !== arr.length) {
    //             form.classList.remove('shown')
    //         }
    //         // isShowedForm ? form.classList.add('shown') : form.classList.remove('shown');
    //     })
    //     console.log(froms);
    //     froms
    // }, [isShowedForm])

    const showForm = () => {
        setIsShowedForm(prev => !prev)
    }

    // useEffect(() => {
    //     if (typeof document !== 'undefined') {
    //         let forms = document.querySelectorAll('.b24-form');
    //         console.log(forms, 'forms');
    //         forms.forEach((form, i, arr) => {
    //             console.log(i, 'in arr');
    //             if (i !== arr.length) {
    //                 console.log('remove');
    //                 form.remove();
    //             }

    //         })
    //     }
    // }, [onceLoaded])

    return (
        <FormContainer className="bx24-form-wrapper">
            <StyledFormContent  id="bx24-form-content">
               
                {/* <StyledInput name="name" className="bx24-form__input" placeholder="Ім'я" onChange={(e) => {
                    setInputsVal((prev) => {
                        return {
                            ...prev,
                            name: e.target.value
                        }
                    })
                }} autoComplete="given-name" type="string" value={inputsVal.name}/>

                <StyledInput name="tel" className="bx24-form__input" placeholder="Телефон" autoComplete="given-tel" type="tel" pattern="[\+]\d{2}\s[\(]\d{3}[\)]\s\d{3}[\-]\d{2}[\-]\d{2}" minLength="13" maxLength="13" value={inputsVal.tel} onChange={(e) => {
                    setInputsVal((prev) => {
                        return {
                            ...prev,
                            tel: e.target.value
                        }
                    })
                }}/>

                <StyledInput name="email" className="bx24-form__input" placeholder="E-mail" autoComplete="given-email" type="email" value={inputsVal.email} onChange={(e) => {
                    setInputsVal((prev) => {
                        return {
                            ...prev,
                            email: e.target.value
                        }
                    })
                }}/>
                <StyledFormButton onClick={showForm}>{"Зв`язатись"}</StyledFormButton>
                <StyledModalOverlay shown={isShowedForm} onClick={showForm}/> */}
                {/* {onceLoaded && b24SScript} */}
                <iframe src="/form.html" width="100%" height="240px" frameBorder="0"></iframe>
            </StyledFormContent>
           

        </FormContainer>
    );
};

export default Form;