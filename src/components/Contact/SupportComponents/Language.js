import {
  StyledLanguage,
  LanguageCircle,
  LanguageItem,
} from "../../Pages/Brand/StyledBrand";
import { useEffect, useState } from "react";
import Link from "next/link";
import { useRouter } from "next/router";

export const Language = ({ color }) => {
  const router = useRouter();
  const currLanguage = router?.locale;
  const { asPath } = router;
  const [languageList, setLanguageList] = useState(["ua", "ru", "en"]);
  const [isOpen, setIsOpen] = useState(false);
  const [selectedLanguage, setSelectedLanguage] = useState(currLanguage);
  const isDefWhiteColor = color == "#FFFFFF";
  const defColor = isDefWhiteColor ? "#FFFFFF" : "#494E54";
  const textColor = isDefWhiteColor ? "#494E54" : "#FFFFFF";

  useEffect(() => {
    onClick(currLanguage);
  }, [currLanguage]);

  const onClick = (item) => {
    setSelectedLanguage(item);
    // удаляю выбранный елемент из масива и добаляю его на первре место в этом масиве
    setLanguageList((pre) => [item, ...pre.filter((it) => it != item)]);
  };
  return (
    <StyledLanguage
      style={{ cursor: "pointer", flex: "0 0 auto", minWidth: 26 }}
      onClick={() => setIsOpen((pre) => !pre)}
    >
      <LanguageCircle
        color={defColor}
        backgroundColor={isOpen && defColor}
        isOpen={isOpen}
      >
        {languageList.map((item, i) => {
          const isNotFirstElement = i != 0;
          const isDefaltValue = !isOpen && isNotFirstElement;
          const firstElement = i == 0;
          return (
            <>
              {firstElement && (
                <LanguageItem
                  firstElement={firstElement}
                  className="LanguageItem"
                  defColor={defColor}
                  color={textColor}
                  isDefaltValue={isDefaltValue}
                  isOpen={isOpen}
                  onClick={
                    selectedLanguage != item ? () => onClick(item) : () => {}
                  }
                >
                  {item}
                </LanguageItem>
              )}

              {!firstElement && (
                <Link href={`${item}${asPath}`} locale={item}>
                  <LanguageItem
                    defColor={defColor}
                    color={textColor}
                    isDefaltValue={isDefaltValue}
                    isOpen={isOpen}
                    onClick={
                      selectedLanguage != item ? () => onClick(item) : () => {}
                    }
                  >
                    {item}
                  </LanguageItem>
                </Link>
              )}
            </>
          );
        })}
      </LanguageCircle>
    </StyledLanguage>
  );
};
