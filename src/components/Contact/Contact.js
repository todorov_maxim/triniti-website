import { FacebookText, InstagramText } from "../Pages/Main/StyledMain";
import {
  StyledContact,
  TelephoneIcon,
  WrapperLink,
  Telephone,
  Number,
  ContactCircle,
} from "../Pages/Brand/StyledBrand";
import { Language } from "./SupportComponents/Language";
import { useContext } from "react";
import GlobalContactContext from "../../GlobalContactContext";
import useIsEmpty from "../../hooks/useIsEmpty";

export const Contact = ({
  colorLanguage = false,
  color = "gray",
  isRenderTelephone = true,
}) => {
  const { contactData, setContactData } = useContext(GlobalContactContext);
  const facebookText = contactData?.facebook?.title?.toUpperCase();
  const instagramText = contactData?.instagram?.title?.toUpperCase();
  const facebookLink = contactData?.facebook?.url;
  const instagramLink = contactData?.instagram?.url;
  return (
    <StyledContact>
      {/* <Language color={colorLanguage ? colorLanguage : color} /> */}
      <WrapperLink>
        {!useIsEmpty(contactData?.facebook?.title || facebookLink) && (
          <FacebookText
            color={colorLanguage ? colorLanguage : color}
            href={facebookLink}
            target="_blank"
          >
            {facebookText}
          </FacebookText>
        )}
        {!useIsEmpty(contactData?.instagram?.title || instagramLink) && (
          <InstagramText
            color={colorLanguage ? colorLanguage : color}
            href={instagramLink}
            target="_blank"
            letterSpacing="0.25em"
            marginLeft="33px"
            mobMarginLeft="24px"
          >
            {instagramText}
          </InstagramText>
        )}
      </WrapperLink>

      {isRenderTelephone && !useIsEmpty(contactData?.phone) && (
        <Telephone href={`tel:+${contactData?.phone}`}>
          <ContactCircle color="#CD8B7D" mobWidthHeight="32px">
            <TelephoneIcon className="icon-telephone" />
          </ContactCircle>
          <Number>{contactData?.phone}</Number>
        </Telephone>
      )}
    </StyledContact>
  );
};
