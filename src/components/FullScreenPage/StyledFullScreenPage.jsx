import styled, { css, keyframes } from "styled-components";
import { screenSize } from "../../constants/media";
import Image from "next/image";

export const StyledFullScreenPage = styled.div(() => ({
  position: "relative",
  display: "flex",
  flexDirection: "row",
  height: "100%",
  width: "100%",
}));

export const ContentFullScreenPage = styled.div(({isChangeWidth}) => ({
  display: "flex",
  flexDirection: "column",
  // height: "100vh",
 maxHeight: "100vh", 
 height: "100%",
  width: "29.63%",
  [`@media (max-width: ${screenSize.laptopL})`]: {
    width: "37.2%",
  },
  [`@media (max-width: ${screenSize.laptop})`]: {
    width: "43.8%",
  },
  [`@media (max-width: ${screenSize.laptopPortrait}) and (orientation: portrait)`]:
    {
      width: "100%",
    },
  [`@media (max-width: ${screenSize.mobileL}) and (orientation: portrait)`]: {
    width: "100%",
  },
  [`@media (max-width: ${screenSize.mobileLLandscape}) and (orientation: landscape)`]:
    {
      width:isChangeWidth?'53.82%': "47.05%",
    },

    

    [`@media (max-width: 600px) and (orientation: landscape)`]:{
      width: '53%',
    },
    

    [`@media (max-width: 500px) and (orientation: landscape)`]:{
      width: '65%',
    }
}));

export const StyledFullScreenHeader = styled.div(({ isContact,hiddenHeader }) => ({
  padding: "18px 21px",
  maxHeight: "47.68%",
  width: "100%",
  height: "100%",
  display: "flex",
  justifyContent: "flex-start",
  flexDirection: "column",
  background: 'white',zIndex:20,
  [`@media (max-width: ${screenSize.laptopL})`]: {
    maxHeight: "41.38%",
  },
  [`@media (max-width: ${screenSize.laptop})`]: {
    padding: "16.8px 21px",
  },
  [`@media (max-width: ${screenSize.laptopPortrait}) and (orientation: portrait)`]:
    {
      maxHeight: "23.3%",
      padding: "18px 21px",
    },
  [`@media (max-width: ${screenSize.mobileL}) and (orientation: portrait)`]: {
    padding: "14px 21px",
    maxHeight: !isContact ? "25.11%" : "25.2%",
    paddingBottom: isContact && 1,
  },
  [`@media (max-width: ${screenSize.mobileLLandscape}) and (orientation: landscape)`]:
    {
      maxHeight: "46.52%",
      justifyContent: "space-between",
      padding: "14px 0px 15px 21px",
      display:hiddenHeader&& 'none',
    },
    [`@media (max-width: 360px ) and (max-width: 600px)`] : {
      maxHeight: !isContact ? "25.11%" : "25.11%",
      }
}));


export const StyledMobFullScreeImg = styled.div(({ maxHeight }) => ({
  display: "none",
  maxHeight: "100vh",
  width: "69.94%",
  height: "100%",
  position: "relative",
  right: 0,
  top: 0,
  span: {
    width: "100% !important",
    height: "100% !important",
  },
  [`@media (max-width: ${screenSize.laptopPortrait}) and (orientation: portrait)`]:
    {
      display: "flex",
      width: "100%",
      maxHeight:maxHeight?maxHeight: "49.65%",
    },

  [`@media (max-width: ${screenSize.mobileL}) and (orientation: portrait)`]: {
    display: "flex",
    width: "100%",
    maxHeight: maxHeight ? maxHeight : "36.32%",
  },
  [`@media (max-width: 360px) and (max-height: 600px)`]: {
    maxHeight: "30.32%"
  }
}));


export const FullScreenImageFontImage = styled.div(({signaturePosition = 'l-t'}) => ({
  right: signaturePosition.split("-")[0] === "r" && 0,
  left: signaturePosition.split("-")[0] === "l" && 0,
  position: 'absolute',
  bottom: signaturePosition.split("-")[1] === "b" && 0,
  top: signaturePosition.split("-")[1] === "t" ? 75 : signaturePosition.split("-")[1] === "c" && "50%",
  // maxWidth: "30%",
  // width: "100%",
  objectFit: "contain",

  [".image-cover"] : {
    objectFit: "contain",
  },
  
}));


export const FullScreenImageFontBox = styled(FullScreenImageFontImage)(({signaturePosition = 'l-t', bgColor = '#CD8B7D', fontColor = "black"}) => ({
  padding: "32px 25px", 
  // background: "#bec2254f",
  color: fontColor !== "white" ? '#000' : '#fff',
  textAlign: "center",
  width:"auto",

  [`@media (max-width: 1700px)`]: {
    padding: "32px 15px", 
  },
  
  [`@media (max-width: 992px)`]: {
    padding: "25px 15px",
    top: "auto",
    bottom: "0",
    right: 0,
    left: "auto",
    background: `${bgColor}69`,
    backdropFilter: fontColor !== "white" ? "grayscale(1)" : "grayscale(1)",
  },

  [`@media (max-width: ${screenSize.mobileL}) and (orientation: portrait)`]: {
    right: "auto",
    left: 0,
    backdropFilter: fontColor === "white" && "grayscale(1) brightness(0.8)",
    // background: "none",
    
    bottom: 0,
    top: 0,
    height: "100%",
    display: "flex",
    flexDirection: "column",
    justifyContent: signaturePosition.split("-")[1] === "t" ? "flex-start" : signaturePosition.split("-")[1] === "c" ? "center" : "flex-end",
  },
  [`@media (max-width: 430px) and (orientation: portrait)`]: {
    padding: '18px 10px',
    justifyContent: 'center',
  },

  
}))

export const FullScreenImageFontText = styled.div(() => ({
  fontFamily: "Silently",
  transform: 'rotate(-18deg)',
  fontSize: 58,
  lineHeight: 1,
  opacity: .7,
  textTransform: "capitalize",

  [`@media (max-width: 1700px)`]: {
    fontSize: 45,
  },

  [`@media (max-width: ${screenSize.laptopL})`]: {
    fontSize: 35
  },

  [`@media (max-width: 1300px)`]: {
    fontSize: 28,
  },

  [`@media (max-width: ${screenSize.mobileL}) and (orientation: portrait)`]: {
    fontSize: 24,
  },

  [`@media (max-width: 430px) and (orientation: portrait)`]: {
    fontSize: 22,
  },

  [`@media (max-width: 370px) and (orientation: portrait)`]: {
    fontSize: 20,
  },
  [`@media (max-width: 340px) and (orientation: portrait)`]: {
    fontSize: 18,
  },
}));

const calcIsActiveImgFullScreeImg=(isActiveImg,mobVersionDesctopPhoto=false)=>{
  // mobVersionDesctopPhoto - это параметр когда в  моб версии меняют ориентацию экрана то показываеть фото с desctop
  if(mobVersionDesctopPhoto){
   return {display:'flex'}
  }
else if(isActiveImg){
    return {display:'flex',top:0,right:0,position:'absolute', transform: 'translateY(0%)',
    transition: "opacity .5s .5s, transform .5s .5s",
    zIndex: 2,
    opacity: 1,}
  }else {
    return {
      top:0,right:0,position:'absolute',
    transform: 'translateY(20%)',
zIndex: -1,
    transition: "opacity .5s, transform .5s",
    opacity: 0,}
  }
}



export const StyledFullScreeImg = styled.div(({isChangeWidth,isActiveImg,mobVersionDesctopPhoto=false}) => ({
  // maxWidth: 1343,
  maxHeight: "100vh",
  width: "calc(100% - 29.63%)",
  position: 'relative',
  height: "100%",
  right: 0,
  top: 0,
  span: {
    width: "100% !important",
    height: "100% !important",
  },
  ...calcIsActiveImgFullScreeImg(isActiveImg,mobVersionDesctopPhoto),
  [`@media (max-width: ${screenSize.laptopL})`]: {
    width: "calc(100% - 37.2%)",
  },
  [`@media (max-width: ${screenSize.laptop})`]: {
    width: "calc(100% - 43.8%)",
  },
  [`@media (max-width: ${screenSize.laptopPortrait}) and (orientation: portrait)`]:
    {
      display: "none",
    },
  [`@media (max-width: ${screenSize.mobileL}) and (orientation: portrait)`]: {
    display: "none",
  },
  [`@media (max-width: ${screenSize.mobileLLandscape}) and (orientation: landscape)`]:
    {
      width:isChangeWidth ?'calc( 100% - 53.82% )': "53.4%",
    },

    [`@media (max-width: 600px) and (orientation: landscape)`]:{
      width: '47%',
    },
    [`@media (max-width: 500px) and (orientation: landscape)`]:{
      width: '35%',
    }
}));

export const AfterDescription = styled.div`
  &:after {
    width: 14.58%;
    height: 100%;
    left: calc(100% - 1px);
    position: absolute;
    top: 0px;
    z-index: 5;
    content: "";
    @media (max-width: ${screenSize.laptopL}) {
      width: 17.2%;
    }
    @media (max-width: ${screenSize.laptop}) {
      width: 21.34%;
    }

    @media (max-width: ${screenSize.mobileL}) and (orientation: portrait) {
      display: none;
    }
    @media (max-width: ${screenSize.mobileLLandscape}) and (orientation: landscape) {
      width: 15.34%;
    }
  }
`;

const calcIsActiveDescriptionFullScreeDescription=(isActiveDescription,mob)=>{
  if(mob){
    return  {display:'flex'}
  }
  else if(isActiveDescription){
    return {display:'flex',bottom:0,left:0,zIndex:20,opacity: 1}
  }else {
    return {opacity: 0,  transition: 'opacity .5s', maxHeight: 0, padding: '0 !important', overflow: 'hidden'}
  }
}

export const StyledFullScreeDescription = styled(AfterDescription)(
  ({ background, color,isActiveDescription,mob=true }) => ({
    maxHeight: "calc( 100% - 41.38%)",
    padding: '20px 0 20px 45px',
    width: "100%",
    height: "100%",
    background,
    position: "relative",
    // display: "flex",
    justifyContent: "space-around",
    alignItems: "flex-end",
    flexDirection: "column",
 ...calcIsActiveDescriptionFullScreeDescription(isActiveDescription,mob),
    color,
    ":after": {
      background,
    },
    [`@media (max-width: ${screenSize.laptop})`]: {
      paddingTop: 31,
    },
    [`@media (max-width: ${screenSize.laptop}) and (max-height: 450px)`]: {
      paddingTop: 10,
      paddingLeft: 20,
    },
    [`@media (max-width: ${screenSize.laptopPortrait}) and (orientation: portrait)`]:
      {
    maxHeight: "calc( 100% - 23.3% - 49.60%)",

        alignItems: "center",
      },
    [`@media (max-width: ${screenSize.mobileL}) and (orientation: portrait)`]: {
      paddingTop: 25,
      paddingLeft: 0,
      alignItems: "center",
      maxHeight: "39.41%",
    },
    [`@media (max-width: ${screenSize.mobileLLandscape}) and (orientation: landscape)`]:
      {
        maxHeight: "calc( 100% - 46.52%)",
        paddingTop: 19,
        alignItems: "center",
        justifyContent: "center",
        paddingLeft: 20,
      },
      [`@media (max-width: ${screenSize.mobileLLandscape}) and (max-height: 300px)`]: {
        paddingTop: 10,
        paddingLeft: 10,
      },
      [`@media (max-width: 360px) and (max-height: 600px)`]: {
        maxHeight: "45.41%",
      }
  })
);

export const WrapeperLogo = styled.div(() => ({
  display: "flex",
  justifyContent: "center",
  alignItems: "center",
  width: "100%",
  height: "100%",
  [`@media (max-width: ${screenSize.mobileLLandscape}) and (orientation: landscape)`]:
    {
      alignItems: "center",
maxHeight:144,
    },
    
}));


const calcIsActiveDescriptionFullScreeDescriptionContent=(isActiveDescription, mob)=>{
  if(mob){
    return  {display:'flex'}
  } else if(isActiveDescription){
    return {opacity: 1, transition: 'opacity 1s .5s, transform .6s .4s', transform: 'translateY(0)'}
  } else {
    return {opacity: 0,  transition: 'opacity .5s, transform .6s',  transform: 'translateY(-60px)'}
  }
}

export const Content = styled.div(({isActiveDescription, mob=true}) => ({
  marginBottom: 6,
  maxWidth: 450,
  paddingRight: 15,
  paddingLeft: 2,
  width: "100%",
  maxHeight: 358,
  height: "100%",
  display: "flex",
  flexDirection: "column",
  justifyContent: "space-between",
  position: "relative",

  ...calcIsActiveDescriptionFullScreeDescriptionContent(isActiveDescription, mob),

  [`@media (max-width: ${screenSize.laptopL})`]: {
    maxHeight: 300,
    maxWidth: "82.7%",
    marginBottom: 30,
  },
  [`@media (max-width: ${screenSize.laptop})`]: {
    // maxWidth: "78.7%",
    // maxWidth: "320px",
    maxHeight: 330,
  },
  [`@media (max-width: ${screenSize.laptop}) and (max-height: 450px)`]: {
    marginBottom: 10,
  },
  
  [`@media (max-width: ${screenSize.laptopPortrait}) and (orientation: portrait)`]:
    {
      maxWidth: "73.1%",
      maxHeight: 168,
      marginLeft: 98,
    },
    
    [`@media (min-width: 968px) and (max-height: 600px)`]: {
      paddingTop: 15,
      maxWidth: 330
    },

    
    
  [`@media (max-width: ${screenSize.mobileL}) and (orientation: portrait)`]: {
    position: "unset",
    maxWidth: "85%",
    justifyContent: "flex-start",
    marginLeft: 0,
    marginBottom: 0,
    paddingLeft: 3,
    maxHeight: 210,
  },
  [`@media (max-width: ${screenSize.mobileLLandscape}) and (orientation: landscape)`]:
    {
      position: "unset",
      maxHeight: 140,
      marginBottom: 0,
      maxWidth: "100%",
    },
    [`@media (max-width: ${screenSize.mobileLLandscape}) and (max-height: 300px)`]: {
      height: "auto"
    }
}));

export const CurentPage = styled.div(({ isContactPage }) => ({
  fontSize: 14,
  lineHeight: "18px",
  [`@media (max-width: ${screenSize.laptopL})`]: {
    fontSize: 12,
    marginRight: isContactPage && 4,
  },
}));

export const StyledTitle = styled.div`
  // overflow: hidden;
  // white-space: pre-wrap;
  // -webkit-line-clamp: 3;
  // -webkit-box-orient: vertical;
`;
export const Title = styled(StyledTitle)(({ mob }) => ({
  display: mob && "none",
  // marginBottom:10,
  fontSize: 40,
  lineHeight: "42px",
  letterSpacing: "0.15em",
  textTransform: "uppercase",
  minHeight: 72,
  maxHeight: 122,
  maxWidth: 360,

  [`@media (max-width: 1920px) and (max-height: 680px)`]: {
    fontSize: 32,
    lineHeight: "36px",
  },

  [`@media (max-width: 1920px) and (max-height: 620px)`]: {
    fontSize: 24,
    lineHeight: "32px",
  },
  [`@media (max-width: ${screenSize.laptopL})`]: {
    fontSize: 32,
    lineHeight: "42px",
    minHeight: 100,
    flex: "0 0 auto",
  },
  [`@media (max-width: ${screenSize.laptop})`]: {
    marginBottom: "unset",
    minHeight: 90,
    flex: "0 0 auto",
  },
  [`@media (max-width: ${screenSize.laptop}) and (max-height: 450px)`]: {
    minHeight: 0,
    fontSize: 18,
  lineHeight: "22px",
  },
  [`@media (max-width: 991px)`]:{
    fontSize: 24,
    lineHeight: "32px",
  },

  [`@media (max-width: 991px) and (max-height: 450px)`]: {
    fontSize: 18,
  lineHeight: "22px",
  },

  [`@media (max-width: ${screenSize.laptopPortrait}) and (orientation: portrait)`]:
    {
      minHeight: "74px",
      flex: "0 0 auto",
    },

  [`@media (max-width: ${screenSize.mobileL}) and (orientation: portrait)`]: {
    fontSize: 24,
    lineHeight: "28px",
    display: mob ? "flex" : "block",
    marginTop: 0,
    minHeight: "unset",
    flex: "0 0 auto",
  },
  [`@media (max-width: ${screenSize.mobileLLandscape}) and (orientation: landscape)`]:
    {
      display: mob ? "flex" : "block",
      fontSize: 20,
      lineHeight: "26px",
      minHeight: "unset",
      flex: "0 0 auto",
    },

    [`@media (max-width: ${screenSize.mobileLLandscape}) and (max-height: 300px)`]: {
      lineHeight:'18px',
      fontSize:"16px",
    },
    [`@media (max-width: 359px)`]: {
      fontSize:"16px",
      lineHeight:'18px',
    }
}));


export const TextDescriptionBig = styled.div((biggerSize = false) => ({
  marginTop: 10,
  maxHeight: 140,
  // height: '100%',
  overflow: "hidden",
  fontSize: 22,
  lineHeight: 1.2,

  [`@media (max-width: ${screenSize.laptopL})`]: {
    fontSize: 18,
    marginTop: "unset",
    maxWidth: "84%",
  },
  [`@media (max-width: ${screenSize.laptop})`]: {
    maxWidth: "100%",
    paddingTop: 10,
  },
  [`@media (max-width: ${screenSize.laptopPortrait}) and (orientation: portrait)`]:
    {
      maxWidth: "411px",
    },
  [`@media (max-width: ${screenSize.mobileL}) and (orientation: portrait)`]: {
    fontSize: 16,
    paddingTop: 5,
  },

  [`@media (max-width: ${screenSize.mobileLLandscape}) and (orientation: landscape)`]:
    {
      lineHeight: "14px",
    },
    [`@media (max-width: ${screenSize.mobileLLandscape}) and (max-height: 300px)`]: {
      fontSize:"12px",
      lineHeight:'14px',
      paddingTop: 5,
    },

    [`@media (max-width: 360px) and (max-height: 600px)`]:
    {
      maxHeight: '100%',
    },
    [`@media (max-width: 359px)`]: {
      fontSize:"12px",
      lineHeight:'14px',
    }
}))
export const TextDescription = styled.div((biggerSize = false) => ({
  marginTop: 10,
  maxHeight: 118,
  // height: '100%',
  overflow: "hidden",
  [`@media (max-width: ${screenSize.laptopL})`]: {
    fontSize: 12,
    lineHeight: "18px",
    marginTop: "unset",
    maxWidth: "84%",
  },
  [`@media (max-width: ${screenSize.laptop})`]: {
    maxWidth: "100%",
    paddingTop: 10,
  },
  [`@media (max-width: ${screenSize.laptopPortrait}) and (orientation: portrait)`]:
    {
      maxWidth: "411px",
    },
  [`@media (max-width: ${screenSize.mobileL}) and (orientation: portrait)`]: {
    fontSize: 13,
    lineHeight: "16px",
    paddingTop: 5,
  },

  [`@media (max-width: ${screenSize.mobileLLandscape}) and (orientation: landscape)`]:
    {
      lineHeight: "14px",
    },
    [`@media (max-width: ${screenSize.mobileLLandscape}) and (max-height: 300px)`]: {
      fontSize:"12px",
      lineHeight:'14px',
      paddingTop: 5,
    },

    [`@media (max-width: 360px) and (max-height: 600px)`]:
    {
      maxHeight: '100%',
    },
    [`@media (max-width: 359px)`]: {
      fontSize:"12px",
      lineHeight:'14px',
    }
}));

export const WrapperTitleAndDescription = styled.div(() => ({
  height: "100%",
  maxHeight: "100%",
  marginTop: 75,
  display: "flex",
  flexDirection: "column",
  justifyContent: "start",
  [`@media (max-width: ${screenSize.laptopL})`]: {
    marginTop: 81,
    maxWidth: "85%",
  },

  [`@media (max-width: ${screenSize.laptopL})`]: {
    marginTop: 79,
  },

  [`@media (min-width: 968px) and (max-height: 660px)`]: {
    marginTop: 30,
  },
  [`@media (min-width: 968px) and (max-height: 600px)`]: {
    marginTop: 25,
    maxHeight: '100%',
    minHeight: 0,
    marginBottom: 15,
  },
  [`@media (max-width: 967px) and (max-height: 600px)`]: {
    marginTop: 15,
  },

  [`@media (max-width: ${screenSize.laptopPortrait}) and (orientation: portrait)`]:
    {
      marginTop: 2,
      maxWidth: "100%",
    },
  [`@media (max-width: ${screenSize.mobileL}) and (orientation: portrait)`]: {
    maxWidth: "100%",
    flex: "1 1 auto",
    marginTop: 10,
  },
  [`@media (max-width: ${screenSize.mobileLLandscape}) and (orientation: landscape)`]:
    {
      marginTop: 0,
    },
}));

export const LinkFullPage = styled.div(() => ({
  [`@media (max-width: ${screenSize.laptop})`]: {},

  [`@media (max-width: ${screenSize.laptopPortrait}) and (orientation: portrait)`]:
    {
      display: "flex",
      justifyContent: "flex-end",
      width: "100%",
    },

  [`@media (max-width: ${screenSize.mobileL}) and (orientation: portrait)`]: {
    display: "flex",
    justifyContent: "flex-end",
    width: "100%",
    marginBottom: 0,
  },
}));

export const WrapperNumberPages = styled.div(({ mob }) => ({
  display: mob && "none",

  maxWidth: 40,
  width: "100%",
  maxHeight: 18,
  height: "100%",
  display: "flex",
  justifyContent: "space-around",
  fontSize: 14,
  lineHeight: "18px",
  flex: "0 0 auto",
  [`@media (max-width: ${screenSize.mobileL}) and (orientation: portrait)`]: {
    display: mob ? "flex" : "none",
  },
  [`@media (max-width: 992px) and (orientation: landscape)`]:
    {
      display: mob ? "flex" : "none",
      position: "absolute",
      top: 0,
      right: -19,
      zIndex: 10,
    },
  [`@media (max-width: ${screenSize.mobileLLandscape}) and (orientation: landscape)`]:
    {
      display: mob ? "flex" : "none",
      position: "absolute",
      top: 0,
      right: -30,
      zIndex: 10,
    },
}));

export const AllPages = styled.div(() => ({
  opacity: "0.25",
  fontSize: 14,
  lineHeight: "18px",
  [`@media (max-width: ${screenSize.laptopL})`]: {
    fontSize: 12,
  },
}));

export const IconComma = styled.i(({color}) => ({
  top: 51,
  left: -39,
  position: "absolute",
  fontSize: 112,
  zIndex: 1,
  color: color && color,
  opacity: "0.25",
  [`@media (max-width: ${screenSize.laptopL})`]: {
    fontSize: 113,
    top: 51,
    left: -39,
  },
  [`@media (max-width: ${screenSize.laptopPortrait}) and (orientation: portrait)`]:
    {
      fontSize: 91,
      top: -22,
      left: -15,
    },

  [`@media (min-width: 768px) and (max-height: 600px)`]: {
      fontSize: 65,
      top:50
  },

  [`@media (max-width: ${screenSize.mobileL}) and (orientation: portrait)`]: {
    fontSize: 60,
    top: 2,
    left: 20,
  },
  [`@media (max-width: ${screenSize.mobileLLandscape}) and (orientation: landscape)`]:
    {
      fontSize: 60,
      top: 8,
      left: 10,
    },
}));

export const DescriptionMobileL = styled.div(() => ({
  display: "none",
  [`@media (max-width: ${screenSize.mobileL}) and (orientation: portrait)`]: {
    display: "flex",
    justifyContent: "space-between",
    width: "100%",
    flex: "0 0 auto"
    // height: "min-content",
    // maxHeight: "80px",
    // minHeight:52
  },
  [`@media (max-width: ${screenSize.mobileLLandscape}) and (orientation: landscape)`]:
    {
      display: "flex",
      justifyContent: "space-between",
      width: "100%",
      // height: "min-content",
      // maxHeight: "80px",
      // minHeight:27,
      position: "relative",
      flex: "0 0 auto",
    },
}));

const lineHeight = (portraitLineHeight, mobileLlineHeight) => {
  if (portraitLineHeight && mobileLlineHeight) {
    return "4px";
  }
  if (portraitLineHeight) {
    return "16px";
  } else if (mobileLlineHeight) {
    return "4px";
  }
};

export const ContactTitle = styled.div(
  ({ portraitLineHeight, mobileLlineHeight }) => ({
    color: "#494E54",
    [`@media (max-width: ${screenSize.laptopPortrait}) and (orientation: portrait)`]:
      {
        lineHeight: portraitLineHeight && 0,
      },
    [`@media (max-width: ${screenSize.mobileL}) and (orientation: portrait)`]: {
      lineHeight: lineHeight(portraitLineHeight, mobileLlineHeight),
    },
    [`@media (max-width: 360px ) and (max-width: 600px)`] : {
      lineHeight: "inherit"
    }
  })
);

export const ContactText = styled.div(({ portraitWidth }) => ({
  color: "#FFFFFF",
  [`@media (max-width: ${screenSize.laptopPortrait}) and (orientation: portrait)`]:
    {
      marginTop: portraitWidth && "6.5%",
    },
  [`@media (max-width: ${screenSize.mobileL}) and (orientation: portrait)`]: {
    marginTop: portraitWidth && "0%",
  },

  [`@media (max-width: 359px)`]: {
    lineHeight: "12px"
  }
}));

export const ContactNumber = styled.div(({ isEditeMode }) => ({
  maxHeight: isEditeMode ? 40 : 'max-content',
  height: "100%",
  display: "flex",
  flexDirection: "column",
  justifyContent: "space-between",
  [`@media (max-width:768px)`]: {
    justifyContent: "flex-start",
    height: "auto",
    minHeight: 38,

  },
  [`@media (max-width:480px) and (max-height: 500px) and (orientation: portrait)`]: {
    // maxHeight: 100%,
  },
  [`@media (max-width: 360px ) and (max-height: 600px)`] : {
    maxHeight: "100%",
  }
}));

export const Schedule = styled(ContactNumber)(() => ({
  marginTop:10,

  [`@media (max-width: 460px ) and (max-height: 700px)`] : {
    // marginTop:5,
  },
}));

export const Adress = styled(ContactNumber)(() => ({}));

export const Email = styled(ContactNumber)(() => ({
  // marginTop:5
}));

export const PencilIcon = styled.i(() => ({
  height: "100%",
  fontSize: 15,
  maxHeight: "29.61%",

  color: "#FFFFFF",
}));

export const StatusPage = styled.div(({ notMobileLandscape }) => ({
  [`@media (max-width: ${screenSize.laptopPortrait}) and (orientation: portrait)`]:
    {
      width: "100%",
      height: "max-content",
      display: "flex",
      justifyContent: "flex-end",
      marginTop: 7,
    },
  [`@media (max-width: ${screenSize.mobileL}) and (orientation: portrait)`]: {
    marginTop: 0,
    display: "none",
  },
  [`@media (max-width: ${screenSize.mobileLLandscape}) and (orientation: landscape)`]:
    {
      display: notMobileLandscape && "none",
    },
}));

export const WrapperContactNumberPages = styled.div(({ isContactPage }) => ({
  width: "100%",
  maxHeight: "5.72%",
  height: "100%",
  display: "flex",
  marginBottom: isContactPage && "11%",
  [`@media (max-width: ${screenSize.laptopL})`]: {
    width: !isContactPage && 37,
    display: !isContactPage && "flex",
    justifyContent: !isContactPage && "space-between",
    marginBottom: isContactPage && "7%",
  },
  [`@media (max-width: ${screenSize.laptop})`]: {
    marginBottom: isContactPage && "9%",
  },
  [`@media (max-width: ${screenSize.laptopPortrait}) and (orientation: portrait)`]:
    {
      width: !isContactPage && 42,
      order: isContactPage && 2,
      width: isContactPage && "20%",
      marginBottom: isContactPage && 0,
      maxHeight: isContactPage && "30.72%",
      justifyContent: "center",
      alignItems: "flex-end",
      paddingBottom: "0.6%",
      paddingRight: "0.5%",
    },
    [`@media (min-width: 968px) and (max-height: 660px)`]: {
      marginBottom: 10,
    },
    [`@media (min-width: 968px) and (max-height: 600px)`]: {
      marginBottom: 10,
    },
  
    [`@media (max-width: 967px) and (max-height: 600px)`]: {
      marginBottom: 10,
    },
  [`@media (max-width: ${screenSize.mobileL}) and (orientation: portrait)`]: {
    marginBottom: isContactPage && "0%",
    order: isContactPage && 1,
    display: !isContactPage && "none",
    justifyContent: "flex-end",
    maxHeight: "5%",
    width: isContactPage && "100%",
  },
  [`@media (max-width: ${screenSize.mobileLLandscape}) and (orientation: landscape)`]:
    {
      order: 3,
      position: "absolute",
      zIndex: 20,
      top: 37,
      left: '100%',
    },

    [`@media (max-width:359px)`]: {
      maxHeight: "100%",
      height: "auto",
    }
}));

const calcIsActiveDescriptionFullScreeDescriptionContactContent=(isActiveDescription, mob)=>{
  if(mob){
    return  {display:'flex'}
  } else if(isActiveDescription){
    return {opacity: 1, transition: 'opacity 1s .5s, transform .6s .4s', transform: 'none' }
  } else {
    return {opacity: 0, transition: 'opacity .5s, transform .6s',  transform: 'translateY(-60px)', maxHeight: 0, padding: '0 !important', overflow: 'hidden'}
  }
}

export const ContactContent = styled.div(({ isEditeMode, isActiveDescription, mob=true }) => ({
  maxHeight: isEditeMode ? "430px" : " 360px",
  maxWidth: isEditeMode ? "81%" : "77%",
  width: "100%",
  height: "100%",
  display: "flex",
  flexWrap: "wrap",
  justifyContent: "center",
  position: !isEditeMode && "relative",
  alignContent: "start",
  marginBottom: isEditeMode && "53px",
  marginRight: isEditeMode && 23,
  ...calcIsActiveDescriptionFullScreeDescriptionContactContent(isActiveDescription, mob),
  [`@media (max-width: ${screenSize.laptopL})`]: {
    width: "100%",
    maxWidth: !isEditeMode ? 391 : 403,
    maxHeight: !isEditeMode ? 326 : 382,
    marginRight: !isEditeMode ? "2.2%" : 0,
    marginBottom: "0.8%",
  },
  [`@media (max-width: ${screenSize.laptop})`]: {
    width: "100%",
    maxWidth: 302,
    maxHeight: !isEditeMode && 326,
    marginRight: !isEditeMode && "2.2%",
    marginBottom: "0.8%",
    ...(() =>
      isEditeMode && {
        maxWidth: 427,
      })(),
  },
  [`@media (max-width: ${screenSize.laptopPortrait}) and (orientation: portrait)`]:
    {
      maxWidth: "100%",
      marginRight: 0,
      marginBottom: 0,
    },

    [`@media (max-width: ${screenSize.mobileLLandscape}) and (max-height: 300px)`]:{

    },
  [`@media (max-width: ${screenSize.mobileL}) and (orientation: portrait)`]: {
    position: "unset",
    maxWidth: "100%",
    justifyContent: "flex-start",
    marginBottom: isEditeMode && "unset",
    padding: "0% 7% 0% 0%",
    maxHeight:isEditeMode&&'100%'
  },
  [`@media (max-width: ${screenSize.mobileLLandscape}) and (orientation: landscape)`]:
    {
      maxWidth: "100%",
      marginRight: 0,
    position: isEditeMode&&"unset",
    },
    [`@media (max-width: 370px)`]: {
      padding: "0% 2% 0% 0%",
    },
}));

export const WrapperContact = styled.div(({ colReverse, isEditeMode }) => ({
  display: "flex",
  flexDirection: "column",
  height: 'max-content',
  maxHeight: isEditeMode ? 92 : 'unset',
  width: isEditeMode ? "100%" : "33%",
  justifyContent: "space-between",
  fontSize: 12,
  lineHeight: "18px",
  marginLeft: colReverse && "4%",

  [`@media (max-width: ${screenSize.laptopL})`]: {
    width: !colReverse ? "39%" : "35%",
    marginLeft: colReverse && "1%",
    paddingLeft: colReverse && "1.5%",
    // maxHeight: "29%",
  },
  [`@media (max-width: ${screenSize.laptop})`]: {
    width: "50%",
    marginLeft: 0,
    paddingLeft: colReverse && "4%",
  },
  [`@media (max-width: ${screenSize.laptopPortrait}) and (orientation: portrait)`]:
    {
      width: colReverse ? "20%" : "32.2%",
      alignItems: !colReverse && "center",
      order: !colReverse ? 3 : 4,
      paddingLeft: colReverse ? 0 : "5%",
      maxHeight: colReverse ? "31%" : "32%",
      marginLeft: !colReverse && "1%",
    },
  [`@media (max-width: ${screenSize.mobileL}) and (orientation: portrait)`]: {
    width: colReverse ? "50%" : "48%",
    paddingLeft: colReverse ? 0 : 0,
    alignItems: !colReverse && "start",
    flexDirection: colReverse ? "column-reverse" : "column",
    marginLeft: !colReverse && "0%",
    maxHeight: "26%",
    paddingRight: "3px"
  },
  [`@media (max-width: ${screenSize.mobileLLandscape}) and (orientation: landscape)`]:
    {
      order: !colReverse ? 1 : 2,
      fontSize: 10,
      lineHeight: "14px",
      width: !colReverse ? "47%" : "53%",
      paddingLeft: colReverse ? 0 : 0,
      flexDirection: colReverse ? "column-reverse" : "column",
      maxHeight: "43%",
      paddingLeft: !colReverse ? "8%" : 0,
      paddingTop: "5%",
    },

    [`@media (max-width: ${screenSize.mobileLLandscape})`]: {
      maxHeight: "100%",
      height: "auto",
      paddingTop: 5,
      justifyContent: colReverse && "flex-end",
      paddingLeft: !colReverse ? 10 : 0,
    },
    
    [`@media (max-width: 380px )`] : {
      paddingLeft: 0,
      paddingRight: !colReverse && 8,
    },
    

    [`@media (max-width: 360px ) and (max-height: 600px)`] : {
      maxHeight: "100%",
      height: "auto",
    }
}));

export const PencilCircle = styled.div`
  position: relative;
  width: 45px;
  height: 45px;
  border-radius: 50%;
  border: solid 1px #ffffff;
  display: flex;
  justify-content: center;
  align-items: center;

  &:before {
    content: "";
    width: 55px;
    height: 55px;
    border-radius: 50%;
    border: solid 1px #ffffff;
    position: absolute;
    opacity: 0.25;

    top: 50%;
    left: 50%;
    transform: translate(-50%, -50%);
  }
  &:after {
    content: "";
    width: 64px;
    height: 64px;
    border-radius: 50%;
    border: solid 1px #ffffff;
    opacity: 0.1;
    position: absolute;
    top: 50%;
    left: 50%;
    transform: translate(-50%, -50%);
  }

  @media (max-width: 768px) and (max-height:700px) {
    width: 36px;
    height: 36px;

    &:before {
      width: 42px;
      height: 42px;
    }

    &:after {
      width: 48px;
      height: 48px;
    }
  }
`;

export const UniquePhrase = styled.div(() => ({
  maxHeight: "36%",
  // height: "100%",
  width: "47%",
  display: "flex",
  justifyContent: "center",
  alignItems: "flex-end",
  fontFamily: "Jonathan",
  fontSize: 50,
  lineHeight: "54px",
  color: "#FFFFFF",
  textAlign: "center",
  marginRight: "18%",
  marginTop: "1%",
  [`@media (max-width: ${screenSize.laptopL})`]: {
    maxHeight: "100%",
    marginRight: "8%",
  },
  [`@media (max-width: ${screenSize.laptop})`]: {
    width: "64%",
    marginRight: "0%",
    marginLeft: "1%",
    fontSize: 42,
    lineHeight: 1,
  },

  [`@media (min-width: 968px) and (max-height: 660px)`]: {
    height: "auto",
    fontSize: 42,
    lineHeight: "48px"
  },

  [`@media (min-width: 968px) and (max-height: 600px)`]: {
    maxHeight: '100%',
    marginTop: 15,
  },

  [`@media (max-width: 967px) and (max-height: 600px)`]: {
    maxHeight: '100%',
    height: 'auto',
    marginTop: 10,
  },

  [`@media (max-width: ${screenSize.laptopPortrait}) and (orientation: portrait)`]:
    {
      width: "30%",
      order: 5,
      alignItems: "flex-end",
      justifyContent: "flex-end",
      marginLeft: "1%",
      paddingLeft: "5%",
    },
  [`@media (max-width: ${screenSize.mobileL}) and (orientation: portrait)`]: {
    fontSize: 36,
    lineHeight: "40px",
    marginLeft: "0%",
    paddingLeft: "1%",
    marginTop: 5,
    width: "45%",
    height: "auto"
  },
  [`@media (max-width: ${screenSize.mobileLLandscape}) and (orientation: landscape)`]:
    {
      order: 5,
      fontSize: 36,
      lineHeight: "40px",
      paddingLeft: "18%",
      paddingRight: "6%",
      maxHeight: "52%",
    },
    [`@media (max-width: ${screenSize.mobileLLandscape}) and (max-height: 300px)`]: {
      fontSize: 24,
      lineHeight: "24px",
    },
    [`@media (max-width:480px) and (max-height: 600px) and (orientation: portrait)`]: {
      marginTop: 25,
      },
    [`@media (max-width:359px)`]: {
      marginTop: 10,
      width: "55%",
      },
}));

const moveImgStart = keyframes`
  0% {
    transform: translateY(100%);
  }

  100% {
    transform: rotate(0);
  }
`;

const moveImgEnd = keyframes`
  0% {
    transform: translateY(0);
  }

  100% {
    transform: rotate(-100%);
  }
`;

const animatedContactStart = (props) => {
  return css`${moveImgStart} 1s forwards`
}

const animatedContactEnd = (props) => {
  return css`${moveImgEnd} 1s forwards`
}

const calcIsActiveDescriptionFullContactScreeDescription=(isActiveDescription,mob)=>{
  if(mob){
    return  {display:'flex'}
  }
  else if(isActiveDescription){
    return {display:'flex',bottom:0,left:0,zIndex:20, opacity: 1,
  
        // animation: `${animatedContactStart}`,
    // animation: css`${moveImgStart} 1s forwards`,
  }
  }else {
    return { transition: 'opacity .5s', opacity: 0, maxHeight: 0, padding: '0 !important', overflow: 'hidden'
  
        // ...animatedContactEnd,
    
    // animation: `${moveImgEnd} 1s forwards`,
    // display: 'none',
  }
  }
}







export const StyledFullContactScreeDescription = styled(AfterDescription)(
  ({ background, color, isEditeMode,isActiveDescription,mob=true }) => ({
    paddingTop: isEditeMode && "8%",
    maxHeight: "calc( 100% - 41.38% )",
    width: "100%",
    height: "100%",
    background,
    position: "relative",
    display:"flex",
    justifyContent: isEditeMode ? "start" : "space-around",
    alignItems: "flex-end",
    flexDirection: "column",
    color,
    justifyContent:'center',
    ":after": {
      background,
    },
...calcIsActiveDescriptionFullContactScreeDescription(isActiveDescription,mob),
    [`@media (max-width: ${screenSize.laptopL})`]: {
      paddingTop: 46,
      paddingLeft:25,
    },
    [`@media (min-width: 968px) and (max-height: 600px)`]: {
      paddingTop: 30,
    },
  
    [`@media (max-width: 967px) and (max-height: 600px)`]: {
      paddingTop: 30,
    },
    [`@media (max-width: ${screenSize.mobileL}) and (orientation: portrait)`]: {
      justifyContent:'center',
      paddingTop:isEditeMode? 73:18,
      alignItems: "center",
      maxHeight: isEditeMode ? "100%" : "calc( 100% - 25.2% - 34% )",
    },
    [`@media (max-width: ${screenSize.mobileLLandscape}) and (orientation: landscape)`]:
      {
        justifyContent:'center',
        ":after": {
          display:isEditeMode&& 'none',
        },
padding:isEditeMode&&'20px 0px 10px 0px' ,
        maxHeight:isEditeMode? "100%":"calc( 100% - 41.52% )",
        ...(()=>!isEditeMode&&{paddingTop: 0,paddingLeft:0})()
      },
      [`@media (max-width: ${screenSize.mobileLLandscape}) and (max-height: 300px)`]: {
        padding:isEditeMode&&'4px 0px 4px 0px' ,
      },
      [`@media (max-width: 360px ) and (max-width: 600px)`] : {
        maxHeight:isEditeMode? "100%":'44.52%',
        paddingTop: 7,
      }
  })
);

const calcIsActiveBtn=(isActiveDescription,mob)=>{
  if(mob){
    return  {display:'flex'}
  }
  else if(isActiveDescription){
    return {opacity: 1, transition: 'opacity .5s 2s',}
  
  } else {
    return {opacity: 0, transition: 'opacity .5s',}
  }

}


export const WrapperPencil = styled.div(({isActiveDescription,mob=true}) => ({
  cursor: "pointer",
  width: "30%",
  display: "flex",
  justifyContent: "center",
  paddingTop: "2%",
  paddingRight: "7%",
  ...calcIsActiveBtn(isActiveDescription,mob),

  [`@media (max-width: ${screenSize.laptopL})`]: {
    width: "25%",
    justifyContent: "flex-end",

    paddingTop: "2.4%",
    paddingRight: "3%",
  },
  [`@media (max-width: ${screenSize.laptop})`]: {
    position: "absolute",
    top: -30,
    right: -57,
    zIndex: 10,
  },
  [`@media (max-width: ${screenSize.laptopPortrait}) and (orientation: portrait)`]:
    {
      order: 1,
      width: "80%",
      position: "unset",
      paddingTop: 0,
      justifyContent: "flex-end",
      alignItems: "flex-end",
      paddingRight: "8%",
      height: "30.72%",
    },
  [`@media (max-width: ${screenSize.mobileL}) and (orientation: portrait)`]: {
    width: "37%",
    order: 4,
    height: "unset",
    paddingRight: "unset",
    justifyContent: "start",
    alignItems: "start",
    paddingTop: "20px",
    right: -57,
  },
  [`@media (max-width: ${screenSize.mobileLLandscape}) and (orientation: landscape)`]:
    {
      position: "unset",
      order: 4,
      alignItems: "center",
      paddingBottom: 0,
      paddingTop: "3.4%",
      paddingRight: "2%",
    },
    [`@media (max-width: 768px) and (max-height:600px)`]:{
      justifyContent: "center",
    },
    
    [`@media (max-width: 483px) and (max-height:600px)`]:{
      justifyContent: 'center'
    },
    [`@media (max-width: 536px) and (orientation: portrait)`]:{
      paddingTop:'20px',
    },
    [`@media (max-width: 407px) and (orientation: portrait)`]:{
      paddingTop:'25px',
      justifyContent: "center",
    },
    [`@media (max-width: 360px) and (orientation: portrait)`]:{
      paddingTop:'20px',
    },

    [`@media (max-width: 360px) and (max-height:600px)`]:{
      paddingTop:'12px',
    }
}));

export const ContactDescriptionText = styled.div(() => ({}));

export const TextLink = styled.a(() => ({
  paddingBottom: 2,
  fontSize: 14,
  lineHeight: "16px",
  borderBottom: "solid 1px",
  width: "max-content",
  letterSpacing: "0.15em",
  position: "relative",
  zIndex: 2,
  marginRight: 10,
  ":last-child": {
    marginRight: 0
  },

  [`@media (max-width: ${screenSize.laptopL})`]: {
    fontSize: 10,
    lineHeight: "16px",
    paddingBottom: 1,
    marginLeft: 1,
    marginBottom: 10,
  },
}));

export const PrivacyPolicy = styled.div(() => ({
  display: "inline-block",
  color: "#494E54",
}));

//EditeContactDescription

export const StyledEditeContactDescription = styled.div`
  width: 100%;
  display: flex;
  height: 100%;
  flex-direction: column;
  justify-content: space-between;
  @media (max-width: ${screenSize.mobileL}) and (orientation: portrait) {
    align-items: center;
  };
  @media (max-width: ${screenSize.laptop}) {
         position: absolute;
        z-index:10;
        transform: translate(-50%,-50%);
        top:50%;
        left:59%;
        max-height: 382px;
  };
  @media (max-width: ${screenSize.mobileL}) {
         position: unset;
         transform: unset;
         max-height: 94%;
  };
  @media (max-width: ${screenSize.mobileLLandscape}) and (orientation: landscape)
    {
      max-height: 100%;
    };

`;

export const ContactClose = styled.div`
  max-width: 80px;
  max-height: 80px;
  width: 100%;
  height: 100%;
  position: absolute;
  top: 0px;
  left: 0px;
  background-color: #494e54;
  display: flex;
  flex-direction: column;
  font-size: 8px;
  line-height: 28px;
  justify-content: center;
  align-items: center;
  color: #ffffff;
  letter-spacing: 0.25em;
  text-transform: uppercase;
  z-index: 11;
  @media (max-width: ${screenSize.laptopL}) {
    top: 0px;
    left: 0px;
  }
  @media (max-width: ${screenSize.laptop}) {
    /* top: -px;
    left: -32px; */
  }

  @media (min-width: 968px) and (max-height: 600px) {
    max-width: 50px;
    max-height: 42px;
  }

  @media (max-width: 967px) and (max-height: 600px) {
    max-width: 50px;
    max-height: 42px;
  }


  @media (max-width: ${screenSize.mobileL}) and (orientation: portrait) {
    width: 50px;
    height: 50px;
    top: 0px;
    left: 0px;
  };

  @media (max-width: ${screenSize.mobileLLandscape}) and (orientation: landscape)
    {
      top: 0px;
    left: 0px;
      width: 50px;
    height: 50px;
    };
`;

export const IconBottomArow = styled.div`
  font-size: 10px;
  margin-bottom: 4px;
`;

export const TopText = styled.div`
  color: #ffffff;
  width: 100%;
  height: 100%;
  max-height: 36px;
  font-size: 14px;
  line-height: 18px;
  max-width: 344px;
  margin-left: 23px;
  z-index: 12;
  @media (max-width: 1376px) {
    margin-left: 75px;
  }

  @media (max-width: ${screenSize.laptop}) {
    margin-left: 60px;
    max-width: 81%;
  }

  @media (min-width: 968px) and (max-height: 600px) {
    max-height: 100%;
    font-size:12px  
  }
  @media (max-width: 967px) and (max-height: 600px) {
    max-height: 100%;
    font-size:11px  
  }
  @media (max-width: ${screenSize.mobileL}) and (orientation: portrait) {
    margin-top: 0px;
    max-width: 92%;
  margin-left: 0px;
  max-height: 52px;
  }
  @media (max-width: ${screenSize.mobileLLandscape}) and (orientation: landscape)
    {
      max-width: 62%;
    };
    @media (max-width: ${screenSize.mobileLLandscape}) and (max-height: 550px){

    }
    @media (max-width: ${screenSize.mobileLLandscape}) and (max-height: 300px)
    {
      max-width: 100%;
      margin-left: 0;
      padding-left: 60px;
      font-size: 10px;
      line-height: 12px;
    };
    @media (max-width: 360px) and (orientation: portrait) {
      max-width: calc(100% - 60px);
      font-size: 10px;
      line-height: 12px;
    }
`;

export const WrapperImgAndText = styled.div`
  margin-top: 17%;
  display: flex;
  width: 100%;
  max-width: 465px;
  height: 100%;
  max-height: 200px;
  justify-content: space-between;
  font-size: 12px;
  line-height: 18px;
  @media (max-width: ${screenSize.laptopL}) {
    margin-top: 5%;
  }
  @media (max-width: ${screenSize.laptop}) {
    margin-top: 7%;
  }
  @media (min-width: 968px) and (max-height: 600px) {
    margin-top: 10px;
    margin-bottom: 5px;
  }
  @media (max-width: 967px) and (max-height: 600px) {
    margin-top: 10px;
    margin-bottom: 5px;
  }
  @media (max-width: ${screenSize.mobileL}) and (orientation: portrait) {
    justify-content: center;
    max-width: 284px;
    margin-top: 3%;
    margin-bottom:10px;
  }
  @media (max-width: ${screenSize.mobileLLandscape}) and (orientation: landscape)
    {
justify-content: center;
height: max-content;
    };
    @media (max-width: ${screenSize.mobileLLandscape}) and (max-height: 300px){
      margin-top: 5px;
    margin-bottom:5px;
    }
    @media (max-width: 359px) and (orientation: portrait) {
      max-height: 100%;
      height: auto;
    }
`;

export const WrapperImg = styled.div`
  width: 100%;
  height: 100%;
  max-width: 235px;
  @media (max-width: ${screenSize.mobileL}) and (orientation: portrait) {
    max-width: 100% !important;
    max-width: 250px !important;
  };
  @media (max-width: 992px) and (max-height: 550px) and (orientation: landscape) {
    max-width: 190px;
    margin-left: 22px;
  }
  @media (max-width: 992px) and (max-height: 500px) and (orientation: landscape) {
    max-width: 150px;
    margin-left: 40px;
  }
  @media (max-width: ${screenSize.mobileLLandscape}) and (orientation: landscape)
    {
max-width:222px;
margin-left: 0;
    };
    // @media (max-width: ${screenSize.mobileLLandscape}) and (max-height: 550px) and (orientation: landscape) {
    //   max-width: 190px;
    //   margin-left: 0;
    // }
    // @media (max-width: ${screenSize.mobileLLandscape}) and (max-height: 500px) and (orientation: landscape) {
    //   max-width: 150px;
    //   margin-left: 0;
    // }

    @media (max-width: 700px) and (orientation: landscape){
      max-width: 222px;
      margin-left: 0;
    }
    
    @media (max-width: ${screenSize.mobileLLandscape}) and (max-height: 300px) {
      max-width:40%;
    }
    
    @media (max-width: 359px) and (orientation: portrait) {
      max-width: 200px !important;
    }

`;

export const WrapperText = styled.div`
  width: 100%;
  height: 100%;
  max-width: 157px;
  display: flex;
  flex-direction: column;
  justify-content: space-between;
  padding-left:15px;
  @media (max-width: ${screenSize.laptopL}) {
    max-width: 134px;
  }
  @media (max-width: ${screenSize.laptop}) {
    max-width: 134px;
  }

  @media (max-width: 992px) and (max-height: 550px) and (orientation: landscape) {
    max-width: 150px;
  }

  @media (max-width: 992px) and (max-height: 550px) and (orientation: landscape) {
    max-width: 170px;
  }

  @media (max-width: ${screenSize.mobileL}) and (orientation: portrait) {
    display: none !important;
  }
  @media (max-width: ${screenSize.mobileLLandscape}) and (orientation: landscape)
    {
display: none;
    };

`;

export const WrapperButton = styled.div`
  margin-left: 22px;
  @media (max-width: ${screenSize.laptop}) {
    margin-bottom: 9px;
  }
  @media (max-width: ${screenSize.mobileL}) and (orientation: portrait) {
    margin-left: unset !important;
  }
  @media (max-width: ${screenSize.mobileLLandscape}) and (orientation: landscape)
    {
    margin-bottom: 0px;
    margin-left: 0px;

      display: flex;
justify-content: center;
    };
    @media (max-width: 359px) and (orientation: portrait) {

    }
`;

export const SendButton = styled.button`
  width: 188px;
  padding: 15px;
  color: #ffffff;
  background: #494e54;
  border-radius: 10px;
  letter-spacing: 0.5em;
  text-transform: uppercase;
  font-size: 12px;
  line-height: 12px;
  @media (max-width: ${screenSize.mobileL}) and (orientation: portrait) {
    font-size: 10px;
  }
  @media (max-width: 992px) and (max-height: 550px) and (orientation: landscape) {
    padding: 10px;
  }
  @media (max-width: ${screenSize.mobileLLandscape}) and (orientation: landscape)
    {
      padding: 13px;

    };
    @media (max-width: ${screenSize.mobileLLandscape}) and (max-height: 350px) {
      padding: 10px;
      font-size: 11px;
      line-height: 12px;
    } 
    @media (max-width: ${screenSize.mobileLLandscape}) and (max-height: 300px) 
    {
      padding: 8px;
      font-size: 10px;
      line-height: 10px;
    };
    @media (max-width: 359px) and (orientation: portrait) {
      padding: 10px;
    }
`;

export const ContactDescriptionFooterText = styled.div`
  display: block;
  font-size: 10px;
  line-height: 12px;
  width: 100%;
  max-width: 402px;
  color: #ffffff;
  @media (max-width: ${screenSize.laptop}) {
    max-width: 78%;
    text-align: left;
  }
  @media (min-width: 968px) and (max-height: 600px) {
    max-width: 100%;
  }
  @media (max-width: 967px) and (max-height: 600px) {
    max-width: 100%;
  }
  @media (max-width: ${screenSize.mobileL}) and (orientation: portrait) {
    max-width: 78%;
    text-align: center;
  }
  @media (max-width: ${screenSize.mobileLLandscape}) and (orientation: landscape)
    {
    max-width: 100%;

      text-align: center;
      /* display: flex;
justify-content: center; */
    };
    @media (max-width: ${screenSize.mobileLLandscape}) and (max-height: 300px) {
      font-size: 9px;
      line-height: 9px;
      margin-top: 4px;
    }
`;

//EditeContactDescription

export const LearnMoreLink = styled.a`
font-size: 10px;
line-height: 16px;
letter-spacing: 0.15em;
margin-top: 10px;
border-bottom: solid 1px white;
width: max-content;
`

