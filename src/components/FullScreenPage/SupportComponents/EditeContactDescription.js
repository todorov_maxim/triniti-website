import {
  ContactTitle,
  ContactText,
  ContactClose,
  ContactDescriptionText,
  IconBottomArow,
  StyledEditeContactDescription,
  TopText,
  WrapperImgAndText,
  WrapperImg,
  WrapperText,
  WrapperButton,
  SendButton,
  ContactDescriptionFooterText,
  PrivacyPolicy,
} from "../StyledFullScreenPage";
import Image from "next/image";
import contactsEditeImg from "../../../public/images/PagesFS/contactEdite.png";

export const EditeContactDescription = ({ onChangeEdite }) => {
  return (
    <>
      <ContactClose onClick={onChangeEdite}>
        <ContactDescriptionText>close</ContactDescriptionText>
        <IconBottomArow className="icon-bottomArow" />
      </ContactClose>

      <StyledEditeContactDescription>
        <TopText>
          Якщо у вас виникли питання - зв’яжiться з нами або заповнiть форму
          зворотнього зв’язку
        </TopText>

        <WrapperImgAndText>
          <WrapperImg>
            <Image layout="responsive" src={contactsEditeImg} objectFit="contain" width={"100%"} height={75}/>
          </WrapperImg>
          <WrapperText>
            <ContactTitle>Контактний телефон</ContactTitle>
            <ContactText>+380 44 123 45 67</ContactText>

            <ContactTitle>Графік роботи</ContactTitle>
            <ContactText>пн-пт. з 9.00 до 20.00</ContactText>

            <ContactTitle>Головний офіс</ContactTitle>
            <ContactText>Голосіївський пр-т, 60, Київ, 03039</ContactText>

            <ContactTitle>E-mail</ContactTitle>
            <ContactText>info@triiinity.com.ua</ContactText>
          </WrapperText>
        </WrapperImgAndText>

        <WrapperButton>
          <SendButton>надіслати</SendButton>
        </WrapperButton>
        <ContactDescriptionFooterText>
          Натискаючи кнопку “Надіслати” ви даєте згоду на обробку персональних
          даних згідно з нашою{" "}
          <PrivacyPolicy>
            <u>політикою конфіденційності</u>
          </PrivacyPolicy>
        </ContactDescriptionFooterText>
      </StyledEditeContactDescription>
    </>
  );
};
