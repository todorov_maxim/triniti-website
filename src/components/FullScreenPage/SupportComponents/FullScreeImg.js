import { useRef } from "react";
import {
  FullScreenImageFontImage,
  StyledFullScreeImg,
  StyledMobFullScreeImg,
  FullScreenImageFontBox,
  FullScreenImageFontText
} from "../StyledFullScreenPage";
import Image from "next/image";

export const FullScreeImg = ({
  imgSrc,
  signature,
  mob,
  themeColor,
  maxHeight,
  isChangeWidth,
  isActiveImg,
  mobVersionDesctopPhoto,
}) => {


  const textRef = useRef(null);
  textRef.current && (textRef.current.innerHTML = signature?.caption);


  return (
    <>
      {mob && (
        <>
        <StyledMobFullScreeImg
          maxHeight={maxHeight}
          isChangeWidth={isChangeWidth}
        >
          {imgSrc && <Image
            layout="responsive"
            src={imgSrc}
            className="image-cover"
            width={"100%"}
            height={"100%"}
            priority={true}
          />}
          {signature?.caption && 
           <FullScreenImageFontBox signaturePosition={signature?.position}  bgColor={themeColor?.background} fontColor={signature?.fontColor}>
             <FullScreenImageFontText ref={textRef}/>
           </FullScreenImageFontBox>
          }
         
          {/* {
          signature?.img?.sourceUrl && 
          <FullScreenImageFontImage signaturePosition={signature?.position}>
            <Image 
            layout="responsive"
            src={signature?.img?.sourceUrl}
            className="image-cover"
            width={300}
            height={300}/>
          </FullScreenImageFontImage>
          } */}
        </StyledMobFullScreeImg >
        </>
      )}
      {!mob && (
        <>
        <StyledFullScreeImg
          isChangeWidth={isChangeWidth}
          isActiveImg={isActiveImg}
          mobVersionDesctopPhoto={mobVersionDesctopPhoto}
        >
          {imgSrc && <Image
            layout="responsive"
            src={imgSrc}
            className="image-cover"
            width={"100%"}
            height={"100%"}
            priority={true}
          />}
          {signature?.caption && 
           <FullScreenImageFontBox signaturePosition={signature?.position} bgColor={themeColor?.background} fontColor={signature?.fontColor}>
            <FullScreenImageFontText ref={textRef}/>
           </FullScreenImageFontBox>
          }
          {/* {
          signature?.img?.sourceUrl && 
          <FullScreenImageFontImage signaturePosition={signature?.position}>
            <Image 
            layout="responsive"
            src={signature?.img?.sourceUrl}
            className="image-cover"
            width={300}
            height={300}/>
          </FullScreenImageFontImage>
          } */}
        </StyledFullScreeImg>
        
        
        </>
      )}
    </>
  );
};
