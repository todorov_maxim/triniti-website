import {
  CurentPage,
  AllPages,
  Email,
  Adress,
  Schedule,
  ContactNumber,
  ContactTitle,
  ContactText,
  PencilIcon,
  ContactContent,
  WrapperContactNumberPages,
  WrapperContact,
  PencilCircle,
  StyledFullContactScreeDescription,
  WrapperPencil,
  LearnMoreLink,
} from "../StyledFullScreenPage";
import { StyledFormContent, StyledInput, StyledFormButton, StyledModalOverlay } from './../../Contact/Form/StyledForm';
import Script from 'next/script';
import React, { useEffect, useMemo, useState } from 'react';
import { SwitchPageArrow } from "../../SwitchPageArrow/SwitchPageArrow";
import { EditeContactDescription } from "./EditeContactDescription";
import useIsEmpty from "../../../hooks/useIsEmpty";
import withHardcoreTranslate from "../../../hoc/withHardcoreTranslate";
import { compose } from "redux";

const FullScreeContactDescription = ({
  numberPage,
  background,
  allPages,
  textColor,
  isEditeMode,
  onChangeEdite,
  isActiveDescription,
  mob,
  pageCount,
  pageData,
  showForm,
  onceLoaded,
  currLanguage,
}) => {
  const isCurrentNumberMoreThan10 = pageCount >= 10;
  const currNum = isCurrentNumberMoreThan10 ? `${pageCount}` : `0${pageCount}`;
  const b24SScript = useMemo(() => {
    return (<Script data-b24-form="inline/92/favjx5" strategy="lazyOnload" async="true">{`(function(w,d,u){
        var s=d.createElement('script');s.async=true;s.src=u+'?'+(Date.now()/180000|0);
        var h=d.getElementsByTagName('script')[0];
        var formBox = d.getElementById('bx24-form-content');
        formBox.parentNode.insertBefore(s,formBox);
      })(window,document,'https://cdn.bitrix24.ua/b18512913/crm/form/loader_92.js');` }</Script>)
  }, [onceLoaded])
  return (
    <StyledFullContactScreeDescription
      color={textColor}
      background={background}
      isEditeMode={isEditeMode}
      isActiveDescription={isActiveDescription}
      mob={mob}
    >
      {!isEditeMode && (
        <ContactContent isActiveDescription={isActiveDescription} mob={mob} id={'bx24-form-content'}>
          <>
            <WrapperContactNumberPages isContactPage>
              <CurentPage isContactPage>{`${currNum}`}</CurentPage>
              <AllPages>{`/ ${allPages}`}</AllPages>
            </WrapperContactNumberPages>

            <WrapperContact>
              {!useIsEmpty(pageData?.tel) && (
                <ContactNumber>
                  <ContactTitle portraitLineHeight>
                    {currLanguage?.contactNumber}
                  </ContactTitle>
                  <ContactText>{pageData?.tel}</ContactText>
                </ContactNumber>
              )}

              {!useIsEmpty(pageData?.schedule) && (
                <Schedule>
                  <ContactTitle>{currLanguage?.workSchedule}</ContactTitle>
                  <ContactText>{pageData?.schedule}</ContactText>
                  <LearnMoreLink href={pageData?.linkUrl}>
                    Дізнатись детально
                  </LearnMoreLink>
                </Schedule>
              )}
            </WrapperContact>

            <WrapperContact colReverse>
              {!useIsEmpty(pageData?.address) && (
                <Adress>
                  <ContactTitle portraitLineHeight>
                    {currLanguage?.mainOffice}
                  </ContactTitle>
                  <ContactText portraitWidth>{pageData?.address}</ContactText>
                </Adress>
              )}
              {!useIsEmpty(pageData?.email) && (
                <Email>
                  <ContactTitle>{currLanguage?.email}</ContactTitle>
                  <ContactText>{pageData?.email}</ContactText>
                </Email>
              )}
            </WrapperContact>
            
            <WrapperPencil
              onClick={showForm}
              isActiveDescription={isActiveDescription}
              mob={mob}
            >
              <PencilCircle>
                <PencilIcon className="icon-telephone"></PencilIcon>
              </PencilCircle>
            </WrapperPencil>
          </>
        </ContactContent>
      )}

      {/* {isEditeMode && (
        <ContactContent isEditeMode isActiveDescription={isActiveDescription}>
          <EditeContactDescription onChangeEdite={onChangeEdite} />
        </ContactContent>
      )} */}

      {!isEditeMode && <SwitchPageArrow lastpage pages />}
      {/* {onceLoaded && b24SScript} */}
    </StyledFullContactScreeDescription>
  );
};

export default compose(withHardcoreTranslate)(FullScreeContactDescription);
