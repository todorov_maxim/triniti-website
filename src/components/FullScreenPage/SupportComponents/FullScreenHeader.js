import { Contact } from "../../Contact/Contact";
import { LogoWithText } from "../../LogoWithText/LogoWithText";
import { StyledFullScreenHeader, WrapeperLogo } from "../StyledFullScreenPage";

export const FullScreenHeader = ({ isContact, hiddenHeader }) => {
  return (
    <StyledFullScreenHeader isContact={isContact} hiddenHeader={hiddenHeader}>
      <Contact color="#494E54" renderTelephonNumberBlock />
      <WrapeperLogo>
        <LogoWithText namePage="MainPage" />
      </WrapeperLogo>
    </StyledFullScreenHeader>
  );
};
