import {useRef} from "react"
import {
  StyledFullScreeDescription,
  Content,
  CurentPage,
  Title,
  TextDescription,
  LinkFullPage,
  WrapperNumberPages,
  AllPages,
  IconComma,
  DescriptionMobileL,
  WrapperContactNumberPages,
  TextLink,
  WrapperTitleAndDescription,
  StatusPage,
  TextDescriptionBig,
} from "../StyledFullScreenPage"
import {SwitchPageArrow} from "../../SwitchPageArrow/SwitchPageArrow"
const descriptionTheme = {
  brown: {background: "#CD8B7D", textColor: "#FFFFFF"},
  gray: {background: "#9C8D88", textColor: "#FFFFFF"},
  orange: {background: "#F0B8A9", textColor: "#494E54"},
  pink: {background: "#EAD6D5", textColor: "#494E54"},
  biege: {background: "#D9D3C5", textColor: "#494E54"},
}

export const FullScreeDescription = ({numberPage, title, description, linkUrl, linkText, link1, link2, link3, allPages, fullpageApi, isActiveDescription, mob, sectionTheme = "brown", pageCount}) => {
  const {textColor, background} = descriptionTheme[sectionTheme]
  const isCurrentNumberMoreThan10 = pageCount >= 11
  const currNum = isCurrentNumberMoreThan10 ? `${pageCount}` : `0${pageCount}`
  const textRef = useRef(null)
  textRef.current && (textRef.current.innerHTML = title)

  return (
    <StyledFullScreeDescription color={textColor} background={background} isActiveDescription={isActiveDescription} mob={mob}>
      <Content isActiveDescription={isActiveDescription} mob={mob}>
        <IconComma className="icon-comma" />
        <StatusPage notMobileLandscape>
          <WrapperContactNumberPages>
            <CurentPage>{`${currNum}`}</CurentPage>
            <AllPages>{`/ ${allPages}`}</AllPages>
          </WrapperContactNumberPages>
        </StatusPage>

        {/*   DescriptionMobileL при ширине screenSize.mobileL*/}
        <DescriptionMobileL>
          {!!title && <Title ref={textRef} mob />}
          <WrapperNumberPages mob>
            <CurentPage>{`${currNum}`}</CurentPage>
            <AllPages>{`/ ${allPages}`}</AllPages>
          </WrapperNumberPages>
        </DescriptionMobileL>
        {/*   DescriptionMobileL при ширине screenSize.mobileL*/}

        <WrapperTitleAndDescription>
          {!!title && <Title ref={textRef} />}

          {!title ? <TextDescriptionBig>{description}</TextDescriptionBig> : <TextDescription>{description}</TextDescription>}
        </WrapperTitleAndDescription>

        <LinkFullPage>
          {linkText && <TextLink href={linkUrl}>{linkText}</TextLink>}
          {link1 && (
            <TextLink target={link1.target || "_self"} href={link1.url}>
              {link1.title}
            </TextLink>
          )}
          {link2 && (
            <TextLink target={link2.target || "_self"} href={link2.url}>
              {link2.title}
            </TextLink>
          )}
          {link3 && (
            <TextLink target={link3.target || "_self"} href={link3.url}>
              {link3.title}
            </TextLink>
          )}
        </LinkFullPage>
      </Content>
      <SwitchPageArrow pages fullpageApi={fullpageApi} />
    </StyledFullScreeDescription>
  )
}
