import {
  StyledFullScreenPage,
  ContentFullScreenPage,
} from "./StyledFullScreenPage";
import { compose } from "redux";
import { useState } from "react";
import { pageTheme } from "../../constants/themes";
import { FullScreenHeader } from "./SupportComponents/FullScreenHeader";
import { FullScreeImg } from "./SupportComponents/FullScreeImg";
import FullScreeContactDescription from "./SupportComponents/FullScreeContactDescription";
import { FullScreeDescription } from "./SupportComponents/FullScreeDescription";

const FullScreenPage = ({
  page,
  arr,
  fullpageApi,
  isContact,
  mobVersionDesctopPhoto,
  pageCount,
  pageData,
  showForm,
  onceLoaded,
}) => {
  const [isEditeMode, setIsEditeMode] = useState(false);
  const onChangeEdite = () => setIsEditeMode((pre) => !pre);

  return (
    <StyledFullScreenPage>
      <ContentFullScreenPage isChangeWidth={isEditeMode && isContact}>
        {/* hiddenHeader если моб версия в албомном режиме на странице конатктов то скрываем header */}
        <FullScreenHeader
          isContact={isContact}
          hiddenHeader={isEditeMode && isContact}
        />

        {/* меняеться высота блока с картинкой если это страница контакта */}

        {!page?.isContact && (
          <FullScreeImg mob={true} signature={page?.signature} imgSrc={page?.img?.sourceUrl} themeColor={pageTheme[pageData?.sectionTheme]}/>
        )}
        {page?.isContact && !isEditeMode && (
          <FullScreeImg
            maxHeight="34%"
            mob={true}
            imgSrc={page?.img?.sourceUrl}
          />
        )}

        {!page?.isContact && (
          <FullScreeDescription
            fullpageApi={fullpageApi}
            {...pageData}
            allPages={arr?.length}
            pageCount={pageCount}
          />
        )}

        {/* меняеться вложеность  блока Description если это страница контакта */}

        {page?.isContact && (
          <FullScreeContactDescription
            fullpageApi={fullpageApi}
            {...pageData}
            pageData={pageData}
            allPages={arr?.length}
            onChangeEdite={onChangeEdite}
            isEditeMode={isEditeMode}
            pageCount={pageCount}
            showForm={showForm}
            onceLoaded={onceLoaded}
          />
        )}
      </ContentFullScreenPage>
      <FullScreeImg
        mob={false}
        themeColor={pageTheme[pageData?.sectionTheme]}
        signature={page?.signature}
        imgSrc={page?.img?.sourceUrl}
        isChangeWidth={isEditeMode && isContact}
        mobVersionDesctopPhoto={mobVersionDesctopPhoto}
      />
    </StyledFullScreenPage>
  );
};
export default compose()(FullScreenPage);
