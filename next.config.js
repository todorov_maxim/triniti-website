const path = require("path");

module.exports = {
  // async redirects() {
  //   return [
  //     {
  //       source: '/article/:path*',
  //       destination: '/articles/:path*',
  //       permanent: true,
  //     },
  //   ];
  // },
  webpack: (config) => {
    config.resolve.alias.components = path.resolve(__dirname, "src/components");
    config.resolve.alias.data = path.resolve(__dirname, "src/public/images");
    config.resolve.alias.reducers = path.resolve(__dirname, "src/reducers");
    return config;
  },
  "presets": ["next/babel"],
  eslint: {
    ignoreDuringBuilds: true,
  },
  // i18n: {
  //   locales: ["ua", "en", "ru"],
  //   defaultLocale: "ua",
  //   localeDetection: false,
  //   defaultFallback: false,
  // },

  
  images: {
    minimumCacheTTL: 31536000,
    domains: [
      "wp.re-triiinity.com.ua",
      "triiinity.com.ua",
      "localhost:3000",
    ],
    // formats: ["image/avif", "image/webp", "next/image"],
  },
};
