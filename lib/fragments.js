export const menuFragment = `
menus(where: {slug: $menuName}) {
  nodes {
      menuItems(first: 20){
        nodes {
          path
          label
        }
      }
    }
}
`

export const globalContactsInfoFragment = `
posts(where: {categoryName: "glob-contacts"}) {
  nodes {
    globalContactsInfo {
      phone
      instagram {
        title
        url
      }
      facebook {
        title
        url
      }
    }
  }
}`

const commonFragmentMoreLinks = `
description
img {
  sourceUrl
  sizes
  srcSet
}
signature {
  caption
  position
  fontColor
}
link1 {
  title
  url
  target
}
link2 {
  title
  url
  target
}
link3 {
  title
  url
  target
}
sectionTheme
title
`

const commonFragment = `
description
img {
  sourceUrl
  sizes
  srcSet
}
signature {
  caption
  position
  fontColor
}
linkText
linkUrl {
  ... on Page {
    slug
  }
}
sectionTheme
title
`

export const mainPageFragment = `
pages(where: {language: $lang, name: "main"}) {
  nodes {
    mainPageData {
        brand {
          mainText
          secondaryText
          steps {
            stepText
          }
        }
        grid {
            mainText
            secondaryText
            logo {
                sourceUrl
                sizes
                srcSet
            }
            images {
                sourceUrl
                sizes
                srcSet
            }
        }
        slide1 {
          ${commonFragment}
        }
        slide2 {
          ${commonFragment}
        }
        slide3 {
          ${commonFragmentMoreLinks}
        }
        slide4 {
          ${commonFragment}
        }
        slide5 {
          ${commonFragment}
        }
        contactsInfo {
            address
            email
            schedule
            tel
            img {
                sourceUrl
                sizes
                srcSet
            }
            linkUrl {
              ... on Page {
                id
                slug
              }
            }
        }
      }
    }
  }`

export const aboutPageFragment = `
pages(where: {language: $lang, name: "about"}) {
  nodes {
      aboutPageData {
        pageTheme
        statistics {
          diagram {
            caption
            value
          }
          list {
            title
            points {
              item
            }
          }
          roadmap {
            caption
            value
            active
          }
        }
        profilesSlider {
          ... on Post {
            id
            profilesSliderData {
              description
              name
              post
              img {
                sourceUrl
                sizes
                srcSet
              }
            }
          }
        }
        fieldGroupName
        intro {
            description
            pageName
            title
            link {
              linkText
              linkUrl
            }
            background {
                sourceUrl
                sizes
                srcSet
            }
            sideImg {
                sourceUrl
                sizes
                srcSet
            }
            stats {
              point {
                caption
                value
              }
            }
          }
        profile {
          description
          name
          title
          note {
            description
            mainText
          }
          img {
            sourceUrl
                sizes
                srcSet
          }
        }
      }
    }
  }
  `

export const realEstateSalesPageFragment = `
pages(where: {language: $lang, name: "real-estate-sales"}) {
  nodes {
    realEstatesSalesPageData {
        pageTheme
        departmentContacts {
          ... on Post {
            departmentContactsData {
              address
              department
              email
              schedule
              tel
              text
              title
              image {
                srcSet
                sourceUrl
              }
              facebook {
                url
              }
              instagram {
                url
              }
            }
          }
        }
        flats {
          button {
            link
            text
          }
          flatsTypes {
            name
            image {
              sourceUrl
            }
          }
        }
        profilesSlider {
          ... on Post {
            id
            profilesSliderData {
              description
              name
              post
              img {
                sourceUrl
                sizes
                srcSet
              }
            }
          }
        }
        fieldGroupName
        intro {
            description
            pageName
            title
            background {
                sourceUrl
                sizes
                srcSet
            }
            link {
              linkText
              linkUrl
            }
            videoLink {
              src
              poster {
                sourceUrl
              }
            }
          }
        profile {
          description
          name
          title
          note {
            description
            mainText
          }
          img {
            sourceUrl
                sizes
                srcSet
          }
        }
      }
    }
  }
  `

export const heritagePageFragment = `
pages(where: {language: $lang, name: "heritage"}) {
  nodes {
        heritagePageData {
          pageTheme
          intro {
            background {
              sourceUrl
              srcSet
            }
            description
            pageName
            title
            link {
              linkText
              linkUrl
            }
          }
          profilesSlider {
            ... on Post {
              profilesSliderData {
                description
                name
                post
                img {
                  sourceUrl
                  srcSet
                }
              }
            }
          }
          notes {
            description
            
            title
          }
          projects {
            
            projectData {
              ... on Post {
                
                projectsData {
                  address
                  description
                
                  link
                  name
                  gallery {
                    sourceUrl
                    srcSet
                  }
                }
              }
            }
          }
        }
      }
    }
      
  `

export const realEstatePageFragment = `
  pages(where: {language: $lang, name: "real-estate"}) {
    nodes {
      projectsPageData {
        departmentContacts {
          ... on Post {
            departmentContactsData {
              address
              department
              email
              schedule
              tel
              text
              title
              image {
                srcSet
                sourceUrl
              }
              facebook {
                url
              }
              instagram {
                url
              }
            }
          }
        }
      
        intro {
          background {
            srcSet
            sourceUrl
          }
          description
          link {
            linkText
            linkUrl
          }
          pageName
          title
          videoLink {
            src
            poster {
              sourceUrl
            }
          }
        }
        pageTheme
        profilesSlider {
          ... on Post {
            profilesSliderData {
              description
              name
              post
              img {
                sourceUrl
                srcSet
              }
            }
          }
        }
      }
    }
      }
    `

export const rentalPageFragment = `
    pages(where: {language: $lang, name: "rental"}) {
      nodes {
        rentalPageData {
          intro {
            background {
              srcSet
              sourceUrl
            }
            description
            link {
              linkText
              linkUrl
            }
            videoLink {
              src
              poster {
                sourceUrl
              }
            }
            pageName
            title
          }
          tabsBlock {
            tabs {
              description
              tabName
              img {
                srcSet
                sourceUrl
              }
              inModal
              icon {
                srcSet
                sourceUrl
              }
            }
            title
            subtitle
            description
          }
          profilesSlider {
            ... on Post {
              id
              profilesSliderData {
                description
                name
                post
                img {
                  sourceUrl
                  sizes
                  srcSet
                }
              }
            }
          }
          departmentContacts {
            ... on Post {
              departmentContactsData {
                address
                department
                email
                schedule
                tel
                text
                title
                image {
                  srcSet
                  sourceUrl
                }
                facebook {
                  url
                }
                instagram {
                  url
                }
              }
            }
          }
          pageTheme
        }
      }
    }
`

export const appPageFragment = `
    pages(where: {language: $lang, name: "application"}) {
      nodes {
        appPageData {
          intro {
            background {
              srcSet
              sourceUrl
            }
            description
            link {
              linkText
              linkUrl
            }
            videoLink {
              src
              poster {
                sourceUrl
              }
            }
            pageName
            title
          }
          pageTheme
          profilesSlider {
            ... on Post {
              profilesSliderData {
                description
                name
                post
                img {
                  sourceUrl
                  srcSet
                }
              }
            }
          }
          
        }
      }
        }
      `
export const propertyManagementPageFragment = `
pages(where: {language: $lang, name: "property-management"}) {
  nodes {
    propertyManagementPageData {
      intro {
        background {
          srcSet
          sourceUrl
        }
        link {
          linkText
          linkUrl
        }
        description
        pageName
        title
      }
      pageTheme
      profilesSlider {
        ... on Post {
          profilesSliderData {
            description
            name
            post
            img {
              srcSet
              sourceUrl
            }
          }
        }
      }
      services {
        description
        name
        image {
          srcSet
          sourceUrl
        }
      }
      contacts {
        ... on Post {
          departmentContactsData {
            address
            department
            email
            schedule
            tel
            text
            title
            facebook {
              url
            }
            instagram {
              url
            }
            image {
              srcSet
              sourceUrl
            }
          }
        }
      }
    }
  }
}

`

export const utilityManagementPageFragment = `
pages(where: {language: $lang, name: "utility-management"}) {
  nodes {
    utilityManagementPageData {
      intro {
        background {
          srcSet
          sourceUrl
        }
        link {
          linkText
          linkUrl
        }
        description
        pageName
        title
      }
      pageTheme
      profilesSlider {
        ... on Post {
          profilesSliderData {
            description
            name
            post
            img {
              srcSet
              sourceUrl
            }
          }
        }
      }
      services {
        description
        name
        image {
          srcSet
          sourceUrl
        }
      }
      contacts {
        ... on Post {
          departmentContactsData {
            address
            department
            email
            schedule
            tel
            text
            title
            facebook {
              url
            }
            instagram {
              url
            }
            image {
              srcSet
              sourceUrl
            }
          }
        }
      }
    }
  }
}

`

export const homeFeelingsYouPageFragment = `
pages(where: {language: $lang, name: "home-feelings-you"}) {
      nodes {
        homeFeelingsYouPageData {
          intro {
            description
            pageName
            title
            background {
              srcSet
              sourceUrl
            }
            link {
              linkText
              linkUrl
            }
          }
          contacts {
            ... on Post {
              departmentContactsData {
                address
                department
                email
                schedule
                tel
                text
                title
                image {
                  srcSet
                  sourceUrl
                }
                facebook {
                  url
                }
                instagram {
                  url
                }
              }
            }
          }
          production {
            ... on Post {

              production {
                description
                link
                linktext
                name
                price
                img {
                  srcSet
                  sourceUrl
                }
              }
            }
          }
          pageTheme
          profilesSlider {
            ... on Post {
              profilesSliderData {
                description
                img {
                  sourceUrl
                  srcSet
                }
                name
                post
              }
            }
          }
        }
      }
  }
`

export const contactsPageDataFragment = `
pages(where: {language: $lang, name: "contacts"}) {
  nodes {
    contactsPageData {
      intro {
        description
        pageName
        title
        background {
          srcSet
          sourceUrl
        }
        link {
          linkText
          linkUrl
        }
      }
      maplink
      pageTheme
      departmentsContacts {
        ... on Post {
          departmentContactsData {
            address
            department
            email
            facebook {
              url
              title
            }
            instagram {
              url
              title
            }
            schedule
            tel
          }
        }
      }
    }
  }
}
`

export const galleryPageDataFragment = `
pages(where: {language: $lang, name: "gallery"}) {
    nodes {
      galleryPageData {
        slider {
          id
          srcSet
          sourceUrl
        }
      }
    }
}
`
