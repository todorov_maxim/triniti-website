import {
  aboutPageFragment,
  appPageFragment,
  contactsPageDataFragment,
  globalContactsInfoFragment,
  heritagePageFragment,
  homeFeelingsYouPageFragment,
  mainPageFragment,
  menuFragment,
  propertyManagementPageFragment,
  realEstatePageFragment,
  rentalPageFragment,
  realEstateSalesPageFragment,
  utilityManagementPageFragment,
  galleryPageDataFragment,
} from "./fragments";

const API_URL = process.env.WORDPRESS_API_URL;
const SUBDOMAIN_PATH = process.env.WORDPRESS_SUBDOMAIN_PATH;

const getMenuData = (data) => {
  return data?.menus?.nodes[0]?.menuItems?.nodes?.map((menuItem) => {
    return {
      path: menuItem.path.replace(SUBDOMAIN_PATH, ""),
      label: menuItem.label,
    };
  });
};

async function fetchAPI(query, { variables } = {}) {
  const headers = {
    "Content-Type": "application/json",
  };

  if (process.env.WORDPRESS_AUTH_REFRESH_TOKEN) {
    headers[
      "Authorization"
    ] = `Bearer ${process.env.WORDPRESS_AUTH_REFRESH_TOKEN}`;
  }

  const res = await fetch(API_URL, {
    method: "POST",
    headers,
    body: JSON.stringify({
      query,
      variables,
    }),
  });

  const json = await res.json();
  if (json.errors) {
    console.error(json.errors);
    throw new Error("Failed to fetch API");
  }
  return json.data;
}

export async function getDefaultPageData(lang, menuName) {
  const data = await fetchAPI(
    `query pageData($lang: LanguageCodeFilterEnum, $menuName: String) {
      ${menuFragment}
      ${globalContactsInfoFragment}
    }
    `,
    {
      variables: {
        lang,
        menuName,
      },
    }
  );

  const menuData = getMenuData(data);
  const globalContactsData = data?.posts?.nodes[0]?.globalContactsInfo;

  const pageData = {
    menu: menuData,
    globalContactsData,
  };

  return pageData;
}

export async function getMainPageData(lang, menuName) {
  const data = await fetchAPI(
    `query pageData($lang: LanguageCodeFilterEnum, $menuName: String) {
      ${menuFragment}
      ${globalContactsInfoFragment}
      ${mainPageFragment}
    }
    `,
    {
      variables: {
        lang,
        menuName,
      },
    }
  );
  const menuData = getMenuData(data);
  const globalContactsData = data?.posts?.nodes[0]?.globalContactsInfo;
  let receivedData = data?.pages?.nodes[0].mainPageData;

  let mainPageData = {
    menu: menuData,
    globalContactsData,
    brand: {},
    grid: {},
    commonSlides: [],
    contactsInfo: {},
  };

  for (const [key, value] of Object.entries(receivedData)) {
    if (key === "brand") {
      mainPageData.brand = {
        ...value,
        // mainText: receivedData?.brand?.mainText?.replace()
        steps: value.steps.map((text) => {
          return text.stepText;
        }),
      };
      continue;
    }
    if (key === "grid") {
      mainPageData.grid = {
        ...value,
      };
      continue;
    }
    if (key === "contactsInfo") {
      mainPageData.contactsInfo = {
        ...value,
        linkUrl:
        typeof value.linkUrl?.slug === "string"
          ? "/" + value.linkUrl?.slug
          : value.linkUrl
          ? value.linkUrl
          : null,
      };
      continue;
    }

    mainPageData.commonSlides.push({
      name: `${key}`,
      ...value,
      linkUrl:
        typeof value.linkUrl?.slug === "string"
          ? "/" + value.linkUrl?.slug
          : value.linkUrl
          ? value.linkUrl
          : null,
    });
  }

  return mainPageData;
}

export async function getAboutPageData(lang, menuName) {
  const data = await fetchAPI(
    `query pageData($lang: LanguageCodeFilterEnum, $menuName: String) {
      ${menuFragment}
      ${globalContactsInfoFragment}
      ${aboutPageFragment}
      }
    `,
    {
      variables: {
        lang,
        menuName,
      },
    }
  );

  const menuData = getMenuData(data);
  const globalContactsData = data?.posts?.nodes[0]?.globalContactsInfo;
  let receivedData = data?.pages?.nodes[0].aboutPageData;

  const aboutData = {
    menu: menuData,
    globalContactsData,
    ...receivedData,
  };

  return aboutData;
}

export async function getAppPageData(lang, menuName) {
  const data = await fetchAPI(
    `query pageData($lang: LanguageCodeFilterEnum, $menuName: String) {
        ${menuFragment}
        ${globalContactsInfoFragment}
        ${appPageFragment}
      }
    `,
    {
      variables: {
        lang,
        menuName,
      },
    }
  );

  const menuData = getMenuData(data);
  const globalContactsData = data?.posts?.nodes[0]?.globalContactsInfo;
  let receivedData = data?.pages?.nodes[0].appPageData;
  

  let appPageData = {
    menu: menuData,
    globalContactsData,
    ...receivedData,
  };

  return appPageData;
}

export async function getHeritagePageData(lang, menuName) {
  const data = await fetchAPI(
    `query pageData($lang: LanguageCodeFilterEnum, $menuName: String) {
        ${menuFragment}
        ${globalContactsInfoFragment}
        ${heritagePageFragment}
      }
    `,
    {
      variables: {
        lang,
        menuName,
      },
    }
  );

  const menuData = getMenuData(data);
  const globalContactsData = data?.posts?.nodes[0]?.globalContactsInfo;
  let receivedData = data?.pages?.nodes[0].heritagePageData;
  let projectsData = receivedData.projects?.map((data) => data?.projectData?.projectsData || null) ;

  let heritagePageData = {
    menu: menuData,
    globalContactsData,
    intro: {
      ...receivedData.intro,
    },
    notes: {
      ...receivedData.notes,
    },
    projects: [
      ...projectsData
    ],
    profilesSlider: [...receivedData.profilesSlider],
    pageTheme: receivedData.pageTheme,
  };

  return heritagePageData;
}

export async function getRealEstateSalesPageData(lang, menuName) {
  const data = await fetchAPI(
    `query pageData($lang: LanguageCodeFilterEnum, $menuName: String) {
        ${menuFragment}
        ${globalContactsInfoFragment}
        ${realEstateSalesPageFragment}
      }
    `,
    {
      variables: {
        lang,
        menuName,
      },
    }
  );

  const menuData = getMenuData(data);
  const globalContactsData = data?.posts?.nodes[0]?.globalContactsInfo;
  let receivedData = data?.pages?.nodes[0].realEstatesSalesPageData;
  

  let realEstatePageData = {
    menu: menuData,
    globalContactsData,
    ...receivedData,
    contacts: {...receivedData?.departmentContacts?.departmentContactsData}
  };

  return realEstatePageData;
}


export async function getRealEstatePageData(lang, menuName) {
  const data = await fetchAPI(
    `query pageData($lang: LanguageCodeFilterEnum, $menuName: String) {
        ${menuFragment}
        ${globalContactsInfoFragment}
        ${realEstatePageFragment}
      }
    `,
    {
      variables: {
        lang,
        menuName,
      },
    }
  );

  const menuData = getMenuData(data);
  const globalContactsData = data?.posts?.nodes[0]?.globalContactsInfo;
  let receivedData = data?.pages?.nodes[0].projectsPageData;
  

  let realEstatePageData = {
    menu: menuData,
    globalContactsData,
    ...receivedData,
    contacts: {...receivedData?.departmentContacts?.departmentContactsData}
  };

  return realEstatePageData;
}

export async function getRentalPageData(lang, menuName) {
  const data = await fetchAPI(
    `query pageData($lang: LanguageCodeFilterEnum, $menuName: String) {
        ${menuFragment}
        ${globalContactsInfoFragment}
        ${rentalPageFragment}
      }
    `,
    {
      variables: {
        lang,
        menuName,
      },
    }
  );

  const menuData = getMenuData(data);
  const globalContactsData = data?.posts?.nodes[0]?.globalContactsInfo;
  let receivedData = data?.pages?.nodes[0].rentalPageData;
  

  let rentalPageData = {
    menu: menuData,
    globalContactsData,
    ...receivedData,
    contacts: {...receivedData?.departmentContacts?.departmentContactsData}
  };

  return rentalPageData;
}

export async function getPropertyManagementPageData(lang, menuName) {
  const data = await fetchAPI(
    `query pageData($lang: LanguageCodeFilterEnum, $menuName: String) {
        ${menuFragment}
        ${globalContactsInfoFragment}
        ${propertyManagementPageFragment}
      }
    `,
    {
      variables: {
        lang,
        menuName,
      },
    }
  );

  const menuData = getMenuData(data);
  const globalContactsData = data?.posts?.nodes[0]?.globalContactsInfo;
  let receivedData = data?.pages?.nodes[0].propertyManagementPageData;
  const departmentContactsData =
    receivedData?.contacts[0]?.departmentContactsData;

  let propManagementPageData = {
    menu: menuData,
    globalContactsData,
    ...receivedData,
    contacts: departmentContactsData,
  };

  return propManagementPageData;
}

export async function getUtilityManagementPageData(lang, menuName) {
  const data = await fetchAPI(
    `query pageData($lang: LanguageCodeFilterEnum, $menuName: String) {
        ${menuFragment}
        ${globalContactsInfoFragment}
        ${utilityManagementPageFragment}
      }
    `,
    {
      variables: {
        lang,
        menuName,
      },
    }
  );

  const menuData = getMenuData(data);
  const globalContactsData = data?.posts?.nodes[0]?.globalContactsInfo;
  let receivedData = data?.pages?.nodes[0].utilityManagementPageData;
  const departmentContactsData =
    receivedData?.contacts[0]?.departmentContactsData;

  let utilityManagementPageData = {
    menu: menuData,
    globalContactsData,
    ...receivedData,
    contacts: departmentContactsData,
  };

  return utilityManagementPageData;
}

export async function getHomeFeelingsYouPageData(lang, menuName) {
  const data = await fetchAPI(
    `query pageData($lang: LanguageCodeFilterEnum, $menuName: String) {
        ${menuFragment}
        ${globalContactsInfoFragment}
        ${homeFeelingsYouPageFragment}
      }
    `,
    {
      variables: {
        lang,
        menuName,
      },
    }
  );

  const menuData = getMenuData(data);
  const globalContactsData = data?.posts?.nodes[0]?.globalContactsInfo;
  let receivedData = data?.pages?.nodes[0].homeFeelingsYouPageData;
  const departmentContactsData =
    receivedData?.contacts[0]?.departmentContactsData;

  let homeFeelingsYouPageData = {
    menu: menuData,
    globalContactsData,
    ...receivedData,
    contacts: departmentContactsData,
  };

  return homeFeelingsYouPageData;
}

export async function getContactsPageData(lang, menuName) {
  const data = await fetchAPI(
    `query pageData($lang: LanguageCodeFilterEnum, $menuName: String) {
        ${menuFragment}
        ${globalContactsInfoFragment}
        ${contactsPageDataFragment}
      }
    `,
    {
      variables: {
        lang,
        menuName,
      },
    }
  );

  // galleryPageDataFragment

  const menuData = getMenuData(data);
  const globalContactsData = data?.posts?.nodes[0]?.globalContactsInfo;
  let receivedData = data?.pages?.nodes[0].contactsPageData;

  let contactsPageData = {
    menu: menuData,
    globalContactsData,
    ...receivedData,
    departmentsContacts: receivedData.departmentsContacts.map(
      (dep) => dep.departmentContactsData
    ),
  };

  return contactsPageData;
}


export async function getGalleryPageData(lang, menuName) {
  const data = await fetchAPI(
    `query pageData($lang: LanguageCodeFilterEnum, $menuName: String) {
        ${menuFragment}
        ${globalContactsInfoFragment}
        ${galleryPageDataFragment}
      }
    `,
    {
      variables: {
        lang,
        menuName,
      },
    }
  )

  const menuData = getMenuData(data)
  const globalContactsData = data?.posts?.nodes[0]?.globalContactsInfo;
  let receivedData = data?.pages?.nodes[0].galleryPageData

  let propManagementPageData = {
    menu: menuData,
    globalContactsData,
    ...receivedData,
  }

  return propManagementPageData
}

export async function getGlobalContactsData(lang, menuName) {
  const data = await fetchAPI(
    `query pageData($lang: LanguageCodeFilterEnum, $menuName: String) {
        ${globalContactsInfoFragment}
      }
    `,
    {
      variables: {
        lang,
        menuName,
      },
    }
  );

  const globalContactsData = data?.posts?.nodes[0]?.globalContactsInfo;
  return globalContactsData;
}
